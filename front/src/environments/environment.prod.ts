export const environment = {
  api: 'https://app.kitagil.com/api/1.0',
  oauth: 'https://app.kitagil.com',
  stripePk:'pk_live_6KCWvSfjNHesV09523rQtZfA',
  smartlook: 'ca03bc7f3b56280a172723857cac7dbee1224d00',
  title: ` | Kitagil.com`,
  production: true,
  daysTokenExpire: 8,
  copilotDelay: 2000,
  version: '1.5.0'
}
