import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from "../services/auth.service";

@Injectable({
  providedIn: 'root'
})
export class RegisterGuardGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router) {
  }

  canActivate() {
    return this.auth.checkTenant().then(res => {
      if (res) {
        this.router.navigate(['/', 'oauth', 'login'])
      }
      return true;
    }).catch(() => {
      return false;
    });
  }

}

export interface HasUnsavedData {
  hasUnsavedData(): boolean;
}
