import { TestBed } from '@angular/core/testing';

import { ReferredGuard } from './referred.guard';

describe('ReferredGuard', () => {
  let guard: ReferredGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ReferredGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
