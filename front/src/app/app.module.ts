import {WindowRef} from './window.ref';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {LoadingBarHttpClientModule} from '@ngx-loading-bar/http-client';
import {LoadingBarModule} from '@ngx-loading-bar/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BsDropdownModule} from "ngx-bootstrap/dropdown";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {BsDatepickerConfig, BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {LottieModule} from 'ngx-lottie';
import player from 'lottie-web';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from "@angular/common/http";
import {Page404Component} from './components/page404/page404.component';
import {TagInputModule} from "ngx-chips";
import {CookieService} from "ngx-cookie-service";
import {ListGeneralComponent} from './components/list-general/list-general.component';
import {SharedModule} from "./modules/shared/shared.module";
import {NgSelectModule} from "@ng-select/ng-select";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {LoadingSvgComponent} from './components/loading-svg/loading-svg.component';
import {ErrorLayerComponent} from './components/error-layer/error-layer.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {ModalImageComponent} from './components/modal-image/modal-image.component';
import {DEFAULT_TIMEOUT, TimeoutInterceptor} from "./TimeOutInterceptor";
import {ButtonProgressComponent} from './components/button-progress/button-progress.component';
import {ModalProductComponent} from './components/modal-product/modal-product.component';
import {ProductModule} from "./modules/product/product.module";
import {AvatarModule} from "ngx-avatar";
import {TooltipModule} from "ngx-bootstrap/tooltip";
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {CopilotComponent} from './components/copilot/copilot.component';
import {DeviceDetectorModule} from "ngx-device-detector";
import {ModalUpdateComponent} from './components/modal-update/modal-update.component';
import {ModalProfileComponent} from './components/modal-profile/modal-profile.component';
import {ModalWizardComponent} from './components/modal-wizard/modal-wizard.component';
import {FilePickerModule} from "ngx-awesome-uploader";
import {ModalViewAddComponent} from './components/modal-view-add/modal-view-add.component';
import {OauthModule} from "./modules/oauth/oauth.module";
import {NgxCopilotModule} from "../../projects/ngx-copilot/src/lib/ngx-copilot.module";
import {ModalFiltersComponent} from './components/modal-filters/modal-filters.component';
import {CarouselModule} from "ngx-owl-carousel-o";
import {KitAgilStripeModule} from "../../projects/kit-agil-stripe/src/lib/kit-agil-stripe.module";
import {ScriptKitAgilService} from "../../projects/kit-agil-stripe/src/lib/script-kit-agil.service";
import {NgxDropzoneModule} from "ngx-dropzone";
import {NgxLocalStorageModule} from "ngx-localstorage";
import {KitAgilPaypalModule} from "../../projects/kit-agil-paypal/src/lib/kit-agil-paypal.module";
import {ModalAddTaxComponent} from './components/modal-add-tax/modal-add-tax.component';
import {CalendaryComponent} from './components/calendary/calendary.component';
import {CalendarModule} from "angular-calendar";
import {ModalBarCodesComponent} from './components/modal-bar-codes/modal-bar-codes.component';
import {AuthInterceptorService} from "./services/auth-interceptor.service";
import {KitMercadoPagoModule} from "../../projects/kit-mercado-pago/src/lib/kit-mercado-pago.module";
import {ToastNotificationsModule} from "ngx-toast-notifications";
import {ModalAddTaxOffsetComponent} from './components/modal-add-tax-offset/modal-add-tax-offset.component';
import {PlanExpiredComponent} from './components/plan-expired/plan-expired.component';
import {StripeHtmlPipe} from './pipe/stripe-html.pipe';
import {ListPlanComponent} from './components/list-plan/list-plan.component';
import {MethodPaymentComponent} from './components/method-payment/method-payment.component';
import {ModalCardPayComponent} from './components/modal-card-pay/modal-card-pay.component';
import {ClearCacheService} from "./services/clear-cache.service";
import { NotImagesDirective } from './directives/not-images.directive';
import { VideoHelpComponent } from './components/video-help/video-help.component';
import { OnlySessionDirective } from './directives/only-session.directive';


export function getDatepickerConfig(): BsDatepickerConfig {
  return Object.assign(new BsDatepickerConfig(), {
    dateInputFormat: 'YYYY-MMM-DD',
    isAnimated: true,
  });
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


export function playerFactory() {
  return player;
}

@NgModule({
  declarations: [
    AppComponent,
    Page404Component,
    ListGeneralComponent,
    LoadingSvgComponent,
    ErrorLayerComponent,
    ModalImageComponent,
    ButtonProgressComponent,
    ModalProductComponent,
    CopilotComponent,
    ModalUpdateComponent,
    ModalProfileComponent,
    ModalWizardComponent,
    ModalViewAddComponent,
    ModalFiltersComponent,
    ModalAddTaxComponent,
    ModalBarCodesComponent,
    ModalAddTaxOffsetComponent,
    VideoHelpComponent,
    OnlySessionDirective,

  ],
  imports: [
    ToastNotificationsModule.forRoot({
      duration: 6000, type: 'light',
      position: 'top-right',
      autoClose: true
    }),
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    LoadingBarModule,
    KitAgilStripeModule.forRoot({
      pk: '',
      sk: '',
      api: environment.api
    }),
    KitAgilPaypalModule,
    KitMercadoPagoModule,
    NgxLocalStorageModule.forRoot(),
    DeviceDetectorModule.forRoot(),
    LoadingBarHttpClientModule,
    TagInputModule,
    TranslateModule.forRoot({
      defaultLanguage: 'es-ES',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      extend: true
    }),
    LottieModule.forRoot({player: playerFactory}),
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    SharedModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    ProductModule,
    AvatarModule,
    TooltipModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    FilePickerModule,
    OauthModule,
    NgxCopilotModule,
    CarouselModule,
    NgxDropzoneModule,
    CalendarModule
  ],
  providers: [
    ScriptKitAgilService,
    {provide: BsDatepickerConfig, useFactory: getDatepickerConfig},
    CookieService,
    [{
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }],
    WindowRef,
    [{provide: HTTP_INTERCEPTORS, useClass: TimeoutInterceptor, multi: true}],
    [{provide: DEFAULT_TIMEOUT, useValue: 30000}],
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ClearCacheService,
      multi: true
    },
  ],
  exports: [
    CalendaryComponent,
    TranslateModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
