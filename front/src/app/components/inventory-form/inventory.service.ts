import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {
  @Output() serials = new EventEmitter<any>();
  constructor() { }
}
