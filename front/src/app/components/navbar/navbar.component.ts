import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild } from '@angular/core';
import {
  faAngleRight, faBox, faCartPlus, faCashRegister, faChartPie, faClipboardList, faCrown,
  faIndustry, faTruck, faCog, faUsers, faArrowLeft, faPlug, faTruckPickup,
  faSignOutAlt, faWarehouse, faChartLine, faCalendarAlt
} from '@fortawesome/free-solid-svg-icons';
import { ShareService } from 'src/app/services/share.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, AfterViewInit {

  public menu: any = []
  constructor(
    private share: ShareService
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.menu = [
      {
        name: 'SIDE_BAR.HOME',
        icon: faChartPie,
        copilot: 1,
        copilotName: 'STEP_HOME',
        route: ['/', 'home'],
      },
      {
        name: 'SIDE_BAR.PURCHASES',
        icon: faCartPlus,
        route: ['/', 'purchase'],
        copilot: 2,
        copilotName: 'STEP_PURCHASES',
        disable: !this.share.checkRole(['admin', 'manager', 'seller'])
      },
      {
        name: 'SIDE_BAR.CALENDAR',
        icon: faCalendarAlt,
        copilot: 3,
        copilotName: 'STEP_PURCHASES',
        disable: !this.share.checkRole(['admin', 'manager']),
        route: ['/', 'calendar'],
      },
      {
        name: 'SIDE_BAR.PRODUCTS',
        icon: faBox,
        copilot: 4,
        copilotName: 'STEP_ITEMS',
        route: ['/', 'products']
      },
      {
        name: 'SIDE_BAR.ORDERS',
        icon: faCashRegister,
        copilot: 5,
        copilotName: 'STEP_ORDERS',
        disable: !this.share.checkRole(['admin', 'manager', 'seller']),
        route: ['/', 'orders'],
      },
      {
        name: 'SIDE_BAR.USERS',
        icon: faUsers,
        route: ['/', 'users'],
        copilot: 6,
        copilotName: 'STEP_USERS',
        disable: !this.share.checkRole(['admin', 'manager']),
      },
      {
        name: 'SIDE_BAR.STOCK',
        icon: faClipboardList,
        route: ['/', 'inventory'],
        copilot: 7,
        copilotName: 'STEP_INVENTORY',
        disable: !this.share.checkRole(['admin', 'manager']),
      },
      {
        name: 'SIDE_BAR.DEPOSITS',
        // disable: true,
        icon: faWarehouse,
        route: ['/', 'deposits'],
        copilot: 8,
        copilotName: 'STEP_DEPOSITS',
        disable: !this.share.checkRole(['admin', 'manager']),
      },
      {
        name: 'SIDE_BAR.PROVIDERS',
        icon: faIndustry,
        route: ['/', 'providers'],
        copilot: 9,
        copilotName: 'STEP_PROVIDERS',
        disable: !this.share.checkRole(['admin', 'manager']),
      },
      {
        name: 'SIDE_BAR.ANALYTICS',
        icon: faChartLine,
        disable: !this.share.checkRole(['admin']),
        route: ['/', 'analytics']
      },
      {
        name: 'SIDE_BAR.SETTINGS',
        icon: faCog,
        route: ['/', 'settings'],
        copilot: 11,
        copilotName: 'STEP_DUMMY',
        disable: !this.share.checkRole(['admin'])
      },
    ]
  }
}
