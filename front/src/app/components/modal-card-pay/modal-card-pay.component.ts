import {AfterContentChecked, Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {TranslateService} from "@ngx-translate/core";
import {RestService} from "../../services/rest.service";
import {ShareService} from "../../services/share.service";
import {ScriptsService} from "../../services/script.service";
import {environment} from "../../../environments/environment";
import {finalize} from "rxjs/operators";
import {BsModalRef} from "ngx-bootstrap/modal";
import {SettingsService} from "../../modules/settings/settings.service";

@Component({
  selector: 'app-modal-card-pay',
  templateUrl: './modal-card-pay.component.html',
  styleUrls: ['./modal-card-pay.component.css']
})
export class ModalCardPayComponent implements OnInit, AfterContentChecked, OnDestroy {
  private elementStripe: any = [];
  private STRIPE: any;
  private cardNumber: any = null;
  private cardExp: any = null;
  private cardCvv: any = null;
  public form: FormGroup;
  public error: any = null;
  loading: any;
  section: any;

  constructor(private scriptService: ScriptsService,
              public translate: TranslateService,
              private rest: RestService,
              public share: ShareService,
              public bsModalRef: BsModalRef,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.loadScripts();
    this.form = this.formBuilder.group({
      placeholder: ['', [Validators.required]],
      token: ['', [Validators.required]],
      plan: ['']
    });

    const {title, price} = this.section
    this.form.patchValue({
      plan: {
        title,
        price
      }
    })
  }

  ngAfterContentChecked(): void {

  }

  private loadScripts = () => {
    this.scriptService
      .load('stripe')
      .then((data: any) => {
        if (data) {
          data.map((a) => {
            if (a.script === 'stripe') {
              // @ts-ignore
              this.STRIPE = window.Stripe(environment.stripePk);
              this.createStripeElement();
            }
          });
        }
      })
      .catch((error) => console.log(error));
  };

  private createStripeElement = () => {
    this.elementStripe = this.STRIPE.elements({
      fonts: [
        {
          cssSrc:
            'https://fonts.googleapis.com/css2?family=Hind+Siliguri:wght@300;400;500;600&display=swap',
        },
      ],
    });
    let cardNumberLabel = '';
    let cardEXP = '';
    let cardCVC = '';
    this.translate.get('CARD.CARD_NUMBER').subscribe((res: string) => {
      cardNumberLabel = res;
    });
    this.translate.get('CARD.CARD_EXP').subscribe((res: string) => {
      cardEXP = res;
    });
    this.translate.get('CARD.CARD_CVC').subscribe((res: string) => {
      cardCVC = res;
    });
    const stripeStyle = {
      style: {
        base: {
          color: '#000000',
          fontWeight: 400,
          fontFamily: '\'Hind Siliguri\', sans-serif',
          fontSize: '1.1em',
          '::placeholder': {
            color: '#eaeaea',
          },
        },
        invalid: {
          color: '#EC2D2D',
        },
      },
      placeholder: '4242 XXXX XXXX XXXX',
    };
    const cardNumber = this.elementStripe.create('cardNumber', stripeStyle);
    const cardExp = this.elementStripe.create('cardExpiry', {
      ...stripeStyle,
      ...{placeholder: 'MM/YY'},
    });
    const cardCvc = this.elementStripe.create('cardCvc', {
      ...stripeStyle,
      ...{placeholder: '000'},
    });
    cardNumber.mount('#card_number');
    cardExp.mount('#card_exp');
    cardCvc.mount('#card_cvc');
    this.cardNumber = cardNumber;
    this.cardExp = cardExp;
    this.cardCvv = cardCvc;
  };


  beginProcess = async () => {
    this.loading = true
    this.STRIPE.createToken(this.cardNumber)
      .then(async (res) => {
        if (res.error) {
          this.loading = false
          throw await res;
        } else {
          this.form.patchValue({
            token: res.token.id
          })
          this.saveCard();
        }
      })
      .catch((err) => {
        this.loading = false
        // this.bsModalRef.hide()
      });
  };

  saveCard = () => {
    this.rest.patch(`settings/payment`, this.form.value)
      .pipe(finalize(() => this.loading = false))
      .subscribe(res => {
        // this.share.refresh.emit('payment');
        this.share.payPlan.emit(res)
        this.bsModalRef.hide()
      }, error => {
        this.error = error.e.error.errors
      });
  }


  ngOnDestroy(): void {

  }
}
