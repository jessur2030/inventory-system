import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ModalUserComponent } from "../modal-user/modal-user.component";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { ActivatedRoute, Router } from "@angular/router";
import { ShareService } from "../../services/share.service";
import { RestService } from "../../services/rest.service";
import { CurrencyMaskInputMode } from 'ngx-currency';
import { ScriptKitAgilService } from "../../../../projects/kit-agil-stripe/src/lib/script-kit-agil.service";
import { PurchaseService } from "../../modules/purchases/purchase.service";
import { KitAgilPaypalService } from "../../../../projects/kit-agil-paypal/src/lib/kit-agil-paypal.service";
import { concat, Observable, of, Subject } from "rxjs";
import { catchError, distinctUntilChanged, finalize, map, switchMap, tap } from "rxjs/operators";
import { faSave } from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.css']
})
export class OrderFormComponent implements OnInit {
  @Input() formInit: any = {};
  @Input() redirect = true
  @Output() callbackSubmit = new EventEmitter<any>();
  public form: FormGroup;
  public data: any = []
  public dataIn: any = []
  public purchase: any = []
  public users: any
  loading = false;
  userLoading = false;
  purchaseLoading = false;
  userInput$ = new Subject<string>();
  results$: Observable<any>;
  purchase$: Observable<any>;
  purchaseInput$ = new Subject<string>();
  public id: any = null;
  faSave = faSave;
  public currency: any = null;
  public currencySymbol: any = null;
  itemsAsObjects = [];
  public priceTmp = 0;
  public ngxCurrencyOptions = {
    prefix: ``,
    thousands: ',',
    decimal: '.',
    allowNegative: false,
    nullable: true,
    max: 250_000_000,
    inputMode: CurrencyMaskInputMode.FINANCIAL,
  };
  bsModalRef: BsModalRef;
  @ViewChild('selectUserInput') selectUserInput;
  @ViewChild('selectPurchaseInput') selectPurchaseInput;
  public status = [
    {
      name: 'Exitoso',
      value: 'success'
    },
    {
      name: 'En espera',
      value: 'wait'
    },
    {
      name: 'Cancelado',
      value: 'cancel'
    },
    {
      name: 'Falló',
      value: 'fail'
    }
  ];

  public methods = [
    {
      name: 'Efectivo',
      value: 'cash'
    },
    {
      name: 'Transferencia',
      value: 'transfer'
    },
    {
      name: 'Pago en linea',
      value: 'online'
    },
    {
      name: 'Otro',
      value: 'other'
    }
  ];

  constructor(private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    public shared: ShareService,
    public router: Router,
    private purchaseService: PurchaseService,
    public kitAgilStripeService: ScriptKitAgilService,
    public kitAgilPaypalService: KitAgilPaypalService,
    private rest: RestService) {
  }

  ngOnInit(): void {
    const { currency, currencySymbol } = this.shared.getSettings();
    this.currency = currency;
    this.currencySymbol = currencySymbol;
    this.form = this.formBuilder.group({
      customer: ['', Validators.required],
      reference: [''],
      status: ['wait', Validators.required],
      platform: ['', Validators.required],
      amount: ['', [Validators.required, Validators.min(0)]],
      purchase: [null, Validators.required],
      tag: [[]],
      description: [''],
    });

    this.ngxCurrencyOptions = {
      ...this.ngxCurrencyOptions, ...{
        prefix: `${currency} `
      }
    }

    this.route.params.subscribe(params => {
      this.id = (params.id === 'add') ? '' : params.id;
    });

    if (this.id && (this.id.length)) {
      this.loadData()
      // const dataIn = await this.purchaseService.getPurchase(purchase) as any;
    }

    this.shared.registerUser.subscribe(res => {
      this.form.patchValue({ customer: res })
    })

    this.loadPlugins()
    this.loadUsers()
    this.loadPurchase()
    if (this.form && this.formInit) {
      this.form.patchValue(this.formInit)
    }
  }

  onSubmit(): void {
    this.loading = true;
    const method = (this.id) ? 'patch' : 'post';
    this.rest[method](`orders${(method === 'patch') ? `/${this.id}` : ''}`, this.form.value)
      .pipe(finalize(() => {
        this.loading = false;
        this.callbackSubmit.emit(true)
      }))
      .subscribe(res => {
        this.data = res;
        if (this.redirect) {
          this.router.navigate(['/', 'orders', res._id])
        }
      })
  }

  private loadData = () => {
    this.loading = true
    this.rest.get(`orders/${this.id}`)
      .pipe(finalize(() => this.loading = false))
      .subscribe(dataIn => {
        this.dataIn = dataIn;
        this.form.patchValue({
          customer: dataIn.customer,
          purchase: dataIn?.purchase,
          platform: dataIn.platform,
          status: (dataIn.status === 'hold') ? 'wait' : dataIn.status,
          amount: dataIn.purchase?.total,
          description: dataIn.description,
          tag: dataIn.tag
        })
      })
  }

  private loadUsers() {
    this.results$ = concat(
      of([]), // default items
      this.userInput$.pipe(
        distinctUntilChanged(),
        tap(() => this.userLoading = true),
        switchMap(term => this.singleSearchUser$(term).pipe(
          catchError(() => of([])), // empty list on error
          tap(() => this.userLoading = false)
        ))
      )
    );
  }

  private loadPurchase() {
    this.purchase$ = concat(
      of([]), // default items
      this.purchaseInput$.pipe(
        distinctUntilChanged(),
        tap(() => this.purchaseLoading = true),
        switchMap(term => this.singleSearchPurchase$(term).pipe(
          catchError(() => of([])), // empty list on error
          tap(() => this.purchaseLoading = false)
        ))
      )
    );
  }


  loadPlugins = () => {
    this.shared.checkPlugins('stripe').then(() => {
      this.methods = [...this.methods, {
        name: '💳 Stripe (Tarjeta de Débito o Crédito)',
        value: 'stripe'
      }]
    })

    this.shared.checkPlugins('paypal').then(() => {
      this.methods = [...this.methods, {
        name: 'Paypal',
        value: 'paypal'
      }]
    })

    this.shared.checkPlugins('mercadopago').then(() => {
      this.methods = [...this.methods, {
        name: 'MercadoPago',
        value: 'mercadopago'
      }]
    })

  }

  trackByFn(item: any) {
    return item._id;
  }


  singleSearchUser$ = (term) => {
    const q = [
      `users?`,
      `filter=${term}`,
      `&fields=name,email,nameBusiness`,
      `&page=1&limit=5`,
      `&sort=name&order=-1`,
    ];
    return this.rest.get(q.join(''), true,
      { ignoreLoadingBar: '' }).pipe(
        map((a) => a.docs)
      )
  }

  singleSearchPurchase$ = (term) => {
    const q = [
      `purchase?`,
      `filter=${term}`,
      `&fields=name,email,customer.name,customer.email,controlNumber`,
      `&page=1&limit=5`,
      `&sort=name&order=-1`,
    ];
    return this.rest.get(q.join(''), true,
      { ignoreLoadingBar: '' }).pipe(
        map((a) => a.docs)
      )
  }

  selectUser = (e) => {
    if (e) {
      if (!e._id) {
        const initialData = { ...e, ...{ manager: null, role: 'customer' } }
        this.open(initialData)
      } else {
        this.form.patchValue({ deliveryAddress: e.address })
      }
    }
  }


  selectPurchase = (e) => {
    try {
      if (e.value === 'new') {
        this.form.patchValue({ purchase: null })
      } else {
        this.form.patchValue({ amount: e.total })
        this.form.patchValue({ customer: e.customer })
      }
    } catch (e) {
      this.form.patchValue({ purchase: null })
      this.form.patchValue({ amount: null })
      this.form.patchValue({ customer: null })
    }
  }

  selectPlatform($event: any) {
    if ($event.value === 'stripe') {
      this.form.patchValue({ status: 'wait' })
    }
  }


  parseData = (data: any, source: string) => {
    const tmp = [];
    data.docs.map(a => tmp.push({
      ...a, ...{
        name: (source === 'purchase') ? `${a.customer.name} (${a.total})` : a.name,
        router: ['/', 'orders', a._id]
      }
    }));
    return tmp;
  }

  cbList = () => {
    this.router.navigate(['/', 'orders'])
  }

  cbTrash = () => {
    this.rest.delete(`orders/${this.id}`)
      .subscribe(res => {
        this.router.navigate(['/', 'purchase', this.id])
      })
  }

  open(data: any = null) {
    const initialState: any = {
      section: data
    };

    this.bsModalRef = this.modalService.show(
      ModalUserComponent,
      Object.assign({ initialState }, {
        class: 'modal-light-plan',
        ignoreBackdropClick: false
      })
    );
  }

  src = (e, source = '') => {
    const { term } = e;
    const q = [
      `${source}?`,
      `filter=${term}`,
      `&fields=name,email,customer.name,customer.email,controlNumber`,
      `&page=1&limit=5`,
      `&sort=name&order=-1`,
    ];

    this.rest.get(q.join(''), true, { ignoreLoadingBar: '' })
      .subscribe(res => {
        this[source] = [...this.parseData(res, source)];
        console.log(this[source], source)
      })
  }

  onFocus = (e) => {
    this.priceTmp = null
  }

}
