import {AfterViewInit, ChangeDetectorRef, Component, NgZone, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TabsetComponent} from "ngx-bootstrap/tabs";
import * as _ from 'lodash';
import {
  faFileAlt,
  faHandPointer,
  faList,
  faMinus,
  faMoneyBill,
  faPaperclip,
  faPlus,
  faSave,
  faTrashAlt,
  faTruck,
  faUser
} from '@fortawesome/free-solid-svg-icons';
import {faQuestionCircle} from '@fortawesome/free-regular-svg-icons';
import {ActivatedRoute, Router} from "@angular/router";
import {RestService} from "../../services/rest.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {ModalProductComponent} from "../modal-product/modal-product.component";
import {ShareService} from "../../services/share.service";
import {animate, style, transition, trigger} from "@angular/animations";
import {AnimationOptions} from "ngx-lottie";
import {AnimationItem} from "lottie-web";
import {MediaService} from "../drop-gallery/media.service";
import {ModalEditPriceComponent} from "../../modules/purchases/components/modal-edit-price/modal-edit-price.component";
import {PurchaseService} from "../../modules/purchases/purchase.service";
import {WindowRef} from "../../window.ref";
import {finalize} from "rxjs/operators";
import {ModalPayComponent} from "../modal-pay/modal-pay.component";
import {PlatformLocation} from "@angular/common";
import {ModalSellerComponent} from "../../modules/purchases/components/modal-seller/modal-seller.component";

@Component({
  selector: 'app-detail-invoice',
  templateUrl: './detail-invoice.component.html',
  styleUrls: ['./detail-invoice.component.css'],
  animations: [
    trigger('swipe', [
      transition(':enter', [
        style({opacity: '.6'}),
        animate('0.15s ease-in')
      ])
    ])
  ]

})
export class DetailInvoiceComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('tabset') tabset: TabsetComponent;
  faUser = faUser;
  faTruck = faTruck;
  faFileAlt = faFileAlt;
  faPlus = faPlus;
  faTrashAlt = faTrashAlt;
  faSave = faSave;
  faList = faList;
  faPaperclip = faPaperclip;
  faQuestionCircle = faQuestionCircle;
  faHandPointer = faHandPointer;
  faMoneyBill = faMoneyBill;
  faMinus = faMinus;
  public total = 0;
  public subTotal = 0;
  public sumTaxesTotal = 0;
  public id: any = null;
  public data: any = [];
  public files: any = [];
  public items: any = [];
  public taxes: any;
  bsModalRef: BsModalRef;
  private animationItem: AnimationItem;
  public currency: any = null;
  public currencySymbol: any = null;
  public description: string;
  public listSubscribers: any = [];
  loading: any;

  constructor(private route: ActivatedRoute, private windowRef: WindowRef,
              private rest: RestService, public share: ShareService, private ngZone: NgZone,
              private router: Router, private media: MediaService, private cd: ChangeDetectorRef,
              private location: PlatformLocation,
              private modalService: BsModalService, public purchaseService: PurchaseService) {
  }

  ngOnInit(): void {
    this.share.unSave = null;
    const {currency, currencySymbol} = this.share.getSettings();
    this.currencySymbol = currencySymbol;
    this.currency = currency;
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.loadData()
    });

    this.route.queryParams.subscribe(async (queryParams) => {
      const {tab = null} = queryParams;
      if (tab) {
        setTimeout(() => {
          this.tabset.tabs[tab].active = true;
        }, 150)
      }
    })

    const observer1$ = this.share.common.subscribe(data => {
      const checkPrev = () => {
        return _.findIndex(this.items, (item: any) => {
          const diff = this.purchaseService.checkDiffPrice(item?.prices, data?.prices)
          if ((item?._id === data?._id) && diff) {
            return item
          }
        })
      }

      const indexPrev = checkPrev();

      if (indexPrev > -1) {
        const singleItem = this.items[indexPrev];
        this.items[indexPrev] = {
          ...singleItem, ...{
            qty: parseFloat(singleItem.qty) + parseFloat(data.qty)
          }
        }
      } else {
        this.items.push(data);
      }
      this.share.unSave = true;
      this.share.disablePlugin = (!this.items.length) ? 'stripe' : null;
      this.parseData()
    })

    const observer2$ = this.share.addPurchase.subscribe(data => {
      if (data === 'add_product') {
        this.openModalProduct()
      }
      if (data === 'add_pay') {
        this.openModalPay()
      }
      if (data === 'clonePurchase') {
        this.clonePurchase()
      }
    })

    const observer3$ = this.share.savePurchase.subscribe(data => {
      if (data.toString().includes('order_')) {
        this.submitDataConvert(data).then(r => {
        })
      } else {
        this.submitData().then()
      }
    })

    const observer4$ = this.share.deletePurchase.subscribe(data => {
      this.deletePurchase()
    })

    const observer5$ = this.purchaseService.changePrice.subscribe(data => {
      this.items = this.items.map(item => {
        if (item._id === data._id) {
          item = data;
        }
        return item
      })
      this.parseData()
    })

    const observer6$ = this.purchaseService.refreshData.subscribe(data => {
      this.loadData()
    })

    const observer7$ = this.purchaseService.changeSeller.subscribe(data => {
      this.data.author = data;
      this.share.unSave = true;
    })

    this.listSubscribers = [observer1$, observer2$, observer3$, observer4$, observer5$, observer6$, observer7$]
  }

  ngOnDestroy() {
    this.listSubscribers.forEach(a => a.unsubscribe())
  }

  ngAfterViewInit() {
    this.cd.detectChanges();
  }

  clonePurchase = () => {
    this.loading = true;
    this.rest.post(`purchase/${this.id}/duplicate`)
      .pipe(finalize(() => this.loading = false))
      .subscribe(res => {
        this.router.navigate(['/', 'purchase', res?._id])
      })
  }

  getTaxList = () => {
    this.taxes =
      _(this.items).map(a => {
        return {
          tax: {
            ...a.tax,
            ...{
              prices: parseFloat(a.prices.find(() => true).amount) * parseFloat(a.qty)
            }
          },
        }
      }).flatMap('tax').groupBy('amount').map(x => {
        return {
          tax_name: _.head(x).name,
          tax_value: _.head(x).amount,
          tax_total: _.sumBy(x, 'prices'),
          tax_percentage: _.sumBy(x, 'prices') * parseFloat(_.head(x).amount) / 100
        }
      })
        .map(i => {
          if (i.tax_percentage) {
            return i
          }
        })
        .compact().value()

    this.sumTaxesTotal = _.sumBy(this.taxes, 'tax_percentage');

  }

  loadData = () => {
    this.loading = true;
    this.rest.get(`purchase/${this.id}`)
      .pipe(finalize(() => this.loading = false))
      .subscribe(res => {
        this.share.unSave = false;
        const {items, description, attachment} = res
        this.data = res;
        this.description = description
        this.items = items;
        this.media.files = [...attachment]
        this.share.purchaseData.emit(res)
        this.parseData();
      })
  }

  options: AnimationOptions = {
    path: '/assets/images/click.json',
  };

  deletePurchase = () => {
    if (this.data.convert.includes('invoice')) {
      this.share.alert('Error', 'Can\' delete a invoice ')
    } else {
      this.rest.delete(`purchase/${this.id}`)
        .subscribe(res => this.router.navigate(['/', 'purchase']))
    }
  }

  parseNumber = (n: any = 0) => {
    return parseFloat(n);
  }

  onRemove(event) {
    this.cbSwipe({action: 'trash', value: event._id})
  }

  parseSubTotal = () => {
    try {
      let subTotal = 0;
      this.items.forEach(i => {
        const prices = i.prices.find(a => a.amount)
        subTotal += parseFloat(String(i.qty * prices.amount))
      })
      this.subTotal = subTotal;
    } catch (e) {
      this.subTotal = 0;
    }
  }

  parseTotal = () => {
    this.total = this.subTotal + this.sumTaxesTotal;
  }

  parseData = (data: any = null) => {

    try {
      if (!data) {
        data = this.items;
      }
      this.parseSubTotal();
      this.items = data.map(a => {
        return {...a, ...{id: a._id}}
      })
      this.getTaxList()
      this.parseTotal()
    } catch (e) {
      this.items = []
    }

  }

  openModalProduct(data: any = null) {
    const initialState: any = {
      section: data
    };

    this.bsModalRef = this.modalService.show(
      ModalProductComponent,
      Object.assign({initialState}, {
        class: 'modal-light-plan',
        ignoreBackdropClick: false
      })
    );
  }

  openModalPay() {
    const initialState: any = {
      section: this.data
    };

    this.bsModalRef = this.modalService.show(
      ModalPayComponent,
      Object.assign({initialState}, {
        class: 'modal-light-plan',
        ignoreBackdropClick: false
      })
    );
  }

  animationCreated(animationItem: AnimationItem): void {
    this.animationItem = animationItem;
    // this.animationItem.stop();
  }

  loopComplete(e): void {
    // e.stop().then();
    this.pause()
  }

  stop(): void {
    this.ngZone.runOutsideAngular(() => this.animationItem.stop());
  }

  pause(): void {
    this.ngZone.runOutsideAngular(() => this.animationItem.setSegment(43, 44));
  }

  save() {
    this.share.confirm().then(r => {
      this.submitData();
    })
  }

  changeStatusPurchase = (status: string) => {
    this.data.status = status
    this.save()
  }

  openEditPrice(data: any = null) {
    const initialState: any = {
      section: data
    };

    this.bsModalRef = this.modalService.show(
      ModalEditPriceComponent,
      Object.assign({initialState}, {
        class: 'modal-light-plan',
        ignoreBackdropClick: false
      })
    );
  }

  openEditSeller(data: any = null) {
    const initialState: any = {
      section: data
    };

    this.bsModalRef = this.modalService.show(
      ModalSellerComponent,
      Object.assign({initialState}, {
        class: 'modal-light-plan',
        ignoreBackdropClick: false
      })
    );
  }

  submitData = async () => {
    const attachment = await this.media.loadImages(true) as any;
    const body = {
      ...this.data,
      ...{
        author: this.data.author?._id,
        items: this.items,
        taxes: this.taxes,
        sumTaxesTotal: this.sumTaxesTotal,
        subTotal: this.subTotal,
        total: this.total,
        description: this.description,
        attachment
      }
    }
    this.rest.patch(`purchase/${this.id}`,
      body)
      .subscribe(res => {
        this.files = []
        this.loadData()
      })
  }

  submitDataConvert = async (convert = false) => {

    const body = {
      convert,
      items: this.items,
      taxes: this.taxes,
      sumTaxesTotal: this.sumTaxesTotal,
      subTotal: this.subTotal,
      total: this.total,
    }
    this.rest.patch(`purchase/convert/${this.id}`,
      body)
      .subscribe(res => {
        if (res.download) {
          this.windowRef.nativeWindow.open(res.download)
        }
        this.files = []
        this.loadData()
      })
  }

  cbSwipe($event: any) {
    this.share.unSave = true;
    const {action, value} = $event;
    if (action === 'trash') {
      this.items = this.items.filter(a => {
        return (!Object.values(a).includes(value));
      })
      this.parseData();
    }
  }

  addStock = ($event: any) => {

    _.forEach(this.items, (item) => {
      if ((`${item._id}` === `${$event._id}`) &&
        this.purchaseService.checkDiffPrice(item?.prices, $event?.prices)) {
        const qty = parseFloat($event.qty) + 1
        item.qty = qty;
        $event.qty = qty;
        return item;
      }
    })

    this.share.unSave = true;
    this.parseData()
  }

  removeStock = ($event: any) => {
    _.forEach(this.items, (item) => {
      if ((`${item._id}` === `${$event._id}`) &&
        this.purchaseService.checkDiffPrice(item?.prices, $event?.prices)) {
        const qty = parseFloat($event.qty) + -1
        item.qty = qty;
        $event.qty = qty;
        return item;
      }
    })
    this.share.unSave = true;
    this.parseData()
  }

  afterAdded() {
    this.share.unSave = true;
  }

}
