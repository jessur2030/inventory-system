import {Component, OnInit} from '@angular/core';
import {ShareService} from "../../services/share.service";
import {BsModalRef} from "ngx-bootstrap/modal";
import {animate, style, transition, trigger} from "@angular/animations";
import {FormBuilder, Validators} from "@angular/forms";
import {faAngleLeft, faExclamation, faSave, faQrcode} from '@fortawesome/free-solid-svg-icons';
import * as _ from 'lodash'

@Component({
  selector: 'app-modal-product',
  templateUrl: './modal-product.component.html',
  styleUrls: ['./modal-product.component.css'],
  animations: [
    trigger('swipe', [
      transition(':enter', [
        style({transform: 'translateY(-20%)', opacity: '0'}),
        animate('0.2s ease-in')
      ]),
      transition(':leave', [
        animate('0.2s ease-out', style({transform: 'translateY(20%)', opacity: '1'}))
      ])
    ]),
    trigger('swipeR', [
      transition(':enter', [
        style({transform: 'translate(14%, 0px)', opacity: '0'}),
        animate('0.2s ease-in')
      ]),
      transition(':leave', [
        animate('0.2s ease-out', style({transform: 'translate(14%, 0px)', opacity: '1'}))
      ])
    ])
  ]

})
export class ModalProductComponent implements OnInit {
  public select: any = false;
  form: any;
  prices: any = []
  depositList: any = []
  providers: any = []
  serials: any = []
  faAngleLeft = faAngleLeft;
  faSave = faSave;
  faQrcode = faQrcode;
  faExclamation = faExclamation;
  public currency: any = null;
  public currencySymbol: any = null;
  tax = [];

  constructor(private share: ShareService,
              private formBuilder: FormBuilder,
              public bsModalRef: BsModalRef) {
  }

  ngOnInit(): void {
    const {currency, currencySymbol} = this.share.getSettings();
    this.currency = currency;
    this.currencySymbol = currencySymbol;
    this.form = this.formBuilder.group({
      deposit: [''],
      serials: [[]],
      tax: [''],
      price: ['', Validators.required],
      qty: ['', Validators.required]
    });
  }

  checkValue($event: any): void {
    try {
      const number = isNaN(parseFloat($event.term))
      if (!number) {
        this.form.patchValue({price: 88})

      }
    } catch (e) {
      return null
    }
  }

  getClick($event: any) {
    this.select = $event;
    const {prices, providers, depositList, tax} = $event;
    if (tax && tax.length && (tax.length === 1)) {
      this.form.patchValue({tax: tax[0]})
    }
    this.prices = prices;
    this.depositList = depositList;
    this.providers = providers;
    this.tax = tax;

  }

  onSubmit() {
    const {qty, deposit, tax, serials} = this.form.value;
    this.select = {
      ...this.select,
      ...{qty, deposit, tax, serials}
    };
    this.form.disable()
    this.share.common.emit(this.select)
    this.bsModalRef.hide()
  }

  selectProduct($event: any) {
    try {
      this.serials =_.compact($event.serial)
    } catch (e) {
      return []
    }
  }

  selectPrice($event: any = {}) {
    this.select = {...this.select, ...{prices: [$event]}}
  }
}
