import { NgModule } from '@angular/core';
import {LeaveDialogComponent} from "./leave-dialog.component";

@NgModule({
  imports: [  ],
  declarations: [ LeaveDialogComponent ],
  exports: [ LeaveDialogComponent ]
})
export class DialogModule { }
