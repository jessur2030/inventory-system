import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ShareService} from "../../services/share.service";
import {RestService} from "../../services/rest.service";
import {ActivatedRoute, Router} from "@angular/router";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {ModalUserComponent} from "../modal-user/modal-user.component";
import {faSave} from '@fortawesome/free-solid-svg-icons';
import {catchError, debounceTime, distinctUntilChanged, finalize, map, switchMap, tap} from "rxjs/operators";
import {concat, Observable, of, Subject} from "rxjs";

@Component({
  selector: 'app-purchase-form',
  templateUrl: './purchase-form.component.html',
  styleUrls: ['./purchase-form.component.css']
})
export class PurchaseFormComponent implements OnInit {
  faSave = faSave
  public form: FormGroup;
  public data: any = []
  public users: any = []
  public products: any = []
  public deliveryType: any = [
    {
      name: 'Envio a dirección',
      value: 'to_send'
    },
    {
      name: 'Retirar en tienda',
      value: 'in_house'
    }
  ];
  public status: any = [
    {
      name: 'Pagado',
      value: 'paid'
    },
    {
      name: 'Pago contraentrega',
      value: 'delivery-paid'
    },
    {
      name: 'Anticipo',
      value: 'pre-paid'
    },
    {
      name: 'Credito',
      value: 'credit'
    },
    {
      name: 'En espera',
      value: 'hold'
    },
    // {
    //   name: 'Procesando',
    //   value: 'process'
    // },
    {
      name: 'Otro',
      value: 'exceptional'
    }
  ];
  public id: any = null
  itemsAsObjects = [];
  bsModalRef: BsModalRef;
  loading = false;
  today = new Date()
  userInput$ = new Subject<string>();
  results$: Observable<any>;
  userLoading = false;

  constructor(private formBuilder: FormBuilder,
              private modalService: BsModalService,
              private route: ActivatedRoute,
              private shared: ShareService,
              public router: Router,
              private rest: RestService) {
  }

  ngOnInit(): void {

    this.form = this.formBuilder.group({
      customer: ['', Validators.required],
      items: [''],
      invoiceAddress: ['', Validators.required],
      deliveryAddress: [''],
      deliveryDate: [''],
      deliveryType: ['', Validators.required],
      status: ['hold'],
      description: [''],
      total: [''],
      tag: ['']
    });

    this.route.params.subscribe(params => {
      this.id = (params.id === 'add') ? '' : params.id;
    });

    this.shared.registerUser.subscribe((res: any) => {
      this.form.patchValue({customer: res})
      this.form.patchValue({deliveryAddress: res.address})
    })

    this.loadUsers()
  }

  selectUser = (e) => {
    if (e) {
      if (!e._id) {
        const initialData = {...e, ...{manager: null, role: 'customer'}}
        this.open(initialData)
      } else {
        this.form.patchValue({deliveryAddress: e.address})
      }
    }
  }


  private loadUsers() {
    this.results$ = concat(
      of([]), // default items
      this.userInput$.pipe(
        distinctUntilChanged(),
        tap(() => this.userLoading = true),
        switchMap(term => this.singleSearch$(term).pipe(
          catchError(() => of([])), // empty list on error
          tap(() => this.userLoading = false)
        ))
      )
    );
  }

  singleSearch$ = (term) => {
    const q = [
      `users?`,
      `filter=${term}`,
      `&fields=name,email,nameBusiness`,
      `&page=1&limit=5`,
      `&sort=name&order=-1`,
    ];
    return this.rest.get(q.join(''), true,
      {ignoreLoadingBar: ''}).pipe(
      map((a) => a.docs)
    )
  }


  trackByFn(item: any) {
    return item._id;
  }

  onSubmit(): void {
    this.loading = true;
    const method = (this.id) ? 'patch' : 'post';
    this.rest[method](`purchase${(method === 'patch') ? `/${this.id}` : ''}`, this.form.value)
      .pipe(finalize(() => this.loading = false))
      .subscribe(res => {
        this.router.navigate(['/', 'purchase', res._id])
      })
  }

  cbList = () => {
    this.router.navigate(['/', 'purchase'])
  }

  cbTrash = () => {
    this.rest.delete(`purchase/${this.id}`)
      .subscribe(res => this.cbList())
  }


  open(data: any = null) {
    const initialState: any = {
      section: data
    };

    this.bsModalRef = this.modalService.show(
      ModalUserComponent,
      Object.assign({initialState}, {
        class: 'modal-light-plan',
        ignoreBackdropClick: false
      })
    );
  }


  cbSave = () => {
    console.log('aa')
  }

}
