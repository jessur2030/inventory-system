import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ShareService} from "../../services/share.service";
import {BsModalRef} from "ngx-bootstrap/modal";
import {SettingsService} from "../../modules/settings/settings.service";

@Component({
  selector: 'app-modal-add-tax',
  templateUrl: './modal-add-tax.component.html',
  styleUrls: ['./modal-add-tax.component.css']
})
export class ModalAddTaxComponent implements OnInit {
  public form: FormGroup;

  constructor(private formBuilder: FormBuilder, private shared: ShareService,
              public bsModalRef: BsModalRef, private settingsService: SettingsService) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      amount: ['', [Validators.required, Validators.min(0)]]
    });
  }

  update() {
    this.settingsService.tax.emit(this.form.value)
    this.bsModalRef.hide();
  }
}
