import {
  AfterViewInit,
  ChangeDetectorRef,
  Component, OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {
  faAngleRight, faBox, faCartPlus, faCashRegister, faChartPie, faClipboardList, faCrown,
  faIndustry, faTruck, faCog, faUsers, faArrowLeft, faPlug, faTruckPickup,
  faSignOutAlt, faWarehouse, faChartLine
} from '@fortawesome/free-solid-svg-icons';

import {faCalendarAlt} from '@fortawesome/free-regular-svg-icons';

import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";
import {ShareService} from "../../services/share.service";
import {ModalProfileComponent} from "../modal-profile/modal-profile.component";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {NgxCopilotService} from "../../../../projects/ngx-copilot/src/lib/ngx-copilot.service";
import {CookieService} from "ngx-cookie-service";
import {DeviceDetectorService} from "ngx-device-detector";
import {SideBarService} from "./side-bar.service";
import {VideoHelpService} from "../../services/video-help.service";

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('step1') public step1: TemplateRef<any>;
  @ViewChild('step2') public step2: TemplateRef<any>;
  @ViewChild('step3') public step3: TemplateRef<any>;
  @ViewChild('step4') public step4: TemplateRef<any>;
  @ViewChild('step5') public step5: TemplateRef<any>;
  @ViewChild('step6') public step6: TemplateRef<any>;
  @ViewChild('step7') public step7: TemplateRef<any>;
  @ViewChild('step8') public step8: TemplateRef<any>;
  @ViewChild('step9') public step9: TemplateRef<any>;
  @ViewChild('step10') public step10: TemplateRef<any>;
  @ViewChild('step11') public step11: TemplateRef<any>;

  public logo: null;
  bsModalRef: BsModalRef;
  openShow: boolean;
  public listSubscribers: any = [];
  name: any;
  faSignOutAlt = faSignOutAlt
  faCrown = faCrown
  faArrowLeft = faArrowLeft
  faChartPie = faChartPie
  faCartPlus = faCartPlus
  faUsers = faUsers
  faTruck = faTruck
  faIndustry = faIndustry
  faAngleRight = faAngleRight
  faWarehouse = faWarehouse
  faCashRegister = faCashRegister
  faTruckPickup = faTruckPickup
  faCog = faCog
  public menu: any = []
  public currentUser: any
  loading: boolean;
  listVideos = {
    introVideo: 'https://player.vimeo.com/video/510207599'
  }

  constructor(public auth: AuthService, private router: Router, private share: ShareService,
              private modalService: BsModalService, private copilot: NgxCopilotService,
              private cookie: CookieService, private device: DeviceDetectorService,
              public sideBarService: SideBarService, public videoHelpService: VideoHelpService,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {


    // this.share.checkCopilot().then(res => {
    //   if (!res) {
    //     this.copilot.checkInit();
    //   }
    // })


    this.listObserver()
    const {logo, name} = this.share.getSettings();
    this.logo = logo;
    this.name = name;
    this.openShow = !this.device.isMobile();

  }

  skipAndPlay = () => {
    this.sideBarService.done();
    this.videoHelpService.open({url: this.listVideos.introVideo})
  }

  listObserver = () => {
    const observer1$ = this.share.changeSetting.subscribe(res => {
      const {name, logo} = res
      this.name = name;
      this.logo = logo;
      this.copilot.checkInit();
    })
    const observer2$ = this.share.openMenu.subscribe(res => {
      if (!this.device.isDesktop()) {
        this.openShow = res
      }
    })

    this.listSubscribers.push(observer1$)
    this.listSubscribers.push(observer2$)
  }

  closeMenu = () => this.share.openMenu.emit(false)

  skip = (o: any = null) => this.copilot.next(o);

  close = () => {
    this.cookie.set('globalCopilot', 'true', 365, '/');
    this.copilot.removeWrapper()
  }

  ngAfterViewInit(): void {
    this.menu = [
      {
        name: 'SIDE_BAR.HOME',
        icon: faChartPie,
        copilot: 1,
        copilotName: 'STEP_HOME',
        copilotTemplate: this.step1,
        route: ['/', 'home'],
      },
      {
        name: 'SIDE_BAR.PURCHASES',
        icon: faCartPlus,
        route: ['/', 'purchase'],
        copilot: 2,
        copilotName: 'STEP_PURCHASES',
        copilotTemplate: this.step2,
        disable: !this.share.checkRole(['admin', 'manager', 'seller'])
      },
      {
        name: 'SIDE_BAR.CALENDAR',
        icon: faCalendarAlt,
        copilot: 3,
        copilotName: 'STEP_PURCHASES',
        copilotTemplate: this.step3,
        disable: !this.share.checkRole(['admin', 'manager']),
        route: ['/', 'calendar'],
      },
      {
        name: 'SIDE_BAR.PRODUCTS',
        icon: faBox,
        copilot: 4,
        copilotName: 'STEP_ITEMS',
        copilotTemplate: this.step4,
        route: ['/', 'products']
      },
      {
        name: 'SIDE_BAR.ORDERS',
        icon: faCashRegister,
        copilot: 5,
        copilotName: 'STEP_ORDERS',
        copilotTemplate: this.step5,
        disable: !this.share.checkRole(['admin', 'manager', 'seller']),
        route: ['/', 'orders'],
      },
      {
        name: 'SIDE_BAR.USERS',
        icon: faUsers,
        route: ['/', 'users'],
        copilot: 6,
        copilotName: 'STEP_USERS',
        copilotTemplate: this.step6,
        disable: !this.share.checkRole(['admin', 'manager']),
      },
      {
        name: 'SIDE_BAR.STOCK',
        icon: faClipboardList,
        route: ['/', 'inventory'],
        copilot: 7,
        copilotName: 'STEP_INVENTORY',
        copilotTemplate: this.step7,
        disable: !this.share.checkRole(['admin', 'manager']),
      },
      {
        name: 'SIDE_BAR.DEPOSITS',
        // disable: true,
        icon: faWarehouse,
        route: ['/', 'deposits'],
        copilot: 8,
        copilotName: 'STEP_DEPOSITS',
        copilotTemplate: this.step8,
        disable: !this.share.checkRole(['admin', 'manager']),
      },
      {
        name: 'SIDE_BAR.PROVIDERS',
        icon: faIndustry,
        route: ['/', 'providers'],
        copilot: 9,
        copilotName: 'STEP_PROVIDERS',
        copilotTemplate: this.step9,
        disable: !this.share.checkRole(['admin', 'manager']),
      },
      {
        name: 'SIDE_BAR.ANALYTICS',
        icon: faChartLine,
        disable: !this.share.checkRole(['admin']),
        route: ['/', 'analytics']
      },
      // {
      //   name: 'SIDE_BAR.PLUGINS',
      //   icon: faPlug,
      //   copilot: 10,
      //   copilotName: 'STEP_PROVIDERS',
      //   copilotTemplate: this.step10,
      //   disable: !this.share.checkRole(['admin']),
      //   route: ['/', 'add-ons']
      // },
      {
        name: 'SIDE_BAR.SETTINGS',
        icon: faCog,
        route: ['/', 'settings'],
        copilot: 11,
        copilotName: 'STEP_DUMMY',
        copilotTemplate: this.step11,
        disable: !this.share.checkRole(['admin'])
      },
    ]

    this.currentUser = this.auth.currentUser()
    const {name, currency, logo} = this.share.getSettings()
    if (name && currency && logo) {
      setTimeout(() => {
        if (this.currentUser?.stepper && !this.currentUser?.stepper.includes('intro_user')) {
          this.sideBarService.initPosition(1)
        }
      }, 1500)
    }

    this.cdr.detectChanges();
  }

  open(data: any = null) {
    const initialState: any = {
      section: data
    };

    this.bsModalRef = this.modalService.show(
      ModalProfileComponent,
      Object.assign({initialState}, {
        class: 'modal-profile-small',
        ignoreBackdropClick: false
      })
    );
  }

  ngOnDestroy() {
    this.listSubscribers.forEach(a => a.unsubscribe())
  }

  logOut = () => {
    this.auth.logout().then(() => {
      this.router.navigate(['/', 'oauth'])
    })
  }


}
