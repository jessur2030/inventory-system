import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropGalleryComponent } from './drop-gallery.component';

describe('DropGalleryComponent', () => {
  let component: DropGalleryComponent;
  let fixture: ComponentFixture<DropGalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropGalleryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
