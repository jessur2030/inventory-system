import {Component, OnInit} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import {BsModalRef} from "ngx-bootstrap/modal";

@Component({
  selector: 'app-video-help',
  templateUrl: './video-help.component.html',
  styleUrls: ['./video-help.component.css']
})
export class VideoHelpComponent implements OnInit {
  section: any
  link: any;

  constructor(private sanitizer: DomSanitizer, public bsModalRef: BsModalRef) {
  }

  ngOnInit(): void {
    this.link = this.sanitizer.bypassSecurityTrustResourceUrl(
      `${this.section?.url}?title=0&byline=0&portrait=0&transparent=1&autoplay=0`);
  }

}
