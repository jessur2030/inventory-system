import {Component, OnInit} from '@angular/core';
import {
  faFileInvoice,
  faEllipsisV,
  faPlug
} from '@fortawesome/free-solid-svg-icons';
import {ModalViewAddComponent} from "../modal-view-add/modal-view-add.component";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {PageChangedEvent} from "ngx-bootstrap/pagination";
import {RestService} from "../../services/rest.service";
import {ActivatedRoute, Router} from "@angular/router";
import {PaginationServiceService} from "../../services/pagination-service.service";
import {ShareService} from "../../services/share.service";

@Component({
  selector: 'app-list-addons',
  templateUrl: './list-addons.component.html',
  styleUrls: ['./list-addons.component.css']
})
export class ListAddonsComponent implements OnInit {
  public data = [];
  public source = 'plugins';
  faPlug = faPlug
  faEllipsisV = faEllipsisV
  bsModalRef: BsModalRef;

  constructor(private modalService: BsModalService,
              private rest: RestService,
              private router: Router,
              public pagination: PaginationServiceService,
              private route: ActivatedRoute,
              private share: ShareService) {
  }

  ngOnInit(): void {
    this.pagination.init();
    this.route.queryParams.subscribe(
      params => {
        const {q = ''} = params
        this.onSrc(q)
      });
  }

  process = (item: any, mode = true) => {
    if (mode) {
      // this.router.navigate(['/', 'add-ons', item._id])
    } else {
      this.open(item)
    }

  }

  open(data: any = null) {
    const initialState: any = {
      data
    };

    this.bsModalRef = this.modalService.show(
      ModalViewAddComponent,
      Object.assign({initialState}, {
        class: 'modal-light-plan',
        ignoreBackdropClick: false
      })
    );
  }

  /**** GLOBAL FUNCTIONS ****/

  load = (src: string = '', merge = true) => {
    let fields = [
      `?fields=name`,
      `&page=${this.pagination.page}`
    ];
    fields.push(`&limit=${this.pagination.limit}`)
    const q = this.share.parseLoad(src, this.source, fields);
    this.pagination.paginationData(q, this.data, merge, this.source).then((res: any) => {
      this.data = res.data;
      this.pagination.paginationConfig = res.pagination;
    }).catch(error => {
      // (error.status === 403) ? this.cbMode = 'blocked' : null
    })
  }

  goTo = () => this.share.goTo(this.source)

  onSrc = (e) => {
    this.pagination.src = (e && e.length) ? e : '';
    this.load(e, false);
  }

  paginate = () => {
    this.pagination.page = this.pagination.page + 1;
    this.load();
  }

  pageChanged($event: PageChangedEvent) {
    const {page} = $event;
    this.pagination.page = page;
    this.load(this.pagination.src, false);
  }

  /**** END GLOBAL FUNCTIONS ****/

}
