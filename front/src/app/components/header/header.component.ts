import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {faLifeRing, faBell, faQuestionCircle, faUserCircle} from '@fortawesome/free-regular-svg-icons';
import {faAngleRight, faBars, faEllipsisV} from '@fortawesome/free-solid-svg-icons';
import {faTired} from '@fortawesome/free-solid-svg-icons';
import {BsDropdownConfig} from "ngx-bootstrap/dropdown";
import {ShareService} from "../../services/share.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {NotificationsService} from "../../services/notifications.service";
import {ScriptsService} from "../../services/script.service";
import {environment} from "../../../environments/environment";


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [{provide: BsDropdownConfig, useValue: {isAnimated: true, autoClose: true}}]
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Input() label: string;
  @Input() history: any = [];
  public title: null;
  public logo: null;
  public listSubscribers: any = [];
  public limitAccount: any = null;
  faTired = faTired
  faUserCircle = faUserCircle
  faBars = faBars
  faLifeRing = faLifeRing
  faAngleRight = faAngleRight
  faBell = faBell
  faQuestionCircle = faQuestionCircle
  public form: FormGroup;
  public menu: any = [

    {
      name: 'Ayuda',
      icon: faLifeRing,
      target: `https://help.kitagil.com`
    }
  ]

  constructor(private share: ShareService,
              private router: Router,
              private scriptsService: ScriptsService,
              public notificationsService: NotificationsService,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.loadScript();
    this.listObserver()
    const {name, logo} = this.share.getSettings();
    this.title = name;
    this.logo = logo;
    this.form = this.formBuilder.group({
      q: ['']
    });
  }


  loadScript = () => {
    if(environment.production){
      this.scriptsService.loadScript('tawk').then(r => {
        console.log('Loaded chat')
      })
    }
  }

  listObserver = () => {
    const observer1$ = this.share.changeSetting.subscribe(res => {
      const {name} = res
      this.title = name;
    })

    const observer2$ = this.share.limitAccount.subscribe(res => {
      if (res) {
        this.limitAccount = res;
      }
    })
    this.listSubscribers.push(observer1$)
    this.listSubscribers.push(observer2$)
  }

  ngOnDestroy() {
    this.listSubscribers.forEach(a => a.unsubscribe())
  }

  search = () => {
    this.router.navigate(['/', 'search'], {queryParams: this.form.value})
    console.log(this.form.value)
  }

  openM = () => this.share.openMenu.emit(true)
}
