import {AfterViewInit, Component, Input, NgZone, OnInit} from '@angular/core';
import {environment} from "../../../environments/environment";
import {AnimationItem} from "lottie-web";
import {AnimationOptions} from "ngx-lottie";
import {DeviceDetectorService} from "ngx-device-detector";
import {ShareService} from "../../services/share.service";

@Component({
  selector: 'app-copilot',
  templateUrl: './copilot.component.html',
  styleUrls: ['./copilot.component.css']
})
export class CopilotComponent implements OnInit, AfterViewInit {
  @Input() inside: any;
  private animationItem: AnimationItem;

  constructor(private ngZone: NgZone,
              private share: ShareService) {
  }

  copilot: AnimationOptions = {
    path: '/assets/images/swipe-left.json',
  };

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {

  }

  skip = () => this.share.copilot.emit(false)


}


