import {Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {CheckoutComponent as StripeCheckOut} from "../../../../../../projects/kit-agil-stripe/src/lib/checkout/checkout.component";
import {CallbackPayComponent} from "../../../../../../projects/kit-agil-paypal/src/lib/callback-pay/callback-pay.component";
import {MpInitComponent} from "../../../../../../projects/kit-mercado-pago/src/lib/mp-init/mp-init.component";
import {RestService} from "../../../../services/rest.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
})
export class AddComponent implements OnInit {
  @ViewChild('eventContainer', {read: ViewContainerRef}) entry: ViewContainerRef;
  componentRef: any;
  private id: any;
  private event: any;
  public params: any
  public queryParams: any
  private index = [
    {
      name: 'stripe-checkout',
      class: StripeCheckOut,
      path: 'stripe',
      event: 'get_order',
      auto: true,
      func: (id: string) => {
        this.rest.postEventsPublic(`stripe/events/get_order`, {
          "value": {
            "tracker": id
          }
        }).subscribe(res => this.createComponent(StripeCheckOut, res))
      }
    },
    {
      name: 'paypal-cb',
      class: CallbackPayComponent,
      path: 'paypal',
      event: 'get_order',
      auto: true,
      func: (id: string) => {
        this.rest.postEventsPublic(`paypal/events/get_order`, {
          "value": {
            "tracker": id
          }
        }).subscribe(res => this.createComponent(CallbackPayComponent, res))
      }
    },
    {
      name: 'mercadopago-checkout',
      class: MpInitComponent,
      path: 'mercadopago',
      event: 'get_order',
      auto: true,
      func: (id: string) => {
        this.rest.postEventsPublic(`mercadopago/events/get_order`, {
          "value": {
            "tracker": id
          }
        }).subscribe(res => this.createComponent(MpInitComponent, res))
      }
    }
  ];

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private rest: RestService,
              private resolver: ComponentFactoryResolver,
              private route: ActivatedRoute,
              public router: Router) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const {id, event} = params
      this.params = params;
      this.id = id
      this.event = event
      if (this.id) {
        this.load(this.id, this.event);
      }
    });

    this.route.queryParams.subscribe(params => {
      this.queryParams = params;
      // if (this.params && (this.params.event.includes('paypal'))) {
      //   this.executeFn('paypal-cb', this.queryParams.token)
      // }
    })
  }

  createComponent(nameClass: any, value: any) {
    try {
      this.entry.clear();
      const factory = this.resolver.resolveComponentFactory(nameClass);
      this.componentRef = this.entry.createComponent(factory);
      this.componentRef.instance.content = value;
    } catch (e) {
      console.log(e)
      return null
    }
  }

  // private executeFn = (event, id) => {
  //   this.index.filter((a: any) => {
  //     if (a.name === event) {
  //       a.func(id)
  //     }
  //   })
  // }

  private load(id: string, event: string) {
    this.index.filter((a: any) => {
      if (a.name === event && a.auto) {
        a.func(id)
      }
    })
  }

}
