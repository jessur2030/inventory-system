import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddEventsRoutingModule } from './add-events-routing.module';
import { AddComponent } from './pages/add/add.component';
import {SharedModule} from "../shared/shared.module";


@NgModule({
  declarations: [AddComponent],
    imports: [
        CommonModule,
        AddEventsRoutingModule,
        SharedModule
    ]
})
export class AddEventsModule { }
