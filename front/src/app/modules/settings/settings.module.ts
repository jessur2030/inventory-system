import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SettingsRoutingModule} from './settings-routing.module';
import {AddComponent} from './pages/add/add.component';
import {SharedModule} from "../shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {FilePickerModule} from "ngx-awesome-uploader";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {OauthModule} from "../oauth/oauth.module";
import {NgSelectModule} from "@ng-select/ng-select";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {HtmlEditComponent} from './pages/html-edit/html-edit.component';
import {ModalVariablesComponent} from './pages/html-edit/modal-variables/modal-variables.component';
import {MatTabsModule} from "@angular/material/tabs";
import {ListPlanComponent} from "../../components/list-plan/list-plan.component";
import {MethodPaymentComponent} from "../../components/method-payment/method-payment.component";


@NgModule({
  declarations: [AddComponent, HtmlEditComponent, ModalVariablesComponent, ListPlanComponent, MethodPaymentComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    SharedModule,
    TranslateModule,
    FilePickerModule,
    ReactiveFormsModule,
    OauthModule,
    NgSelectModule,
    FontAwesomeModule,
    FormsModule,
    MatTabsModule
  ]
})
export class SettingsModule {
}
