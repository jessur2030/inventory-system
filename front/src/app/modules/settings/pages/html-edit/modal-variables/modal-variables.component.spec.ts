import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalVariablesComponent } from './modal-variables.component';

describe('ModalVariablesComponent', () => {
  let component: ModalVariablesComponent;
  let fixture: ComponentFixture<ModalVariablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalVariablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalVariablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
