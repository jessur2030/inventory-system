import {HostListener, Injectable} from '@angular/core';
import {Observable, of} from "rxjs";
import {RestService} from 'src/app/services/rest.service';
import {Clipboard} from "@angular/cdk/clipboard";

@Injectable({
  providedIn: 'root'
})
export class HtmlEditService {
  private dataTemplate: Observable<any>
  tooltipVideo: boolean
  public gasperConfig = {}
  public listVideos = {
    builderHtml: 'https://player.vimeo.com/video/509911065'
  }

  public gasperVariableInvoice = {
    stylePrefix: 'mdl-',
    title: 'Variable',
    content: `<table class="gps-variables-table" style="width: 100%">
              <tr>
              <th>Code</th>
              <th>Description</th>
              </tr>
              <tr>
              <td><input class="copy-cpl" type="text" value="__CUSTOMER_NAME__"  readonly/></td>
              <td>Customer name</td>
              </tr>
              <tr>
              <td><input class="copy-cpl" type="text" value="__CUSTOMER_LAST_NAME__"  readonly/></td>
              <td>Customer Lastname</td>
              </tr>
              <tr>
              <td><input class="copy-cpl" type="text" value="__CUSTOMER_ID__"  readonly/></td>
              <td>Customer Number ID</td>
              </tr>
              <tr>
              <td><input class="copy-cpl" type="text" value="__CUSTOMER_EMAIL__"  readonly/></td>
              <td>Customer Email</td>
              </tr>
              <tr>
              <td><input class="copy-cpl" type="text" value="__CUSTOMER_PHONE__"  readonly/></td>
              <td>Customer Phone</td>
              </tr>
              <tr>
              <td><input class="copy-cpl" type="text" value="__CUSTOMER_ADDRESS__"  readonly/></td>
              <td>Customer Address</td>
              </tr>
              <tr>
              <td><input class="copy-cpl" type="text" value="__CUSTOMER_BUSINESS_NAME__"  readonly/></td>
              <td>Customer Business name</td>
              </tr>
              <tr>
              <td><input class="copy-cpl" type="text" value="__INVOICE_NUMBER__"  readonly/></td>
              <td>Number of invoice</td>
              </tr>
              <tr>
              <td><input class="copy-cpl" type="text" value="__PURCHASE_ORDER__"  readonly/></td>
              <td>Control number of purchase</td>
              </tr>
              <tr>
              <td><input class="copy-cpl" type="text" value="__PURCHASE_OBSERVATION__"  readonly/></td>
              <td>Purchase observation</td>
              </tr>
              <tr>
              <td><input class="copy-cpl" type="text" value="__PURCHASE_DATE__"  readonly/></td>
              <td>Purchase date</td>
              </tr>
              <tr>
              <td><input class="copy-cpl" type="text" value="__LOGO__"  readonly/></td>
              <td>Url logo</td>
              </tr>
              <tr>
              <td><input class="copy-cpl" type="text" value="__SELLER_NAME__"  readonly/></td>
              <td>Seller name</td>
              </tr>
              <tr>
              <td><input class="copy-cpl" type="text" value="__SELLER_ID__"  readonly/></td>
              <td>Seller ID</td>
              </tr>
              <tr>
              <td><input class="copy-cpl" type="text" value="__SELLER_EMAIL__"  readonly/></td>
              <td>Seller Email</td>
              </tr>
              <tr>
              <td><input class="copy-cpl" type="text" value="__SELLER_PHONE__"  readonly/></td>
              <td>Seller Phone</td>
              </tr>
              <tr>
              <td><input class="copy-cpl" type="text" value="__SELLER_ADDRESS__"  readonly/></td>
              <td>Seller Address</td>
              </tr>
            </table>`,
    backdrop: true
  }

  constructor(
    private clipboard: Clipboard,
    private rest: RestService,
  ) {
  }

  getData = () => {
    return of([])
  }

  callConfig = (data = {}) => {
    this.gasperConfig = {
      container: '#gjs',
      fromElement: true,
      height: '100%',
      width: 'auto',
      storageManager: {
        autoload: false,
      },
      panels: {},

    }
    return this.gasperConfig

  }

  copyVarTemplate = (varName) => {
    this.clipboard.copy(varName);
  }

  makeZone = () => {
    const iframe: HTMLIFrameElement = document.querySelector('iframe')
    const bodyInside = iframe.contentDocument;
    const css = `.secure-amount, .secure-zone{
      border: dashed 2px #e02efe !important;
      background-color: #e02efe30;
}`,
      head = bodyInside.head || bodyInside.getElementsByTagName('head')[0],
      style: any = bodyInside.createElement('style');

    head.appendChild(style);

    style.type = 'text/css';
    if (style.styleSheet) {
      // This is required for IE8 and below.
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }
  }
}
