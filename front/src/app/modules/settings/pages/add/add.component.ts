import {Component, ElementRef, NgZone, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {RestService} from "../../../../services/rest.service";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DomSanitizer} from "@angular/platform-browser";
import {environment} from "../../../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ShareService} from "../../../../services/share.service";
import {Observable, throwError} from "rxjs";
import {catchError, finalize} from "rxjs/operators";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {ActivatedRoute, Router} from "@angular/router";
import * as _ from "lodash";
import {CookieService} from "ngx-cookie-service";
import {ModalAddTaxComponent} from "../../../../components/modal-add-tax/modal-add-tax.component";
import {
  faTrashAlt
} from '@fortawesome/free-regular-svg-icons';
import {CurrenciesDataService} from "../../../../services/currencies-data.service";
import {ModalAddTaxOffsetComponent} from "../../../../components/modal-add-tax-offset/modal-add-tax-offset.component";
import {SettingsService} from "../../settings.service";

function invoiceValidator() {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    if (!control.value.includes('%%%')) {
      return {invoiceValidator: true};
    }
    return null;
  };
}

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit, OnDestroy {
  indexTab: any = 0;
  public history: any = [
    {
      name: 'Ajustes',
      router: ['/', 'settings']
    }
  ]
  public listSubscribers: any = [];
  public formGeneral: FormGroup;
  public formTaxes: FormGroup;
  public preview = {
    image: null,
    blob: null
  }
  taxList = []
  taxListOffset = []
  faTrashAlt = faTrashAlt;
  bsModalRef: BsModalRef;
  currencies = []
  languages = [
    {
      name: 'Español (España)',
      value: 'es-ES'
    },
    {
      name: 'Español (México)',
      value: 'es-MX'
    }
  ]
  templates = [
    {name: 'Diseñar factura', value: 'invoice'},
    {name: 'Diseñar prespuesto', value: 'purchase'}
  ];
  loading: any;
  selectedTemplate: any;
  initialInvoice:number = 0

  constructor(private ngZone: NgZone,
              private http: HttpClient,
              public share: ShareService,
              private rest: RestService,
              private sanitizer: DomSanitizer,
              private router: Router,
              public currencyService: CurrenciesDataService,
              private modalService: BsModalService,
              private cookieService: CookieService,
              private settingsService: SettingsService,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.indexTab = this.route.snapshot.paramMap.get('tab') || 0;
    // this.selectedTemplate = 'purchase'
    this.formGeneral = this.formBuilder.group({
      name: ['', Validators.required],
      currency: ['', Validators.required],
      currencySymbol: ['', Validators.required],
      language: ['', Validators.required],
      logo: [''],
    });

    this.formTaxes = this.formBuilder.group({
      tax: [[]],
      taxOffset: [[]],
      initialInvoice: [0, [Validators.required, Validators.min(0)]],
      invoiceFormat: ['', [
        Validators.required,
        invoiceValidator()]
      ]
    });
    this.listObserver()
    this.loadCurrencies()
    this.load(0)
  }

  loadCurrencies = () => {
    this.currencies = Object.values(this.currencyService.listCurrencies)
  }

  listObserver = () => {
    const observer1$ = this.settingsService.tax.subscribe(res => {
      this.taxList.push(res)
      this.taxList = _.uniq(this.taxList)
    })
    const observer2$ = this.settingsService.taxOffset.subscribe(res => {
      this.taxListOffset.push(res)
      this.taxListOffset = _.uniq(this.taxListOffset)
    })

    this.listSubscribers.push(observer1$)
    this.listSubscribers.push(observer2$)
  }

  loadGeneral = () => {
    this.loading = true
    this.rest.get(`settings/general`)
      .pipe(finalize(() => this.loading = false))
      .subscribe(res => {
        this.preview.image = res.logo;
        this.initialInvoice = (typeof res.initialInvoice === 'number') ? res.initialInvoice : 0
        this.formGeneral.patchValue(res)
      })
    }

    loadTaxes = () => {
      this.loading = true
      this.rest.get(`settings/taxes`)
      .pipe(finalize(() => this.loading = false))
      .subscribe(res => {
        this.taxList = res.tax || []
        this.taxListOffset = res.taxOffset || []
        console.log('initialInvoice:',res)
        res = {
          initialInvoice:  this.initialInvoice,
          invoiceFormat: (res.invoiceFormat) ? res.invoiceFormat : '%%%%%%',
        }
        this.formTaxes.patchValue(res)
      })
  }

  load = ($event: any) => {
    switch ($event) {
      case 0:
        this.loadGeneral()
        break
      case 1:
        this.loadTaxes()
        break
      default:
        this.loadGeneral()
    }
    console.log($event)

  }

  ngOnDestroy() {
    this.listSubscribers.forEach(a => a.unsubscribe())
  }

  updateGeneral = () => {

    const {name, currency, currencySymbol, language} = this.formGeneral.value;
    const formData = new FormData();
    if (this.preview.blob) {
      formData.append('logo', this.preview.blob)
    }
    formData.append('name', name)
    formData.append('currency', currency)
    formData.append('language', language)
    formData.append('currencySymbol', currencySymbol)
    this.loading = true
    this.saveRest(`settings/general`, formData)
      .pipe(finalize(() => this.loading = false))
      .subscribe(res => {
          this.cookieService.set(
            'settings',
            JSON.stringify(res),
            environment.daysTokenExpire,
            '/');
          this.preview.image = res.logo;
          this.formGeneral.patchValue(res)
          this.share.loading.emit(false)
          this.settingsService.generalSave.emit(res);
        },
        error => console.log('err', error))
  }


  updateTaxes = () => {
    this.formTaxes.patchValue({
      tax: this.taxList,
      taxOffset: this.taxListOffset
    })
    this.loading = true
    this.rest.patch(`settings/taxes`, this.formTaxes.value)
      .pipe(finalize(() => this.loading = false))
      .subscribe(res => {
          this.formTaxes.patchValue(res)
          this.share.loading.emit(false)
        },
        error => console.log('err', error))
  }

  update = () => {

  }

  fileAdded($event: any) {
    const unsafeImg = URL.createObjectURL($event.file);
    const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
    this.preview = {
      blob: $event.file,
      image
    }
  }

  reset = () => {
    this.preview = {
      image: null,
      blob: null
    }
  }

  saveRest(path = '', body = {}): Observable<any> {
    try {
      this.share.loading.emit(true)
      return this.http.patch(`${environment.api}/${path}`, body,
        {headers: this.parseHeader()})
        .pipe(
          catchError((e: any) => {
            this.share.loading.emit(false)
            return throwError({
              status: e.status,
              statusText: e.statusText,
              e
            });
          }),
        );
    } catch (e) {
      this.share.loading.emit(false)
    }
  }

  parseHeader = () => {
    const token = this.cookieService.get('session');
    let header = {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`
    };
    return new HttpHeaders(header);
  };

  addTagFn = () => {
    this.openNewTax()
  }

  openNewTax(data: any = null) {
    const initialState: any = {
      section: data
    };

    this.bsModalRef = this.modalService.show(
      ModalAddTaxComponent,
      Object.assign({initialState}, {
        class: ' modal-light-plan',
        ignoreBackdropClick: false
      })
    );
  }

  openNewTaxOffset(data: any = null) {
    const initialState: any = {
      section: data
    };

    this.bsModalRef = this.modalService.show(
      ModalAddTaxOffsetComponent,
      Object.assign({initialState}, {
        class: ' modal-light-plan',
        ignoreBackdropClick: false
      })
    );
  }

  addTax() {

  }

  deletePrice = (i, source) => {
    this[source].splice(i, 1)
  }

  save(tab: number) {
    switch (tab) {
      case 0:
        this.updateGeneral()
        break
      case 1:
        this.updateTaxes()
        break
    }
  }

  selectBuilder($event: any) {
    this.router.navigate(['/', 'settings', '2', 'design', $event?.value])
    console.log($event)
  }
}
