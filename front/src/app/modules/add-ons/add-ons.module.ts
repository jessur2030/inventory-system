import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AddOnsRoutingModule} from './add-ons-routing.module';
import {AddComponent} from './pages/add/add.component';
import {ListComponent} from './pages/list/list.component';
import {SharedModule} from "../shared/shared.module";
import {LottieModule} from "ngx-lottie";
import {CarouselModule} from "ngx-owl-carousel-o";
import {PDFReportModule} from "../../../../projects/pdfreport/src/lib/pdfreport.module";
import {KitAgilStripeModule} from "../../../../projects/kit-agil-stripe/src/lib/kit-agil-stripe.module";


@NgModule({
  declarations: [AddComponent, ListComponent],
  imports: [
    CommonModule,
    CarouselModule,
    AddOnsRoutingModule,
    SharedModule,
    LottieModule,
    PDFReportModule
  ]
})
export class AddOnsModule {
}
