import {Component, OnInit} from '@angular/core';
import {AnimationOptions} from "ngx-lottie";
import {AnimationItem} from "lottie-web";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {PageChangedEvent} from "ngx-bootstrap/pagination";
import {RestService} from "../../../../services/rest.service";
import {ActivatedRoute, Router} from "@angular/router";
import {PaginationServiceService} from "../../../../services/pagination-service.service";
import {ShareService} from "../../../../services/share.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  public data = [];
  public source = 'plugins';
  bsModalRef: BsModalRef;
  public history: any = [
    {
      name: 'Complementos',
      faqUrl: 'https://help.kitagil.com/search/plugins'
    }
  ]

  options: AnimationOptions = {
    path: '/assets/images/add-ons.json',
  };

  constructor() {
  }

  ngOnInit(): void {

  }


}
