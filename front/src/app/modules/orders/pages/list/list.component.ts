import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output
} from '@angular/core';
import {RestService} from "../../../../services/rest.service";
import {ActivatedRoute, Router} from "@angular/router";
import {faPhoneAlt, faIndustry, faUser} from '@fortawesome/free-solid-svg-icons';
import {ShareService} from "../../../../services/share.service";
import {PageChangedEvent} from "ngx-bootstrap/pagination";
import {PaginationServiceService} from "../../../../services/pagination-service.service";
import {faAngleLeft, faAngleRight, faAngleDoubleLeft, faAngleDoubleRight} from '@fortawesome/free-solid-svg-icons';
import {PDFReportService} from "../../../../../../projects/pdfreport/src/lib/pdfreport.service";
import {SearchService} from "../../../search/search.service";
import {animate, query, stagger, style, transition, trigger} from "@angular/animations";
import {debounceTime, finalize, map, tap} from "rxjs/operators";
import {Observable, of} from "rxjs";

@Component({
  selector: 'app-list-orders',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  animations: [
    trigger('listAnimation', [
      transition('* => *', [
        query(':enter', [
          style({opacity: 0}),
          stagger(20, [
            animate(25, style({opacity: 1}))
          ])
        ], {optional: true})
      ])
    ])
  ]
})
export class ListComponent implements OnInit, OnChanges, AfterViewChecked {
  @Input() mode: string = 'page'
  @Input() title: any = false;
  @Input() limit: any = 15;
  @Input() searchTool: boolean = true;
  @Input() viewMore: boolean = true;
  @Input() autoLoad: boolean = true;
  @Input() dataInside: any;
  @Output() cbClick = new EventEmitter<any>();
  public cbMode: any = null;
  public currency: any = null;
  public currencySymbol: any = null;
  public dataTake: any
  loading: any;

  constructor(private rest: RestService,
              private share: ShareService,
              private route: ActivatedRoute,
              private cdr: ChangeDetectorRef,
              public pagination: PaginationServiceService,
              private pdf: PDFReportService,
              private search: SearchService,
              private router: Router) {
  }

  faAngleDoubleLeft = faAngleDoubleLeft
  faAngleDoubleRight = faAngleDoubleRight
  faAngleLeft = faAngleLeft
  faAngleRight = faAngleRight
  faPhoneAlt = faPhoneAlt
  faIndustry = faIndustry
  faUser = faUser
  public page: number = 1;
  public data: Observable<any>
  public source = 'orders';

  public history: any = [
    {
      name: 'Pagos'
    }
  ]

  ngOnInit(): void {
    let fields = [
      `?fields=purchase.controlNumber,customer.name,customer.email,customer.lastName,tag`
    ];
    this.search.setConfig({fields, key: this.source, limit: this.limit})
    const {currency, currencySymbol} = this.share.getSettings();
    this.currencySymbol = currencySymbol;
    this.currency = currency

    if (this.autoLoad) {
      this.route.queryParams.subscribe(
        params => {
          const {q = ''} = params
          this.onSrc(q)
        });
    }

  }

  emitCbClick = (inside: any = {}) => {
    if (this.mode === 'modal') {
      this.cbClick.emit(inside);
    } else {
      this.router.navigate(['/', 'orders', inside?._id])
    }

  }

  /**** GLOBAL FUNCTIONS ****/

  cbPdf() {
    this.pdf.generateList({source: this.source, page: this.page, data: this.dataTake})
  }

  load = (src: any, resetPagination = true) => {
    this.loading = true;
    this.data = this.search.find({
      source: this.source,
      query: src
    }, resetPagination).pipe(
      debounceTime(500),
      tap(a => this.dataTake = a.docs),
      finalize(() => this.loading = false),
      map((a) => a.docs)
    )
  }

  goTo = () => this.share.goTo(this.source)

  onSrc = (e) => this.load(e);

  pageChanged($event: PageChangedEvent) {
    const {page} = $event;
    this.pagination.page = page;
    this.load(undefined, false);
  }

  ngAfterViewChecked(): void {
    this.cdr.detectChanges();
  }

  ngOnChanges(changes: any): void {
    setTimeout(() => {
      if (changes['dataInside']) {
        this.data = of(changes['dataInside'].currentValue).pipe(tap(a => this.dataTake = a))
      }
    }, 0)
  }

  /**** END GLOBAL FUNCTIONS ****/


}
