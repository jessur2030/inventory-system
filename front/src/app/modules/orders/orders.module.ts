import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import { OrdersRoutingModule } from './orders-routing.module';
import {AddComponent} from "./pages/add/add.component";
import {ListComponent} from "./pages/list/list.component";
import {SharedModule} from "../shared/shared.module";
import {PaginationModule} from "ngx-bootstrap/pagination";
import {AvatarModule} from "ngx-avatar";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [AddComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    OrdersRoutingModule,
    SharedModule,
    PaginationModule,
    AvatarModule,
    TranslateModule
  ]
})
export class OrdersModule { }
