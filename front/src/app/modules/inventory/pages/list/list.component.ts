import {Component, Input, OnInit} from '@angular/core';
import {RestService} from "../../../../services/rest.service";
import {ActivatedRoute, Router} from "@angular/router";
import {faPhoneAlt, faUser, faLongArrowAltUp, faLongArrowAltDown} from '@fortawesome/free-solid-svg-icons';
import {
  faAngleLeft,
  faIndustry,
  faAngleRight,
  faAngleDoubleLeft,
  faAngleDoubleRight
} from '@fortawesome/free-solid-svg-icons';
import {animate, query, stagger, style, transition, trigger} from "@angular/animations";
import {ShareService} from "../../../../services/share.service";
import {PageChangedEvent} from "ngx-bootstrap/pagination";
import {PaginationServiceService} from "../../../../services/pagination-service.service";
import {PDFReportService} from "../../../../../../projects/pdfreport/src/lib/pdfreport.service";
import {AuthService} from "../../../../services/auth.service";
import {SearchService} from "../../../search/search.service";
import {debounceTime, finalize, map, tap} from "rxjs/operators";
import {Observable} from "rxjs";

@Component({
  selector: 'app-list-inventory',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  animations: [
    trigger('listAnimation', [
      transition('* => *', [
        query(':enter', [
          style({opacity: 0}),
          stagger(20, [
            animate(25, style({opacity: 1}))
          ])
        ], {optional: true})
      ])
    ])
  ]
})
export class ListComponent implements OnInit {
  @Input() mode: string = 'page'
  @Input() title: any = false;
  @Input() limit: any = 15;
  @Input() viewMore: boolean = true;
  public page: number = 1;
  @Input() dataTake: any
  @Input() data: Observable<any>
  loading: any;

  constructor(private rest: RestService,
              private router: Router,
              public pagination: PaginationServiceService,
              private route: ActivatedRoute,
              private pdf: PDFReportService,
              public search: SearchService,
              private auth: AuthService,
              private share: ShareService) {
  }

  faAngleDoubleLeft = faAngleDoubleLeft
  faAngleDoubleRight = faAngleDoubleRight
  faAngleLeft = faAngleLeft
  faAngleRight = faAngleRight
  faPhoneAlt = faPhoneAlt
  faLongArrowAltUp = faLongArrowAltUp
  faLongArrowAltDown = faLongArrowAltDown
  faUser = faUser
  public cbMode: any = false;
  public source = 'inventory';
  public history: any = [
    {
      name: 'Inventario / Movimientos'
    }
  ]

  ngOnInit(): void {
    let fields = [
      `?fields=product.name,product.categories,product.sku,provider.name,product.tag,email,lastName,tag,serials`
    ];
    this.search.setConfig({fields, key: this.source, limit: this.limit})
    this.route.queryParams.subscribe(
      params => {
        const {q = ''} = params
        if(!this.data){
          this.onSrc(q)
        }
      });
  }

  goToDetail(data = null) {
    this.router.navigate(['/', 'products', data.product._id])
  }

  /**** GLOBAL FUNCTIONS ****/
  cbPdf() {
    this.pdf.generateList({source: this.source, page: this.page, data: this.dataTake})
  }

  load = (src: any, resetPagination = true) => {
    this.loading = true;
    this.data = this.search.find({
      source: this.source,
      query: src
    }, resetPagination).pipe(
      debounceTime(500),
      tap(a => this.dataTake = a.docs),
      finalize(() =>  this.loading = false),
      map((a) => a.docs)
    )
  }

  goTo = () => this.share.goTo(this.source)

  onSrc = (e) => this.load(e);

  pageChanged($event: PageChangedEvent) {
    const {page} = $event;
    this.pagination.page = page;
    this.load(undefined, false);
  }


  /**** END GLOBAL FUNCTIONS ****/



}
