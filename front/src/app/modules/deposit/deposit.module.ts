import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DepositRoutingModule } from './deposit-routing.module';
import { AddComponent } from './pages/add/add.component';
import {ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";
import { ListComponent } from './pages/list/list.component';
import {AvatarModule} from "ngx-avatar";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {TranslateModule} from "@ngx-translate/core";
import {PaginationModule} from "ngx-bootstrap/pagination";
import {PDFReportModule} from "../../../../projects/pdfreport/src/lib/pdfreport.module";
import {TooltipModule} from "ngx-bootstrap/tooltip";


@NgModule({
  declarations: [AddComponent, ListComponent],
    imports: [
        CommonModule,
        DepositRoutingModule,
        ReactiveFormsModule,
        SharedModule,
        AvatarModule,
        FontAwesomeModule,
        TranslateModule,
        PaginationModule,
        PDFReportModule,
        TooltipModule
    ]
})
export class DepositModule { }
