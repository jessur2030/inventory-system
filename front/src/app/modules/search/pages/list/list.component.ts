import {Component, OnInit} from '@angular/core';
import {
  faBox,
  faCartPlus,
  faIndustry,
  faUser,
  faUsers,
  faPhoneAlt,
  faWarehouse
} from '@fortawesome/free-solid-svg-icons';
import {RestService} from "../../../../services/rest.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ShareService} from "../../../../services/share.service";
import {animate, query, stagger, style, transition, trigger} from "@angular/animations";
import {finalize} from "rxjs/operators";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  animations: [
    trigger('listAnimation', [
      transition('* => *', [
        query(':enter', [
          style({opacity: 0}),
          stagger(20, [
            animate(25, style({opacity: 1}))
          ])
        ], {optional: true})
      ])
    ])
  ]
})
export class ListComponent implements OnInit {
  data: any = []
  public q = '';
  faPhoneAlt = faPhoneAlt;
  faIndustry = faIndustry;
  faCartPlus = faCartPlus;
  faWarehouse = faWarehouse;
  faUser = faUser;
  faUsers = faUsers;
  faBox = faBox;
  public source = 'search';
  public cbMode: any = false;
  public currency = null;
  public currencySymbol = null;
  loading: any;

  constructor(private rest: RestService,
              private router: Router,
              private route: ActivatedRoute,
              private share: ShareService) {
  }

  ngOnInit(): void {
    const {currency, currencySymbol} = this.share.getSettings();
    this.currency = currency;
    this.currencySymbol = currencySymbol
    this.countDocs();
    this.route.queryParams.subscribe(
      params => {
        const {q} = params
        if (q) {
          this.q = q;
          this.load(q)
        }
      });
  }

  load = (src: string = '') => {
    this.loading = true;
    let fields = [
      `?fields=name,lastName,controlNumber,sku,items.serials`,
      `&filter=${src}`,
      `&limit=5`
    ];

    const q = this.share.parseLoad(src, this.source, fields);
    this.rest.get(q.join(''))
      .pipe(finalize(() => this.loading = false))
      .subscribe(res => {
        this.data = {...this.data, ...res}
      }, error => {
        (error.status === 403) ? this.cbMode = 'blocked' : null
      })
  }

  countDocs = () => {
    this.data.map(a => console.log(a.docs))
  }


}
