import {Injectable} from '@angular/core';
import {RestService} from "../../services/rest.service";
import {defer, Observable} from "rxjs";
import {debounceTime, distinctUntilChanged, finalize, map} from "rxjs/operators";
import {PaginationServiceService} from "../../services/pagination-service.service";
import * as _ from "lodash"

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  public fields = [];
  public filters = [];
  public data$: Observable<any>
  public loading: boolean
  public src: string
  private fieldsByKey = []

  constructor(private rest: RestService, private paginationService: PaginationServiceService) {
  }


  prepare<T>(callback: () => void): (source: Observable<T>) => Observable<T> {
    return (source: Observable<T>): Observable<T> => defer(() => {
      callback();
      return source;
    });
  }

  public find = (data: SourceIn, resetPagination = true) => {
    if (data.query !== undefined) {
      this.src = data.query
    }
    if (resetPagination) {
      this.paginationService.init()
    }
    let url = [
      data.source,
      _.find(this.fieldsByKey, {key: data.source}).fields.join(''),
      `&filter=${this.src}`
    ]

    url.push(`&page=${this.paginationService.page}&limit=${_.find(this.fieldsByKey, {key: data.source}).limit}`)

    this.data$ = this.rest.get(url.join(''), true, {ignoreLoadingBar: ''})
      .pipe(
        map(a => {
          this.paginationService.totalDocs = a.totalDocs;
          this.paginationService.page = a.page;
          this.paginationService.limit = a.limit;
          this.paginationService.hasPrevPage = a.hasPrevPage;
          this.paginationService.hasNextPage = a.hasNextPage;
          return a
        }),
        debounceTime(400),
        distinctUntilChanged(),
        this.prepare(() => this.loading = true),
        finalize(() => this.loading = false)
      );
    return this.data$
  }

  reset = () => {
    this.filters = []
    this.fields = []
    this.fieldsByKey = []
  }

  public setConfig = (data: SourceField) => {
    this.paginationService.init();
    this.fieldsByKey.push({
      key: data.key,
      fields: data.fields,
      limit: data.limit
    })
  }

  public snipQuery = (pagination: any, $event: any = [], initialFilter = true, resetFilter = false) => {
    if (initialFilter) {
      this.fields = this.fields.concat($event.map(a => {
          return a.pre.field;
        })
      )
      this.filters = this.filters.concat($event.map(a => {
        if (a.condition) {
          if (a.condition.value === '>') {
            return `[${a.value}|infinity]`;
          }
          if (a.condition.value === '<') {
            return `[infinity|${a.value}]`;
          }
          if (a.condition.value === '=') {
            return `[${a.value}]`;
          }
        } else {
          return ((typeof a.value) !== 'string') ? a.value.name : a.value;
        }
      }))
    } else {
      this.fields = $event.map(a => {
        return a.pre.field;
      });

      this.filters = $event.map(a => {
        return ((typeof a.value) !== 'string') ? a.value.name : a.value;
      })
    }

    if (resetFilter) {
      this.fields = [];
      this.filters = [];
    }

    let first = [
      `?page=${pagination.page}`,
      `&limit=${pagination.limit}`
    ];


    const indexName = this.fields.indexOf('name');
    if (indexName === -1) {
      if (pagination.src.length > 2) {
        this.fields.push('name');
        this.fields.push('tag');
        this.fields.push('sku');
        this.filters.push(pagination.src);
      }
    } else {
      this.filters = this.filters.map((item, index) => {
        if (indexName === index) {
          item = pagination.src;
        }

        return item;
      })
    }

    const uniqueFields = new Set(this.fields);
    const uniqueFilters = new Set(this.filters);

    this.fields = [...uniqueFields]
    this.filters = [...uniqueFilters]

    let second = (this.filters && (this.fields.length) && (this.filters.length)) ? [
      `&fields=${this.fields.join(',')}`,
      `&filter=${this.filters.join(',')}`,
    ] : [];

    first = [...first, ...second];
    return first;
  }
}

export class SourceIn {
  source: string;
  query: string | undefined
}

export class SourceField {
  fields: string[];
  key: string;
  limit: number;
}
