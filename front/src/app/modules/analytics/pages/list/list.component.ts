import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {RestService} from "../../../../services/rest.service";
import {ActivatedRoute, Router} from "@angular/router";
import {PaginationServiceService} from "../../../../services/pagination-service.service";
import {SearchService} from "../../../search/search.service";
import {ShareService} from "../../../../services/share.service";
import {debounceTime, finalize, map, tap} from "rxjs/operators";
import {PageChangedEvent} from "ngx-bootstrap/pagination";
import {faBox, faPhoneAlt} from '@fortawesome/free-solid-svg-icons';
import {
  faAngleLeft,
  faGem,
  faShoppingBag,
  faAngleRight,
  faAngleDoubleLeft,
  faAngleDoubleRight
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ListComponent implements OnInit {  public cbMode: any = null;
  @Input() viewMore: boolean = true;
  public page: number = 1;
  dataTake: any;
  loading: any;

  constructor(private rest: RestService,
              private router: Router,
              public pagination: PaginationServiceService,
              private route: ActivatedRoute,
              public search: SearchService,
              private share: ShareService) {
  }

  @Input() limit: any = 15;

  faPhoneAlt = faPhoneAlt;
  faAngleDoubleLeft = faAngleDoubleLeft
  faAngleDoubleRight = faAngleDoubleRight
  faBox = faBox
  faAngleLeft = faAngleLeft
  faAngleRight = faAngleRight
  public data: any
  public source = 'analytics';
  public history: any = [
    {
      name: 'Estadística'
    }
  ]

  ngOnInit(): void {
    let fields = [
      `?fields=name,email,phone,address,tag`
    ];
    this.search.setConfig({fields, key: this.source, limit: this.limit})
    // this.route.queryParams.subscribe(
    //   params => {
    //     const {q = ''} = params
    //     this.onSrc(q)
    //   });
  }

  goToFilter = ($event, deposit: string) => {
    $event.stopPropagation();
    this.router.navigate(['/', 'products'], {queryParams: {fields: 'deposits', filter: deposit}})
  }
  /**** GLOBAL FUNCTIONS ****/

  load = (src: any, resetPagination = true) => {
    this.loading = true;
    this.data = this.search.find({
      source: this.source,
      query: src
    }, resetPagination).pipe(
      debounceTime(500),
      tap(a => this.dataTake = a.docs),
      finalize(() =>  this.loading = false),
      map((a) => a.docs)
    )
  }


  goTo = () => this.share.goTo(this.source)

  onSrc = (e) => this.load(e);

  pageChanged($event: PageChangedEvent) {
    const {page} = $event;
    this.pagination.page = page;
    this.load(undefined, false);
  }


  /**** END GLOBAL FUNCTIONS ****/



}
