import { Component, OnInit, Input, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { debounceTime, finalize, map, tap } from 'rxjs/operators';
import { SearchService } from 'src/app/modules/search/search.service';
import { PaginationServiceService } from 'src/app/services/pagination-service.service';
import { RestService } from 'src/app/services/rest.service';
import { ShareService } from 'src/app/services/share.service';
import { AnalyticsService } from '../../analytics.service';
import * as moment from 'moment';
@Component({
  selector: 'app-list-purchases',
  templateUrl: './list-purchases.component.html',
  styleUrls: ['./list-purchases.component.css']
})
export class ListPurchasesComponent implements OnInit {
  @ViewChild('dateRangePicker') rangePickerView;
  public page: number = 1;
  dataTake: any;
  loading: any;
  @Input() limit: any = 15;

  public data: any
  public source = 'analytics/purchases';
  public currency: any;
  public currencySymbol: any;
  selectRangeSelectValue: any;
  displayedColumns: string[] = ['createdAt', 'customer', 'total', 'convert'];
  rangesTime = [];
  rangeDate: any
  rangeDateDisable = true
  constructor(private rest: RestService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    public pagination: PaginationServiceService,
    private route: ActivatedRoute,
    public search: SearchService,
    public analyticsService: AnalyticsService,
    public share: ShareService) {
    this.rangesTime = analyticsService.rangesTime
  }

  ngOnInit(): void {
    console.log(this.analyticsService.rangeSelect)
    const { startDate, endDate, selectValue } = this.analyticsService.rangeSelect
    let fields = [
      `?strict=true&fields=status`,
      `&start=${startDate}&end=${endDate}`
    ];
    this.selectRangeSelectValue = selectValue;
    this.search.setConfig({ fields, key: this.source, limit: this.limit })
    if (startDate && endDate && selectValue) {
      this.onSrc(`*paid`)
    }
  }



  load = (src: any, resetPagination = true) => {
    this.loading = true;
    this.data = this.search.find({
      source: this.source,
      query: src
    }, resetPagination).pipe(
      debounceTime(500),
      tap(a => this.dataTake = a.data),
      finalize(() => this.loading = false),
      map((a) => a.data)
    )
  }


  goTo = () => this.share.goTo(this.source)

  onSrc = (e) => this.load(e);


  selectRangeSelect($event: any, source: string) {
    if ($event && $event.value === 'custom') {
      this.rangeDateDisable = false
      this.rangePickerView.toggle()
    } else {
      const value = $event.value || $event

      if (value && value[0] && value[1]) {
        const starDate = moment(value[0]).format('YYYY-MM-DD')
        const endDate = moment(value[1]).format('YYYY-MM-DD')

        if (source === 'select') {
          this.analyticsService.rangeSelect = { startDate: starDate, endDate: endDate, selectValue: $event }
          this.rangeDate = [moment(value[0]).toDate(), moment(value[1]).toDate()]
        }
        this.search.reset()

        let fields = [
          `?strict=true&fields=status`,
          `&start=${this.analyticsService.rangeSelect.startDate}&end=${this.analyticsService.rangeSelect.endDate}`
        ];

        this.search.setConfig({ fields, key: this.source, limit: this.limit })
        this.load(`*paid`)
      }
    }
  }
}
