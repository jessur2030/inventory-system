import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListComponent} from "./pages/list/list.component";
import {AddComponent} from "../add-ons/pages/add/add.component";


const routes: Routes = [
  {path: '', component: ListComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnalyticsRoutingModule { }
