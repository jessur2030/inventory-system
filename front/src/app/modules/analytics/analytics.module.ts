import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnalyticsRoutingModule } from './analytics-routing.module';
import { ListComponent } from './pages/list/list.component';
import {SharedModule} from "../shared/shared.module";
import { ChartBarComponent } from './components/chart-bar/chart-bar.component';
import {ChartsModule} from "ng2-charts";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {PaginationModule} from "ngx-bootstrap/pagination";
import {AvatarModule} from "ngx-avatar";
import {TranslateModule} from "@ngx-translate/core";
import {TooltipModule} from "ngx-bootstrap/tooltip";
import {MatTabsModule} from "@angular/material/tabs";
import { ListSellerComponent } from './components/list-seller/list-seller.component';
import {PDFReportModule} from "../../../../projects/pdfreport/src/lib/pdfreport.module";
import {MatTableModule} from "@angular/material/table";
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {NgSelectModule} from "@ng-select/ng-select";
import {FormsModule} from "@angular/forms";
import { ListPurchasesComponent } from './components/list-purchases/list-purchases.component';


@NgModule({
  declarations: [ListComponent, ChartBarComponent, ListSellerComponent, ListPurchasesComponent],
  imports: [
    CommonModule,
    AnalyticsRoutingModule,
    SharedModule,
    ChartsModule,
    FontAwesomeModule,
    PaginationModule,
    AvatarModule,
    TranslateModule,
    TooltipModule,
    MatTabsModule,
    PDFReportModule,
    MatTableModule,
    BsDatepickerModule,
    NgSelectModule,
    FormsModule
  ]
})
export class AnalyticsModule { }
