import { Component, OnInit } from '@angular/core';
import { RestService } from "../../../../services/rest.service";
import { Router } from "@angular/router";
import { ShareService } from "../../../../services/share.service";
import { faPhoneAlt, faUser, faLongArrowAltUp, faLongArrowAltDown } from '@fortawesome/free-solid-svg-icons';
import { animate, query, stagger, style, transition, trigger } from "@angular/animations";
import { AuthService } from "../../../../services/auth.service";
import { HomeService } from "../../home.service";
import { forkJoin, Observable, of } from "rxjs";
import { finalize, map, take, tap } from "rxjs/operators";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
  animations: [
    trigger('listAnimation', [
      transition('* => *', [
        query(':enter', [
          style({ opacity: 0 }),
          stagger(20, [
            animate(25, style({ opacity: 1 }))
          ])
        ], { optional: true })
      ])
    ])
  ]
})
export class HomePageComponent implements OnInit {
  public inventory: any = []
  faPhoneAlt = faPhoneAlt;
  faUser = faUser;
  faLongArrowAltUp = faLongArrowAltUp;
  faLongArrowAltDown = faLongArrowAltDown;
  private callsHttp = [];
  public loading: boolean;
  public dataResponse: any = {
    one: {
      tap: {},
      rxjs: {}
    },
    two: {
      tap: {},
      rxjs: {}
    },
    three: {
      tap: {},
      rxjs: {}
    }
  }

  private sources = []

  constructor(private rest: RestService,
    private auth: AuthService,
    public share: ShareService) {

  }

  ngOnInit(): void {
    const currentUser = this.auth.currentUser()
    this.sources = [
      {
        key: 'one',
        available: ['admin', 'manager', 'seller'].includes(currentUser?.role),
        value: `purchase?limit=10`
      },
      {
        key: 'two',
        available: ['admin', 'manager', 'seller'].includes(currentUser?.role),
        value: `products?limit=10`
      },
      {
        key: 'three',
        available: ['admin', 'manager'].includes(currentUser?.role),
        value: `inventory?limit=10`
      },
    ]

    this.loadInitial();
  }

  loadInitial = () => {
    this.loading = true;
    this.requestMultipleData()
      .pipe(finalize(() => this.loading = false))
      .subscribe(list => {
        this.dataResponse.one.rxjs = of(list[0])
        this.dataResponse.two.rxjs = of(list[1])
        this.dataResponse.three.rxjs = of(list[2])
      });

    console.log(this.dataResponse);
  }

  public requestMultipleData(): Observable<any[] | any> {
    this.callsHttp = [];
    this.sources.forEach((value,) => {
      if (value.available) {
        this.callsHttp.push(this.rest.get(value.value)
          .pipe(
            map(b => b.docs),
            tap(a => this.dataResponse[value.key] = { tap: a }),
          )
        );
      }
    })
    return forkJoin(this.callsHttp);
  }
}
