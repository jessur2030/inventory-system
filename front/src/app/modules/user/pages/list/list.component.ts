import {Component, Input, OnInit} from '@angular/core';
import {RestService} from "../../../../services/rest.service";
import {ActivatedRoute, Router} from "@angular/router";
import {faPhoneAlt, faIndustry, faUser} from '@fortawesome/free-solid-svg-icons';
import {animate, query, stagger, style, transition, trigger} from "@angular/animations";
import {ShareService} from "../../../../services/share.service";
import {PageChangedEvent} from "ngx-bootstrap/pagination";
import {PaginationServiceService} from "../../../../services/pagination-service.service";
import {faAngleLeft, faAngleRight, faAngleDoubleLeft, faAngleDoubleRight} from '@fortawesome/free-solid-svg-icons';
import {SearchService} from "../../../search/search.service";
import {debounceTime, finalize, find, map, tap} from "rxjs/operators";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  animations: [
    trigger('listAnimation', [
      transition('* => *', [
        query(':enter', [
          style({opacity: 0}),
          stagger(20, [
            animate(25, style({opacity: 1}))
          ])
        ], {optional: true})
      ])
    ])
  ]
})
export class ListComponent implements OnInit {
  public cbMode: any = null;
  @Input() viewMore: boolean = true;
  @Input() limit = 15;
  dataTake: any;
  loading: any;

  constructor(private rest: RestService,
              private router: Router,
              public pagination: PaginationServiceService,
              public search: SearchService,
              private route: ActivatedRoute,
              private share: ShareService) {
  }

  faAngleDoubleLeft = faAngleDoubleLeft
  faAngleDoubleRight = faAngleDoubleRight
  faAngleLeft = faAngleLeft
  faAngleRight = faAngleRight
  faPhoneAlt = faPhoneAlt
  faIndustry = faIndustry
  faUser = faUser
  public data: any
  public source: string = 'users'

  public history: any = [
    {
      name: 'Usuarios'
    }
  ]

  ngOnInit(): void {
    let fields = [
      `?fields=name,email,lastName,tag`
    ];
    this.search.setConfig({fields, key: this.source, limit: this.limit})
    this.route.queryParams.subscribe(
      params => {
        const {q = ''} = params
        this.onSrc(q)
      });
  }

  /**** GLOBAL FUNCTIONS ****/

  load = (src: any, resetPagination = true) => {
    this.loading = true;
    this.data = this.search.find({
      source: this.source,
      query: src
    }, resetPagination).pipe(
      debounceTime(500),
      tap(a => this.dataTake = a.docs),
      finalize(() =>  this.loading = false),
      map((a) => a.docs)
    )
  }

  goTo = () => this.share.goTo(this.source)

  onSrc = (e) => this.load(e);

  pageChanged($event: PageChangedEvent) {
    const {page} = $event;
    this.pagination.page = page;
    this.load(undefined, false);
  }

  /**** END GLOBAL FUNCTIONS ****/

}
