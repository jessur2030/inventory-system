import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PurchasesRoutingModule} from './purchases-routing.module';
import {AddComponent} from './pages/add/add.component';
import {ListComponent} from './pages/list/list.component';
import {SharedModule} from "../shared/shared.module";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {AvatarModule} from "ngx-avatar";
import {SwipeAngularListModule} from "swipe-angular-list";
import {TranslateModule} from "@ngx-translate/core";
import {PaginationModule} from "ngx-bootstrap/pagination";
import {PDFReportModule} from "../../../../projects/pdfreport/src/lib/pdfreport.module";
import {TooltipModule} from "ngx-bootstrap/tooltip";
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import { ModalEditPriceComponent } from './components/modal-edit-price/modal-edit-price.component';
import {NgxCurrencyModule} from "ngx-currency";
import {FormsModule} from "@angular/forms";
import { ModalSellerComponent } from './components/modal-seller/modal-seller.component';
import {NgSelectModule} from "@ng-select/ng-select";
import { MatTabsModule } from '@angular/material/tabs';


@NgModule({
  declarations: [AddComponent, ListComponent, ModalEditPriceComponent, ModalSellerComponent],
  exports: [
    ListComponent

  ],
  imports: [
    CommonModule,
    SwipeAngularListModule,
    BsDatepickerModule.forRoot(),
    PurchasesRoutingModule,
    SharedModule,
    FontAwesomeModule,
    AvatarModule,
    TranslateModule,
    PaginationModule,
    PDFReportModule,
    TooltipModule,
    NgxCurrencyModule,
    FormsModule,
    MatTabsModule,
    NgSelectModule
  ]
})
export class PurchasesModule {
}
