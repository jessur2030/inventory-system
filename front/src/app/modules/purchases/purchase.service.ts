import {EventEmitter, Injectable, Output} from '@angular/core';
import {RestService} from "../../services/rest.service";
import * as _ from "lodash";

@Injectable({
  providedIn: 'root'
})
export class PurchaseService {
  filterBy = '';
  @Output() changeSeller = new EventEmitter<any>();
  @Output() changePrice = new EventEmitter<any>();
  @Output() refreshData = new EventEmitter<any>();

  constructor(private rest: RestService) {
  }

  public statusPurchases = [
    {name: 'paid', value: 'paid'},
    {name: 'hold', value: 'hold'},
    {name: 'process', value: 'process'},
    {name: 'exceptional', value: 'exceptional'},
    {name: 'pre-paid', value: 'pre-paid'},
    {name: 'credit', value: 'credit'},
    {name: 'delivery-paid', value: 'delivery-paid'}
  ]

  public checkDiffPrice = (priceA, priceB) => {
    priceA = _.head(priceA)
    priceB = _.head(priceB)
    return (priceA?.amount === priceB?.amount)
  }


  public getPurchase = (id: string) => new Promise((resolve, reject) => {
      this.rest.get(`purchase/${id}`).subscribe(res => {
        resolve(res)
      }, e => reject(null))
    }
  )
}
