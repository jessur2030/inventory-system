import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEditPriceComponent } from './modal-edit-price.component';

describe('ModalEditPriceComponent', () => {
  let component: ModalEditPriceComponent;
  let fixture: ComponentFixture<ModalEditPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEditPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEditPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
