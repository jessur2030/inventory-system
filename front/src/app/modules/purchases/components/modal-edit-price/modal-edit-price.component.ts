import {Component, OnInit} from '@angular/core';
import {CurrencyMaskInputMode} from "ngx-currency";
import {ShareService} from "../../../../services/share.service";
import {BsModalRef} from "ngx-bootstrap/modal";
import {PurchaseService} from "../../purchase.service";

@Component({
  selector: 'app-modal-edit-price',
  templateUrl: './modal-edit-price.component.html',
  styleUrls: ['./modal-edit-price.component.css']
})
export class ModalEditPriceComponent implements OnInit {
  public ngxCurrencyOptions = {
    prefix: ``,
    thousands: ',',
    decimal: '.',
    allowNegative: false,
    nullable: true,
    max: 250_000_000,
    inputMode: CurrencyMaskInputMode.FINANCIAL,
  };
  priceTmp: any = 0
  public section: any
  private currency: any;
  private currencySymbol: any;

  constructor(private shared: ShareService, public bsModalRef: BsModalRef, private purchaseService: PurchaseService) {
  }

  ngOnInit(): void {
    const {currency, currencySymbol} = this.shared.getSettings();
    this.currency = currency;
    this.currencySymbol = currencySymbol;
    this.ngxCurrencyOptions = {
      ...this.ngxCurrencyOptions, ...{
        prefix: `${currency} `
      }
    };
    this.setPrice()
  }

  setPrice = () => {
    try {
      this.priceTmp = this.section.prices.find((a) => true).amount
    } catch (e) {
      this.priceTmp = 0;
    }
  }

  close = () => {
    this.section = {...this.section, ...{prices: [{amount: this.priceTmp}]}}
    this.purchaseService.changePrice.emit(this.section)
    this.bsModalRef.hide()
  }

  onFocus = (e) => {
    // console.log(this.section.prices)

  }
}
