import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OauthRoutingModule} from './oauth-routing.module';
import {LoginComponent} from './pages/login/login.component';
import {RegisterComponent} from './pages/register/register.component';
import {SharedModule} from "../shared/shared.module";
import {LottieModule} from "ngx-lottie";
import {TranslateModule} from "@ngx-translate/core";
import {ReactiveFormsModule} from "@angular/forms";
import { ForgotComponent } from './pages/forgot/forgot.component';
import { ResetComponent } from './pages/reset/reset.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import { CallbackComponent } from './pages/callback/callback.component';


@NgModule({
    declarations: [LoginComponent, RegisterComponent, ForgotComponent, ResetComponent, CallbackComponent],
    exports: [
    ],
  imports: [
    CommonModule,
    OauthRoutingModule,
    SharedModule,
    LottieModule,
    TranslateModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ]
})
export class OauthModule {
}
