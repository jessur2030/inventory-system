import {Component, OnInit} from '@angular/core';
import {AnimationItem} from "lottie-web";
import {AnimationOptions} from "ngx-lottie";
import {RestService} from "../../../../services/rest.service";
import {AuthService} from "../../../../services/auth.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ShareService} from "../../../../services/share.service";
import {faEye, faEyeSlash} from '@fortawesome/free-solid-svg-icons';
import {faGoogle, faFacebookF} from '@fortawesome/free-brands-svg-icons';
import {environment} from "../../../../../environments/environment";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  options: AnimationOptions = {
    path: '/assets/images/dashboard.json',
  };
  public form: FormGroup;
  loading: boolean;
  data: any;
  faEye = faEye
  faEyeSlash = faEyeSlash
  faGoogle = faGoogle
  faFacebookF = faFacebookF
  public showPass: boolean
  public urlOauth = {
    fb: '',
    google: ''
  }

  constructor(private rest: RestService,
              private route: ActivatedRoute,
              private router: Router,
              private auth: AuthService,
              private share: ShareService,
              private formBuilder: FormBuilder) {

  }

  ngOnInit(): void {
    this.checkErrors()
    const tenant = window.location.host.split('.')[0]
    this.urlOauth.fb = `${environment.oauth}/login-facebook?tenant=${tenant}`
    this.urlOauth.google = `${environment.oauth}/login-google?tenant=${tenant}`

    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });

    this.auth.checkSession(true, true)
      .then(a => this.router.navigate(['/']));

    this.check()

  }

  checkErrors = () => {
    this.route.queryParams.subscribe(
      params => {
        if (params.error) {
            this.share.showToast('LOGIN.ERRORS.USER_DOES_NOT_EXIST')
        }
      });
  }

  animationCreated(animationItem: AnimationItem): void {

  }

  check = () => {
    this.rest.get(`check`).subscribe(res => {
      if (!res || !res._id) {
        this.share.alert(`Error`, `Customer not found`)
      } else {
        this.data = res;
      }
    })
  }

  onSubmit = () => {
    this.loading = true;
    this.auth.login(this.form.value).then(res => {
      this.loading = false;
      this.router.navigate(['/']).then();
    }).catch(() => {
      console.log('AQUI')
      this.loading = false
    })
  };

}
