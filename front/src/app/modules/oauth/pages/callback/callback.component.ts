import {Component, OnInit} from '@angular/core';
import {RestService} from "../../../../services/rest.service";
import {ActivatedRoute} from "@angular/router";
import {AuthService} from "../../../../services/auth.service";

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.css']
})
export class CallbackComponent implements OnInit {

  constructor(private rest: RestService, private route: ActivatedRoute, private auth: AuthService) {
  }

  ngOnInit(): void {
    this.generateToken()
  }

  generateToken = () => {
    this.route.queryParams.subscribe(
      params => {
        const {token = ''} = params
        this.auth.checkSessionByToken(token)
      });
  }

}
