import {Component, NgZone, OnInit} from '@angular/core';
import {AnimationOptions} from "ngx-lottie";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {RestService} from "../../../../services/rest.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../../../services/auth.service";
import {AnimationItem} from "lottie-web";

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {

  options: AnimationOptions = {
    path: '/assets/images/dashboard.json',
  };

  optionsUnlocked: AnimationOptions = {
    path: '/assets/images/unlocked.json',
  };

  public form: FormGroup;
  loading: boolean;
  private animationMail: AnimationItem;
  public unlocked: boolean;

  constructor(private rest: RestService,
              private router: Router,
              private auth: AuthService,
              private ngZone: NgZone,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      password: ['', Validators.required],
      confirm_password: ['', [Validators.required]],
      id: ['', [Validators.required]]
    }, {
      validator: this.confirmedValidator('password', 'confirm_password')
    });
    this.auth.clear();
    this.route.params.subscribe(params => {
      this.form.patchValue({id: params.code})
    });
  }

  animationMailCreated(animationItem: AnimationItem): void {
    this.animationMail = animationItem;
  }

  stop(): void {
    this.ngZone.runOutsideAngular(() => {
      this.animationMail.setSegment(136, 137);
      // this.animationMail.pause();
    });
  }

  confirmedValidator = (controlName: string, matchingControlName: string) => {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
        return;
      }
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({confirmedValidator: true});
      } else {
        matchingControl.setErrors(null);
      }
    }
  }

  animationCreated(animationItem: AnimationItem): void {
    console.log(animationItem);
  }

  onSubmit = () => {
    const {id, password} = this.form.value;
    this.loading = true;
    this.rest.post('reset', {
      password,
      id
    }).subscribe(res => {
      this.loading = false;
      this.unlocked = true;
      // this.router.navigate(['/']).then();
    }, error => {
      this.loading = false;
    })
  };

}
