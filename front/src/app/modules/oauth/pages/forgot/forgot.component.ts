import {Component, NgZone, OnInit} from '@angular/core';
import {AnimationOptions} from "ngx-lottie";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {RestService} from "../../../../services/rest.service";
import {Router} from "@angular/router";
import {AuthService} from "../../../../services/auth.service";
import {AnimationItem} from "lottie-web";

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {
  options: AnimationOptions = {
    path: '/assets/images/dashboard.json',
  };
  optionsMail: AnimationOptions = {
    path: '/assets/images/send-mail.json',
  };
  public form: FormGroup;
  loading: boolean;
  public send: boolean = false;
  private animationMail: AnimationItem;

  constructor(private rest: RestService,
              private router: Router,
              private auth: AuthService,
              private ngZone: NgZone,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });


  }

  animationCreated(animationItem: AnimationItem): void {
    console.log(animationItem);
  }

  animationMailCreated(animationItem: AnimationItem): void {
    this.animationMail = animationItem;
  }

  stop(): void {
    this.ngZone.runOutsideAngular(() => {
      this.animationMail.setSegment(51, 52);
      // this.animationMail.pause();
    });
  }

  onSubmit = () => {
    this.loading = true;
    this.rest.post('forgot', this.form.value).subscribe(res => {
      this.loading = false;
      this.send = true;
      // this.router.navigate(['/']).then();
    }, error => {
      this.loading = false
    });
  };

}
