import { Component, Inject, OnInit } from '@angular/core';
import { AnimationItem } from "lottie-web";
import { AnimationOptions } from "ngx-lottie";
import { RestService } from "../../../../services/rest.service";
import { AuthService } from "../../../../services/auth.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ShareService } from "../../../../services/share.service";
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { faGoogle, faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { environment } from "../../../../../environments/environment";
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  options: AnimationOptions = {
    path: '/assets/images/dashboard.json',
  };
  public form: FormGroup;
  loading: boolean;
  data: any;
  faEye = faEye
  faEyeSlash = faEyeSlash
  public showPass: boolean
  public domain: string = null;
  public tenant: any = null;


  constructor(private rest: RestService,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    @Inject(DOCUMENT) private document: Document,
    private share: ShareService,
    private formBuilder: FormBuilder) {

  }

  ngOnInit(): void {
    const { host } = this.document.location;
    this.domain = host;
    const tenant = window.location.host.split('.')[0]
    this.domain = this.domain.replace(`${tenant}.`, '')
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      tenant: [tenant, Validators.required],
      password: ['', Validators.required],
    });
    this.checkErrors()

    // SI esta tenant detectar si es valido

  }

  checkErrors = () => {
    this.route.queryParams.subscribe(
      params => {
        this.form.patchValue(params)
        if (params.error) {
          this.share.showToast('LOGIN.ERRORS.USER_DOES_NOT_EXIST')
        }
      });
  }

  animationCreated(animationItem: AnimationItem): void {

  }

  // check = () => {
  //   this.rest.get(`check`).subscribe(res => {
  //     if (!res || !res._id) {
  //       this.share.alert(`Error`, `Customer not found`)
  //     } else {
  //       this.data = res;
  //     }
  //   })
  // }

  onSubmit = () => {
    this.loading = true;
    this.auth.register(this.form.value).then(res => {
      this.loading = false;
      setTimeout(() => this.router.navigate(['/']).then(), 150)
    }).catch(() => {
      this.loading = false
    })
  };

}
