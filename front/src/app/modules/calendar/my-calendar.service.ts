import {Injectable} from '@angular/core';
import {ModalBarCodesComponent} from "../../components/modal-bar-codes/modal-bar-codes.component";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {ModalDetailCalendarComponent} from "./components/modal-detail-calendar/modal-detail-calendar.component";

@Injectable({
  providedIn: 'root'
})
export class MyCalendarService {
  bsModalRef: BsModalRef;

  constructor(private modalService: BsModalService) {
  }

  public openModalCalendar = (data) => {
    console.log(data)
    const initialState: any = {
      data
    };
    this.bsModalRef = this.modalService.show(
      ModalDetailCalendarComponent,
      Object.assign({initialState}, {
        class: 'modal-light-plan',
        ignoreBackdropClick: false
      })
    );
  }
}
