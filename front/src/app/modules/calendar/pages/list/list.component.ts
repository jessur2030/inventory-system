import {Component, Input, OnInit} from '@angular/core';
import {ShareService} from "../../../../services/share.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @Input() mode: string = 'page'
  @Input() title: any = false;
  @Input() limit: any = 15;
  @Input() viewMore: boolean = true;
  public source = 'calendar';
  public history: any = [
    {
      name: 'Calendario'
    }
  ]

  constructor(public share: ShareService) {
  }

  ngOnInit(): void {
  }

}
