import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'randomAvatar'
})
export class RandomAvatarPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    return `${value}/${this.random()}`;
  }

  public random = () => {
    const myArray = ["1.jpg", "2.jpg", "3.jpg", "4.jpg", "5.jpg", "6.jpg", "7.jpg", "8.jpg"];
    return myArray[Math.floor(Math.random() * myArray.length)];
  }

}
