import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'checkArray'
})
export class CehckArrayPipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
    try {
      return {
        check: Array.isArray(value),
        length: value.length
      }
    } catch (e) {
      return {
        check: false,
        length: 0
      }
    }
  }

}
