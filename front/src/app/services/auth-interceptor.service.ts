import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CookieService} from "ngx-cookie-service";
import * as _ from 'lodash'

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private cookieService: CookieService) {
  }

  // @ts-ignore
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string = this.cookieService.get('session')
    const userReferred: string = this.cookieService.get('userReferred');
    let request = req;
    let urlPath: any = req?.url;
    console.log('----->', urlPath)
    urlPath = urlPath.split('/');
    if (userReferred && _.indexOf(urlPath, 'register') > -1) {
      if (req.method.toLowerCase() === 'post') {
        if (req.body instanceof FormData) {
          req = req.clone({
            body: req.body.append('userReferred', userReferred),
          });
        } else {
          const foo: any = {};
          foo.userReferred = userReferred;
          req = req.clone({
            body: {...req.body, ...foo},
          });
        }
      }
      request = req;
    }

    if (token) {
      request = req.clone({
        setHeaders: {
          authorization: `Bearer ${token}`
        }
      });
    }


    return next.handle(request);
  }

}
