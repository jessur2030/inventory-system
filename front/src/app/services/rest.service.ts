import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {catchError} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {ShareService} from './share.service';
import {TranslateService} from '@ngx-translate/core';
import {Toaster} from "ngx-toast-notifications";

@Injectable({
  providedIn: 'root'
})
export class RestService {
  @Output() catchError = new EventEmitter<any>();
  public headers: HttpHeaders;
  public readonly url: string = environment.api;

  constructor(public http: HttpClient, private router: Router, private sharedService: ShareService,
              private cookieService: CookieService,
              private toaster: Toaster,
    private translateService: TranslateService
  ) {


  }

  clearSession = () => {
    this.cookieService.delete('session', ' / ');
    this.cookieService.delete('user', ' / ');
    this.router.navigate(['/', 'list']);
  };

  parseHeader = (custom: any = null) => {
    let header = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    };
    if (custom) {
      header = custom
    }
    return new HttpHeaders(header);
  };


  cookieCheck = () => {

  };

  localCheck = () => {

  };

  alertError = () => {

  };

  handleError = (code = 401, message = '', e: any = {}) => {
    try {
      this.translateService.get(`ERRORS.${message}`).subscribe(res => {
        if (typeof res === 'string') {
          this.translateService.get(`ERRORS.UNDEFINED`).subscribe(r => {
            this.toaster.open({
              text: r.TEXT,
              caption: r.TITLE,
            });
          })
        } else {
          this.toaster.open({
            text: res.TEXT,
            caption: res.TITLE,
          });
        }
      })
    } catch (e) {
      return 422;
    }
  }


  post(path = '', body = {}, toast = true, header: any = null): Observable<any> {
    try {
      this.sharedService.loading.emit(true)
      return this.http.post(`${this.url}/${path}`, body,
        {headers: this.parseHeader(header)})
        .pipe(
          catchError((e: any) => {
            this.handleError(e.status, e.error.errors.msg ? e.error.errors.msg : e.statusText, e.error);
            return throwError(e);
          })
        );
    } catch (e) {
      this.sharedService.loading.emit(false)
    }
  }

  patch(path = '', body = {}, toast = true): Observable<any> {
    try {
      this.sharedService.loading.emit(true)
      return this.http.patch(`${this.url}/${path}`, body, {headers: this.parseHeader()})
        .pipe(
          catchError((e: any) => {
            this.handleError(e.status, e.error.errors.msg ? e.error.errors.msg : e.statusText, e.error);
            return throwError(e);
          })
        );
    } catch (e) {
      this.sharedService.loading.emit(false)
    }
  }

  get(path = '', toast = true, customHeader = null): Observable<any> {
    try {
      return this.http.get(`${this.url}/${path}`, {headers: this.parseHeader(customHeader)})
        .pipe(
          catchError((e: any) => {
            this.handleError(e.status, e.error.errors.msg ? e.error.errors.msg : e.statusText, e.error);
            return throwError(e);
          })
        );
    } catch (e) {

    }
  }

  delete(path = '', toast = true): Observable<any> {
    try {
      return this.http.delete(`${this.url}/${path}`, {headers: this.parseHeader()})
        .pipe(
          catchError((e: any) => {
            this.handleError(e.status, e.error.errors.msg ? e.error.errors.msg : e.statusText, e.error);
            return throwError(e);
          })
        );
    } catch (e) {

    }
  }

  /**
   * Services for plugins
   */

  getEvents(path = '', toast = true): Observable<any> {
    try {
      return this.http.get(`${this.url}/plugins/${path}`, {headers: this.parseHeader()})
        .pipe(
          catchError((e: any) => {
            this.handleError(e.status, e.error.errors.msg ? e.error.errors.msg : e.statusText, e.error);
            return throwError(e);
          })
        );
    } catch (e) {
      return null;
    }
  };

  postEvents(path = '', body = {}, toast = true, header: any = null): Observable<any> {
    try {
      this.sharedService.loading.emit(true)
      return this.http.post(`${this.url}/plugins/${path}`, body,
        {headers: this.parseHeader(header)})
        .pipe(
          catchError((e: any) => {
            this.handleError(e.status, e.error.errors.msg ? e.error.errors.msg : e.statusText, e.error);
            return throwError(e);
          })
        );
    } catch (e) {
      this.sharedService.loading.emit(false)
    }
  }

  postEventsPublic(path = '', body = {}, toast = true, header: any = null): Observable<any> {
    try {
      this.sharedService.loading.emit(true)
      return this.http.post(`${this.url}/plugins/public/${path}`, body,
        {headers: {}})
        .pipe(
          catchError((e: any) => {
            this.handleError(e.status, e.error.errors.msg ? e.error.errors.msg : e.statusText, e.error);
            return throwError(e);
          })
        );
    } catch (e) {
      this.sharedService.loading.emit(false)
    }
  }
}
