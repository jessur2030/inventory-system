import {Injectable} from '@angular/core';
import {Router} from "@angular/router";
import {environment} from "../../environments/environment";
import {CookieService} from "ngx-cookie-service";
import {RestService} from "./rest.service";
import {ShareService} from "./share.service";
import {ModalWizardComponent} from "../components/modal-wizard/modal-wizard.component";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {LocalStorageService} from "ngx-localstorage";
import {TranslateService} from "@ngx-translate/core";
import {CurrenciesDataService} from "./currencies-data.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  bsModalRef: BsModalRef;

  constructor(private rest: RestService,
              private router: Router,
              private modalService: BsModalService,
              private share: ShareService,
              private translate: TranslateService,
              private storageService: LocalStorageService,
              private cookieService: CookieService) {
  }

  public login = (data: any) => new Promise((resolve, reject) => {
    this.rest.post(`login`,
      data)
      .subscribe(res => {
        this.setterSettings(res)
        resolve(res);
      }, error => {
        reject(error);
      });
  });

  public register = (data: any) => new Promise((resolve, reject) => {
    this.rest.post(`register`,
      data)
      .subscribe(res => {
        this.setterSettings(res)
        resolve(res);
      }, error => {
        reject(error);
      });
  });

  public clear = () => {
    this.cookieService.delete('session', '/');
    this.cookieService.delete('settings', '/');
    this.cookieService.delete('user', '/');
  }

  public updateUser = (key, value) => {
    try {
      const user = JSON.parse(this.cookieService.get('user'));
      user[key] = value;
      this.cookieService.set(
        'user',
        JSON.stringify(user),
        environment.daysTokenExpire,
        '/');
    } catch (e) {
      return null;
    }
  };

  public logout = () => new Promise((resolve, reject) => {
    try {
      this.clear();
      resolve(true);
    } catch (e) {
      reject(false);
    }
  });

  public openWizard = (data: any = {}) => {
    const initialState: any = {
      section: data
    };
    this.bsModalRef = this.modalService.show(
      ModalWizardComponent,
      Object.assign({initialState}, {
        class: 'modal-wizard',
        ignoreBackdropClick: false
      })
    );
  }

  checkSessionByToken = (token = '') => {
    try {
      this.cookieService.set(
        'session',
        token,
        environment.daysTokenExpire,
        '/');
      this.rest.get(`token`, true, {ignoreLoadingBar: ''}).subscribe(res => {
        this.setterSettings(res)
        this.router.navigate(['/'])
      }, err => {
        this.clear();
        this.redirectLogin();
      })
    } catch (e) {
      this.clear();
      this.redirectLogin();
    }
  }


  checkSession = (verify = false, redirect = true, extra: any = {}) => {
    return new Promise((resolve, reject) => {
        if (this.cookieService.check('session')) {
          this.rest.get(`token`, true, {ignoreLoadingBar: ''}).subscribe(res => {
            this.translate.use(res.settings.language || 'es-ES')
            if (res.user && (res.user.role === 'admin') &&
              (!res.settings || !res.settings.currency || !res.settings.logo ||
                !res.settings.currencySymbol || !res.settings.name)) {
              this.openWizard()
            }
            if (res.parentAccount && res.parentAccount.status) {
              this.share.limitAccount.emit(res.parentAccount)
            }
            this.setterSettings(res)
            reject(false)
          }, error => {
            this.clear();
            this.redirectLogin();
          })
          resolve(true);
        } else {
          redirect ? this.redirectLogin() : null;
          reject(false);
        }
      }
    );
  };

  checkTenant = () => {
    return new Promise((resolve, reject) => {
      this.rest.get(`check`).subscribe(res => {
        // if (res != null) this.redirectLogin()
        resolve(!!res)
      }, error => {
        resolve(false)
      })
    })
  }

  setterSettings = (res) => {
    this.cookieService.set(
      'session',
      res.session,
      environment.daysTokenExpire,
      '/');
    this.cookieService.set(
      'user',
      JSON.stringify(res.user),
      environment.daysTokenExpire,
      '/');
    this.cookieService.set(
      'settings',
      JSON.stringify(res.settings),
      environment.daysTokenExpire,
      '/');
    this.storageService.set(
      'plugins', JSON.stringify(res.plugins));
    this.share.updateTitle(`${res.settings?.name}${environment.title}`)
  }

  currentUser = () => {
    try {
      return (this.cookieService.get('user')) ? JSON.parse(this.cookieService.get('user')) : null;
    } catch (e) {
      return null
    }
  }

  redirectLogin = () => {
    this.router.navigate(['/', 'oauth', 'login']);
  }
}
