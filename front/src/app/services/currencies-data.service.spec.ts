import { TestBed } from '@angular/core/testing';

import { CurrenciesDataService } from './currencies-data.service';

describe('CurrenciesDataService', () => {
  let service: CurrenciesDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CurrenciesDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
