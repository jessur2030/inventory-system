import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  public open: boolean
  public count: 1;

  constructor() {
  }
}
