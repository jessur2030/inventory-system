import { __decorate } from "tslib";
import { Component } from '@angular/core';
var NgxCopilotComponent = /** @class */ (function () {
    function NgxCopilotComponent() {
    }
    NgxCopilotComponent.prototype.ngOnInit = function () {
    };
    NgxCopilotComponent = __decorate([
        Component({
            selector: 'lib-ngx-copilot',
            template: "\n    <p>\n      ngx-copilot works!\n    </p>\n  "
        })
    ], NgxCopilotComponent);
    return NgxCopilotComponent;
}());
export { NgxCopilotComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWNvcGlsb3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWNvcGlsb3QvIiwic291cmNlcyI6WyJsaWIvbmd4LWNvcGlsb3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBWWxEO0lBRUU7SUFBZ0IsQ0FBQztJQUVqQixzQ0FBUSxHQUFSO0lBQ0EsQ0FBQztJQUxVLG1CQUFtQjtRQVYvQixTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsaUJBQWlCO1lBQzNCLFFBQVEsRUFBRSxtREFJVDtTQUdGLENBQUM7T0FDVyxtQkFBbUIsQ0FPL0I7SUFBRCwwQkFBQztDQUFBLEFBUEQsSUFPQztTQVBZLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2xpYi1uZ3gtY29waWxvdCcsXG4gIHRlbXBsYXRlOiBgXG4gICAgPHA+XG4gICAgICBuZ3gtY29waWxvdCB3b3JrcyFcbiAgICA8L3A+XG4gIGAsXG4gIHN0eWxlczogW1xuICBdXG59KVxuZXhwb3J0IGNsYXNzIE5neENvcGlsb3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gIH1cblxufVxuIl19