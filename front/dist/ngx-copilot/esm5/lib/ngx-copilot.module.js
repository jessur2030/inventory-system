import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NgxCopilotComponent } from './ngx-copilot.component';
import { CopilotDirective } from './copilot.directive';
import { NgxWrapperCopilotComponent } from './ngx-wrapper-copilot/ngx-wrapper-copilot.component';
import { CommonModule } from "@angular/common";
var NgxCopilotModule = /** @class */ (function () {
    function NgxCopilotModule() {
    }
    NgxCopilotModule = __decorate([
        NgModule({
            declarations: [NgxCopilotComponent, CopilotDirective, NgxWrapperCopilotComponent],
            imports: [
                CommonModule
            ],
            exports: [NgxCopilotComponent, CopilotDirective, NgxWrapperCopilotComponent]
        })
    ], NgxCopilotModule);
    return NgxCopilotModule;
}());
export { NgxCopilotModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWNvcGlsb3QubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWNvcGlsb3QvIiwic291cmNlcyI6WyJsaWIvbmd4LWNvcGlsb3QubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQzlELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLHFEQUFxRCxDQUFDO0FBQ2pHLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQVc3QztJQUFBO0lBQWdDLENBQUM7SUFBcEIsZ0JBQWdCO1FBUDVCLFFBQVEsQ0FBQztZQUNSLFlBQVksRUFBRSxDQUFDLG1CQUFtQixFQUFFLGdCQUFnQixFQUFFLDBCQUEwQixDQUFDO1lBQ2pGLE9BQU8sRUFBRTtnQkFDUCxZQUFZO2FBQ2I7WUFDRCxPQUFPLEVBQUUsQ0FBQyxtQkFBbUIsRUFBRSxnQkFBZ0IsRUFBRSwwQkFBMEIsQ0FBQztTQUM3RSxDQUFDO09BQ1csZ0JBQWdCLENBQUk7SUFBRCx1QkFBQztDQUFBLEFBQWpDLElBQWlDO1NBQXBCLGdCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOZ3hDb3BpbG90Q29tcG9uZW50IH0gZnJvbSAnLi9uZ3gtY29waWxvdC5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ29waWxvdERpcmVjdGl2ZSB9IGZyb20gJy4vY29waWxvdC5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgTmd4V3JhcHBlckNvcGlsb3RDb21wb25lbnQgfSBmcm9tICcuL25neC13cmFwcGVyLWNvcGlsb3Qvbmd4LXdyYXBwZXItY29waWxvdC5jb21wb25lbnQnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gXCJAYW5ndWxhci9jb21tb25cIjtcblxuXG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW05neENvcGlsb3RDb21wb25lbnQsIENvcGlsb3REaXJlY3RpdmUsIE5neFdyYXBwZXJDb3BpbG90Q29tcG9uZW50XSxcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZVxuICBdLFxuICBleHBvcnRzOiBbTmd4Q29waWxvdENvbXBvbmVudCwgQ29waWxvdERpcmVjdGl2ZSwgTmd4V3JhcHBlckNvcGlsb3RDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIE5neENvcGlsb3RNb2R1bGUgeyB9XG4iXX0=