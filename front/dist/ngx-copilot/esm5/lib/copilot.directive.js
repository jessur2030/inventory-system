import { __decorate } from "tslib";
import { AfterViewInit, Directive, ElementRef, Input, OnInit, TemplateRef } from '@angular/core';
import { NgxCopilotService } from "./ngx-copilot.service";
var CopilotDirective = /** @class */ (function () {
    function CopilotDirective(service, elem) {
        this.service = service;
        this.elem = elem;
        this.mode = 'vertical';
        this.overviewcolor = 'false';
        this.animatedEffect = '';
    }
    CopilotDirective.prototype.ngOnInit = function () {
    };
    CopilotDirective.prototype.ngAfterViewInit = function () {
        if (this.template) {
            this.elem.nativeElement.classList.add('ngx-copilot-init');
            if (this.step) {
                this.elem.nativeElement.dataset.step = this.step;
                this.elem.nativeElement.dataset.comode = this.mode;
                this.elem.nativeElement.dataset.overviewcolor = this.overviewcolor;
                this.elem.nativeElement.dataset.animatedEffect = this.animatedEffect;
                this.service.template.emit({
                    step: this.step,
                    template: this.template,
                    mode: this.mode,
                    overviewcolor: this.overviewcolor,
                    animatedEffect: this.animatedEffect
                });
            }
        }
    };
    CopilotDirective.ctorParameters = function () { return [
        { type: NgxCopilotService },
        { type: ElementRef }
    ]; };
    __decorate([
        Input('copilot-step')
    ], CopilotDirective.prototype, "step", void 0);
    __decorate([
        Input('copilot-template')
    ], CopilotDirective.prototype, "template", void 0);
    __decorate([
        Input('copilot-mode')
    ], CopilotDirective.prototype, "mode", void 0);
    __decorate([
        Input('copilot-color')
    ], CopilotDirective.prototype, "overviewcolor", void 0);
    __decorate([
        Input('copilot-class')
    ], CopilotDirective.prototype, "animatedEffect", void 0);
    CopilotDirective = __decorate([
        Directive({
            selector: '[copilot]'
        })
    ], CopilotDirective);
    return CopilotDirective;
}());
export { CopilotDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29waWxvdC5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtY29waWxvdC8iLCJzb3VyY2VzIjpbImxpYi9jb3BpbG90LmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUNMLGFBQWEsRUFDYixTQUFTLEVBQ1QsVUFBVSxFQUNWLEtBQUssRUFDTCxNQUFNLEVBQ04sV0FBVyxFQUNaLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBSzFEO0lBT0UsMEJBQW9CLE9BQTBCLEVBQVUsSUFBZ0I7UUFBcEQsWUFBTyxHQUFQLE9BQU8sQ0FBbUI7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFZO1FBSmpELFNBQUksR0FBRyxVQUFVLENBQUM7UUFDakIsa0JBQWEsR0FBRyxPQUFPLENBQUM7UUFDeEIsbUJBQWMsR0FBRyxFQUFFLENBQUM7SUFJNUMsQ0FBQztJQUVELG1DQUFRLEdBQVI7SUFFQSxDQUFDO0lBRUQsMENBQWUsR0FBZjtRQUVFLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFDMUQsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO2dCQUNiLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDakQsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUNuRCxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7Z0JBQ25FLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQztnQkFDckUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO29CQUN6QixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7b0JBQ2YsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO29CQUN2QixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7b0JBQ2YsYUFBYSxFQUFFLElBQUksQ0FBQyxhQUFhO29CQUNqQyxjQUFjLEVBQUUsSUFBSSxDQUFDLGNBQWM7aUJBQ3BDLENBQUMsQ0FBQTthQUNIO1NBQ0Y7SUFDSCxDQUFDOztnQkExQjRCLGlCQUFpQjtnQkFBZ0IsVUFBVTs7SUFOakQ7UUFBdEIsS0FBSyxDQUFDLGNBQWMsQ0FBQztrREFBVztJQUNOO1FBQTFCLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQztzREFBNEI7SUFDL0I7UUFBdEIsS0FBSyxDQUFDLGNBQWMsQ0FBQztrREFBbUI7SUFDakI7UUFBdkIsS0FBSyxDQUFDLGVBQWUsQ0FBQzsyREFBeUI7SUFDeEI7UUFBdkIsS0FBSyxDQUFDLGVBQWUsQ0FBQzs0REFBcUI7SUFMakMsZ0JBQWdCO1FBSDVCLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxXQUFXO1NBQ3RCLENBQUM7T0FDVyxnQkFBZ0IsQ0FrQzVCO0lBQUQsdUJBQUM7Q0FBQSxBQWxDRCxJQWtDQztTQWxDWSxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIEFmdGVyVmlld0luaXQsXHJcbiAgRGlyZWN0aXZlLFxyXG4gIEVsZW1lbnRSZWYsXHJcbiAgSW5wdXQsXHJcbiAgT25Jbml0LFxyXG4gIFRlbXBsYXRlUmVmXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5neENvcGlsb3RTZXJ2aWNlIH0gZnJvbSBcIi4vbmd4LWNvcGlsb3Quc2VydmljZVwiO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgc2VsZWN0b3I6ICdbY29waWxvdF0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb3BpbG90RGlyZWN0aXZlIGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0IHtcclxuICBASW5wdXQoJ2NvcGlsb3Qtc3RlcCcpIHN0ZXA6IGFueTtcclxuICBASW5wdXQoJ2NvcGlsb3QtdGVtcGxhdGUnKSB0ZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PjtcclxuICBASW5wdXQoJ2NvcGlsb3QtbW9kZScpIG1vZGUgPSAndmVydGljYWwnO1xyXG4gIEBJbnB1dCgnY29waWxvdC1jb2xvcicpIG92ZXJ2aWV3Y29sb3IgPSAnZmFsc2UnO1xyXG4gIEBJbnB1dCgnY29waWxvdC1jbGFzcycpIGFuaW1hdGVkRWZmZWN0ID0gJyc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgc2VydmljZTogTmd4Q29waWxvdFNlcnZpY2UsIHByaXZhdGUgZWxlbTogRWxlbWVudFJlZikge1xyXG5cclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG5cclxuICB9XHJcblxyXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpOiB2b2lkIHtcclxuXHJcbiAgICBpZiAodGhpcy50ZW1wbGF0ZSkge1xyXG4gICAgICB0aGlzLmVsZW0ubmF0aXZlRWxlbWVudC5jbGFzc0xpc3QuYWRkKCduZ3gtY29waWxvdC1pbml0Jyk7XHJcbiAgICAgIGlmICh0aGlzLnN0ZXApIHtcclxuICAgICAgICB0aGlzLmVsZW0ubmF0aXZlRWxlbWVudC5kYXRhc2V0LnN0ZXAgPSB0aGlzLnN0ZXA7XHJcbiAgICAgICAgdGhpcy5lbGVtLm5hdGl2ZUVsZW1lbnQuZGF0YXNldC5jb21vZGUgPSB0aGlzLm1vZGU7XHJcbiAgICAgICAgdGhpcy5lbGVtLm5hdGl2ZUVsZW1lbnQuZGF0YXNldC5vdmVydmlld2NvbG9yID0gdGhpcy5vdmVydmlld2NvbG9yO1xyXG4gICAgICAgIHRoaXMuZWxlbS5uYXRpdmVFbGVtZW50LmRhdGFzZXQuYW5pbWF0ZWRFZmZlY3QgPSB0aGlzLmFuaW1hdGVkRWZmZWN0O1xyXG4gICAgICAgIHRoaXMuc2VydmljZS50ZW1wbGF0ZS5lbWl0KHtcclxuICAgICAgICAgIHN0ZXA6IHRoaXMuc3RlcCxcclxuICAgICAgICAgIHRlbXBsYXRlOiB0aGlzLnRlbXBsYXRlLFxyXG4gICAgICAgICAgbW9kZTogdGhpcy5tb2RlLFxyXG4gICAgICAgICAgb3ZlcnZpZXdjb2xvcjogdGhpcy5vdmVydmlld2NvbG9yLFxyXG4gICAgICAgICAgYW5pbWF0ZWRFZmZlY3Q6IHRoaXMuYW5pbWF0ZWRFZmZlY3RcclxuICAgICAgICB9KVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==