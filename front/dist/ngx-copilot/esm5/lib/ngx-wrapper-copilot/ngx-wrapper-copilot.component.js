import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { NgxCopilotService } from "../ngx-copilot.service";
var NgxWrapperCopilotComponent = /** @class */ (function () {
    function NgxWrapperCopilotComponent(service) {
        this.service = service;
        this.viewTemplate = [];
    }
    NgxWrapperCopilotComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscriber = this.service.template.subscribe(function (a) {
            if (!_this.service.checkIfExistDom(a.step)) {
                _this.viewTemplate.push(a);
            }
        });
        this.service.nextEmit.subscribe(function (next) { return _this.service.find(next); });
    };
    NgxWrapperCopilotComponent.prototype.ngOnDestroy = function () {
        try {
            this.subscriber.unsubscribe();
            this.viewTemplate = [];
        }
        catch (e) {
            this.viewTemplate = [];
        }
    };
    NgxWrapperCopilotComponent.ctorParameters = function () { return [
        { type: NgxCopilotService }
    ]; };
    NgxWrapperCopilotComponent = __decorate([
        Component({
            selector: 'ngx-wrapper-copilot',
            template: "<div class=\"ngx-wrapper-overview\">\r\n  <ng-container *ngFor=\"let te  of viewTemplate\" >\r\n    <div [attr.step]=\"te?.step\"\r\n         class=\"{{(!te?.animatedEffect?.length) ? 'animate__animated animate__bounceIn': te?.animatedEffect}}\r\n     copilot-view copilot-view-step-{{te?.step}}\">\r\n\r\n      <ng-container *ngTemplateOutlet=\"te?.template\"></ng-container>\r\n    </div>\r\n  </ng-container>\r\n</div>\r\n",
            styles: [".ngx-wrapper-overview{display:none;position:fixed;z-index:9999;width:100%;height:100%;top:0;left:0}.ngx-wrapper-overview .copilot-view{position:fixed;max-width:300px;max-height:-webkit-min-content;max-height:-moz-min-content;max-height:min-content;padding:10px;border-radius:10px;background-color:#6b66ff;color:#fff;display:none}.ngx-copilot-pulse{display:block;border-radius:50%;background:#cca92c;cursor:pointer;box-shadow:0 0 0 rgba(204,169,44,.8);animation:2s infinite ngx-copilot-pulse}.ngx-copilot-pulse:hover{animation:none}@keyframes ngx-copilot-pulse{0%{box-shadow:0 0 0 0 rgba(204,169,44,.4)}70%{box-shadow:0 0 0 10px rgba(204,169,44,0)}100%{box-shadow:0 0 0 0 rgba(204,169,44,0)}}"]
        })
    ], NgxWrapperCopilotComponent);
    return NgxWrapperCopilotComponent;
}());
export { NgxWrapperCopilotComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LXdyYXBwZXItY29waWxvdC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtY29waWxvdC8iLCJzb3VyY2VzIjpbImxpYi9uZ3gtd3JhcHBlci1jb3BpbG90L25neC13cmFwcGVyLWNvcGlsb3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFvQixNQUFNLGVBQWUsQ0FBQztBQUMzRCxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSx3QkFBd0IsQ0FBQztBQVF6RDtJQUtFLG9DQUFvQixPQUEwQjtRQUExQixZQUFPLEdBQVAsT0FBTyxDQUFtQjtRQUp2QyxpQkFBWSxHQUFHLEVBQUUsQ0FBQztJQU16QixDQUFDO0lBRUQsNkNBQVEsR0FBUjtRQUFBLGlCQVFDO1FBUEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsVUFBQSxDQUFDO1lBQ2pELElBQUksQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ3pDLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzNCO1FBRUgsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBdkIsQ0FBdUIsQ0FBQyxDQUFBO0lBQ2xFLENBQUM7SUFFRCxnREFBVyxHQUFYO1FBQ0UsSUFBSTtZQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUIsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7U0FDeEI7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNWLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1NBQ3hCO0lBQ0gsQ0FBQzs7Z0JBckI0QixpQkFBaUI7O0lBTG5DLDBCQUEwQjtRQUx0QyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUscUJBQXFCO1lBQy9CLHFiQUFtRDs7U0FFcEQsQ0FBQztPQUNXLDBCQUEwQixDQTZCdEM7SUFBRCxpQ0FBQztDQUFBLEFBN0JELElBNkJDO1NBN0JZLDBCQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkRlc3Ryb3ksIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7Tmd4Q29waWxvdFNlcnZpY2V9IGZyb20gXCIuLi9uZ3gtY29waWxvdC5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7U3Vic2NyaXB0aW9ufSBmcm9tIFwicnhqc1wiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICduZ3gtd3JhcHBlci1jb3BpbG90JyxcclxuICB0ZW1wbGF0ZVVybDogJy4vbmd4LXdyYXBwZXItY29waWxvdC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vbmd4LXdyYXBwZXItY29waWxvdC5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIE5neFdyYXBwZXJDb3BpbG90Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG4gIHB1YmxpYyB2aWV3VGVtcGxhdGUgPSBbXTtcclxuICBwdWJsaWMgcXVldWU6IFtdO1xyXG4gIHByaXZhdGUgc3Vic2NyaWJlcjogU3Vic2NyaXB0aW9uO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNlcnZpY2U6IE5neENvcGlsb3RTZXJ2aWNlKSB7XHJcblxyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLnN1YnNjcmliZXIgPSB0aGlzLnNlcnZpY2UudGVtcGxhdGUuc3Vic2NyaWJlKGEgPT4ge1xyXG4gICAgICBpZiAoIXRoaXMuc2VydmljZS5jaGVja0lmRXhpc3REb20oYS5zdGVwKSkge1xyXG4gICAgICAgIHRoaXMudmlld1RlbXBsYXRlLnB1c2goYSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICB9KTtcclxuICAgIHRoaXMuc2VydmljZS5uZXh0RW1pdC5zdWJzY3JpYmUobmV4dCA9PiB0aGlzLnNlcnZpY2UuZmluZChuZXh0KSlcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xyXG4gICAgdHJ5IHtcclxuICAgICAgdGhpcy5zdWJzY3JpYmVyLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgIHRoaXMudmlld1RlbXBsYXRlID0gW107XHJcbiAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgIHRoaXMudmlld1RlbXBsYXRlID0gW107XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuXHJcbn1cclxuIl19