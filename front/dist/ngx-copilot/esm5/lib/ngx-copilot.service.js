import { __decorate } from "tslib";
import { EventEmitter, Injectable, Output } from '@angular/core';
import * as i0 from "@angular/core";
var NgxCopilotService = /** @class */ (function () {
    function NgxCopilotService() {
        var _this = this;
        this.template = new EventEmitter();
        this.nextEmit = new EventEmitter();
        this.tmpColor = null;
        /**
         * Private functions
         */
        this.isInViewport = function (el) {
            if (el === void 0) { el = null; }
            var rect = el.getBoundingClientRect();
            return {
                view: (rect.top >= 0 &&
                    rect.left >= 0 &&
                    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                    rect.right <= (window.innerWidth || document.documentElement.clientWidth)),
                axis: rect
            };
        };
        this.scrollLocated = function (element) { return new Promise(function (resolve, reject) {
            var countFlag = 0;
            var init = 100;
            var options = { block: 'start', behavior: 'smooth' };
            element.scrollIntoView(options);
            var id = setInterval(function () {
                countFlag++;
                var top = _this.isInViewport(element).axis.top;
                var fullHeight = (window.innerHeight || document.documentElement.clientHeight);
                var percentageView = parseFloat(String(top * 100)) / fullHeight;
                if ((top < 1) || (countFlag > init)) {
                    clearInterval(id);
                    if (countFlag > init) {
                        reject(false);
                    }
                    else {
                        resolve(true);
                    }
                }
                else {
                    if (countFlag === 20) {
                        element.scrollIntoView(options);
                    }
                    else if ((countFlag > 5) && (countFlag < 15)) {
                        if ((percentageView > 10) && (percentageView < 80)) {
                            clearInterval(id);
                            resolve(true);
                        }
                    }
                }
            }, 100);
        }); };
        this.getParent = function () {
            try {
                _this.removeWrapper();
                var copilotsElement = document.querySelectorAll(".ngx-copilot-init"); //Elementos donde se debe hacer foco
                Array.from(copilotsElement).map(function (e) {
                    var order = e.getAttribute('data-step');
                    if (order) {
                        var el = e.getBoundingClientRect();
                        var single = document.querySelector(".copilot-view-step-" + order); // Ubicamos el template para ubicarlo donde foco
                        var top_1 = el.top, right = el.right, left = el.left, bottom = el.bottom, width = el.width;
                        single.style.marginLeft = width + "px";
                        single.style.top = top_1 + "px";
                        single.style.right = right + "px";
                        single.style.bottom = bottom + "px";
                        single.style.left = left + "px";
                    }
                });
            }
            catch (e) {
                return null;
            }
        };
        this.getTemplates = function (o) {
            if (o === void 0) { o = '1'; }
            try {
                _this.elementsDomActive = document.querySelectorAll(".copilot-view.copilot-active");
                _this.elementsDom = document.querySelectorAll(".copilot-view");
                if (!(_this.elementsDomActive.length)) {
                    _this.find(o);
                }
            }
            catch (e) {
                return null;
            }
        };
        this.checkInit = function (position) {
            if (position === void 0) { position = '1'; }
            // window.scrollTo({ top: 0});
            _this.elementsDomActive = {};
            _this.elementsDom = {};
            _this.tmpColor = null;
            setTimeout(function () {
                _this.getParent();
                _this.getTemplates(position);
            }, 60);
        };
        this.removeWrapper = function () {
            try {
                var body = document.querySelector("body");
                var html = document.querySelector("html");
                var wrapper = document.querySelector(".ngx-wrapper-overview");
                var list = document.querySelector(".copilot-view");
                var listParent = document.querySelectorAll(".ngx-copilot-init");
                body.classList.remove('ngx-copilot-active');
                Array.from(document.querySelectorAll(".copilot-view")).map(function (e) {
                    if (e && e.style) {
                        e.style.display = 'none';
                    }
                });
                Array.from(document.querySelectorAll(".ngx-copilot-pulse")).map(function (e) {
                    if (e) {
                        e.classList.remove('ngx-copilot-pulse');
                    }
                });
                Array.from(listParent).map(function (e) {
                    e.style.backgroundColor = 'initial';
                });
                Array.from(list).map(function (e) {
                    e.style.display = "none";
                });
                body.classList.remove('ngx-copilot-active');
                wrapper.style.display = 'none';
                html.style.overflow = 'auto';
                body.style.overflow = 'auto';
            }
            catch (e) {
                return null;
            }
        };
        this.removeClassByPrefix = function (el, prefix) {
            var regx = new RegExp('\\b' + prefix + '.*?\\b', 'g');
            el.className = el.className.replace(regx, '');
            return el;
        };
        this.find = function (order) {
            if (order === void 0) { order = null; }
            try {
                var wrapper_1 = document.querySelector("body");
                if (!document.querySelector(".ngx-copilot-init[data-step='" + order + "']")) {
                    var wrapperSingle = document.querySelector(".ngx-wrapper-overview");
                    wrapperSingle.style.display = 'none';
                    _this.removeClassByPrefix(wrapper_1, 'ngx-copilot-');
                }
                else {
                    Array.from(document.querySelectorAll(".copilot-view")).map(function (e) {
                        var step = e.getAttribute('step'); // Obtenemos el paso al que va el template
                        var trace = ".ngx-copilot-init[data-step='" + step + "']"; // Buscamos el element hacer focus
                        var single = document.querySelector(trace);
                        if (single) {
                            if ("" + order === step) { // Si el template con el paso y el focus son el mismo mostramos
                                var _a = single.dataset, comode_1 = _a.comode, overviewcolor_1 = _a.overviewcolor;
                                single.style.backgroundColor = '#cfceff';
                                single.classList.add('ngx-copilot-pulse');
                                wrapper_1.classList.add('ngx-copilot-active');
                                wrapper_1.classList.add("ngx-copilot-active-step-" + step);
                                /**
                                 * Fix perfomance
                                 */
                                var checkViewPort = _this.isInViewport(single);
                                if (checkViewPort.view) { // Yes in viewport
                                    _this.setZone(trace, comode_1, overviewcolor_1);
                                    e.style.display = "block";
                                }
                                else { //Must scrolled
                                    _this.scrollLocated(single).then(function () {
                                        _this.setZone(trace, comode_1, overviewcolor_1);
                                        e.style.display = "block";
                                    });
                                }
                            }
                            else {
                                single.style.backgroundColor = 'initial';
                                wrapper_1.classList.remove("ngx-copilot-active-step-" + step);
                                single.classList.remove('ngx-copilot-pulse');
                                e.style.display = "none";
                            }
                        }
                        else {
                            single.style.backgroundColor = 'initial';
                            wrapper_1.classList.remove("ngx-copilot-active-step-" + step);
                            single.classList.remove('ngx-copilot-pulse');
                            e.style.display = "none";
                        }
                    });
                }
            }
            catch (e) {
                return null;
            }
        };
        this.setZone = function (element, mode, overviewcolor) {
            if (element === void 0) { element = null; }
            if (mode === void 0) { mode = 'vertical'; }
            if (overviewcolor === void 0) { overviewcolor = 'false'; }
            try {
                var html = document.querySelector("html");
                var body = document.querySelector("body");
                var wrapper = document.querySelector(".ngx-wrapper-overview");
                html.style.overflow = 'hidden';
                body.style.overflow = 'hidden';
                wrapper.style.display = 'block';
                element = document.querySelector(element);
                var root = document.documentElement;
                var bound = element.getBoundingClientRect();
                var top_2 = bound.top, left = bound.left, right = bound.right, height = bound.height, bottom = bound.bottom, width = bound.width;
                var centralPointHeight = parseFloat(String(bottom - top_2)) / 2;
                var centralPointWidth = parseFloat(String(right - left)) / 2;
                root.style.setProperty('--zoneY', parseFloat(left + centralPointWidth) + 'px');
                root.style.setProperty('--zoneX', parseFloat(top_2 + centralPointHeight) + 'px');
                if (overviewcolor !== 'false') {
                    root.style.setProperty('--zoneColor', overviewcolor);
                }
                else {
                    _this.tmpColor = (!_this.tmpColor) ? getComputedStyle(root).getPropertyValue('--zoneColor') : _this.tmpColor;
                    root.style.setProperty('--zoneColor', _this.tmpColor);
                }
                if (mode === 'vertical') {
                    root.style.setProperty('--zoneSize', parseFloat(height) + parseFloat(String(height * 0.1)) + 'px');
                }
                else {
                    root.style.setProperty('--zoneSize', parseFloat(width) - parseFloat(String(width * 0.5)) + 'px');
                }
            }
            catch (e) {
                return null;
            }
        };
        this.next = function (data) {
            if (data === void 0) { data = null; }
            _this.nextEmit.emit(data);
        };
        this.checkIfExistDom = function (step) {
            try {
                var dom = document.querySelector(".copilot-view-step-" + step);
                return (dom);
            }
            catch (e) {
                return null;
            }
        };
    }
    NgxCopilotService.ɵprov = i0.ɵɵdefineInjectable({ factory: function NgxCopilotService_Factory() { return new NgxCopilotService(); }, token: NgxCopilotService, providedIn: "root" });
    __decorate([
        Output()
    ], NgxCopilotService.prototype, "template", void 0);
    __decorate([
        Output()
    ], NgxCopilotService.prototype, "nextEmit", void 0);
    NgxCopilotService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], NgxCopilotService);
    return NgxCopilotService;
}());
export { NgxCopilotService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWNvcGlsb3Quc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1jb3BpbG90LyIsInNvdXJjZXMiOlsibGliL25neC1jb3BpbG90LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBQyxZQUFZLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBQyxNQUFNLGVBQWUsQ0FBQzs7QUFLL0Q7SUFPRTtRQUFBLGlCQUVDO1FBUlMsYUFBUSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDbkMsYUFBUSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFHdEMsYUFBUSxHQUFHLElBQUksQ0FBQztRQU12Qjs7V0FFRztRQUVLLGlCQUFZLEdBQUcsVUFBQyxFQUFTO1lBQVQsbUJBQUEsRUFBQSxTQUFTO1lBQy9CLElBQU0sSUFBSSxHQUFHLEVBQUUsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1lBQ3hDLE9BQU87Z0JBQ0wsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDO29CQUNsQixJQUFJLENBQUMsSUFBSSxJQUFJLENBQUM7b0JBQ2QsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLElBQUksUUFBUSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUM7b0JBQzVFLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxJQUFJLFFBQVEsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQzVFLElBQUksRUFBRSxJQUFJO2FBQ1gsQ0FBQztRQUNKLENBQUMsQ0FBQztRQUVNLGtCQUFhLEdBQUcsVUFBQyxPQUFPLElBQUssT0FBQSxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQy9ELElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQztZQUNsQixJQUFNLElBQUksR0FBRyxHQUFHLENBQUM7WUFDakIsSUFBTSxPQUFPLEdBQUcsRUFBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUMsQ0FBQztZQUNyRCxPQUFPLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ2hDLElBQU0sRUFBRSxHQUFHLFdBQVcsQ0FBQztnQkFDckIsU0FBUyxFQUFFLENBQUM7Z0JBQ0wsSUFBQSwwQ0FBRyxDQUFvQztnQkFDOUMsSUFBTSxVQUFVLEdBQUcsQ0FBQyxNQUFNLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ2pGLElBQU0sY0FBYyxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDLEdBQUcsVUFBVSxDQUFDO2dCQUNsRSxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxFQUFFO29CQUNuQyxhQUFhLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQ2xCLElBQUksU0FBUyxHQUFHLElBQUksRUFBRTt3QkFDcEIsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUNmO3lCQUFNO3dCQUNMLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDZjtpQkFDRjtxQkFBTTtvQkFDTCxJQUFJLFNBQVMsS0FBSyxFQUFFLEVBQUU7d0JBQ3BCLE9BQU8sQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7cUJBQ2pDO3lCQUFNLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDLEVBQUU7d0JBQzlDLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDLEVBQUU7NEJBQ2xELGFBQWEsQ0FBQyxFQUFFLENBQUMsQ0FBQzs0QkFDbEIsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO3lCQUNmO3FCQUNGO2lCQUNGO1lBQ0gsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ1YsQ0FBQyxDQUFDLEVBNUJtQyxDQTRCbkMsQ0FBQztRQUVLLGNBQVMsR0FBRztZQUNsQixJQUFJO2dCQUNGLEtBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDckIsSUFBTSxlQUFlLEdBQUcsUUFBUSxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixDQUFRLENBQUMsQ0FBQyxvQ0FBb0M7Z0JBQ25ILEtBQUssQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUMsQ0FBTTtvQkFDckMsSUFBTSxLQUFLLEdBQUcsQ0FBQyxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDMUMsSUFBSSxLQUFLLEVBQUU7d0JBQ1QsSUFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDLHFCQUFxQixFQUFFLENBQUM7d0JBQ3JDLElBQU0sTUFBTSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsd0JBQXNCLEtBQU8sQ0FBUSxDQUFDLENBQUMsZ0RBQWdEO3dCQUN0SCxJQUFBLGNBQUcsRUFBRSxnQkFBSyxFQUFFLGNBQUksRUFBRSxrQkFBTSxFQUFFLGdCQUFLLENBQU87d0JBQzdDLE1BQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFNLEtBQUssT0FBSSxDQUFDO3dCQUN2QyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBTSxLQUFHLE9BQUksQ0FBQzt3QkFDOUIsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQU0sS0FBSyxPQUFJLENBQUM7d0JBQ2xDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFNLE1BQU0sT0FBSSxDQUFDO3dCQUNwQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksR0FBTSxJQUFJLE9BQUksQ0FBQztxQkFDakM7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDSjtZQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNWLE9BQU8sSUFBSSxDQUFDO2FBQ2I7UUFDSCxDQUFDLENBQUM7UUFFSyxpQkFBWSxHQUFHLFVBQUMsQ0FBTztZQUFQLGtCQUFBLEVBQUEsT0FBTztZQUM1QixJQUFJO2dCQUNGLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsOEJBQThCLENBQUMsQ0FBQztnQkFDbkYsS0FBSSxDQUFDLFdBQVcsR0FBRyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7Z0JBQzlELElBQUksQ0FBQyxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDcEMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDZDthQUNGO1lBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ1YsT0FBTyxJQUFJLENBQUM7YUFDYjtRQUNILENBQUMsQ0FBQztRQUVLLGNBQVMsR0FBRyxVQUFDLFFBQWM7WUFBZCx5QkFBQSxFQUFBLGNBQWM7WUFDaEMsOEJBQThCO1lBQzlCLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7WUFDNUIsS0FBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7WUFDdEIsS0FBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDckIsVUFBVSxDQUFDO2dCQUNULEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztnQkFDakIsS0FBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUM5QixDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDVCxDQUFDLENBQUM7UUFFSyxrQkFBYSxHQUFHO1lBQ3JCLElBQUk7Z0JBQ0YsSUFBTSxJQUFJLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDNUMsSUFBTSxJQUFJLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDNUMsSUFBTSxPQUFPLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyx1QkFBdUIsQ0FBUSxDQUFDO2dCQUN2RSxJQUFNLElBQUksR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBUSxDQUFDO2dCQUM1RCxJQUFNLFVBQVUsR0FBRyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQkFDbEUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUMsQ0FBQztnQkFDNUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQyxDQUFNO29CQUNoRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFO3dCQUNoQixDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7cUJBQzFCO2dCQUNILENBQUMsQ0FBQyxDQUFDO2dCQUNILEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQyxDQUFDO29CQUNoRSxJQUFJLENBQUMsRUFBRTt3QkFDTCxDQUFDLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO3FCQUN6QztnQkFDSCxDQUFDLENBQUMsQ0FBQztnQkFDSCxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLENBQU07b0JBQ2hDLENBQUMsQ0FBQyxLQUFLLENBQUMsZUFBZSxHQUFHLFNBQVMsQ0FBQztnQkFDdEMsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQyxDQUFNO29CQUMxQixDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7Z0JBQzNCLENBQUMsQ0FBQyxDQUFDO2dCQUVILElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLG9CQUFvQixDQUFDLENBQUM7Z0JBQzVDLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztnQkFDL0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDO2dCQUM3QixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUM7YUFDOUI7WUFBQyxPQUFPLENBQUMsRUFBRTtnQkFDVixPQUFPLElBQUksQ0FBQzthQUNiO1FBQ0gsQ0FBQyxDQUFDO1FBRUssd0JBQW1CLEdBQUcsVUFBQyxFQUFFLEVBQUUsTUFBTTtZQUN0QyxJQUFNLElBQUksR0FBRyxJQUFJLE1BQU0sQ0FBQyxLQUFLLEdBQUcsTUFBTSxHQUFHLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUN4RCxFQUFFLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztZQUM5QyxPQUFPLEVBQUUsQ0FBQztRQUNaLENBQUMsQ0FBQTtRQUVNLFNBQUksR0FBRyxVQUFDLEtBQVk7WUFBWixzQkFBQSxFQUFBLFlBQVk7WUFDekIsSUFBSTtnQkFDRixJQUFNLFNBQU8sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUMvQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxrQ0FBZ0MsS0FBSyxPQUFJLENBQUMsRUFBRTtvQkFDdEUsSUFBTSxhQUFhLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyx1QkFBdUIsQ0FBUSxDQUFDO29CQUM3RSxhQUFhLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7b0JBQ3JDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFPLEVBQUUsY0FBYyxDQUFDLENBQUM7aUJBQ25EO3FCQUFNO29CQUNMLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUMsQ0FBTTt3QkFDaEUsSUFBTSxJQUFJLEdBQUcsQ0FBQyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLDBDQUEwQzt3QkFDL0UsSUFBTSxLQUFLLEdBQUcsa0NBQWdDLElBQUksT0FBSSxDQUFDLENBQUMsa0NBQWtDO3dCQUMxRixJQUFNLE1BQU0sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBUSxDQUFDO3dCQUNwRCxJQUFJLE1BQU0sRUFBRTs0QkFDVixJQUFJLEtBQUcsS0FBTyxLQUFLLElBQUksRUFBRSxFQUFFLCtEQUErRDtnQ0FDbEYsSUFBQSxtQkFBd0MsRUFBdkMsb0JBQU0sRUFBRSxrQ0FBK0IsQ0FBQztnQ0FDL0MsTUFBTSxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsU0FBUyxDQUFDO2dDQUN6QyxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dDQUMxQyxTQUFPLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO2dDQUM1QyxTQUFPLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyw2QkFBMkIsSUFBTSxDQUFDLENBQUM7Z0NBQ3pEOzttQ0FFRztnQ0FDSCxJQUFNLGFBQWEsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dDQUNoRCxJQUFJLGFBQWEsQ0FBQyxJQUFJLEVBQUUsRUFBRSxrQkFBa0I7b0NBQzFDLEtBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLFFBQU0sRUFBRSxlQUFhLENBQUMsQ0FBQztvQ0FDM0MsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO2lDQUMzQjtxQ0FBTSxFQUFFLGVBQWU7b0NBQ3RCLEtBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDO3dDQUM5QixLQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxRQUFNLEVBQUUsZUFBYSxDQUFDLENBQUM7d0NBQzNDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztvQ0FDNUIsQ0FBQyxDQUFDLENBQUM7aUNBQ0o7NkJBQ0Y7aUNBQU07Z0NBQ0wsTUFBTSxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsU0FBUyxDQUFDO2dDQUN6QyxTQUFPLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyw2QkFBMkIsSUFBTSxDQUFDLENBQUM7Z0NBQzVELE1BQU0sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLENBQUM7Z0NBQzdDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQzs2QkFDMUI7eUJBQ0Y7NkJBQU07NEJBQ0wsTUFBTSxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsU0FBUyxDQUFDOzRCQUN6QyxTQUFPLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyw2QkFBMkIsSUFBTSxDQUFDLENBQUM7NEJBQzVELE1BQU0sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLENBQUM7NEJBQzdDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQzt5QkFDMUI7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7YUFDRjtZQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNWLE9BQU8sSUFBSSxDQUFDO2FBQ2I7UUFDSCxDQUFDLENBQUM7UUFFSyxZQUFPLEdBQUcsVUFBQyxPQUFjLEVBQUUsSUFBaUIsRUFBRSxhQUF1QjtZQUExRCx3QkFBQSxFQUFBLGNBQWM7WUFBRSxxQkFBQSxFQUFBLGlCQUFpQjtZQUFFLDhCQUFBLEVBQUEsdUJBQXVCO1lBQzFFLElBQUk7Z0JBQ0YsSUFBTSxJQUFJLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQVEsQ0FBQztnQkFDbkQsSUFBTSxJQUFJLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQVEsQ0FBQztnQkFDbkQsSUFBTSxPQUFPLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyx1QkFBdUIsQ0FBUSxDQUFDO2dCQUN2RSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7Z0JBQy9CLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztnQkFDL0IsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO2dCQUNoQyxPQUFPLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDMUMsSUFBTSxJQUFJLEdBQUcsUUFBUSxDQUFDLGVBQWUsQ0FBQztnQkFDdEMsSUFBTSxLQUFLLEdBQUcsT0FBTyxDQUFDLHFCQUFxQixFQUFFLENBQUM7Z0JBQ3ZDLElBQUEsaUJBQUcsRUFBRSxpQkFBSSxFQUFFLG1CQUFLLEVBQUUscUJBQU0sRUFBRSxxQkFBTSxFQUFFLG1CQUFLLENBQVU7Z0JBQ3hELElBQU0sa0JBQWtCLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsS0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ2hFLElBQU0saUJBQWlCLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQy9ELElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUMsSUFBSSxHQUFHLGlCQUFpQixDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUM7Z0JBQy9FLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUMsS0FBRyxHQUFHLGtCQUFrQixDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUM7Z0JBQy9FLElBQUksYUFBYSxLQUFLLE9BQU8sRUFBRTtvQkFDN0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLGFBQWEsQ0FBQyxDQUFDO2lCQUN0RDtxQkFBTTtvQkFDTCxLQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDO29CQUMxRyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsS0FBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lCQUN0RDtnQkFDRCxJQUFJLElBQUksS0FBSyxVQUFVLEVBQUU7b0JBQ3ZCLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxVQUFVLENBQUMsTUFBTSxDQUFDLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQztpQkFDcEc7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDO2lCQUNsRzthQUNGO1lBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ1YsT0FBTyxJQUFJLENBQUM7YUFDYjtRQUNILENBQUMsQ0FBQztRQUVLLFNBQUksR0FBRyxVQUFDLElBQVc7WUFBWCxxQkFBQSxFQUFBLFdBQVc7WUFDeEIsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsQ0FBQyxDQUFDO1FBRUssb0JBQWUsR0FBRyxVQUFDLElBQVk7WUFDcEMsSUFBRztnQkFDRCxJQUFNLEdBQUcsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLHdCQUFzQixJQUFNLENBQUMsQ0FBQTtnQkFDaEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFBO2FBQ2I7WUFBQSxPQUFPLENBQUMsRUFBRTtnQkFDVCxPQUFPLElBQUksQ0FBQTthQUNaO1FBQ0gsQ0FBQyxDQUFBO0lBbE9ELENBQUM7O0lBUlM7UUFBVCxNQUFNLEVBQUU7dURBQW9DO0lBQ25DO1FBQVQsTUFBTSxFQUFFO3VEQUFvQztJQUZsQyxpQkFBaUI7UUFIN0IsVUFBVSxDQUFDO1lBQ1YsVUFBVSxFQUFFLE1BQU07U0FDbkIsQ0FBQztPQUNXLGlCQUFpQixDQTZPN0I7NEJBbFBEO0NBa1BDLEFBN09ELElBNk9DO1NBN09ZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7RXZlbnRFbWl0dGVyLCBJbmplY3RhYmxlLCBPdXRwdXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd4Q29waWxvdFNlcnZpY2Uge1xyXG4gIEBPdXRwdXQoKSB0ZW1wbGF0ZSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBuZXh0RW1pdCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIHByaXZhdGUgZWxlbWVudHNEb21BY3RpdmU6IGFueTtcclxuICBwcml2YXRlIGVsZW1lbnRzRG9tOiBhbnk7XHJcbiAgcHVibGljIHRtcENvbG9yID0gbnVsbDtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcblxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogUHJpdmF0ZSBmdW5jdGlvbnNcclxuICAgKi9cclxuXHJcbiAgcHJpdmF0ZSBpc0luVmlld3BvcnQgPSAoZWwgPSBudWxsKSA9PiB7XHJcbiAgICBjb25zdCByZWN0ID0gZWwuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICB2aWV3OiAocmVjdC50b3AgPj0gMCAmJlxyXG4gICAgICAgIHJlY3QubGVmdCA+PSAwICYmXHJcbiAgICAgICAgcmVjdC5ib3R0b20gPD0gKHdpbmRvdy5pbm5lckhlaWdodCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50SGVpZ2h0KSAmJlxyXG4gICAgICAgIHJlY3QucmlnaHQgPD0gKHdpbmRvdy5pbm5lcldpZHRoIHx8IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRXaWR0aCkpLFxyXG4gICAgICBheGlzOiByZWN0XHJcbiAgICB9O1xyXG4gIH07XHJcblxyXG4gIHByaXZhdGUgc2Nyb2xsTG9jYXRlZCA9IChlbGVtZW50KSA9PiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICBsZXQgY291bnRGbGFnID0gMDtcclxuICAgIGNvbnN0IGluaXQgPSAxMDA7XHJcbiAgICBjb25zdCBvcHRpb25zID0ge2Jsb2NrOiAnc3RhcnQnLCBiZWhhdmlvcjogJ3Ntb290aCd9O1xyXG4gICAgZWxlbWVudC5zY3JvbGxJbnRvVmlldyhvcHRpb25zKTtcclxuICAgIGNvbnN0IGlkID0gc2V0SW50ZXJ2YWwoKCkgPT4ge1xyXG4gICAgICBjb3VudEZsYWcrKztcclxuICAgICAgY29uc3Qge3RvcH0gPSB0aGlzLmlzSW5WaWV3cG9ydChlbGVtZW50KS5heGlzO1xyXG4gICAgICBjb25zdCBmdWxsSGVpZ2h0ID0gKHdpbmRvdy5pbm5lckhlaWdodCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50SGVpZ2h0KTtcclxuICAgICAgY29uc3QgcGVyY2VudGFnZVZpZXcgPSBwYXJzZUZsb2F0KFN0cmluZyh0b3AgKiAxMDApKSAvIGZ1bGxIZWlnaHQ7XHJcbiAgICAgIGlmICgodG9wIDwgMSkgfHwgKGNvdW50RmxhZyA+IGluaXQpKSB7XHJcbiAgICAgICAgY2xlYXJJbnRlcnZhbChpZCk7XHJcbiAgICAgICAgaWYgKGNvdW50RmxhZyA+IGluaXQpIHtcclxuICAgICAgICAgIHJlamVjdChmYWxzZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHJlc29sdmUodHJ1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGlmIChjb3VudEZsYWcgPT09IDIwKSB7XHJcbiAgICAgICAgICBlbGVtZW50LnNjcm9sbEludG9WaWV3KG9wdGlvbnMpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoKGNvdW50RmxhZyA+IDUpICYmIChjb3VudEZsYWcgPCAxNSkpIHtcclxuICAgICAgICAgIGlmICgocGVyY2VudGFnZVZpZXcgPiAxMCkgJiYgKHBlcmNlbnRhZ2VWaWV3IDwgODApKSB7XHJcbiAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwoaWQpO1xyXG4gICAgICAgICAgICByZXNvbHZlKHRydWUpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSwgMTAwKTtcclxuICB9KTtcclxuXHJcbiAgcHJpdmF0ZSBnZXRQYXJlbnQgPSAoKSA9PiB7XHJcbiAgICB0cnkge1xyXG4gICAgICB0aGlzLnJlbW92ZVdyYXBwZXIoKTtcclxuICAgICAgY29uc3QgY29waWxvdHNFbGVtZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChgLm5neC1jb3BpbG90LWluaXRgKSBhcyBhbnk7IC8vRWxlbWVudG9zIGRvbmRlIHNlIGRlYmUgaGFjZXIgZm9jb1xyXG4gICAgICBBcnJheS5mcm9tKGNvcGlsb3RzRWxlbWVudCkubWFwKChlOiBhbnkpID0+IHtcclxuICAgICAgICBjb25zdCBvcmRlciA9IGUuZ2V0QXR0cmlidXRlKCdkYXRhLXN0ZXAnKTtcclxuICAgICAgICBpZiAob3JkZXIpIHtcclxuICAgICAgICAgIGNvbnN0IGVsID0gZS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcclxuICAgICAgICAgIGNvbnN0IHNpbmdsZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYC5jb3BpbG90LXZpZXctc3RlcC0ke29yZGVyfWApIGFzIGFueTsgLy8gVWJpY2Ftb3MgZWwgdGVtcGxhdGUgcGFyYSB1YmljYXJsbyBkb25kZSBmb2NvXHJcbiAgICAgICAgICBjb25zdCB7dG9wLCByaWdodCwgbGVmdCwgYm90dG9tLCB3aWR0aH0gPSBlbDtcclxuICAgICAgICAgIHNpbmdsZS5zdHlsZS5tYXJnaW5MZWZ0ID0gYCR7d2lkdGh9cHhgO1xyXG4gICAgICAgICAgc2luZ2xlLnN0eWxlLnRvcCA9IGAke3RvcH1weGA7XHJcbiAgICAgICAgICBzaW5nbGUuc3R5bGUucmlnaHQgPSBgJHtyaWdodH1weGA7XHJcbiAgICAgICAgICBzaW5nbGUuc3R5bGUuYm90dG9tID0gYCR7Ym90dG9tfXB4YDtcclxuICAgICAgICAgIHNpbmdsZS5zdHlsZS5sZWZ0ID0gYCR7bGVmdH1weGA7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIGdldFRlbXBsYXRlcyA9IChvID0gJzEnKSA9PiB7XHJcbiAgICB0cnkge1xyXG4gICAgICB0aGlzLmVsZW1lbnRzRG9tQWN0aXZlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChgLmNvcGlsb3Qtdmlldy5jb3BpbG90LWFjdGl2ZWApO1xyXG4gICAgICB0aGlzLmVsZW1lbnRzRG9tID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChgLmNvcGlsb3Qtdmlld2ApO1xyXG4gICAgICBpZiAoISh0aGlzLmVsZW1lbnRzRG9tQWN0aXZlLmxlbmd0aCkpIHtcclxuICAgICAgICB0aGlzLmZpbmQobyk7XHJcbiAgICAgIH1cclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIGNoZWNrSW5pdCA9IChwb3NpdGlvbiA9ICcxJykgPT4ge1xyXG4gICAgLy8gd2luZG93LnNjcm9sbFRvKHsgdG9wOiAwfSk7XHJcbiAgICB0aGlzLmVsZW1lbnRzRG9tQWN0aXZlID0ge307XHJcbiAgICB0aGlzLmVsZW1lbnRzRG9tID0ge307XHJcbiAgICB0aGlzLnRtcENvbG9yID0gbnVsbDtcclxuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICB0aGlzLmdldFBhcmVudCgpO1xyXG4gICAgICB0aGlzLmdldFRlbXBsYXRlcyhwb3NpdGlvbik7XHJcbiAgICB9LCA2MCk7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIHJlbW92ZVdyYXBwZXIgPSAoKSA9PiB7XHJcbiAgICB0cnkge1xyXG4gICAgICBjb25zdCBib2R5ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgYm9keWApO1xyXG4gICAgICBjb25zdCBodG1sID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgaHRtbGApO1xyXG4gICAgICBjb25zdCB3cmFwcGVyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgLm5neC13cmFwcGVyLW92ZXJ2aWV3YCkgYXMgYW55O1xyXG4gICAgICBjb25zdCBsaXN0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgLmNvcGlsb3Qtdmlld2ApIGFzIGFueTtcclxuICAgICAgY29uc3QgbGlzdFBhcmVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYC5uZ3gtY29waWxvdC1pbml0YCk7XHJcbiAgICAgIGJvZHkuY2xhc3NMaXN0LnJlbW92ZSgnbmd4LWNvcGlsb3QtYWN0aXZlJyk7XHJcbiAgICAgIEFycmF5LmZyb20oZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChgLmNvcGlsb3Qtdmlld2ApKS5tYXAoKGU6IGFueSkgPT4ge1xyXG4gICAgICAgIGlmIChlICYmIGUuc3R5bGUpIHtcclxuICAgICAgICAgIGUuc3R5bGUuZGlzcGxheSA9ICdub25lJztcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICBBcnJheS5mcm9tKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYC5uZ3gtY29waWxvdC1wdWxzZWApKS5tYXAoKGUpID0+IHtcclxuICAgICAgICBpZiAoZSkge1xyXG4gICAgICAgICAgZS5jbGFzc0xpc3QucmVtb3ZlKCduZ3gtY29waWxvdC1wdWxzZScpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIEFycmF5LmZyb20obGlzdFBhcmVudCkubWFwKChlOiBhbnkpID0+IHtcclxuICAgICAgICBlLnN0eWxlLmJhY2tncm91bmRDb2xvciA9ICdpbml0aWFsJztcclxuICAgICAgfSk7XHJcbiAgICAgIEFycmF5LmZyb20obGlzdCkubWFwKChlOiBhbnkpID0+IHtcclxuICAgICAgICBlLnN0eWxlLmRpc3BsYXkgPSBgbm9uZWA7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgYm9keS5jbGFzc0xpc3QucmVtb3ZlKCduZ3gtY29waWxvdC1hY3RpdmUnKTtcclxuICAgICAgd3JhcHBlci5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xyXG4gICAgICBodG1sLnN0eWxlLm92ZXJmbG93ID0gJ2F1dG8nO1xyXG4gICAgICBib2R5LnN0eWxlLm92ZXJmbG93ID0gJ2F1dG8nO1xyXG4gICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBwdWJsaWMgcmVtb3ZlQ2xhc3NCeVByZWZpeCA9IChlbCwgcHJlZml4KSA9PiB7XHJcbiAgICBjb25zdCByZWd4ID0gbmV3IFJlZ0V4cCgnXFxcXGInICsgcHJlZml4ICsgJy4qP1xcXFxiJywgJ2cnKTtcclxuICAgIGVsLmNsYXNzTmFtZSA9IGVsLmNsYXNzTmFtZS5yZXBsYWNlKHJlZ3gsICcnKTtcclxuICAgIHJldHVybiBlbDtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBmaW5kID0gKG9yZGVyID0gbnVsbCkgPT4ge1xyXG4gICAgdHJ5IHtcclxuICAgICAgY29uc3Qgd3JhcHBlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYGJvZHlgKTtcclxuICAgICAgaWYgKCFkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAubmd4LWNvcGlsb3QtaW5pdFtkYXRhLXN0ZXA9JyR7b3JkZXJ9J11gKSkge1xyXG4gICAgICAgIGNvbnN0IHdyYXBwZXJTaW5nbGUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAubmd4LXdyYXBwZXItb3ZlcnZpZXdgKSBhcyBhbnk7XHJcbiAgICAgICAgd3JhcHBlclNpbmdsZS5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xyXG4gICAgICAgIHRoaXMucmVtb3ZlQ2xhc3NCeVByZWZpeCh3cmFwcGVyLCAnbmd4LWNvcGlsb3QtJyk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgQXJyYXkuZnJvbShkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKGAuY29waWxvdC12aWV3YCkpLm1hcCgoZTogYW55KSA9PiB7IC8vIFRlbXBsYXRlc1xyXG4gICAgICAgICAgY29uc3Qgc3RlcCA9IGUuZ2V0QXR0cmlidXRlKCdzdGVwJyk7IC8vIE9idGVuZW1vcyBlbCBwYXNvIGFsIHF1ZSB2YSBlbCB0ZW1wbGF0ZVxyXG4gICAgICAgICAgY29uc3QgdHJhY2UgPSBgLm5neC1jb3BpbG90LWluaXRbZGF0YS1zdGVwPScke3N0ZXB9J11gOyAvLyBCdXNjYW1vcyBlbCBlbGVtZW50IGhhY2VyIGZvY3VzXHJcbiAgICAgICAgICBjb25zdCBzaW5nbGUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHRyYWNlKSBhcyBhbnk7XHJcbiAgICAgICAgICBpZiAoc2luZ2xlKSB7XHJcbiAgICAgICAgICAgIGlmIChgJHtvcmRlcn1gID09PSBzdGVwKSB7IC8vIFNpIGVsIHRlbXBsYXRlIGNvbiBlbCBwYXNvIHkgZWwgZm9jdXMgc29uIGVsIG1pc21vIG1vc3RyYW1vc1xyXG4gICAgICAgICAgICAgIGNvbnN0IHtjb21vZGUsIG92ZXJ2aWV3Y29sb3J9ID0gc2luZ2xlLmRhdGFzZXQ7XHJcbiAgICAgICAgICAgICAgc2luZ2xlLnN0eWxlLmJhY2tncm91bmRDb2xvciA9ICcjY2ZjZWZmJztcclxuICAgICAgICAgICAgICBzaW5nbGUuY2xhc3NMaXN0LmFkZCgnbmd4LWNvcGlsb3QtcHVsc2UnKTtcclxuICAgICAgICAgICAgICB3cmFwcGVyLmNsYXNzTGlzdC5hZGQoJ25neC1jb3BpbG90LWFjdGl2ZScpO1xyXG4gICAgICAgICAgICAgIHdyYXBwZXIuY2xhc3NMaXN0LmFkZChgbmd4LWNvcGlsb3QtYWN0aXZlLXN0ZXAtJHtzdGVwfWApO1xyXG4gICAgICAgICAgICAgIC8qKlxyXG4gICAgICAgICAgICAgICAqIEZpeCBwZXJmb21hbmNlXHJcbiAgICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgICAgY29uc3QgY2hlY2tWaWV3UG9ydCA9IHRoaXMuaXNJblZpZXdwb3J0KHNpbmdsZSk7XHJcbiAgICAgICAgICAgICAgaWYgKGNoZWNrVmlld1BvcnQudmlldykgeyAvLyBZZXMgaW4gdmlld3BvcnRcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0Wm9uZSh0cmFjZSwgY29tb2RlLCBvdmVydmlld2NvbG9yKTtcclxuICAgICAgICAgICAgICAgIGUuc3R5bGUuZGlzcGxheSA9IGBibG9ja2A7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHsgLy9NdXN0IHNjcm9sbGVkXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNjcm9sbExvY2F0ZWQoc2luZ2xlKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5zZXRab25lKHRyYWNlLCBjb21vZGUsIG92ZXJ2aWV3Y29sb3IpO1xyXG4gICAgICAgICAgICAgICAgICBlLnN0eWxlLmRpc3BsYXkgPSBgYmxvY2tgO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHNpbmdsZS5zdHlsZS5iYWNrZ3JvdW5kQ29sb3IgPSAnaW5pdGlhbCc7XHJcbiAgICAgICAgICAgICAgd3JhcHBlci5jbGFzc0xpc3QucmVtb3ZlKGBuZ3gtY29waWxvdC1hY3RpdmUtc3RlcC0ke3N0ZXB9YCk7XHJcbiAgICAgICAgICAgICAgc2luZ2xlLmNsYXNzTGlzdC5yZW1vdmUoJ25neC1jb3BpbG90LXB1bHNlJyk7XHJcbiAgICAgICAgICAgICAgZS5zdHlsZS5kaXNwbGF5ID0gYG5vbmVgO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzaW5nbGUuc3R5bGUuYmFja2dyb3VuZENvbG9yID0gJ2luaXRpYWwnO1xyXG4gICAgICAgICAgICB3cmFwcGVyLmNsYXNzTGlzdC5yZW1vdmUoYG5neC1jb3BpbG90LWFjdGl2ZS1zdGVwLSR7c3RlcH1gKTtcclxuICAgICAgICAgICAgc2luZ2xlLmNsYXNzTGlzdC5yZW1vdmUoJ25neC1jb3BpbG90LXB1bHNlJyk7XHJcbiAgICAgICAgICAgIGUuc3R5bGUuZGlzcGxheSA9IGBub25lYDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBwdWJsaWMgc2V0Wm9uZSA9IChlbGVtZW50ID0gbnVsbCwgbW9kZSA9ICd2ZXJ0aWNhbCcsIG92ZXJ2aWV3Y29sb3IgPSAnZmFsc2UnKSA9PiB7XHJcbiAgICB0cnkge1xyXG4gICAgICBjb25zdCBodG1sID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgaHRtbGApIGFzIGFueTtcclxuICAgICAgY29uc3QgYm9keSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYGJvZHlgKSBhcyBhbnk7XHJcbiAgICAgIGNvbnN0IHdyYXBwZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAubmd4LXdyYXBwZXItb3ZlcnZpZXdgKSBhcyBhbnk7XHJcbiAgICAgIGh0bWwuc3R5bGUub3ZlcmZsb3cgPSAnaGlkZGVuJztcclxuICAgICAgYm9keS5zdHlsZS5vdmVyZmxvdyA9ICdoaWRkZW4nO1xyXG4gICAgICB3cmFwcGVyLnN0eWxlLmRpc3BsYXkgPSAnYmxvY2snO1xyXG4gICAgICBlbGVtZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihlbGVtZW50KTtcclxuICAgICAgY29uc3Qgcm9vdCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudDtcclxuICAgICAgY29uc3QgYm91bmQgPSBlbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG4gICAgICBjb25zdCB7dG9wLCBsZWZ0LCByaWdodCwgaGVpZ2h0LCBib3R0b20sIHdpZHRofSA9IGJvdW5kO1xyXG4gICAgICBjb25zdCBjZW50cmFsUG9pbnRIZWlnaHQgPSBwYXJzZUZsb2F0KFN0cmluZyhib3R0b20gLSB0b3ApKSAvIDI7XHJcbiAgICAgIGNvbnN0IGNlbnRyYWxQb2ludFdpZHRoID0gcGFyc2VGbG9hdChTdHJpbmcocmlnaHQgLSBsZWZ0KSkgLyAyO1xyXG4gICAgICByb290LnN0eWxlLnNldFByb3BlcnR5KCctLXpvbmVZJywgcGFyc2VGbG9hdChsZWZ0ICsgY2VudHJhbFBvaW50V2lkdGgpICsgJ3B4Jyk7XHJcbiAgICAgIHJvb3Quc3R5bGUuc2V0UHJvcGVydHkoJy0tem9uZVgnLCBwYXJzZUZsb2F0KHRvcCArIGNlbnRyYWxQb2ludEhlaWdodCkgKyAncHgnKTtcclxuICAgICAgaWYgKG92ZXJ2aWV3Y29sb3IgIT09ICdmYWxzZScpIHtcclxuICAgICAgICByb290LnN0eWxlLnNldFByb3BlcnR5KCctLXpvbmVDb2xvcicsIG92ZXJ2aWV3Y29sb3IpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMudG1wQ29sb3IgPSAoIXRoaXMudG1wQ29sb3IpID8gZ2V0Q29tcHV0ZWRTdHlsZShyb290KS5nZXRQcm9wZXJ0eVZhbHVlKCctLXpvbmVDb2xvcicpIDogdGhpcy50bXBDb2xvcjtcclxuICAgICAgICByb290LnN0eWxlLnNldFByb3BlcnR5KCctLXpvbmVDb2xvcicsIHRoaXMudG1wQ29sb3IpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChtb2RlID09PSAndmVydGljYWwnKSB7XHJcbiAgICAgICAgcm9vdC5zdHlsZS5zZXRQcm9wZXJ0eSgnLS16b25lU2l6ZScsIHBhcnNlRmxvYXQoaGVpZ2h0KSArIHBhcnNlRmxvYXQoU3RyaW5nKGhlaWdodCAqIDAuMSkpICsgJ3B4Jyk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcm9vdC5zdHlsZS5zZXRQcm9wZXJ0eSgnLS16b25lU2l6ZScsIHBhcnNlRmxvYXQod2lkdGgpIC0gcGFyc2VGbG9hdChTdHJpbmcod2lkdGggKiAwLjUpKSArICdweCcpO1xyXG4gICAgICB9XHJcbiAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBuZXh0ID0gKGRhdGEgPSBudWxsKSA9PiB7XHJcbiAgICB0aGlzLm5leHRFbWl0LmVtaXQoZGF0YSk7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIGNoZWNrSWZFeGlzdERvbSA9IChzdGVwOiBzdHJpbmcpID0+IHtcclxuICAgIHRyeXtcclxuICAgICAgY29uc3QgZG9tID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgLmNvcGlsb3Qtdmlldy1zdGVwLSR7c3RlcH1gKVxyXG4gICAgICByZXR1cm4gKGRvbSlcclxuICAgIH1jYXRjaCAoZSkge1xyXG4gICAgICByZXR1cm4gbnVsbFxyXG4gICAgfVxyXG4gIH1cclxuXHJcbn1cclxuIl19