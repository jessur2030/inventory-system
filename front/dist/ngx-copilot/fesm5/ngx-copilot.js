import { __decorate } from 'tslib';
import { EventEmitter, ɵɵdefineInjectable, Output, Injectable, Component, ElementRef, Input, Directive, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

var NgxCopilotService = /** @class */ (function () {
    function NgxCopilotService() {
        var _this = this;
        this.template = new EventEmitter();
        this.nextEmit = new EventEmitter();
        this.tmpColor = null;
        /**
         * Private functions
         */
        this.isInViewport = function (el) {
            if (el === void 0) { el = null; }
            var rect = el.getBoundingClientRect();
            return {
                view: (rect.top >= 0 &&
                    rect.left >= 0 &&
                    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                    rect.right <= (window.innerWidth || document.documentElement.clientWidth)),
                axis: rect
            };
        };
        this.scrollLocated = function (element) { return new Promise(function (resolve, reject) {
            var countFlag = 0;
            var init = 100;
            var options = { block: 'start', behavior: 'smooth' };
            element.scrollIntoView(options);
            var id = setInterval(function () {
                countFlag++;
                var top = _this.isInViewport(element).axis.top;
                var fullHeight = (window.innerHeight || document.documentElement.clientHeight);
                var percentageView = parseFloat(String(top * 100)) / fullHeight;
                if ((top < 1) || (countFlag > init)) {
                    clearInterval(id);
                    if (countFlag > init) {
                        reject(false);
                    }
                    else {
                        resolve(true);
                    }
                }
                else {
                    if (countFlag === 20) {
                        element.scrollIntoView(options);
                    }
                    else if ((countFlag > 5) && (countFlag < 15)) {
                        if ((percentageView > 10) && (percentageView < 80)) {
                            clearInterval(id);
                            resolve(true);
                        }
                    }
                }
            }, 100);
        }); };
        this.getParent = function () {
            try {
                _this.removeWrapper();
                var copilotsElement = document.querySelectorAll(".ngx-copilot-init"); //Elementos donde se debe hacer foco
                Array.from(copilotsElement).map(function (e) {
                    var order = e.getAttribute('data-step');
                    if (order) {
                        var el = e.getBoundingClientRect();
                        var single = document.querySelector(".copilot-view-step-" + order); // Ubicamos el template para ubicarlo donde foco
                        var top_1 = el.top, right = el.right, left = el.left, bottom = el.bottom, width = el.width;
                        single.style.marginLeft = width + "px";
                        single.style.top = top_1 + "px";
                        single.style.right = right + "px";
                        single.style.bottom = bottom + "px";
                        single.style.left = left + "px";
                    }
                });
            }
            catch (e) {
                return null;
            }
        };
        this.getTemplates = function (o) {
            if (o === void 0) { o = '1'; }
            try {
                _this.elementsDomActive = document.querySelectorAll(".copilot-view.copilot-active");
                _this.elementsDom = document.querySelectorAll(".copilot-view");
                if (!(_this.elementsDomActive.length)) {
                    _this.find(o);
                }
            }
            catch (e) {
                return null;
            }
        };
        this.checkInit = function (position) {
            if (position === void 0) { position = '1'; }
            // window.scrollTo({ top: 0});
            _this.elementsDomActive = {};
            _this.elementsDom = {};
            _this.tmpColor = null;
            setTimeout(function () {
                _this.getParent();
                _this.getTemplates(position);
            }, 60);
        };
        this.removeWrapper = function () {
            try {
                var body = document.querySelector("body");
                var html = document.querySelector("html");
                var wrapper = document.querySelector(".ngx-wrapper-overview");
                var list = document.querySelector(".copilot-view");
                var listParent = document.querySelectorAll(".ngx-copilot-init");
                body.classList.remove('ngx-copilot-active');
                Array.from(document.querySelectorAll(".copilot-view")).map(function (e) {
                    if (e && e.style) {
                        e.style.display = 'none';
                    }
                });
                Array.from(document.querySelectorAll(".ngx-copilot-pulse")).map(function (e) {
                    if (e) {
                        e.classList.remove('ngx-copilot-pulse');
                    }
                });
                Array.from(listParent).map(function (e) {
                    e.style.backgroundColor = 'initial';
                });
                Array.from(list).map(function (e) {
                    e.style.display = "none";
                });
                body.classList.remove('ngx-copilot-active');
                wrapper.style.display = 'none';
                html.style.overflow = 'auto';
                body.style.overflow = 'auto';
            }
            catch (e) {
                return null;
            }
        };
        this.removeClassByPrefix = function (el, prefix) {
            var regx = new RegExp('\\b' + prefix + '.*?\\b', 'g');
            el.className = el.className.replace(regx, '');
            return el;
        };
        this.find = function (order) {
            if (order === void 0) { order = null; }
            try {
                var wrapper_1 = document.querySelector("body");
                if (!document.querySelector(".ngx-copilot-init[data-step='" + order + "']")) {
                    var wrapperSingle = document.querySelector(".ngx-wrapper-overview");
                    wrapperSingle.style.display = 'none';
                    _this.removeClassByPrefix(wrapper_1, 'ngx-copilot-');
                }
                else {
                    Array.from(document.querySelectorAll(".copilot-view")).map(function (e) {
                        var step = e.getAttribute('step'); // Obtenemos el paso al que va el template
                        var trace = ".ngx-copilot-init[data-step='" + step + "']"; // Buscamos el element hacer focus
                        var single = document.querySelector(trace);
                        if (single) {
                            if ("" + order === step) { // Si el template con el paso y el focus son el mismo mostramos
                                var _a = single.dataset, comode_1 = _a.comode, overviewcolor_1 = _a.overviewcolor;
                                single.style.backgroundColor = '#cfceff';
                                single.classList.add('ngx-copilot-pulse');
                                wrapper_1.classList.add('ngx-copilot-active');
                                wrapper_1.classList.add("ngx-copilot-active-step-" + step);
                                /**
                                 * Fix perfomance
                                 */
                                var checkViewPort = _this.isInViewport(single);
                                if (checkViewPort.view) { // Yes in viewport
                                    _this.setZone(trace, comode_1, overviewcolor_1);
                                    e.style.display = "block";
                                }
                                else { //Must scrolled
                                    _this.scrollLocated(single).then(function () {
                                        _this.setZone(trace, comode_1, overviewcolor_1);
                                        e.style.display = "block";
                                    });
                                }
                            }
                            else {
                                single.style.backgroundColor = 'initial';
                                wrapper_1.classList.remove("ngx-copilot-active-step-" + step);
                                single.classList.remove('ngx-copilot-pulse');
                                e.style.display = "none";
                            }
                        }
                        else {
                            single.style.backgroundColor = 'initial';
                            wrapper_1.classList.remove("ngx-copilot-active-step-" + step);
                            single.classList.remove('ngx-copilot-pulse');
                            e.style.display = "none";
                        }
                    });
                }
            }
            catch (e) {
                return null;
            }
        };
        this.setZone = function (element, mode, overviewcolor) {
            if (element === void 0) { element = null; }
            if (mode === void 0) { mode = 'vertical'; }
            if (overviewcolor === void 0) { overviewcolor = 'false'; }
            try {
                var html = document.querySelector("html");
                var body = document.querySelector("body");
                var wrapper = document.querySelector(".ngx-wrapper-overview");
                html.style.overflow = 'hidden';
                body.style.overflow = 'hidden';
                wrapper.style.display = 'block';
                element = document.querySelector(element);
                var root = document.documentElement;
                var bound = element.getBoundingClientRect();
                var top_2 = bound.top, left = bound.left, right = bound.right, height = bound.height, bottom = bound.bottom, width = bound.width;
                var centralPointHeight = parseFloat(String(bottom - top_2)) / 2;
                var centralPointWidth = parseFloat(String(right - left)) / 2;
                root.style.setProperty('--zoneY', parseFloat(left + centralPointWidth) + 'px');
                root.style.setProperty('--zoneX', parseFloat(top_2 + centralPointHeight) + 'px');
                if (overviewcolor !== 'false') {
                    root.style.setProperty('--zoneColor', overviewcolor);
                }
                else {
                    _this.tmpColor = (!_this.tmpColor) ? getComputedStyle(root).getPropertyValue('--zoneColor') : _this.tmpColor;
                    root.style.setProperty('--zoneColor', _this.tmpColor);
                }
                if (mode === 'vertical') {
                    root.style.setProperty('--zoneSize', parseFloat(height) + parseFloat(String(height * 0.1)) + 'px');
                }
                else {
                    root.style.setProperty('--zoneSize', parseFloat(width) - parseFloat(String(width * 0.5)) + 'px');
                }
            }
            catch (e) {
                return null;
            }
        };
        this.next = function (data) {
            if (data === void 0) { data = null; }
            _this.nextEmit.emit(data);
        };
        this.checkIfExistDom = function (step) {
            try {
                var dom = document.querySelector(".copilot-view-step-" + step);
                return (dom);
            }
            catch (e) {
                return null;
            }
        };
    }
    NgxCopilotService.ɵprov = ɵɵdefineInjectable({ factory: function NgxCopilotService_Factory() { return new NgxCopilotService(); }, token: NgxCopilotService, providedIn: "root" });
    __decorate([
        Output()
    ], NgxCopilotService.prototype, "template", void 0);
    __decorate([
        Output()
    ], NgxCopilotService.prototype, "nextEmit", void 0);
    NgxCopilotService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], NgxCopilotService);
    return NgxCopilotService;
}());

var NgxCopilotComponent = /** @class */ (function () {
    function NgxCopilotComponent() {
    }
    NgxCopilotComponent.prototype.ngOnInit = function () {
    };
    NgxCopilotComponent = __decorate([
        Component({
            selector: 'lib-ngx-copilot',
            template: "\n    <p>\n      ngx-copilot works!\n    </p>\n  "
        })
    ], NgxCopilotComponent);
    return NgxCopilotComponent;
}());

var CopilotDirective = /** @class */ (function () {
    function CopilotDirective(service, elem) {
        this.service = service;
        this.elem = elem;
        this.mode = 'vertical';
        this.overviewcolor = 'false';
        this.animatedEffect = '';
    }
    CopilotDirective.prototype.ngOnInit = function () {
    };
    CopilotDirective.prototype.ngAfterViewInit = function () {
        if (this.template) {
            this.elem.nativeElement.classList.add('ngx-copilot-init');
            if (this.step) {
                this.elem.nativeElement.dataset.step = this.step;
                this.elem.nativeElement.dataset.comode = this.mode;
                this.elem.nativeElement.dataset.overviewcolor = this.overviewcolor;
                this.elem.nativeElement.dataset.animatedEffect = this.animatedEffect;
                this.service.template.emit({
                    step: this.step,
                    template: this.template,
                    mode: this.mode,
                    overviewcolor: this.overviewcolor,
                    animatedEffect: this.animatedEffect
                });
            }
        }
    };
    CopilotDirective.ctorParameters = function () { return [
        { type: NgxCopilotService },
        { type: ElementRef }
    ]; };
    __decorate([
        Input('copilot-step')
    ], CopilotDirective.prototype, "step", void 0);
    __decorate([
        Input('copilot-template')
    ], CopilotDirective.prototype, "template", void 0);
    __decorate([
        Input('copilot-mode')
    ], CopilotDirective.prototype, "mode", void 0);
    __decorate([
        Input('copilot-color')
    ], CopilotDirective.prototype, "overviewcolor", void 0);
    __decorate([
        Input('copilot-class')
    ], CopilotDirective.prototype, "animatedEffect", void 0);
    CopilotDirective = __decorate([
        Directive({
            selector: '[copilot]'
        })
    ], CopilotDirective);
    return CopilotDirective;
}());

var NgxWrapperCopilotComponent = /** @class */ (function () {
    function NgxWrapperCopilotComponent(service) {
        this.service = service;
        this.viewTemplate = [];
    }
    NgxWrapperCopilotComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscriber = this.service.template.subscribe(function (a) {
            if (!_this.service.checkIfExistDom(a.step)) {
                _this.viewTemplate.push(a);
            }
        });
        this.service.nextEmit.subscribe(function (next) { return _this.service.find(next); });
    };
    NgxWrapperCopilotComponent.prototype.ngOnDestroy = function () {
        try {
            this.subscriber.unsubscribe();
            this.viewTemplate = [];
        }
        catch (e) {
            this.viewTemplate = [];
        }
    };
    NgxWrapperCopilotComponent.ctorParameters = function () { return [
        { type: NgxCopilotService }
    ]; };
    NgxWrapperCopilotComponent = __decorate([
        Component({
            selector: 'ngx-wrapper-copilot',
            template: "<div class=\"ngx-wrapper-overview\">\r\n  <ng-container *ngFor=\"let te  of viewTemplate\" >\r\n    <div [attr.step]=\"te?.step\"\r\n         class=\"{{(!te?.animatedEffect?.length) ? 'animate__animated animate__bounceIn': te?.animatedEffect}}\r\n     copilot-view copilot-view-step-{{te?.step}}\">\r\n\r\n      <ng-container *ngTemplateOutlet=\"te?.template\"></ng-container>\r\n    </div>\r\n  </ng-container>\r\n</div>\r\n",
            styles: [".ngx-wrapper-overview{display:none;position:fixed;z-index:9999;width:100%;height:100%;top:0;left:0}.ngx-wrapper-overview .copilot-view{position:fixed;max-width:300px;max-height:-webkit-min-content;max-height:-moz-min-content;max-height:min-content;padding:10px;border-radius:10px;background-color:#6b66ff;color:#fff;display:none}.ngx-copilot-pulse{display:block;border-radius:50%;background:#cca92c;cursor:pointer;box-shadow:0 0 0 rgba(204,169,44,.8);animation:2s infinite ngx-copilot-pulse}.ngx-copilot-pulse:hover{animation:none}@keyframes ngx-copilot-pulse{0%{box-shadow:0 0 0 0 rgba(204,169,44,.4)}70%{box-shadow:0 0 0 10px rgba(204,169,44,0)}100%{box-shadow:0 0 0 0 rgba(204,169,44,0)}}"]
        })
    ], NgxWrapperCopilotComponent);
    return NgxWrapperCopilotComponent;
}());

var NgxCopilotModule = /** @class */ (function () {
    function NgxCopilotModule() {
    }
    NgxCopilotModule = __decorate([
        NgModule({
            declarations: [NgxCopilotComponent, CopilotDirective, NgxWrapperCopilotComponent],
            imports: [
                CommonModule
            ],
            exports: [NgxCopilotComponent, CopilotDirective, NgxWrapperCopilotComponent]
        })
    ], NgxCopilotModule);
    return NgxCopilotModule;
}());

/*
 * Public API Surface of ngx-copilot
 */

/**
 * Generated bundle index. Do not edit.
 */

export { CopilotDirective, NgxCopilotComponent, NgxCopilotModule, NgxCopilotService, NgxWrapperCopilotComponent };
//# sourceMappingURL=ngx-copilot.js.map
