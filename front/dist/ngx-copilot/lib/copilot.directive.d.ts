import { AfterViewInit, ElementRef, OnInit, TemplateRef } from '@angular/core';
import { NgxCopilotService } from "./ngx-copilot.service";
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class CopilotDirective implements OnInit, AfterViewInit {
    private service;
    private elem;
    step: any;
    template: TemplateRef<any>;
    mode: string;
    overviewcolor: string;
    animatedEffect: string;
    constructor(service: NgxCopilotService, elem: ElementRef);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<CopilotDirective, never>;
    static ɵdir: ɵngcc0.ɵɵDirectiveDefWithMeta<CopilotDirective, "[copilot]", never, { "mode": "copilot-mode"; "overviewcolor": "copilot-color"; "animatedEffect": "copilot-class"; "step": "copilot-step"; "template": "copilot-template"; }, {}, never>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<CopilotDirective, never>;
    static ɵdir: ɵngcc0.ɵɵDirectiveDefWithMeta<CopilotDirective, "[copilot]", never, { "mode": "copilot-mode"; "overviewcolor": "copilot-color"; "animatedEffect": "copilot-class"; "step": "copilot-step"; "template": "copilot-template"; }, {}, never>;
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29waWxvdC5kaXJlY3RpdmUuZC50cyIsInNvdXJjZXMiOlsiY29waWxvdC5kaXJlY3RpdmUuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQzZEOzs7OztBQWRBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBZnRlclZpZXdJbml0LCBFbGVtZW50UmVmLCBPbkluaXQsIFRlbXBsYXRlUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5neENvcGlsb3RTZXJ2aWNlIH0gZnJvbSBcIi4vbmd4LWNvcGlsb3Quc2VydmljZVwiO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBDb3BpbG90RGlyZWN0aXZlIGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0IHtcclxuICAgIHByaXZhdGUgc2VydmljZTtcclxuICAgIHByaXZhdGUgZWxlbTtcclxuICAgIHN0ZXA6IGFueTtcclxuICAgIHRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xyXG4gICAgbW9kZTogc3RyaW5nO1xyXG4gICAgb3ZlcnZpZXdjb2xvcjogc3RyaW5nO1xyXG4gICAgYW5pbWF0ZWRFZmZlY3Q6IHN0cmluZztcclxuICAgIGNvbnN0cnVjdG9yKHNlcnZpY2U6IE5neENvcGlsb3RTZXJ2aWNlLCBlbGVtOiBFbGVtZW50UmVmKTtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQ7XHJcbiAgICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZDtcclxufVxyXG4iXX0=