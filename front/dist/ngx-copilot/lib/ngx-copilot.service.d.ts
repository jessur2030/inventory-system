import { EventEmitter } from '@angular/core';
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class NgxCopilotService {
    template: EventEmitter<any>;
    nextEmit: EventEmitter<any>;
    private elementsDomActive;
    private elementsDom;
    tmpColor: any;
    constructor();
    /**
     * Private functions
     */
    private isInViewport;
    private scrollLocated;
    private getParent;
    getTemplates: (o?: string) => any;
    checkInit: (position?: string) => void;
    removeWrapper: () => any;
    removeClassByPrefix: (el: any, prefix: any) => any;
    find: (order?: any) => any;
    setZone: (element?: any, mode?: string, overviewcolor?: string) => any;
    next: (data?: any) => void;
    checkIfExistDom: (step: string) => Element;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<NgxCopilotService, never>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<NgxCopilotService, never>;
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWNvcGlsb3Quc2VydmljZS5kLnRzIiwic291cmNlcyI6WyJuZ3gtY29waWxvdC5zZXJ2aWNlLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQXRCQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIE5neENvcGlsb3RTZXJ2aWNlIHtcclxuICAgIHRlbXBsYXRlOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIG5leHRFbWl0OiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIHByaXZhdGUgZWxlbWVudHNEb21BY3RpdmU7XHJcbiAgICBwcml2YXRlIGVsZW1lbnRzRG9tO1xyXG4gICAgdG1wQ29sb3I6IGFueTtcclxuICAgIGNvbnN0cnVjdG9yKCk7XHJcbiAgICAvKipcclxuICAgICAqIFByaXZhdGUgZnVuY3Rpb25zXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgaXNJblZpZXdwb3J0O1xyXG4gICAgcHJpdmF0ZSBzY3JvbGxMb2NhdGVkO1xyXG4gICAgcHJpdmF0ZSBnZXRQYXJlbnQ7XHJcbiAgICBnZXRUZW1wbGF0ZXM6IChvPzogc3RyaW5nKSA9PiBhbnk7XHJcbiAgICBjaGVja0luaXQ6IChwb3NpdGlvbj86IHN0cmluZykgPT4gdm9pZDtcclxuICAgIHJlbW92ZVdyYXBwZXI6ICgpID0+IGFueTtcclxuICAgIHJlbW92ZUNsYXNzQnlQcmVmaXg6IChlbDogYW55LCBwcmVmaXg6IGFueSkgPT4gYW55O1xyXG4gICAgZmluZDogKG9yZGVyPzogYW55KSA9PiBhbnk7XHJcbiAgICBzZXRab25lOiAoZWxlbWVudD86IGFueSwgbW9kZT86IHN0cmluZywgb3ZlcnZpZXdjb2xvcj86IHN0cmluZykgPT4gYW55O1xyXG4gICAgbmV4dDogKGRhdGE/OiBhbnkpID0+IHZvaWQ7XHJcbiAgICBjaGVja0lmRXhpc3REb206IChzdGVwOiBzdHJpbmcpID0+IEVsZW1lbnQ7XHJcbn1cclxuIl19