import { OnDestroy, OnInit } from '@angular/core';
import { NgxCopilotService } from "../ngx-copilot.service";
import * as ɵngcc0 from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class NgxWrapperCopilotComponent implements OnInit, OnDestroy {
    private service;
    viewTemplate: any[];
    queue: [];
    private subscriber;
    constructor(service: NgxCopilotService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<NgxWrapperCopilotComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<NgxWrapperCopilotComponent, "ngx-wrapper-copilot", never, {}, {}, never, never>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<NgxWrapperCopilotComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<NgxWrapperCopilotComponent, "ngx-wrapper-copilot", never, {}, {}, never, never>;
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LXdyYXBwZXItY29waWxvdC5jb21wb25lbnQuZC50cyIsInNvdXJjZXMiOlsibmd4LXdyYXBwZXItY29waWxvdC5jb21wb25lbnQuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ3VFOzs7OztBQVhBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPbkRlc3Ryb3ksIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOZ3hDb3BpbG90U2VydmljZSB9IGZyb20gXCIuLi9uZ3gtY29waWxvdC5zZXJ2aWNlXCI7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIE5neFdyYXBwZXJDb3BpbG90Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG4gICAgcHJpdmF0ZSBzZXJ2aWNlO1xyXG4gICAgdmlld1RlbXBsYXRlOiBhbnlbXTtcclxuICAgIHF1ZXVlOiBbXTtcclxuICAgIHByaXZhdGUgc3Vic2NyaWJlcjtcclxuICAgIGNvbnN0cnVjdG9yKHNlcnZpY2U6IE5neENvcGlsb3RTZXJ2aWNlKTtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQ7XHJcbiAgICBuZ09uRGVzdHJveSgpOiB2b2lkO1xyXG59XHJcbiJdfQ==