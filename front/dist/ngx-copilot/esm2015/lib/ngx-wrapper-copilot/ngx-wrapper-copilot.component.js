import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { NgxCopilotService } from "../ngx-copilot.service";
let NgxWrapperCopilotComponent = class NgxWrapperCopilotComponent {
    constructor(service) {
        this.service = service;
        this.viewTemplate = [];
    }
    ngOnInit() {
        this.subscriber = this.service.template.subscribe(a => {
            if (!this.service.checkIfExistDom(a.step)) {
                this.viewTemplate.push(a);
            }
        });
        this.service.nextEmit.subscribe(next => this.service.find(next));
    }
    ngOnDestroy() {
        try {
            this.subscriber.unsubscribe();
            this.viewTemplate = [];
        }
        catch (e) {
            this.viewTemplate = [];
        }
    }
};
NgxWrapperCopilotComponent.ctorParameters = () => [
    { type: NgxCopilotService }
];
NgxWrapperCopilotComponent = __decorate([
    Component({
        selector: 'ngx-wrapper-copilot',
        template: "<div class=\"ngx-wrapper-overview\">\r\n  <ng-container *ngFor=\"let te  of viewTemplate\" >\r\n    <div [attr.step]=\"te?.step\"\r\n         class=\"{{(!te?.animatedEffect?.length) ? 'animate__animated animate__bounceIn': te?.animatedEffect}}\r\n     copilot-view copilot-view-step-{{te?.step}}\">\r\n\r\n      <ng-container *ngTemplateOutlet=\"te?.template\"></ng-container>\r\n    </div>\r\n  </ng-container>\r\n</div>\r\n",
        styles: [".ngx-wrapper-overview{display:none;position:fixed;z-index:9999;width:100%;height:100%;top:0;left:0}.ngx-wrapper-overview .copilot-view{position:fixed;max-width:300px;max-height:-webkit-min-content;max-height:-moz-min-content;max-height:min-content;padding:10px;border-radius:10px;background-color:#6b66ff;color:#fff;display:none}.ngx-copilot-pulse{display:block;border-radius:50%;background:#cca92c;cursor:pointer;box-shadow:0 0 0 rgba(204,169,44,.8);animation:2s infinite ngx-copilot-pulse}.ngx-copilot-pulse:hover{animation:none}@keyframes ngx-copilot-pulse{0%{box-shadow:0 0 0 0 rgba(204,169,44,.4)}70%{box-shadow:0 0 0 10px rgba(204,169,44,0)}100%{box-shadow:0 0 0 0 rgba(204,169,44,0)}}"]
    })
], NgxWrapperCopilotComponent);
export { NgxWrapperCopilotComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LXdyYXBwZXItY29waWxvdC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtY29waWxvdC8iLCJzb3VyY2VzIjpbImxpYi9uZ3gtd3JhcHBlci1jb3BpbG90L25neC13cmFwcGVyLWNvcGlsb3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFvQixNQUFNLGVBQWUsQ0FBQztBQUMzRCxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSx3QkFBd0IsQ0FBQztBQVF6RCxJQUFhLDBCQUEwQixHQUF2QyxNQUFhLDBCQUEwQjtJQUtyQyxZQUFvQixPQUEwQjtRQUExQixZQUFPLEdBQVAsT0FBTyxDQUFtQjtRQUp2QyxpQkFBWSxHQUFHLEVBQUUsQ0FBQztJQU16QixDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ3BELElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ3pDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzNCO1FBRUgsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBO0lBQ2xFLENBQUM7SUFFRCxXQUFXO1FBQ1QsSUFBSTtZQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUIsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7U0FDeEI7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNWLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1NBQ3hCO0lBQ0gsQ0FBQztDQUdGLENBQUE7O1lBeEI4QixpQkFBaUI7O0FBTG5DLDBCQUEwQjtJQUx0QyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUscUJBQXFCO1FBQy9CLHFiQUFtRDs7S0FFcEQsQ0FBQztHQUNXLDBCQUEwQixDQTZCdEM7U0E3QlksMEJBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uRGVzdHJveSwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtOZ3hDb3BpbG90U2VydmljZX0gZnJvbSBcIi4uL25neC1jb3BpbG90LnNlcnZpY2VcIjtcclxuaW1wb3J0IHtTdWJzY3JpcHRpb259IGZyb20gXCJyeGpzXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ25neC13cmFwcGVyLWNvcGlsb3QnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9uZ3gtd3JhcHBlci1jb3BpbG90LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9uZ3gtd3JhcHBlci1jb3BpbG90LmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd4V3JhcHBlckNvcGlsb3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgcHVibGljIHZpZXdUZW1wbGF0ZSA9IFtdO1xyXG4gIHB1YmxpYyBxdWV1ZTogW107XHJcbiAgcHJpdmF0ZSBzdWJzY3JpYmVyOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgc2VydmljZTogTmd4Q29waWxvdFNlcnZpY2UpIHtcclxuXHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuc3Vic2NyaWJlciA9IHRoaXMuc2VydmljZS50ZW1wbGF0ZS5zdWJzY3JpYmUoYSA9PiB7XHJcbiAgICAgIGlmICghdGhpcy5zZXJ2aWNlLmNoZWNrSWZFeGlzdERvbShhLnN0ZXApKSB7XHJcbiAgICAgICAgdGhpcy52aWV3VGVtcGxhdGUucHVzaChhKTtcclxuICAgICAgfVxyXG5cclxuICAgIH0pO1xyXG4gICAgdGhpcy5zZXJ2aWNlLm5leHRFbWl0LnN1YnNjcmliZShuZXh0ID0+IHRoaXMuc2VydmljZS5maW5kKG5leHQpKVxyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XHJcbiAgICB0cnkge1xyXG4gICAgICB0aGlzLnN1YnNjcmliZXIudW5zdWJzY3JpYmUoKTtcclxuICAgICAgdGhpcy52aWV3VGVtcGxhdGUgPSBbXTtcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgdGhpcy52aWV3VGVtcGxhdGUgPSBbXTtcclxuICAgIH1cclxuICB9XHJcblxyXG5cclxufVxyXG4iXX0=