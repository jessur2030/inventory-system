import {Component, HostListener, Input, OnInit} from '@angular/core';
import {
  faFilePdf
} from '@fortawesome/free-solid-svg-icons';
import {PDFReportService} from "../pdfreport.service";

@Component({
  selector: 'Kit-button-pdf',
  templateUrl: './button-pdf.component.html',
  styleUrls: ['./button-pdf.component.css']
})
export class ButtonPdfComponent implements OnInit {
  @Input() id: string;
  faFilePdf = faFilePdf

  @HostListener('click', ['$event'])
  onClick(btn) {
    btn.stopPropagation()
    console.log(this.id)
    if(this.id) this.pdfService.generatePurchase(this.id)
  }

  constructor(private pdfService: PDFReportService) {
  }

  ngOnInit(): void {
  }

}
