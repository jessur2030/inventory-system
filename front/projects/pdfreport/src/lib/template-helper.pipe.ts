import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'templateHelper'
})
export class TemplateHelperPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {
    try {
      const table = `<tr>
            <td class="service">(SER-001) Azure Cloud Computing</td>
            <td class="unit">189,99 USD</td>
            <td class="qty">3</td>
            <td class="total">569,97 USD</td>
          </tr>`

      const tHead = `<thead>
          <tr>
            <th class="service">SERVICE</th>
            <th width="70">PRICE</th>
            <th>QTY</th>
            <th width="70">TOTAL</th>
          </tr>
        </thead>`;

      const tFoot = `<tr>
            <td colspan="3" class="grand total">TOTAL</td>
            <td class="grand total">569,97 USD</td>
          </tr>`;

      value = value.replace(/__CUSTOMER_NAME__/gi,
        `<span class="helper-variable">Elon Reeve</span>`);
      value = value.replace(/__CUSTOMER_LAST_NAME__/gi,
        `<span class="helper-variable">Musk</span>`);
      value = value.replace(/__CUSTOMER_ADDRESS__/gi,
        `<span class="helper-variable">3500 Deer Creek Road Palo Alto, CA 94304.</span>`);
      value = value.replace(/__CUSTOMER_EMAIL__/gi,
        `<span class="helper-variable">contact@tesla.com</span>`);
      value = value.replace(/__CUSTOMER_PHONE__/gi,
        `<span class="helper-variable">(650) 413-4000.</span>`);
      value = value.replace(/__PURCHASE_DATE__/gi,
        `<span class="helper-variable">30-08-2020</span>`);
      value = value.replace(/__PURCHASE_ORDER__/gi,
        `<span class="helper-variable">000001</span>`);
      value = value.replace(/__CUSTOMER_ID__/gi,
        `<span class="helper-variable">Y99999Y</span>`);
      value = value.replace(/__SELLER_NAME__/gi,
        `<span class="helper-variable">Bill Gates</span>`);
      value = value.replace(/__SELLER_EMAIL__/gi,
        `<span class="helper-variable">bill@msn.com</span>`);
      value = value.replace(/__PURCHASE_OBSERVATION__/gi,
        `<span class="helper-variable">Vigente por 5 días</span>`);
      value = value.replace(/__LOGO__/gi,
        `https://i.imgur.com/eEcLPNy.png`);
      value = value.replace(/__LIST_ITEMS__/gi,
        `<table class="helper-variable">${tHead}<tbody>${table}${tFoot}</tbody></table>`);
      return value
    } catch (e) {
      return null;
    }
  }

}
