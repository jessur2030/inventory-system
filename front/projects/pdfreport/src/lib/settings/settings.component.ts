import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DomSanitizer} from "@angular/platform-browser";
import {PDFReportService} from "../pdfreport.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {ModalCustomTemplateComponent} from "../modal-custom-template/modal-custom-template.component";

@Component({
  selector: 'pdf-report-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  content: any;
  userData: any;
  bsModalRef: BsModalRef;
  templatesList = [
    {
      name: 'Template 1',
      img: 'https://i.pinimg.com/564x/82/f1/f0/82f1f0848057cc71cc50dde53d7f4876.jpg'
    },
    {
      name: 'Template 2',
      img: 'https://i.pinimg.com/564x/cf/7b/c9/cf7bc90cb999d497d75ca58c69b3eacf.jpg'
    },
    {
      name: 'Template 3',
      img: 'https://i.pinimg.com/564x/db/4f/0b/db4f0bdf39abe9bb32bbe2f257ef6aef.jpg'
    }
  ]

  constructor( public sanitized: DomSanitizer,
              private modalService: BsModalService) {
  }

  ngOnInit(): void {

  }



  cbList() {

  }

  cbTrash() {

  }

  cbSave() {

  }

  public openUpdateModal = () => {
    const initialState: any = {
      section: this.content
    };
    this.bsModalRef = this.modalService.show(
      ModalCustomTemplateComponent,
      Object.assign({initialState}, {
        class: 'modal-edit-html',
        ignoreBackdropClick: false
      })
    );
  }
}
