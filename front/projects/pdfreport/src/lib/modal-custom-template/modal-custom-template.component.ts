import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {PDFReportService} from "../pdfreport.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BsModalRef} from "ngx-bootstrap/modal";
// import {EmailEditorComponent} from "angular-email-editor";

@Component({
  selector: 'Kit-modal-custom-template',
  templateUrl: './modal-custom-template.component.html',
  styleUrls: ['./modal-custom-template.component.css']
})
export class ModalCustomTemplateComponent implements OnInit, AfterViewInit {
  public form: FormGroup;
  section: any;
  sample = ''
  // @ViewChild(EmailEditorComponent)
  // private emailEditor: EmailEditorComponent;
  options = {};
  html: string
  data: any;

  constructor(private formBuilder: FormBuilder, private service: PDFReportService,
              private bsModalRef: BsModalRef, private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    const {pluginSetting} = this.section;
    this.form = this.formBuilder.group({
      html: ['', Validators.required]
    });
    // this.data = pluginSetting.plugin.features.pdfPurchase.html;
    this.form.patchValue({html: pluginSetting.plugin.features.pdfPurchase.html})

  }

  editorLoaded() {
    // this.emailEditor.editor.loadDesign(this.data);
  }

  saveDesign() {
    // this.emailEditor.editor.saveDesign(
    //   (data) => console.log('saveDesign', data)
    // );
  }

  exportHtml() {
    // this.emailEditor.editor.exportHtml(
    //   (data) => {
    //     // this.html = data.html
    //     console.log(data)
    //   }
    // );
  }

  ngAfterViewInit(): void {
    this.cdr.detectChanges();
  }

  public submit = () => {
    this.service.updateSettings(this.form.value);
    this.bsModalRef.hide()
  }


}
