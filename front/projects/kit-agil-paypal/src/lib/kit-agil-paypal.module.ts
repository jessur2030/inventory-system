import {NgModule} from '@angular/core';
import {KitAgilPaypalComponent} from './kit-agil-paypal.component';
import {SettingsComponent} from './settings/settings.component';
import {CallbackPayComponent} from './callback-pay/callback-pay.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

@NgModule({
  declarations: [KitAgilPaypalComponent, SettingsComponent, CallbackPayComponent],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule
  ],
  exports: [KitAgilPaypalComponent]
})
export class KitAgilPaypalModule {
}
