import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class KitAgilPaypalService {

  constructor() { }

  public getLink = (data) => {
    const {customData = {}} = data;
    let message = `(PAYPAL) Esta orden tiene vinculado un link de pago online, puedes compartir el siguiente enlace a tu cliente.`;
    message += `<br> <a href="${customData?.paypalLink?.href}" target="_blank">Ver link de pago</a>`
    return message;
  }
}
