import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {RestService} from "../../../../../src/app/services/rest.service";
import {ShareService} from "../../../../../src/app/services/share.service";

@Component({
  selector: 'lib-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {


  public form: FormGroup;
  public data: any;

  constructor(private formBuilder: FormBuilder, private rest: RestService, public shared: ShareService) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      clientID: ['', Validators.required],
      secretID: ['', Validators.required],
      mode: ['test', Validators.required]
    });
    this.load()
  }

  onSubmit() {
    this.rest.postEvents(`mercadopago/events/update_setting`, {
      value: this.form.value
    }).subscribe(res => {
      // this.data = res;
      // this.form.reset()
    })
  }

  load = () => {
    this.rest.getEvents(`mercadopago/events/check_keys`).subscribe(res => {
      this.data = res;
      if(this.data){
        this.form.patchValue(res.keys)
        if (this.data && !this.data.validCurrency) {
          this.form.disable()
        }
      }
    })
  }
}
