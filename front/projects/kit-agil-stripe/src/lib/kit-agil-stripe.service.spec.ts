import { TestBed } from '@angular/core/testing';

import { KitAgilStripeService } from './kit-agil-stripe.service';

describe('KitAgilStripeService', () => {
  let service: KitAgilStripeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KitAgilStripeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
