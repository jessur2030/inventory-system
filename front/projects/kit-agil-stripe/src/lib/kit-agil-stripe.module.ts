import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {KitAgilStripeComponent} from './kit-agil-stripe.component';
import {KitAgilStripeService} from "./kit-agil-stripe.service";
import {SettingsComponent} from './settings/settings.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {CheckoutComponent} from './checkout/checkout.component';
import {KitAgilRoutingModule} from "./kit-agil-routing.module";

@NgModule({
  declarations: [KitAgilStripeComponent, SettingsComponent, CheckoutComponent],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    KitAgilRoutingModule
  ],
  exports: [KitAgilStripeComponent]
})
export class KitAgilStripeModule {
  constructor(@Optional() @SkipSelf() parentModule?: KitAgilStripeModule) {
    if (parentModule) {
      throw new Error('GreetingModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(config: KitAgilStripeService): ModuleWithProviders<KitAgilStripeModule> {
    return {
      ngModule: KitAgilStripeModule,
      providers: [
        {provide: KitAgilStripeService, useValue: config}
      ]
    };
  }
}
