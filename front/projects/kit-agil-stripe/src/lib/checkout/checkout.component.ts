import {Component, OnInit} from '@angular/core';
import {ScriptKitAgilService} from "../script-kit-agil.service";
import {KitAgilStripeService} from "../kit-agil-stripe.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Observable, throwError} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'lib-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  public content: any;
  public addForm: FormGroup;
  private elementStripe: any = null;
  private STRIPE: any;
  private cardObject: any = null;
  private cardCVC: any = null;
  private cardEXP: any = null;
  public loading: boolean
  public error: any
  public pluginActive: boolean = true

  constructor(private script: ScriptKitAgilService, private config: KitAgilStripeService,
              private formBuilder: FormBuilder, public http: HttpClient) {
  }

  ngOnInit(): void {
    this.renderStyle()
    this.script.loadScript('stripe').then(r => {
      this.STRIPE = this.script.WrapperWindow().Stripe(this.content.pk.find(() => true));
      if (!this.content.status.includes('succ')) {
        this.createStripeElement()
      }
    }).catch(r => {
      this.error = {message: 'Error. Must config keys or HTTPS'}
      this.pluginActive = false
    })

    this.addForm = this.formBuilder.group({
      _id: ['', Validators.required],
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });

    this.addForm.patchValue({_id: this.content.customer._id})

  }

  public renderStyle = () => {
    const body = document.querySelector('body')
    const container = document.querySelector('.container-row')
    container.classList.remove('container-row')
    body.style.backgroundColor = 'white'
  }

  private createStripeElement = () => {
    this.elementStripe = this.STRIPE.elements({
      fonts: [
        {
          cssSrc: 'https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,500;1,400;1,500&display=swap',
        }
      ]
    });

    let cardNumberLabel = '4242 4242 4242 4242';
    let cardEXP = 'MM / AA';
    let cardCVC = 'CVC';
    const stripeStyle = {
      style: {
        base: {
          iconColor: '#666EE8',
          color: '#31325F',
          fontFamily: '\'Montserrat\'',
          fontSize: '1.2rem',
          padding: '1rem',
          // fontStyle: 'italic',
          // textAlign: 'center',
          '::placeholder': {
            color: '#E2E0EB',
          },
        },
      },
      placeholder: cardNumberLabel
    };
    const cardNumber = this.elementStripe.create('cardNumber', stripeStyle);
    const cardExp = this.elementStripe.create('cardExpiry', {...stripeStyle, ...{placeholder: cardEXP}});
    const cardCvc = this.elementStripe.create('cardCvc', {...stripeStyle, ...{placeholder: cardCVC}});
    cardNumber.mount('#card_number');
    cardExp.mount('#card_exp');
    cardCvc.mount('#card_cvc');
    this.cardObject = cardNumber;
    this.cardCVC = cardCVC
    this.cardEXP = cardEXP
  };

  pay = () => {
    this.error = null
    this.beginProcess()
  }

  disableAll = () => this.addForm.disable();

  enableAll = () => this.addForm.enable();


  beginProcess = () => {
    this.disableAll();
    this.loading = true;
    this.STRIPE.createToken(this.cardObject)
      .then(res => {
        this.loading = false;
        this.enableAll();
        if (res.error) {
          this.error = res.error
          throw res;
        } else {
          this.payIntent({
            _id: this.addForm.value._id,
            email: this.addForm.value.email,
            token: res.token.id,
            tracker: this.content.customData.tracker
          });
        }
      })
      .catch(err => {
        this.loading = false;
        this.error = {message:err.error.message};
        this.enableAll();
      });
  };


  handlePi = (pi = '') => {
    this.loading = true;
    return new Promise((resolve, reject) => {
      this.STRIPE.handleCardPayment(pi,
        this.cardObject,
        {
          payment_method_data: {}
        }
      ).then(a => {
        this.loading = false;
        if (a.error) {
          reject(a.error);
        } else {
          resolve(a);
        }
      }).catch(err => {
        this.loading = false;
        reject(err);
      });
    });
  };


  payIntent = (data) => {
    this.postEvents(`stripe/events/process_pay`, {
      "value": {
        "tracker": data.tracker,
        "user": {
          "email": data.email,
          "name": data.name,
          "_id": data._id
        },
        token: data.token
      }
    }).subscribe(response => {
      this.addForm.disable()
      this.handlePi(response.client_secret).then((o: any) => {
        const {paymentIntent} = o
        this.updatePay({pi: paymentIntent.id, tracker: data.tracker})
        this.addForm.reset()
        this.cardObject.clear()

      }).catch(error2 => {
        this.addForm.enable()
        this.error = error2
      })
    }, error => {
      this.error = {message: 'Algo ocurrio! actualiza la pagina y vuelve a intentarlo'}
    })
  };

  updatePay = (value) => {
    this.postEvents(`stripe/events/update_pay`, {
      "value": value
    }).subscribe(res => window.location.reload(),
      error1 => this.error = {message: 'Algo ocurrio! actualiza la pagina y vuelve a intentarlo'})
  }

  postEvents(path = '', body = {}, toast = true, header: any = null): Observable<any> {
    try {
      return this.http.post(`${this.config.api}/plugins/public/${path}`, body)
    } catch (e) {
      console.log(e)
    }
  }


}
