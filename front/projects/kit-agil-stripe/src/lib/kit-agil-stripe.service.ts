import {Injectable, Optional} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class KitAgilStripeService {
  pk = '';
  sk = '';
  api = '';

  constructor() {
  }

}

export class UserService {
  private pk = '';
  private sk = '';
  private api = '';

  constructor(@Optional() config?: KitAgilStripeService) {
    if (config) {
      this.pk = config.pk;
      this.sk = config.sk;
      this.api = config.api;
    }
  }
}
