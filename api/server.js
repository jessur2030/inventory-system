require('dotenv-safe').config()
const express = require('express')
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser')
const morganBody = require('morgan-body')
const compression = require('compression')
const helmet = require('helmet')
const cors = require('cors')
const passport = require('passport')
const app = express()
const i18n = require('i18n')
const initMongo = require('./config/mongo')
const path = require('path')
const http = require('http');
const {loggerSlack} = require('./config/logger')
require('./app/plugins')
// Setup express server port from ENV, default: 3000
app.set('port', process.env.PORT || 3001)


// for parsing json
app.use(bodyParser.json({
  limit: '80mb',
  verify: function (req, res, buf) {
    const agent = req.headers['user-agent'].toLowerCase();
    if (agent.includes('stripe')) {
      req.rawBody = buf.toString();
    }
  }
}));

// for parsing application/x-www-form-urlencoded
app.use(
  bodyParser.urlencoded({
    limit: '80mb',
    extended: true
  })
)
// for log send to slack

morganBody(app, {
  skip: function (req, res) {
    return ([403, 404, 409, 401].includes(res.statusCode) || res.statusCode < 400)
  },
  stream: loggerSlack
})

// i18n
i18n.configure({
  locales: ['en', 'es'],
  directory: `${__dirname}/locales`,
  defaultLocale: 'en',
  objectNotation: true
})
app.use(i18n.init)

// Init all other stuff
app.use(cors())
app.use(passport.initialize())
app.use(compression())
app.use(helmet())
app.use(cookieParser());
app.use(express.static('public'))
app.set('views', path.join(__dirname, 'views'))
app.engine('html', require('ejs').renderFile)
app.set('view engine', 'html')
app.use('/api/1.0', require('./app/routesApi'))
app.use('/', require('./app/routes'))
http.createServer(app).listen(process.env.PORT);
// Init MongoDB
initMongo(app)

module.exports = app // for testing
