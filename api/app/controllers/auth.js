const _ = require('lodash');
const mongoose = require('mongoose');
const {activePlugins} = require('./plugins');
const jwt = require('jsonwebtoken')
const Settings = require('../models/settings')
const ReferredUser = require('../models/referredUsers')
const User = require('../models/user')
const UserAccess = require('../models/userAccess')
const ForgotPassword = require('../models/forgotPassword')
const utils = require('../middleware/utils')
const uuid = require('uuid')
const {addHours} = require('date-fns')
const {matchedData} = require('express-validator')
const auth = require('../middleware/auth')
const emailer = require('../middleware/emailer')
const facebookProvider = require('../service/oauth.facebook')
const googleProvider = require('../service/oauth.google')
const HOURS_TO_BLOCK = 2
const LOGIN_ATTEMPTS = 5

/*********************
 * Private functions *
 *********************/
/**
 *
 */

const findByEmailTenant = (tenant, emails, data) => {
  return new Promise(resolve => {
    User.byTenant(tenant).findOneAndUpdate({
      email: {"$in": emails}
    }, data, {
      new: true
    }, (err, item) => {
      resolve(item)
    })
  })
}

/**
 * Generates a token
 * @param {Object} user - user object
 */
const generateToken = (user) => {
  // Gets expiration time
  const expiration =
    Math.floor(Date.now() / 1000) + 60 * process.env.JWT_EXPIRATION_IN_MINUTES

  // returns signed and encrypted token
  return auth.encrypt(
    jwt.sign(
      {
        data: {
          _id: user
        },
        exp: expiration
      },
      process.env.JWT_SECRET
    )
  )
}
const findUserByRefCode = (referredCode) => {
  return new Promise((resolve, reject) => {
    User.findOne(
      {
        referredCode
      },
      (err, item) => {
        utils.itemNotFound(err, item, reject, 'USER_DOES_NOT_EXIST')
        resolve(item)
      }
    )
  })
}


/**
 * Save refcode
 */

const registerUserReferred = async (codeRef, userTo) => {
  if (codeRef) {
    const referredUser = await findUserByRefCode(codeRef)
    const body = {
      userTo: userTo._id,
      userFrom: referredUser._id,
      amountFrom: 1,
      amountTo: 1
    }
    await ReferredUser.create(body)
  }
}

/**
 * Creates an object with user info
 * @param {Object} req - request object
 */
const setUserInfo = (req) => {
  let user = {
    _id: req._id,
    name: req.name,
    email: req.email,
    role: req.role,
    verified: req.verified,
    stepper: req.stepper,
    avatar: req.avatar,
    settings: req.settings,
    referredCode: req.referredCode
  }
  // Adds verification for testing purposes
  if (process.env.NODE_ENV !== 'production') {
    user = {
      ...user,
      verification: req.verification
    }
  }
  return user
}

/**
 * Saves a new user access and then returns token
 * @param {Object} req - request object
 * @param {Object} user - user object
 */
const saveUserAccessAndReturnToken = async (req, user) => {
  return new Promise((resolve, reject) => {
    const tenant = req.clientAccount;
    const userAccess = new UserAccess({
      email: user.email,
      ip: utils.getIP(req),
      browser: utils.getBrowserInfo(req),
      country: utils.getCountry(req)
    })
    userAccess.save(async (err) => {
      if (err) {
        reject(utils.buildErrObject(422, err.message))
      }
      const userInfo = setUserInfo(user)
      // Returns data with access token
      resolve({
        session: generateToken(user._id),
        user: userInfo,
        settings: await getSettings(tenant)
      })
    })
  })
}

/**
 * Blocks a user by setting blockExpires to the specified date based on constant HOURS_TO_BLOCK
 * @param {Object} user - user object
 */
const blockUser = async (user) => {
  return new Promise((resolve, reject) => {
    user.blockExpires = addHours(new Date(), HOURS_TO_BLOCK)
    user.save((err, result) => {
      if (err) {
        reject(utils.buildErrObject(422, err.message))
      }
      if (result) {
        resolve(utils.buildErrObject(409, 'BLOCKED_USER'))
      }
    })
  })
}

/**
 * Saves login attempts to dabatabse
 * @param {Object} user - user object
 */
const saveLoginAttemptsToDB = async (user) => {
  return new Promise((resolve, reject) => {
    user.save((err, result) => {
      if (err) {
        reject(utils.buildErrObject(422, err.message))
      }
      if (result) {
        resolve(true)
      }
    })
  })
}

/**
 * Checks that login attempts are greater than specified in constant and also that blockexpires is less than now
 * @param {Object} user - user object
 */
const blockIsExpired = (user) =>
  user.loginAttempts > LOGIN_ATTEMPTS && user.blockExpires <= new Date()

/**
 *
 * @param {Object} user - user object.
 */
const checkLoginAttemptsAndBlockExpires = async (user) => {
  return new Promise((resolve, reject) => {
    // Let user try to login again after blockexpires, resets user loginAttempts
    if (blockIsExpired(user)) {
      user.loginAttempts = 0
      user.save((err, result) => {
        if (err) {
          reject(utils.buildErrObject(422, err.message))
        }
        if (result) {
          resolve(true)
        }
      })
    } else {
      // User is not blocked, check password (normal behaviour)
      resolve(true)
    }
  })
}

/**
 * Checks if blockExpires from user is greater than now
 * @param {Object} user - user object
 */
const userIsBlocked = async (user) => {
  return new Promise((resolve, reject) => {
    if (user.blockExpires > new Date()) {
      reject(utils.buildErrObject(409, 'BLOCKED_USER'))
    }
    resolve(true)
  })
}

/**
 * Finds user by email
 * @param {string} email - user´s email
 * @param tenant
 */
const findUser = async (email, tenant = null) => {
  return new Promise((resolve, reject) => {
    User.byTenant(tenant).findOne(
      {
        email
      },
      'password loginAttempts blockExpires name email avatar stepper role verified verification',
      (err, item) => {
        utils.itemNotFound(err, item, reject, 'USER_DOES_NOT_EXIST')
        resolve(item)
      }
    )
  })
}

/**
 * Finds user by ID
 * @param {string} id - user´s id
 */

/**
 * Adds one attempt to loginAttempts, then compares loginAttempts with the constant LOGIN_ATTEMPTS, if is less returns wrong password, else returns blockUser function
 * @param {Object} user - user object
 */
const passwordsDoNotMatch = async (user) => {
  user.loginAttempts += 1
  await saveLoginAttemptsToDB(user)
  return new Promise((resolve, reject) => {
    if (user.loginAttempts <= LOGIN_ATTEMPTS) {
      resolve(utils.buildErrObject(409, 'WRONG_PASSWORD'))
    } else {
      resolve(blockUser(user))
    }
    reject(utils.buildErrObject(422, 'ERROR'))
  })
}

/**
 * Registers a settings user in database
 * @param {Object} req - request object
 */
const registerSettings = async (req, tenant = null) => {
  return new Promise((resolve, reject) => {
    const model = Settings.byTenant(tenant);
    const settings = new model({
      name: req.name,
      currency: req.currency,
      logo: req.logo,
      owner: mongoose.Types.ObjectId(req.owner)
    })
    settings.save((err, item) => {
      if (err) {
        reject(utils.buildErrObject(422, err.message))
      }
      resolve(item)
    })
  })
}

/**
 * Get settings by tenant
 */

const getSettings = async (tenant = null) => {
  return new Promise((resolve, reject) => {
    Settings
      .byTenant(tenant)
      .findOne(
        {}, 'currency currencySymbol logo name tenantId plugins.path language planDate',
        (err, user) => {
          if (err) {
            reject(utils.buildErrObject(422, 'NOT_SETTINGS_FOR_TENANT'))
          } else {
            resolve(user)
          }
        }
      )
  })
}

const checkExist = async (tenant = null) => {
  return new Promise((resolve, reject) => {
    Settings
      .byTenant(tenant)
      .findOne(
        {}, 'tenantId logo name',
        (err, user) => {
          if (err) {
            reject(utils.buildErrObject(422, 'NOT_SETTINGS_FOR_TENANT'))
          } else {
            resolve(user)
          }
        }
      )
  })
}


/**
 * Registers a new user in database
 * @param {Object} req - request object
 */
const registerUser = async (req, tenant = null) => {
  return new Promise((resolve, reject) => {
    const model = User.byTenant(tenant);
    const user = new model({
      name: req.name,
      email: req.email,
      password: req.password,
      role: "admin",
      verification: uuid.v4()
    })
    user.save((err, item) => {
      if (err) {
        reject(utils.buildErrObject(422, err.message))
      }
      resolve(item)
    })
  })
}

/**
 * Builds the registration token
 * @param {Object} item - user object that contains created id
 * @param {Object} userInfo - user object
 * @param tenant
 */
const returnRegisterToken = async (item, userInfo, tenant) => {
  if (process.env.NODE_ENV !== 'production') {
    userInfo.verification = item.verification
  }
  const data = {
    session: generateToken(item._id),
    user: userInfo,
    settings: await getSettings(tenant)
  }
  return data
}

/**
 * Checks if verification id exists for user
 * @param {string} id - verification id
 * @param tenant
 */
const verificationExists = async (id, tenant = null) => {
  return new Promise((resolve, reject) => {
    User
      .byTenant(tenant)
      .findOne(
        {
          verification: id,
          verified: false
        },
        (err, user) => {
          utils.itemNotFound(err, user, reject, 'NOT_FOUND_OR_ALREADY_VERIFIED')
          resolve(user)
        }
      )
  })
}

/**
 * Verifies an user
 * @param {Object} user - user object
 */
const verifyUser = async (user) => {
  return new Promise((resolve, reject) => {
    user.verified = true
    user.save((err, item) => {
      if (err) {
        reject(utils.buildErrObject(422, err.message))
      }
      resolve({
        email: item.email,
        verified: item.verified
      })
    })
  })
}

/**
 * Marks a request to reset password as used
 * @param {Object} req - request object
 * @param {Object} forgot - forgot object
 */
const markResetPasswordAsUsed = async (req, forgot) => {
  return new Promise((resolve, reject) => {
    forgot.used = true
    forgot.ipChanged = utils.getIP(req)
    forgot.browserChanged = utils.getBrowserInfo(req)
    forgot.countryChanged = utils.getCountry(req)
    forgot.save((err, item) => {
      utils.itemNotFound(err, item, reject, 'NOT_FOUND')
      resolve(utils.buildSuccObject('PASSWORD_CHANGED'))
    })
  })
}

/**
 * Updates a user password in database
 * @param {string} password - new password
 * @param {Object} user - user object
 */
const updatePassword = async (password, user) => {
  return new Promise((resolve, reject) => {
    user.password = password
    user.save((err, item) => {
      utils.itemNotFound(err, item, reject, 'NOT_FOUND')
      resolve(item)
    })
  })
}

/**
 * Finds user by email to reset password
 * @param {string} email - user email
 */
const findUserToResetPassword = async (email, tenant = null) => {
  return new Promise((resolve, reject) => {
    User.byTenant(tenant).findOne(
      {
        email
      },
      (err, user) => {
        utils.itemNotFound(err, user, reject, 'NOT_FOUND')
        resolve(user)
      }
    )
  })
}

/**
 * Checks if a forgot password verification exists
 * @param {string} id - verification id
 */
const findForgotPassword = async (id, tenant = null) => {
  return new Promise((resolve, reject) => {
    ForgotPassword.byTenant(tenant).findOne(
      {
        verification: id,
        used: false
      },
      (err, item) => {
        utils.itemNotFound(err, item, reject, 'NOT_FOUND_OR_ALREADY_USED')
        resolve(item)
      }
    )
  })
}

/**
 * Creates a new password forgot
 * @param {Object} req - request object
 * @param tenant
 */
const saveForgotPassword = async (req, tenant = null) => {
  return new Promise((resolve, reject) => {
    const model = ForgotPassword.byTenant(tenant);
    const forgot = new model({
      email: req.body.email,
      verification: uuid.v4(),
      ipRequest: utils.getIP(req),
      browserRequest: utils.getBrowserInfo(req),
      countryRequest: utils.getCountry(req)
    })
    forgot.save((err, item) => {
      if (err) {
        reject(utils.buildErrObject(422, err.message))
      }
      resolve(item)
    })
  })
}

/**
 * Builds an object with created forgot password object, if env is development or testing exposes the verification
 * @param {Object} item - created forgot password object
 */
const forgotPasswordResponse = (item) => {
  let data = {
    msg: 'RESET_EMAIL_SENT',
    email: item.email
  }
  if (process.env.NODE_ENV !== 'production') {
    data = {
      ...data,
      verification: item.verification
    }
  }
  return data
}

/**
 * Checks against user if has quested role
 * @param {Object} data - data object
 * @param {*} next - next callback
 */
const checkPermissions = async (data, next) => {
  return new Promise((resolve, reject) => {
    User.byTenant(data.tenant).findById(data.id, (err, result) => {
      utils.itemNotFound(err, result, reject, 'CHECK_PERMISSION_NOT_FOUND')
      if ((result) && data.roles.indexOf(result.role) > -1) {
        return resolve(next())
      }
      return reject(utils.buildErrObject(403, 'UNAUTHORIZED'))
    })
  })
}

/**
 * Gets user id from token
 * @param {string} token - Encrypted and encoded token
 */
const getUserIdFromToken = async (token) => {
  return new Promise((resolve, reject) => {
    // Decrypts, verifies and decode token
    jwt.verify(auth.decrypt(token), process.env.JWT_SECRET, (err, decoded) => {
      if (err) {
        reject(utils.buildErrObject(409, 'BAD_TOKEN'))
      }
      resolve(decoded.data._id)
    })
  })
}

/********************
 * Public functions *
 ********************/

/**
 * Login function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.login = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    if (!tenant) return utils.handleError(res, utils.buildErrObject(404, 'Tenant not found'))
    const data = matchedData(req)
    const user = await findUser(data.email, tenant)
    await userIsBlocked(user)
    await checkLoginAttemptsAndBlockExpires(user)
    const isPasswordMatch = await auth.checkPassword(data.password, user)
    if (!isPasswordMatch) {
      utils.handleError(res, await passwordsDoNotMatch(user))
    } else {
      // all ok, register access and return token
      user.loginAttempts = 0
      await saveLoginAttemptsToDB(user)
      res.status(200).json(await saveUserAccessAndReturnToken(req, user))
    }
  } catch (error) {
    utils.handleError(res, error)
  }
}


/**
 * Login Facebook passport ----> FRONT  origin ---> cookie "tenant"
 */
exports.loginFb = (req, res, next) => {
  const data = matchedData(req)
  res.cookie('tenant', data.tenant, {expire: new Date() + 9999})
  return facebookProvider(req).authenticate('facebook', {scope: ['public_profile', 'email']})(req, res, next)
}

/**
 * Login Facebook passport
 */
exports.loginGoogle = (req, res, next) => {
  const data = matchedData(req)
  res.cookie('tenant', data.tenant, {expire: new Date() + 9999})
  return googleProvider(req).authenticate('google', {
    scope: [
      'https://www.googleapis.com/auth/userinfo.profile',
      'https://www.googleapis.com/auth/userinfo.email'
    ]
  })(req, res, next)
}

/**
 * Login Facebook Callback
 */
exports.loginCbFb = (req, res, next) => {
  return facebookProvider(req).authenticate('facebook', {failureRedirect: '/'},
    async (rq, rs) => {
      const {cookies} = req
      const tenant = cookies.tenant
      if (!rq) {
        const {emails} = rs
        const dataJson = rs._json //picture
        const emailsArray = _.map(emails, 'value')
        const urlRedirect = process.env.FRONTEND_URL_TENANT.replace(/__TENANT__/gi, tenant);
        const user = await findByEmailTenant(tenant, emailsArray, {
          avatar: (dataJson && dataJson.picture) ? dataJson.picture : '',
          socialNetwork: [
            {provider: 'facebook', id: rs.id}
          ]
        })
        if (!user) {
          res.redirect(`${urlRedirect}/oauth/login?error=USER_DOES_NOT_EXIST`);
        } else {
          const token = generateToken(user._id)
          res.redirect(`${urlRedirect}/oauth/callback?token=${token}`);
        }
      } else {
        res.redirect('/');
      }
    })(req, res, next)
}

/**
 * Login Google Callback
 */
exports.loginCbGoogle = (req, res, next) => {
  return googleProvider(req).authenticate('google', {failureRedirect: '/', session: false},
    async (rq, rs) => {
      if (!rq) {
        const {cookies} = req
        const tenant = cookies.tenant
        const {emails} = rs
        const dataJson = rs._json //picture
        const emailsArray = _.map(emails, 'value')
        const urlRedirect = process.env.FRONTEND_URL_TENANT.replace(/__TENANT__/gi, tenant);
        const user = await findByEmailTenant(tenant, emailsArray, {
          avatar: (dataJson && dataJson.picture) ? dataJson.picture : '',
          socialNetwork: [
            {provider: 'google', id: rs.id}
          ]
        })
        if (!user) {
          res.redirect(`${urlRedirect}/oauth/login?error=USER_DOES_NOT_EXIST`);
        } else {
          const token = generateToken(user._id)
          res.redirect(`${urlRedirect}/oauth/callback?token=${token}`);
        }
      } else {
        res.redirect('/');
      }
    })(req, res, next)
}


/**
 * Register function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.register = async (req, res) => {
  try {
    // Gets locale from header 'Accept-Language'
    const tenant = req.clientAccount;
    // const locale = req.getLocale()
    req = matchedData(req)
    // Detectamos si existe el tenant
    const exist = await checkExist(tenant)
    // console.log(exist)
    if (exist) return utils.handleError(res, utils.buildErrObject(422, 'Tenant alredy exist'))
    const doesEmailExists = await emailer.emailExists(req.email, tenant)
    if (!doesEmailExists) {
      const item = await registerUser(req, tenant)
      const settingsParameters = {
        name: req.name,
        currency: null,
        logo: null,
        owner: item._id,
      };
      await registerSettings(settingsParameters, tenant)
      await activePlugins(['excelImport', 'pdfReport'], tenant)
      const userInfo = setUserInfo(item)
      await registerUserReferred(req.userReferred, item)
      const response = await returnRegisterToken(item, userInfo, tenant)
      res.status(201).json(response)
    }
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Verify function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.verify = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    req = matchedData(req)
    const user = await verificationExists(req.id, tenant)
    res.status(200).json(await verifyUser(user))
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Forgot password function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.forgotPassword = async (req, res) => {
  try {
    // Gets locale from header 'Accept-Language'
    const tenant = req.clientAccount;
    const locale = req.getLocale()
    const data = matchedData(req)
    await findUser(data.email, tenant)
    const item = await saveForgotPassword(req, tenant)
    await emailer.sendResetPasswordEmailMessage(locale, item, tenant)
    res.status(200).json(forgotPasswordResponse(item))
  } catch (error) {
    console.log(error)
    utils.handleError(res, error)
  }
}

/**
 * Reset password function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.resetPassword = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    const data = matchedData(req)
    const forgotPassword = await findForgotPassword(data.id, tenant)
    const user = await findUserToResetPassword(forgotPassword.email, tenant)
    await updatePassword(data.password, user)
    const result = await markResetPasswordAsUsed(req, forgotPassword)
    res.status(200).json(result)
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Refresh token function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getRefreshToken = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    const tokenEncrypted = req.headers.authorization
      .replace('Bearer ', '')
      .trim();
    let userId = await getUserIdFromToken(tokenEncrypted)
    userId = await utils.isIDGood(userId)
    const user = await this.findUserById(userId, tenant)
    const token = await saveUserAccessAndReturnToken(req, user)

    // Removes user info from response
    if (req.parentAccount) {
      token.parentAccount = req.parentAccount;
    }
    token.settings = await getSettings(tenant)
    token.plugins = await utils.getPlugins(tenant)
    // delete token.user
    res.status(200).json(token)
  } catch (error) {
    utils.handleError(res, error)
  }
}

exports.checkExist = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    res.status(200).json(await checkExist(tenant))
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Roles authorization function called by route
 * @param {Array} roles - roles specified on the route
 */
exports.roleAuthorization = (roles) => async (req, res, next) => {
  try {
    const data = {
      id: req.user._id,
      roles,
      tenant: req.clientAccount
    }
    await checkPermissions(data, next)
  } catch (error) {
    utils.handleError(res, error)
  }
}


exports.findUserById = async (userId, tenant = null) => {
  return new Promise((resolve, reject) => {
    User.byTenant(tenant).findById(userId, (err, item) => {
      utils.itemNotFound(err, item, reject, 'USER_DOES_NOT_EXIST')
      resolve(item)
    })
  })
}


/**
 * Creates a new password forgot
 * @param {Object} req - request object
 * @param tenant
 */
exports.saveForgotPasswordExport = async (req, tenant = null) => {
  return new Promise((resolve, reject) => {
    const model = ForgotPassword.byTenant(tenant);
    const forgot = new model({
      email: req.body.email,
      verification: uuid.v4(),
      ipRequest: utils.getIP(req),
      browserRequest: utils.getBrowserInfo(req),
      countryRequest: utils.getCountry(req)
    })
    forgot.save((err, item) => {
      if (err) {
        reject(utils.buildErrObject(422, err.message))
      }
      resolve(item)
    })
  })
}
