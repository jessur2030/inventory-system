const { validationResult } = require('../middleware/utils')
const { check } = require('express-validator')


/**
 * Validates get item request
 */

exports.getHooks = [
  check('value').optional(),
  (req, res, next) => {
    validationResult(req, res, next)
  }
]
