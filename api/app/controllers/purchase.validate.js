const { validationResult } = require('../middleware/utils')
const { check } = require('express-validator')

/**
 * Validates create new item request
 */
exports.createItem = [
  check('customer')
    .exists(),
  check('items')
    .optional(),
  check('status')
    .optional(),
  check('deliveryDate')
    .exists(),
  check('deliveryAddress')
    .exists(),
  check('invoiceAddress')
    .optional(),
  check('total')
    .optional(),
  check('subTotal')
    .optional(),
  check('sumTaxesTotal')
    .optional(),
  check('taxes')
    .optional(),
  check('attachment')
    .optional(),
  check('deliveryType')
    .exists()
    .withMessage('MISSING')
    .not()
    .isEmpty()
    .withMessage('IS_EMPTY')
    .trim(),
  check('description')
    .optional(),
  (req, res, next) => {
    validationResult(req, res, next)
  }
]

/**
 * Validates update item request
 */
exports.updateItem = [
  check('customer')
    .exists(),
  check('items')
    .optional(),
  check('status')
    .optional(),
  check('deliveryDate')
    .optional(),
  check('deliveryAddress')
    .exists(),
  check('invoiceAddress')
    .optional(),
  check('subTotal')
    .optional(),
  check('total')
    .optional(),
  check('author')
    .optional(),
  check('sumTaxesTotal')
    .optional(),
  check('taxes')
    .optional(),
  check('attachment')
    .optional(),
  check('deliveryType')
    .exists()
    .withMessage('MISSING')
    .not()
    .isEmpty()
    .withMessage('IS_EMPTY')
    .trim(),
  check('description')
    .optional(),
  check('id')
    .exists()
    .withMessage('MISSING')
    .not()
    .isEmpty()
    .withMessage('IS_EMPTY'),
  (req, res, next) => {
    validationResult(req, res, next)
  }
]
/**
 * Validates update item request
 */
exports.updateItemConvert = [
  check('id')
    .exists()
    .withMessage('MISSING')
    .not()
    .isEmpty()
    .withMessage('IS_EMPTY'),
  check('convert')
    .exists(),
  check('total')
    .optional(),
  check('subTotal')
    .optional(),
  check('sumTaxesTotal')
    .optional(),
  check('taxes')
    .optional(),
  check('items')
    .exists(),
  (req, res, next) => {
    validationResult(req, res, next)
  }
]
/**
 * Validates get item request
 */
exports.getItem = [
  check('id')
    .exists()
    .withMessage('MISSING')
    .not()
    .isEmpty()
    .withMessage('IS_EMPTY'),
  (req, res, next) => {
    validationResult(req, res, next)
  }
]

/**
 * Validates delete item request
 */
exports.deleteItem = [
  check('id')
    .exists()
    .withMessage('MISSING')
    .not()
    .isEmpty()
    .withMessage('IS_EMPTY'),
  (req, res, next) => {
    validationResult(req, res, next)
  }
]
