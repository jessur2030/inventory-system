const moment = require('moment')
// const model = require('../models/clientsMachine')
const notificationService = require('../service/notification')
const Settings = require('../models/settings')
const stripe = require('../service/stripe')
const Products = require('../models/products')
const Orders = require('../models/orders')
const {matchedData} = require('express-validator')
const utils = require('../middleware/utils')
const db = require('../middleware/db')

/*********************
 * Private functions *
 *********************/
const addMonth = (object, numMonth = 1) => {
  try {
    if (object) {
      const currentDate = moment(object.planDate.createdAt)
      const newData = currentDate.add(numMonth, 'month')
      object.planDate.createdAt = newData.toDate();
      object.save();
    }
  } catch (e) {
    return null
  }
}
/**
 * Gets all items from database
 */
const getAllItemsFromDB = async (tenant = null) => {
  return new Promise((resolve, reject) => {
    Products.byTenant(tenant).find(
      {},
      '-updatedAt -createdAt',
      {
        sort: {
          name: 1
        }
      },
      (err, items) => {
        if (err) {
          reject(utils.buildErrObject(422, err.message))
        }
        resolve(items)
      }
    )
  })
}


/********************
 * Public functions *
 ********************/

/**
 * Get all items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 **/

exports.getProducts = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    res.status(200).json(await getAllItemsFromDB(tenant))
  } catch (error) {
    utils.handleError(res, error)
  }
}


/**
 *
 */

exports.validToken = async (req, res) => {
  try {
    res.status(200).json({
      status: 'valid',
      client: req.clientMachine
    })
  } catch (error) {
    utils.handleError(res, error)
  }
}


/* Hook Stripe */
exports.getHookStripe = async (req, res) => {
  try {
    const locale = req.getLocale();
    const {body} = req
    const {customer, invoicePdf} = await stripe.getHookStripe(body) || {customer: null};
    console.log(invoicePdf)
    const dataTenant = await Settings.findOne({
      'payment.cardUser': {$eq: customer, $exists: true}
    }, 'planDate tenantId');
    dataTenant.set('title', `NOTIFICATIONS.SUBSCRIPTION_PAID`, {strict: false})
    const evenHTML = 'Descargar PDF <a target="_blank" href="' + invoicePdf + '">Link</a>';
    dataTenant.set('body', `Plan renovado exitosamente sigue disfrutando de todo el poder`, {strict: false})
    dataTenant.set('event', evenHTML, {strict: false})

    addMonth(dataTenant, 1)

    await notificationService.sendNotifications(dataTenant, null, locale)
    res.status(200).json({data: dataTenant})
  } catch (error) {
    utils.handleError(res, error)
  }
}


/* Hook Stripe Per Tenant */
exports.getHookStripePer = async (req, res) => {
  try {
    /**
     * Tenemos que guardar el id del pedido de stripe para al momento de regresar el hook
     * saber exactamente cual es el pago que esta realizando
     */
    const locale = req.getLocale();
    const {body} = req
    const {customer, idOrder, status} = await stripe.getHookStripePer(body) || {customer: null};
    const orders = await Orders.findOneAndUpdate({_id: idOrder}, {status});
    const dataTenant = await Settings.findOne({
      'payment.cardUser': {$eq: customer, $exists: true}
    }, 'planDate tenantId');
    const urlRedirect = process.env.FRONTEND_URL_TENANT.replace(/__TENANT__/gi, dataTenant.tenantId);

    dataTenant.set('title', `NOTIFICATIONS.ORDER_PAYMENT`, {strict: false})
    const evenHTML = `<a href="${urlRedirect}/orders/${idOrder}" target="_blank">Ver orden</a>`;
    dataTenant.set('body', `Se ha realizado un cobro recurrente al cliente proveniente de la orden`, {strict: false})
    dataTenant.set('event', evenHTML, {strict: false})
    //
    // addMonth(dataTenant, 1)
    // https://leangasoftaware.kitagil.com/orders/6048e079fdedcb7a56fbb027
    await notificationService.sendNotifications(dataTenant, null, locale)
    res.status(200).json({data: orders})
  } catch (error) {
    utils.handleError(res, error)
  }
}
