const Order = require('../models/orders')
const Purchase = require('../models/purchase')
const {matchedData} = require('express-validator')
const utils = require('../middleware/utils')
const db = require('../middleware/db')
const moment = require('moment')
const _ = require('lodash')

/*********************
 * Private functions *
 *********************/

/********************
 * Public functions *
 ********************/

/**
 * Get all items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getItems = async (req, res) => {
  try {
    const tenant = req.clientAccount
    const params = matchedData(req)
    const period = {
      createdAt: {
        $gte: !params.start
          ? moment().startOf('month').toDate()
          : moment(params.start, 'YYYY-MM-DD').startOf('day').toDate(),
        $lte: !params.end
          ? moment().endOf('month').toDate()
          : moment(params.end, 'YYYY-MM-DD').endOf('day').toDate()
      }
    }
    const data = {
      wait: await db.getAnalyticStatus(
        Order,
        {...period, status: 'wait'},
        tenant
      ),
      success: await db.getAnalyticStatus(
        Order,
        {...period, status: 'success'},
        tenant
      )
    }
    res.status(200).json(data)
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Get all items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getAuthor = async (req, res) => {
  try {
    const tenant = req.clientAccount
    const query = await db.checkQueryFilterMulti(req.query, Purchase)
    const params = matchedData(req)
    const period = {
      createdAt: {
        $gte: !params.start
          ? moment().startOf('month').toDate()
          : moment(params.start, 'YYYY-MM-DD').startOf('day').toDate(),
        $lte: !params.end
          ? moment().endOf('month').toDate()
          : moment(params.end, 'YYYY-MM-DD').endOf('day').toDate()
      }
    }
    const body = {
      data: await db.getAnalyticAuthor(
        Purchase,
        {...period, ...query},
        tenant
      )
    }
    res.status(200).json(body)
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Get all items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getAnalyticsItems = async (req, res) => {
  try {
    const tenant = req.clientAccount
    const params = matchedData(req)
    const period = {
      createdAt: {
        $gte: !params.start
          ? moment().startOf('month').toDate()
          : moment(params.start, 'YYYY/MM/DD').startOf('day').toDate(),
        $lte: !params.end
          ? moment().endOf('month').toDate()
          : moment(params.end, 'YYYY/MM/DD').endOf('day').toDate()
      }
    }
    res.status(200).json(await db.getAnalyticItems(Purchase, period, tenant))
  } catch (error) {
    utils.handleError(res, error)
  }
}
/**
 * Get all items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getPurchases = async (req, res) => {
  try {
    const tenant = req.clientAccount
    const query = await db.checkQueryFilterMulti(req.query, Purchase)
    const params = matchedData(req)
    const period = {
      createdAt: {
        $gte: !params.start
          ? moment().startOf('month').toDate()
          : moment(params.start, 'YYYY-MM-DD').startOf('day').toDate(),
        $lte: !params.end
          ? moment().endOf('month').toDate()
          : moment(params.end, 'YYYY-MM-DD').endOf('day').toDate()
      }
    }
    const purchases = await db.getAnalyticPurchases(
      Purchase,
      {...period, ...query},
      tenant
    )
    // eslint-disable-next-line no-use-before-define
    const data = parsePurchases(purchases)

    res.status(200).json({data})
  } catch (error) {
    utils.handleError(res, error)
  }
}

const parsePurchases = (data = []) => {
  let dataArr = [];
  let parseItems = {}

  data.forEach((d) => dataArr.push(...d.items))

  dataArr.forEach(a => {

    const singleItem = {
      [a._id]: {
        name: a.name,
        count: parseInt(a.qty)
      }
    }
    const singleId = parseItems[a._id];
    if (singleId) {
      parseItems[a._id].count = parseInt(singleId.count) + parseInt(a.qty)
    } else {
      parseItems = {...parseItems, ...singleItem}
    }

  })
  parseItems = _.reverse(_.sortBy(_.values(parseItems), ['count']));
  return parseItems
}
