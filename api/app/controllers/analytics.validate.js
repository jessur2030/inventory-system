const { validationResult } = require('../middleware/utils')
const { check } = require('express-validator')
const moment = require('moment')
/**
 * Validates register request
 */
exports.getAnalytic = [
    check('start')
      .custom((value) => {
        if (moment(value, 'YYYY-MM-DD').isValid()) {
          return true
        }
      })
      .optional(),
    check('end')
      .custom((value) => {
        if (moment(value, 'YYYY-MM-DD').isValid()) {
          return true
        }
      })
    .optional(),
    (req, res, next) => {
      validationResult(req, res, next)
    }
  ]
