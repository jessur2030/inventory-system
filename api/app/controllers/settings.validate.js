const { validationResult } = require('../middleware/utils')
const { check } = require('express-validator')

/**
 * Validates update item request
 */
exports.updateItem = [
  check('name')
    .exists()
    .withMessage('MISSING')
    .not()
    .isEmpty()
    .withMessage('IS_EMPTY'),
  check('currencySymbol')
    .exists()
    .withMessage('MISSING')
    .not()
    .isEmpty()
    .withMessage('IS_EMPTY'),
  check('currency')
    .exists()
    .withMessage('MISSING')
    .not()
    .isEmpty()
    .withMessage('IS_EMPTY'),
  check('logo')
    .exists(),
  check('language')
    .optional(),
  check('invoiceDesign')
    .optional(),
  check('id')
    .exists()
    .withMessage('MISSING')
    .not()
    .isEmpty()
    .withMessage('IS_EMPTY'),
  (req, res, next) => {
    validationResult(req, res, next)
  }
]

/**
 * Validates get item request
 */
exports.getItem = [
  check('id')
    .exists()
    .withMessage('MISSING')
    .not()
    .isEmpty()
    .withMessage('IS_EMPTY'),
  (req, res, next) => {
    validationResult(req, res, next)
  }
]
/**
 * Validates get template request
 */
exports.getTemplate = [
  check('desing')
    .exists()
    .withMessage('MISSING')
    .not()
    .isEmpty()
    .withMessage('IS_EMPTY'),
  (req, res, next) => {
    validationResult(req, res, next)
  }
]
/**
 * Validates update template request
 */
exports.updateTemplate = [
  check('design')
    .exists()
    .withMessage('MISSING')
    .not()
    .isEmpty()
    .withMessage('IS_EMPTY'),
  check('invoiceDesign')
    .optional(),
  check('purchaseDesign')
    .optional(),
  (req, res, next) => {
    validationResult(req, res, next)
  }
]
/**
 * Validates update template request
 */
exports.updateTaxes = [
  check('tax')
    .exists(),
  check('taxOffset')
    .optional(),
  check('initialInvoice')
    .exists(),
  check('invoiceFormat')
    .exists(),
  (req, res, next) => {
    validationResult(req, res, next)
  }
]

/**
 * Validates update template request
 */
exports.addCard = [
  check('token')
    .exists(),
  check('placeholder')
    .exists(),
  check('plan')
    .exists(),
  (req, res, next) => {
    validationResult(req, res, next)
  }
]
