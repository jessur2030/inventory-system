const model = require('../models/settings')
const {matchedData} = require('express-validator')
const utils = require('../middleware/utils')
const db = require('../middleware/db')
const multer = require('multer')
const detect = require('detect-file-type');
const managerStorage = require('../service/storage')
const stripeService = require('../service/stripe')
const fs = require('fs')

/*********************
 * Private functions *
 *********************/

/**
 * Checks if a city already exists excluding itself
 * @param path
 */

const textRandom = () => {
  let result = ''
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  const charactersLength = characters.length
  for (let i = 0; i < 30; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}

const updateCard = (tenant, card = {}, planDate) => {
  return new Promise((resolve, reject) => {
    model.byTenant(tenant).findOne({}, (err, setting) => {
      utils.itemNotFound(err, setting, reject, 'NOT_FOUND')
      setting.payment = card
      setting.planDate = planDate
      console.log(planDate)
      setting.save((error) => {
        if (err) {
          reject(utils.buildErrObject(422, error.message))
        }
        resolve(utils.buildSuccObject('CARD_UPDATE'))
      })
    })
  })
}

/**
 *
 * @returns {string}
 * @param mode
 * @param name
 */
const getUrlPath = (mode = '', name = '') => {
  return `${process.env.APP_URL}/media/${mode}_${name}`
}
/**
 *
 * @param path
 * @returns {Promise<unknown>}
 */

const getFile = (path = null) => new Promise((resolve, reject) => {
  detect.fromFile(path, function (err, result) {
    if (err) {
      reject(err)
    } else {
      resolve(result)
    }
  });
});

const updateByTenant = (req, tenant) => new Promise((resolve, reject) => {
  model.byTenant(tenant).findOneAndUpdate(
    {},
    req,
    {
      returnOriginal: false,
      runValidators: true
    },
    (err, item) => {
      console.log(item)
      if (err) {
        reject(err)
      } else {
        resolve(item)
      }
    }
  )
})

/**
 * Get Setting by tenant
 */
const getSettingByTenant = (tenant, select = null) => new Promise((resolve, reject) => {
  model.byTenant(tenant).findOne({}, select, (err, result) => {
    if (!err) {
      resolve(result)
    } else {
      reject(err)
    }
  })
})


/**
 * Gets all items from database
 */
const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, './public/')
  },
  filename(req, file, cb) {
    cb(null, `${file.originalname}`)
  }
})

/**
 * Gets all templates invoice
 */
const getTemplates = async () =>
  new Promise((resolve, reject) => {
    let templates = [];
    let path = __dirname + '/../templates';
    const files = fs.readdirSync(path)
    files.forEach((file, index) => {
      if (file.endsWith('.html')) {
        if (file === 'initial_invoice.html') {
          index = index - 1
        } else {
          templates.push({
            id: `full-design-${index + 1}`,
            label: file.replace('.html', ''),
            attributes: {class: 'gjs-block-section'},
            content: fs.readFileSync(path + `/${file}`, 'utf8')
          })
        }
      }
    });
    resolve(templates)
  })

/********************
 * Public functions *
 ********************/

exports.upload = multer({storage})
/**
 * Get item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */

exports.getItem = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    res.status(200).json(await getSettingByTenant(tenant))
  } catch (error) {
    utils.handleError(res, error)
  }
}

exports.updateItemInvoice = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    const {invoiceDesign} = req.body
    const data = {invoiceDesign};
    res.status(200).json(await updateByTenant(data, tenant))
  } catch (error) {
    utils.handleError(res, error)
  }
}

exports.getItemInvoice = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    res.status(200).json(await getSettingByTenant(tenant, 'invoiceDesign purchaseDesign logo name'))
  } catch (error) {
    utils.handleError(res, error)
  }
}

/**
 * Update item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.updateItem = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    const {file = {}} = req;
    const language = req.body.language || 'es-ES';
    const {mime = '', ext = ''} = (file.path) ? await getFile(file.path) : {}
    const isImage = mime.includes('image');
    let logo = null;

    if (isImage) {
      const nameRandomFile = await textRandom();
      const imageName = `logo_${nameRandomFile}.${ext}`
      await managerStorage.resizeFileOriginal(file.originalname, `${imageName}`)
      logo = getUrlPath('medium', imageName);
    }

    let data = {...req.body, ...{language}}

    if (logo) {
      data = {...data, ...{logo}}
    }
    res.status(200).json(await updateByTenant(data, tenant))
  } catch (error) {
    utils.handleError(res, error)
  }
}

exports.getInvoiceTemplates = async (req, res) => {
  try {
    res.status(200).json(await getTemplates())
  } catch (error) {
    utils.handleError(res, error)
  }
}
exports.getTemplate = async (req, res) => {
  try {
    const design = req.params.design
    const tenant = req.clientAccount;
    let typeDesign = '';
    switch (design) {
      case 'invoice':
        typeDesign = 'invoiceDesign'
        break;
      case 'other':
        typeDesign = 'otherDesign'
        break;
      default:
        return utils.buildErrObject(401, 'Desing invalid')
        break;
    }
    res.status(200).json(await getSettingByTenant(tenant, `${typeDesign} logo name`))
  } catch (error) {
    utils.handleError(res, error)
  }
}

exports.updateTemplate = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    req = matchedData(req)
    let data = {}
    switch (req.design) {
      case 'invoice':
        data.invoiceDesign = req.invoiceDesign
        break;
      case 'purchase':
        data.purchaseDesign = req.purchaseDesign
        break;
      default:
        return utils.buildErrObject(401, 'Design invalid')
        break;
    }
    res.status(200).json(await updateByTenant(data, tenant))
  } catch (error) {
    utils.handleError(res, error)
  }
}

exports.getTaxes = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    res.status(200).json(await getSettingByTenant(tenant, 'tax taxOffset initialInvoice'))
  } catch (error) {
    utils.handleError(res, error)
  }
}

exports.updateTaxes = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    req = matchedData(req)
    console.log(req)
    res.status(200).json(await updateByTenant(req, tenant))
  } catch (error) {
    utils.handleError(res, error)
  }
}


exports.addCard = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    const {user} = req;
    req = matchedData(req)
    const {placeholder, token} = req;
    const customer = await stripeService.createCustomer({
      token: token,
      email: user.email
    });
    const cardSingle = customer.sources.data.find(() => true)
    const card = {
      cardPlaceholder: placeholder,
      cardToken: cardSingle.id,
      cardNumber: cardSingle.last4,
      brand: cardSingle.brand,
      cardEpx: `${cardSingle.exp_month}/${cardSingle.exp_year}`,
    }

    res.status(200).json(await updateCard(tenant, card, req.plan))

  } catch (error) {
    console.log(error)
    utils.handleError(res, {code: 402, message: error.message})
  }
}

exports.getCard = async (req, res) => {
  try {
    const tenant = req.clientAccount;
    const cards = await getSettingByTenant(tenant, 'payment')
    let cardToken = cards.payment || null
    const body = {
      payment: cardToken ? Object.assign(cardToken, {cardToken: null, cardUser: null}) : null
    }

    res.status(200).json(body)
  } catch (error) {
    utils.handleError(res, error)
  }
}
