const mongoose = require('mongoose')
// const mongoosePaginate = require('mongoose-paginate-v2')
const aggregatePaginate = require('mongoose-aggregate-paginate-v2');
// const softDelete = require('mongoose-softdelete');
const mongoose_delete = require('mongoose-delete');
const moment = require('moment'); // require
const mongoTenant = require('mongo-tenant');

const PurchaseSchema = new mongoose.Schema(
  {
    controlNumber: {
      type: String,
      required: false,
      default: null
    },
    customer: {
      type: Object,
      required: true
    },
    items: {
      type: Object,
      required: false
    },
    author: {
      type: mongoose.Types.ObjectId,
      required: true
    },
    status: {
      type: String,
      enum: ['paid', 'hold', 'process', 'exceptional', 'pre-paid', 'credit', 'delivery-paid'],
      default: 'hold'
    },
    deliveryAddress: {
      type: Object,
      required: true
    },
    invoiceAddress: {
      type: Object,
      required: true
    },
    invoiceNumber: {
      type: String
    },
    deliveryDate: {
      type: Object,
      default: null
    },
    deliveryType: {
      type: String,
      enum: ['to_send', 'in_house'],
      default: 'to_send'
    },
    convert: {
      type: String,
      enum: ['order_invoice', 'order_purchase', 'order_pay'],
      default: 'order_purchase',
      required: true
    },
    subTotal: {
      type: Number,
      required: false,
      default: 0
    },
    sumTaxesTotal: {
      type: Number,
      required: false,
      default: 0
    },
    taxes: {
      type: Array,
      required: false,
      default: []
    },
    total: {
      type: Number,
      required: false,
      default: 0
    },
    tag: {
      type: Object
    },
    description: {
      type: String
    },
    download: {
      type: String
    },
    attachment: {
      type: Array,
      default: []
    },
    dummy: {
      type: Boolean,
      default: false
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)

PurchaseSchema.post('save', (obj) => {
  const prefix = process.env.PREFIX_INVOICE;
  const day = moment().format('dd');
  const nextControl = moment().unix();
  obj.set('controlNumber', `${prefix}-${nextControl}${day}`, { strict: false })
  obj.save();
})

PurchaseSchema.plugin(mongoose_delete, { overrideMethods: 'all' });
PurchaseSchema.plugin(mongoose_delete)

PurchaseSchema.plugin(aggregatePaginate)
PurchaseSchema.plugin(mongoTenant)

module.exports = mongoose.model('Purchase', PurchaseSchema)
