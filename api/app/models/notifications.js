const mongoose = require('mongoose')
// const mongoosePaginate = require('mongoose-paginate-v2')
const aggregatePaginate = require('mongoose-aggregate-paginate-v2');
const mongoose_delete = require('mongoose-delete');
const mongoTenant = require('mongo-tenant');
const NotificationSchema = new mongoose.Schema(
  {
    receptor: {
      type: mongoose.Types.ObjectId,
      required: false,
      default: null,
    },
    sender: {
      type: mongoose.Types.ObjectId,
      required: false,
      default: null
    },
    title: {
      type: String,
      required: true
    },
    body: {
      type: String,
      required: false
    },
    url: {
      type: String,
      required: false
    },
    seen: {
      type: Boolean,
      required: false,
      default: false
    },
    behavior: {
      type: String,
      enum: ['modal', 'route'],
      default: 'route'
    },
    event: {
      type: Object,
      required: false
    },
    customData: {
      type: Object
    },
    dummy: {
      type: Boolean,
      default: false
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)


NotificationSchema.plugin(mongoose_delete, { overrideMethods: 'all' });
NotificationSchema.plugin(mongoose_delete)

NotificationSchema.plugin(aggregatePaginate)
NotificationSchema.plugin(mongoTenant)
// NotificationSchema.plugin(softDelete)
module.exports = mongoose.model('Notifications', NotificationSchema)
