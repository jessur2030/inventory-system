const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const mongoose_delete = require('mongoose-delete');
const mongoTenant = require('mongo-tenant');
const StorageSchema = new mongoose.Schema(
  {
    original: {
      type: String,
      required: true
    },
    small: {
      type: String,
      required: false
    },
    medium: {
      type: String,
      required: false
    },
    large: {
      type: String,
      required: false
    },
    fileName: {
      type: String,
      required: true
    },
    fileType: {
      type: String,
      required: true
    },
    author: {
      type: Object,
      required: true
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
StorageSchema.plugin(mongoosePaginate)
StorageSchema.plugin(mongoose_delete, { overrideMethods: 'all' });
StorageSchema.plugin(mongoose_delete)
StorageSchema.plugin(mongoTenant)
module.exports = mongoose.model('Storage', StorageSchema)
