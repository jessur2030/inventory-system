const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
// const softDelete = require('mongoose-softdelete');
const mongoose_delete = require('mongoose-delete');
const mongoTenant = require('mongo-tenant');
const DepositsSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    address: {
      type: Object,
      required: true
    },
    trace: {
      type: Object
    },
    phone: {
      type: String
    },
    tag: {
      type: Array,
      default: []
    },
    manager: {
      type: Object,
      required: true
    },
    description: {
      type: String
    },
    dummy: {
      type: Boolean,
      default: false
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)

DepositsSchema.plugin(mongoose_delete, { overrideMethods: 'all' });
DepositsSchema.plugin(mongoose_delete)
DepositsSchema.plugin(mongoosePaginate)
DepositsSchema.plugin(mongoTenant)
module.exports = mongoose.model('Deposits', DepositsSchema)
