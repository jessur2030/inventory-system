const mongoose = require('mongoose')
const aggregatePaginate = require('mongoose-aggregate-paginate-v2');
const mongoose_delete = require('mongoose-delete');
const mongoTenant = require('mongo-tenant');
const InventorySchema = new mongoose.Schema(
  {
    product: {
      type: Object,
      required: true
    },
    provider: {
      type: Object,
      required: false,
      default: []
    },
    qty: {
      type: Number,
      required: true
    },
    priceBase: {
      type: Number,
      required: false
    },
    deposit: {
      type: Object,
      required: false
    },
    purchase: {
      type: Object,
      required: false,
      default: null,
    },
    author: {
      type: Object,
      required: true
    },
    serials: {
      type: Object,
      required: false,
      default: []
    },
    tag: {
      type: Array,
      default: []
    },
    description: {
      type: String
    },
    dummy: {
      type: Boolean,
      default: false
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)


InventorySchema.plugin(mongoose_delete, { overrideMethods: 'all' });
InventorySchema.plugin(mongoose_delete)
InventorySchema.plugin(aggregatePaginate)
InventorySchema.plugin(mongoTenant)

module.exports = mongoose.model('Inventory', InventorySchema)
