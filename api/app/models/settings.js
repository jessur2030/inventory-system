const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const mongoose_delete = require('mongoose-delete');
const mongoTenant = require('mongo-tenant');
const moment = require('moment');
const fs = require('fs')


const geInitialTemplate = () => {
  return {
    html: fs.readFileSync(__dirname + '/../templates/initial_invoice.html', 'utf8')
  }
}

const getPlanBasic = () => {
  return {
    "title": "Plan Base",
    "price": 0.00
  }
}

const CardSchema = new mongoose.Schema(
  {
    cardPlaceholder: {
      type: String,
      select: false
    },
    cardToken: {
      type: String,
      select: false
    },
    cardNumber: {
      type: String,
    },
    cardUser: {
      type: String,
      required: true
    },
    brand: {
      type: String
    },
    cardEpx: {
      type: String
    }
  }
)

const PlanScheme = new mongoose.Schema(
  {
    title: {
      type: String,
    },
    price: {
      type: Number,
    },
    createdAt: {
      type: Date,
      default: moment().toDate()
    },
  }
)

const SettingsSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: false,
      default: null
    },
    owner: {
      type: String,
      required: true,
      default: null
    },
    currencySymbol: {
      type: String,
      required: false,
      default: null
    },
    currency: {
      type: String,
      required: false,
      default: null
    },
    logo: {
      type: String,
      required: false,
      default: null
    },
    plugins: {
      type: Object,
      default: []
    },
    tax: {
      type: Object,
      default: []
    },
    taxOffset: {
      type: Object,
      default: []
    },
    language: {
      type: String,
      enum: ['es-ES', 'es-MX'],
      default: 'es-ES',
      required: false
    },
    invoiceDesign: {
      type: Object,
      default: geInitialTemplate()
    },
    purchaseDesign: {
      type: Object,
      default: geInitialTemplate()
    },
    initialInvoice: {
      type: Number,
      default: 0
    },
    invoiceFormat: {
      type: String,
      default: '%%%%%%'
    },
    payment: {
      type: CardSchema,
      default: null
    },
    planDate: {
      type: PlanScheme,
      default: getPlanBasic()
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
SettingsSchema.plugin(mongoosePaginate)
SettingsSchema.plugin(mongoTenant)
SettingsSchema.plugin(mongoose_delete, {overrideMethods: 'all'});
SettingsSchema.plugin(mongoose_delete)


module.exports = mongoose.model('Settings', SettingsSchema)
