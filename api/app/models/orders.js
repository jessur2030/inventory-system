const mongoose = require('mongoose')
// const mongoosePaginate = require('mongoose-paginate-v2')
const aggregatePaginate = require('mongoose-aggregate-paginate-v2');
const mongoose_delete = require('mongoose-delete');
const mongoTenant = require('mongo-tenant');
const OrdersSchema = new mongoose.Schema(
  {
    customer: {
      type: mongoose.Types.ObjectId,
      required: true
    },
    author: {
      type: mongoose.Types.ObjectId,
      required: true
    },
    status: {
      type: String,
      enum: ['success', 'wait', 'cancel', 'fail'],
      default: 'wait'
    },
    platform: {
      type: String,
      required: true
    },
    purchase: {
      type: Object,
      default: null
    },
    reference: {
      type: Object,
      required: true
    },
    amount: {
      type: Number,
      required: true
    },
    currency: {
      type: String,
      required: true
    },
    description: {
      type: String
    },
    tag: {
      type: Array,
      default: []
    },
    customData: {
      type: Object
    },
    dummy: {
      type: Boolean,
      default: false
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)


OrdersSchema.plugin(mongoose_delete, { overrideMethods: 'all' });
OrdersSchema.plugin(mongoose_delete)

OrdersSchema.plugin(aggregatePaginate)
OrdersSchema.plugin(mongoTenant)
// OrdersSchema.plugin(softDelete)
module.exports = mongoose.model('Orders', OrdersSchema)
