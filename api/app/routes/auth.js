const controller = require('../controllers/auth')
const validate = require('../controllers/auth.validate')
const origin = require('../middleware/origin')
const express = require('express')
const router = express.Router()

/*
 * Login facebook route
 */
router.get('/login-facebook',
  origin.checkDomain,
  validate.loginSocial,
  controller.loginFb)


/*
 * Login facebook callback route
 */
router.get('/callback/facebook',
  origin.checkDomain,
  controller.loginCbFb)

/*
 * Login google callback route
 */
router.get('/callback/google',
  origin.checkDomain,
  controller.loginCbGoogle)


/*
 * Login gmail route
 */
router.get('/login-google',
  origin.checkDomain,
  validate.loginSocial,
  controller.loginGoogle)

module.exports = router
