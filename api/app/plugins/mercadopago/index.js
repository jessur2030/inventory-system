const settings = require('../../models/settings');
const pluginsSettings = require('../../models/pluginsSettings');
const pathName = 'mercadopago';
const configure = {
  sandbox: true,
  access_token: "TEST-1403144378425535-091312-0959794f45e5ff6a40653ce911c81d61-644502297",
};
const public_key = "TEST-5eb89517-6613-4129-8985-2f00c7c9b736";

const PaymentController = require("./controller/mp.controller");

const PaymentService = require("./service/paymentServiceMp");

const PaymentInstance = new PaymentController(new PaymentService());

/*********************
 * Private functions *
 *********************/
const findSettingTenant = (tenant = null) => new Promise((resolve, reject) => {
  settings.byTenant(tenant).findOne({}, (err, item) => {
    if (!err) {
      resolve(item)
    } else {
      reject(err)
    }
  })
})

const checkKeys = (tenant, secret = 'plugin.features.keys.clientID') => new Promise(async (resolve, reject) => {
  const dataTenant = await findSettingTenant(tenant);
  pluginsSettings.findOne({
      "plugin.path": pathName,
      "plugin.features.keys.clientID": {$ne: null},
      "plugin.features.keys.secretID": {$ne: null},
      "tenantId": tenant
    }, `${secret} currency currencySymbol plugin.features.keys.mode`,
    (err, response) => {
      if (!err && response) {
        const keys = response.get('plugin');
        const validCurrency = (['COP', 'MXN', 'USD'].includes(dataTenant.currency))
        const currencySymbol = dataTenant.currencySymbol
        response = keys
        const {features} = response
        resolve({
          keys: features.keys,
          currency: dataTenant.currency,
          validCurrency,
          currencySymbol
        })
      } else {
        resolve(null)
      }
    })
})

const updateSetting = (purchase, template, value, tenant, dataUser) => new Promise((resolve, reject) => {
  const {clientID = null, secretID = null, mode = 'test'} = value;
  if (dataUser.role === 'admin') {
    pluginsSettings.findOneAndUpdate({"plugin.path": pathName, "tenantId": tenant},
      {
        "$set": {
          "plugin.features.keys": {
            clientID, secretID, mode
          }
        }
      },
      false,
      (err, item) => {
        if (!err) {
          resolve(item)
        } else {
          reject(err)
        }
      })
  }
});

const getSetting = (tenantId) => new Promise((resolve, reject) => {
  settings.findOne({tenantId}, (err, item) => {
    if (!err) {
      resolve(item)
    } else {
      reject(err)
    }
  })
})


const generateLink = (value, tenant, dataUser) => new Promise(async (resolve, reject) => {
  try {
    const dataKeys = await checkKeys(tenant, 'plugin.features.keys.clientID plugin.features.keys.secretID');

    if (dataKeys) {
      const {clientID = null, secretID = null, mode = null} = dataKeys.keys;
      const tenantData = await getSetting(tenant);
      const {price, description, track = ''} = value
      let purchaseData = {
        currency: dataKeys.currency,
        price,
        description
      }
      const urls = {
        success: `${process.env.FRONTEND_URL_TENANT
          .replace(/__TENANT__/gi, tenant)}/add-events/${pathName}-cb/${track}`,
        pending: `${process.env.FRONTEND_URL_TENANT
          .replace(/__TENANT__/gi, tenant)}/add-events/${pathName}-cb/${track}`,
        failure: `${process.env.FRONTEND_URL_TENANT
          .replace(/__TENANT__/gi, tenant)}/add-events/${pathName}-cb/${track}`
      }
      const checkout = await PaymentInstance.getMercadoPagoLink(purchaseData, tenant, urls)
      resolve(checkout)
    } else {
      resolve(null)
    }
  } catch (e) {
    console.log(e)
    reject({message: e.error, code: 422})
  }
})

/********************
 * Public functions *
 ********************/

exports.update_payment = async ({}, parentModule, value, tenant, dataUser) => await updatePayment(value, tenant, dataUser)

exports.generate_link = ({}, parentModule, value, tenant, dataUser) => generateLink(value, tenant, dataUser)

exports.check_keys = (purchase, template, value = {}, tenant = null, dataUser = null) => checkKeys(tenant,
  'plugin.features.keys.clientID plugin.features.keys.secretID plugin.features.keys.mode'
)
exports.get_order = async ({}, parentModule, value, tenant, dataUser) => checkKeys(tenant,
  'plugin.features.keys.clientID plugin.features.keys.secretID plugin.features.keys.mode'
)
