const settings = require('../../models/settings');
const pluginsSettings = require('../../models/pluginsSettings');
const orders = require('../../models/orders');
const Str = require('@supercharge/strings')
/**
 * Private functions
 */

const findSettingTenant = (tenant = null) => new Promise((resolve, reject) => {
  settings.byTenant(tenant).findOne({}, (err, item) => {
    if (!err) {
      resolve(item)
    } else {
      reject(err)
    }
  })
})

const updateSetting = (purchase, template, value, tenant, dataUser) => new Promise((resolve, reject) => {
  const {pk = null, sk = null} = value;
  if (dataUser.role === 'admin') {
    pluginsSettings.findOneAndUpdate({"plugin.path": "stripe", "tenantId": tenant},
      {
        "$set": {
          "plugin.features.keys": {
            pk, sk
          }
        }
      },
      false,
      (err, item) => {
        if (!err) {
          resolve(item)
        } else {
          reject(err)
        }
      })
  }
});

const checkKeys = (tenant, secret = 'plugin.features.keys.pk') => new Promise(async (resolve, reject) => {
  const dataTenant = await findSettingTenant(tenant);
  pluginsSettings.findOne({
      "plugin.path": "stripe",
      "plugin.features.keys.pk": {$ne: null},
      "plugin.features.keys.sk": {$ne: null},
      "tenantId": tenant
    }, `${secret} currency currencySymbol`,
    (err, response) => {
      if (!err && response) {
        const keys = response.get('plugin');
        const validCurrency = (['USD', 'MXN', 'EUR'].includes(dataTenant.currency))
        const currencySymbol = dataTenant.currencySymbol
        response = keys
        const {features} = response
        resolve({
          keys: features.keys,
          currency: dataTenant.currency,
          validCurrency,
          currencySymbol
        })
      } else {
        console.log('Error stripe', err, response)
        resolve(null)
      }
    })
})

const generateLink = (req, tenant) => new Promise(async (resolve, reject) => {
  const plugin = await checkKeys(tenant)
  const randomStr = Str.random(50)
  resolve((plugin) ? randomStr : null)
})

const getOrder = (value) => new Promise((resolve, reject) => {
  const {tracker} = value;
  orders.aggregate(
    [
      {
        $lookup: {
          from: 'users',
          let: {idUser: '$customer'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{$eq: ['$$idUser', '$_id']}]
                }
              }
            },
            {
              $project: {
                _id: 1,
                name: 1,
                lastName: 1
              }
            }
          ],
          as: 'customer'
        }
      },
      {
        $lookup: {
          from: 'settings',
          let: {tenant: '$tenantId'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{$eq: ['$$tenant', '$tenantId']}],
                }
              }
            },
            {
              $project: {
                name: 1,
                logo: 1
              }
            },
          ],
          as: 'settings'
        },
      },
      {
        $lookup: {
          from: 'pluginssettings',
          let: {tenant: '$tenantId'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    {
                      $eq: ['$$tenant', '$tenantId']
                    },
                    {
                      $eq: ['stripe', '$plugin.path']
                    }
                  ],
                }
              }
            },
            {
              $project: {
                plugin: 1
              }
            },
          ],
          as: 'pluginssettings'
        }
      },
      {$unwind: '$customer'},
      {$unwind: '$settings'},
      {$unwind: '$pluginssettings'},
      {
        $project: {
          customData: 1,
          "pk": ["$pluginssettings.plugin.features.keys.pk"],
          "settings.logo": 1,
          "settings.name": 1,
          "customer.name": 1,
          "customer.lastName": 1,
          "customer.email": 1,
          "customer._id": 1,
          status: 1,
          _id: 1,
          description: 1,
          pluginssettings: 1,
          currency: 1,
          amount: 1
        }
      },
      {
        $match: {
          "customData.tracker": {$exists: true, $eq: tracker}
        }
      }
    ],
    (err, res) => {
      resolve(res.find(() => true))
    })
})

const processPay = (value, tenant) => new Promise(async (resolve, reject) => {
  try {
    let stripe = {};
    if (!value.token) {
      reject('NOT_TOKEN')
    }
    const dataKeys = await checkKeys(tenant, 'plugin.features.keys.pk plugin.features.keys.sk')
    const {keys = {}} = dataKeys;
    stripe = require('stripe')(keys.sk);
    const order = await getOrder(value)
    const customer = await createCustomer(stripe, value)
    const pay = singlePay(stripe, value, customer, order)
    resolve(pay)
  } catch (e) {
    console.log('ERROR_PROCESS_PAY_1')
    reject(e)
  }
})

// singlePay(stripe, value, customer, order)
const createCustomer = async (stripe = {}, value) => {
  try {
    const {user, token} = value
    return await stripe.customers.create(
      {
        email: user.email,
        description: `ID: ${user._id}`,
        source: token,
      });
  } catch (e) {
    return 'ERROR_CREATE_CUSTOMER'
  }
}

const singlePay = (stripe = {}, value, customer, order) => new Promise((resolve, reject) => {
  const {amount, currency, description} = order
  console.log(`************* -----------------`,order._id)
  stripe.paymentIntents.create(
    {
      amount: parseFloat(amount) * 100,
      currency,
      customer: customer.id,
      payment_method_types: ['card'],
      description,
      metadata: {
        internal_id:`${order._id}`
      }
    },
    (err, paymentIntent) => {
      if (!err) {
        resolve(paymentIntent)
      } else {
        reject(null)
      }
    }
  );
})

const updatePay = async (value, tenant) => {
  const dataKeys = await checkKeys(tenant, 'plugin.features.keys.pk plugin.features.keys.sk')
  const {keys} = dataKeys
  const {pi} = value
  const stripe = require('stripe')(keys.sk);
  const response = await stripe.paymentIntents.retrieve(
    pi
  );
  const order = await getOrder(value)
  const customData = {...order.customData, ...{stripe: response}}
  const status = (response.status.includes('succe')) ? 'success' : 'wait'
  await orders.findByIdAndUpdate(order._id, {customData, status})
}

const registerHook = (tenant, dataUser) => new Promise(async (resolve, reject) => {
  try {
    const dataKeys = await checkKeys(tenant, [
      'plugin.features.keys.pk',
      'plugin.features.keys.sk',
      'plugin.features.keys.wh_sk'
    ].join(' '));

    const {keys = {}} = dataKeys;
    const stripe = require('stripe')(keys.sk);
    const urlHook = `${process.env.APP_URL}/api/1.0/hooks/${tenant}/stripe`;

    const webhookEndpoint = await stripe.webhookEndpoints.create({
      url: urlHook,
      enabled_events: [
        'charge.failed',
        'charge.succeeded',
        'invoice.payment_succeeded',
        'payment_intent.succeeded'
      ],
    });
    const {secret} = webhookEndpoint;
    if (dataUser.role === 'admin') {
      pluginsSettings.findOneAndUpdate({"plugin.path": "stripe", "tenantId": tenant},
        {
          "$set": {
            "plugin.features.keys.wh_sk": secret
          }
        },
        false,
        (err) => {
          if (!err) {
            resolve(webhookEndpoint)
          } else {
            reject(err)
          }
        })
    } else {
      reject(null)
    }
  } catch (e) {
    reject(null)
  }
})

/**
 * Public functions
 */

exports.get_order = ({}, parentModule, value, tenant) => getOrder(value)

exports.update_pay = ({}, parentModule, value, tenant) => updatePay(value, tenant)

exports.process_pay = ({}, parentModule, value, tenant) => processPay(value, tenant)

exports.register_hook = ({}, parentModule, value, tenant, dataUser = null) => registerHook(tenant, dataUser)

exports.generate_link = (req, tenant) => generateLink(req, tenant)

exports.check_keys = (purchase, template, value = {}, tenant = null, dataUser = null) => checkKeys(tenant)

exports.update_setting = (purchase, template, value = {}, tenant = null, dataUser = null) => {
  return updateSetting(purchase, template, value, tenant, dataUser);
}
