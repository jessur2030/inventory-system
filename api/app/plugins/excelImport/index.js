const utils = require('../../middleware/utils')
const Products = require('../../models/products')
const User = require('../../models/user')
const Provider = require('../../models/providers')
const Deposit = require('../../models/deposits')
const db = require('../../middleware/db')
const xlsxFile = require('read-excel-file/node');
const uuid = require('uuid')
const http = require('http');
const https = require('https');
const fs = require('fs');

const path_temp = `${__dirname}/tmp/`

/*********************
 * Private functions *
 *********************/
const structure_products = ['name', 'description', 'sku']
const structure_users = ['name', 'lastName', 'nameBusiness', 'email', 'address', 'nie', 'phone', 'role']
const structure_providers = ['name', 'email', 'address', 'phone', 'description']
const structure_deposits = ['name', 'address', 'phone', 'description']

const roles = [
  {
    key: 'customer',
    value: 'Cliente'
  },
  {
    key: 'admin',
    value: 'Administrador'
  },
  {
    key: 'manager',
    value: 'Encargado'
  },
  {
    key: 'seller',
    value: 'Vendedor'
  },
  {
    key: 'provider',
    value: 'Proveedor'
  }
]

const registerUser = async (inUser = {}, tenant = null) => {
  return new Promise((resolve, reject) => {
    const model = User.byTenant(tenant);
    const user = new model({
      name: inUser.name,
      lastName: inUser.lastName,
      nameBusiness: inUser.nameBusiness,
      email: inUser.email,
      address: inUser.address,
      nie: inUser.nie,
      phone: inUser.phone,
      role: roles.find((k) => k.value === `${inUser.role}`)['key'],
      password: inUser.password,
      verification: uuid.v4()
    })
    user.save((err, item) => {
      if (err) {
        resolve(err)
      }
      resolve(item)
    })
  })
}

const saveFileTmp = async (url) => new Promise(async (resolve, reject) => {
  try {
    const name_file = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    let file = await fs.createWriteStream(`${path_temp}${name_file}`);
    try {
      const request = await http.get(url, async (response) => {
        await response.pipe(file);
      });
    } catch (error) {
      if (error.code = "ERR_INVALID_PROTOCOL") {
        const request = await https.get(url, async (response) => {
          await response.pipe(file);
        });
      } else {
        reject(utils.buildErrObject(error.code, error.message));
      }
    }
    console.log(name_file)
    file.on('finish', function () {
      resolve(name_file)
    });
  } catch (error) {

    console.log(error);
    reject(utils.buildErrObject(error.code, error.message));

  }
});

const deleteFileTmp = async (name_file) => new Promise(async (resolve, reject) => {
  try {
    console.log('Eliminando archivo')
    await fs.unlinkSync(`${path_temp}${name_file}`)
    resolve(true)
  } catch (error) {
    console.log(error);
    reject(utils.buildErrObject(error.code, error.message));
  }
});

const processProducts = async (path_file, tenant, user) => new Promise(async (resolve, reject) => {
  try {
    const name_file = await saveFileTmp(path_file);
    path_file = `${path_temp}${name_file}`
    let info = await xlsxFile(path_file, {sheet: 'Producto'})
    info.splice(0, 1);
    let prices = await xlsxFile(path_file, {sheet: 'Precios'})
    prices.splice(0, 1);
    let categories = await xlsxFile(path_file, {sheet: 'Categorias'})
    categories.splice(0, 1);
    let tags = await xlsxFile(path_file, {sheet: 'Tags'})
    tags.splice(0, 1);
    let deposits = await xlsxFile(path_file, {sheet: 'Depositos'}).then(()=>{
      deposits.splice(0, 1);
    }).catch(()=>{
      console.log('No se encuentra el Sheet Depositos')
    })
    let providers = await xlsxFile(path_file, {sheet: 'Proveedores'}).then(()=>{
      providers.splice(0, 1);
    }).catch(()=>{
      console.log('No se encuentra el Sheet Provedores')
    })

    let products = [];
    for (const i in info) {
      let product = {};
      for (const j in info[i]) {
        switch (structure_products[j]) {
          case 'name':
            product.name = info[i][j];
            break;
          case 'description':
            product.description = (info[i][j]) ? info[i][j] : '';
            break;
          case 'sku':
            let sku = (info[i][j]) ? info[i][j] : '';
            product.sku = (sku) ? info[i][j] : product.name.toLowerCase().replace(' ', '-');
            break;
        }
      }
      product.gallery = []
      product.prices = []
      if (prices[i]) {
        prices[i].forEach(price => {
          (price) ? product.prices.push({amount: Number(price)}) : null;
        });
      }
      product.categories = []
      if (categories[i]) {
        categories[i].forEach(categorie => {
          (categorie) ? product.categories.push(categorie.toUpperCase()) : null;
        });
      }
      product.tag = []
      if (tags[i]) {
        tags[i].forEach(tag => {
          (tag) ? product.tag.push({name: tag.toUpperCase(), id: tag.toUpperCase()}) : null;
        });
      }
      product = {
        ...product, ...{author: user._id}
      }
      products.push(product)
      await db.createItem(product, Products, tenant)
    }
    await deleteFileTmp(name_file);
    resolve(products)
  } catch (error) {
    console.log('Error desde el convertidos de productos: ',error)
    reject(utils.buildErrObject(error.code, error.message))
  }
});

const processUsers = async (path_file, tenant, user) => new Promise(async (resolve, reject) => {
  const name_file = await saveFileTmp(path_file);
  path_file = `${path_temp}${name_file}`
  let info = await xlsxFile(path_file, {sheet: 'Usuarios'})
  info.splice(0, 1);
  let users = [];
  for (const i in info) {
    let user = {};
    for (const j in info[i]) {
      switch (structure_users[j]) {
        case 'name':
          user.name = (info[i][j]) ? info[i][j] : '';
          break;
        case 'lastName':
          user.lastName = (info[i][j]) ? info[i][j] : '';
          break;
        case 'nameBusiness':
          user.nameBusiness = (info[i][j]) ? info[i][j] : '';
          break;
        case 'email':
          const emailParse = (info[i][j]) ? info[i][j] : '';
          user.email = emailParse.replace(/[^a-zA-Z0-9_\-@.]/g,'_');
          break;
        case 'address':
          user.address = (info[i][j]) ? info[i][j] : '';
          break;
        case 'nie':
          user.nie = (info[i][j]) ? info[i][j] : '';
          break;
        case 'phone':
          user.phone = (info[i][j]) ? info[i][j] : '';
          break;
        case 'role':
          user.role = (info[i][j]) ? info[i][j] : '';
          break;
      }
    }
    user.password = 'Kitagil.01'
    users.push(user)
    await registerUser(user, tenant)
  }
  await deleteFileTmp(name_file);
  resolve(users)
});

const processProviders = async (path_file, tenant, user) => new Promise(async (resolve, reject) => {
  try {
    const name_file = await saveFileTmp(path_file);
    path_file = `${path_temp}${name_file}`
    let info = await xlsxFile(path_file, {sheet: 'Proveedores'})
    info.splice(0, 1);
    let tags = await xlsxFile(path_file, {sheet: 'Tags'})
    tags.splice(0, 1);
    let providers = [];
    for (const i in info) {
      let provider = {};
      for (const j in info[i]) {

        switch (structure_providers[j]) {
          case 'name':
            provider.name = (info[i][j]) ? info[i][j] : '';
            break;
          case 'email':
            provider.email = (info[i][j]) ? info[i][j] : '';
            break;
          case 'address':
            provider.address = (info[i][j]) ? info[i][j] : '';
            break;
          case 'phone':
            provider.phone = (info[i][j]) ? info[i][j] : '';
            break;
          case 'description':
            provider.description = (info[i][j]) ? info[i][j] : '';
            break;
        }
      }
      provider.tag = [];
      if (tags[i]) {
        tags[i].forEach(tag => {
          (tag) ? provider.tag.push(tag) : null;
        });
      }
      provider.manager = {_id: user._id, name: user.name}
      providers.push(provider)

      await db.createItem(provider, Provider, tenant)
    }
    await deleteFileTmp(name_file);
    resolve(providers)
  } catch (error) {
    console.log(error)
    reject(utils.buildErrObject(error.code, error.message))
  }
});

const processDeposits = async (path_file, tenant, user) => new Promise(async (resolve, reject) => {
  try {
    const name_file = await saveFileTmp(path_file);
    path_file = `${path_temp}${name_file}`
    let info = await xlsxFile(path_file, {sheet: 'Alamacenes'})
    info.splice(0, 1);
    // cons
    let tags = await xlsxFile(path_file, {sheet: 'Tags'})
    tags.splice(0, 1);
    let deposits = [];
    for (const i in info) {
      let deposit = {};
      for (const j in info[i]) {
        switch (structure_deposits[j]) {
          case 'name':
            deposit.name = (info[i][j]) ? info[i][j] : '';
            break;
          case 'address':
            deposit.address = (info[i][j]) ? info[i][j] : '';
            break;
          case 'phone':
            deposit.phone = (info[i][j]) ? info[i][j] : '';
            break;
          case 'description':
            deposit.description = (info[i][j]) ? info[i][j] : '';
            break;
        }
      }
      deposit.tag = []
      if (tags[i]) {
        tags[i].forEach(tag => {
          (tag) ? deposit.tag.push(tag) : null;
        });
      }

      deposit.manager = {_id: user._id, name: user.name}
      deposits.push(deposit)
      await db.createItem(deposit, Deposit, tenant)
    }
    await deleteFileTmp(name_file); // Eliminamos el archivo temporal
    resolve(deposits)
  } catch (error) {
    console.log(error)
    reject(utils.buildErrObject(error.code, error.message))
  }
});

const importFile = (value, tenant, author) => new Promise(async (resolve, reject) => {
  try {
    const {url = '', type} = value;
    let data = null
    switch (type) {
      case 'products':
        data = await processProducts(url, tenant, author)
        break;
      case 'users':
        data = await processUsers(url, tenant, author)
        break;
      case 'providers':
        data = await processProviders(url, tenant, author)
        break;
      case 'deposits':
        data = await processDeposits(url, tenant, author)
        break;
      default:
        reject(utils.buildErrObject(400, 'Type is no valid'))
        break;
        resolve(true)
    }
    resolve(data)
  } catch (error) {
    console.log(error)
    reject(utils.buildErrObject(error.code, error.message))
  }
})
/********************
 * Public functions *
 ********************/

/**
 * Los parametros {}, parentModule, value, tenant son obligatorios pasarlos
 */

exports.import_file = async ({}, parentModule, value, tenant, dataUser) => await importFile(value, tenant, dataUser)
