# Plugin para convertir un archivo en excel a MongoDb

Esta libreria necesita ayuda de [express-fileupload](https://www.npmjs.com/package/express-fileupload) y [read-excel-file](https://www.npmjs.com/package/read-excel-file) para su funcionamiento.

## Configuración

### express-fileupload

En el archivo origen server.js ingresar la configuracion de la sig. manera:

```javascript
const app = express()
const fileUpload = require('express-fileupload');
app.use(fileUpload({
  useTempFiles : true,
  tempFileDir : '/tmp/'
}));
```
La configuracion es para facilitar el uso del plugin, ya que el parametro a enviar en la direccion del archivo que se desea procesar (excel)

Se consume de la siguiente manera:

```javascript
const excel_plugin = require('../plugins/excel-converter') // Mandamos a llamar desde el controller

await excel_plugin.convertFileToDb( file[0], req.body.type, user)
// Se reciben 3 parametros, el path del archivo temporal, el tipo de archivo que se procesara y el usuario que realiza la peticion
```
Los tipos permitidos son:

* products