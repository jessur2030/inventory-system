const controller = require('../controllers/settings')
const validate = require('../controllers/settings.validate')
const AuthController = require('../controllers/auth')
const origin = require('../middleware/origin')
const express = require('express')
const router = express.Router()
require('../../config/passport')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {
  session: false
})
const trimRequest = require('trim-request')

/*
 * Get item route
 */
router.get(
  '/',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['customer', 'admin', 'manager', 'seller', 'user']),
  trimRequest.all,
  // validate.getItem,
  controller.getItem
)

// router.patch(
//   '/:id',
//   origin.checkDomain,
//   origin.checkTenant,
//   requireAuth,
//   AuthController.roleAuthorization(['admin']),
//   trimRequest.all,
//   validate.updateItem,
//   controller.upload.single('logo'),
//   controller.updateItem
// )

router.patch(
  '/',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin']),
  trimRequest.all,
  controller.upload.single('logo'),
  controller.updateItem
)

router.patch(
  '/invoice',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin']),
  trimRequest.all,
  controller.updateItemInvoice
)

router.get(
  '/invoice',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin']),
  trimRequest.all,
  controller.getItemInvoice
)

// New configuration
router.get(
  '/general',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin']),
  trimRequest.all,
  // validate.getItem,
  controller.getItem
)

router.patch(
  '/general',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin']),
  trimRequest.all,
  controller.upload.single('logo'),
  controller.updateItem
)

router.get(
  '/invoiceTemplates',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin']),
  trimRequest.all,
  controller.getInvoiceTemplates
)

router.get(
  '/template/:design',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin']),
  trimRequest.all,
  validate.getTemplate,
  controller.getTemplate
)

router.patch(
  '/template/:design',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin']),
  trimRequest.all,
  validate.updateTemplate,
  controller.updateTemplate
)

router.get(
  '/taxes',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin']),
  trimRequest.all,
  controller.getTaxes
)

router.patch(
  '/taxes',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin']),
  trimRequest.all,
  validate.updateTaxes,
  controller.updateTaxes
)

router.get(
  '/payment',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin']),
  trimRequest.all,
  controller.getCard
)

router.patch(
  '/payment',
  origin.checkDomain,
  origin.checkTenant,
  requireAuth,
  AuthController.roleAuthorization(['admin']),
  trimRequest.all,
  validate.addCard,
  controller.addCard
)



module.exports = router
