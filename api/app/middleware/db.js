const mongoose = require('mongoose')
const moment = require('moment')
const _ = require('lodash');
const {
  buildSuccObject,
  buildErrObject,
  itemNotFound
} = require('../middleware/utils')

/**
 * Builds sorting
 * @param {string} sort - field to sort from
 * @param {number} order - order for query (1,-1)
 */
const buildSort = (sort, order) => {
  const sortBy = {}
  sortBy[sort] = order
  return sortBy
}

/**
 * Hack for mongoose-paginate, removes 'id' from results
 * @param {Object} result - result object
 */
const cleanPaginationID = (result) => {
  result.docs.map((element) => delete element.id)
  return result
}

/**
 * Builds initial options for query
 * @param {Object} query - query object
 */
const listInitOptions = async (req) => {
  return new Promise((resolve) => {
    const order = req.query.order || -1
    const sort = req.query.sort || 'createdAt'
    const sortBy = buildSort(sort, order)
    const page = parseInt(req.query.page, 10) || 1
    const limit = parseInt(req.query.limit, 10) || 15
    const options = {
      sort: sortBy,
      lean: true,
      page,
      limit
    }
    resolve(options)
  })
}

/**
 *
 * @param fields
 * @param count
 * @param delimiter
 * @returns {Promise<unknown>}
 */

const parseMultiFilters = async (fields, count = 0, delimiter = ',') => {
  return new Promise((resolve) => {
    try {
      fields = fields.split(delimiter)[count]
      resolve(fields)
    } catch (e) {
      resolve([])
    }
  })
}

/**
 *
 * @param str
 * @returns {Promise<unknown>}
 */

const getFirstAndLast = async (str) => {
  return new Promise((resolve) => {
    try {
      const regex = /\[(.*?)\]/g
      const sensibleData = regex.exec(str)
      // console.log(sensibleData)
      resolve({
        first: str.split('').find(() => true),
        last: str
          .split('')
          .reverse()
          .find(() => true),
        value: sensibleData && sensibleData.length ? sensibleData[1] : {}
      })
    } catch (e) {
      resolve({})
    }
  })
}

/**
 *
 * @param str
 * @param field
 * @param model
 * @returns {Promise<unknown>}
 */

const parseTypeValue = async (str = null, field = null, model = null) => {
  try {
    return new Promise((resolve) => {
      const type = model.schema.paths[field]
        ? model.schema.paths[field].instance
        : null;

      switch (type) {
        case 'Date':
          resolve({
            $gte: moment(str).startOf('day').toISOString(),
            $lt: moment(str).endOf('day').toISOString()
          })
          break
        case 'Boolean':
          resolve({
            $eq: str
          })
          break
        case 'ObjectID':
          resolve({
            $eq: mongoose.Types.ObjectId(str)
          })
          break
        case 'Array':
          resolve({
            $in: [new RegExp(str, 'i')]
          })
          break
        default:
          if (str.includes('*')) {
            str = str.replace('*', '')
            resolve({
              $eq: str
            })
          } else {
            resolve({
              $regex: new RegExp(str, 'i')
            })
          }

          break
      }
    })
  } catch (e) {
    resolve({
      $regex: new RegExp(str, 'i')
    })
  }
}

module.exports = {
  /**
   * New version from filter combined and multiple
   */
  async checkQueryFilterMulti(query, model) {
    return new Promise(async (resolve, reject) => {
      try {
        const strict = (query.strict);
        if (
          // Fields: Name of label on model Mongo
          typeof query.filter !== 'undefined' &&
          typeof query.fields !== 'undefined'
        ) {
          let data;
          if (strict) {
            data = {
              $and: []
            }
          } else {
            data = {
              $or: []
            }
          }

          const array = []
          const arrayFields = query.fields.split(',')
          const arrayFilter = query.filter.split(',')

          let count = -1

          await Promise.all(
            arrayFields.map(async (item) => {
              count += 1
              const multiFilters = await parseMultiFilters(query.filter,
                (arrayFilter.length !== arrayFields.length) ? 0 : count)
              let filterArray = {}
              const {first, last, value} = await getFirstAndLast(multiFilters)

              /**
               * Search in range [1,100]
               */

              if (first === '[' && last === ']') {
                let tmpRangeObject = {}
                const initialValue = value.split('|').find(() => true)
                const lastValue = value
                  .split('|')
                  .reverse()
                  .find(() => true)

                if (initialValue !== 'infinity') {
                  tmpRangeObject = {
                    ...tmpRangeObject,
                    ...{$gte: parseFloat(initialValue)}
                  }
                }
                if (lastValue !== 'infinity') {
                  tmpRangeObject = {
                    ...tmpRangeObject,
                    ...{$lte: parseFloat(lastValue)}
                  }
                }

                filterArray = {
                  [item]: tmpRangeObject
                }
                array.push(filterArray)
              } else {
                const queryValue = await parseTypeValue(multiFilters, item, model)

                filterArray = {
                  [item]: queryValue
                }
                array.push(filterArray)
              }
            })
          )
          data[(strict) ? '$and' : '$or'] = array
          resolve(data)
        } else {
          resolve({})
        }
      } catch (err) {
        reject(buildErrObject(422, 'ERROR_WITH_MULTI_FILTER'))
      }
    })
  },
  /**
   * Checks the query string for filtering records
   * query.filter should be the text to search (string)
   * query.fields should be the fields to search into (array)
   * @param {Object} query - query object
   */
  async checkQueryString(query) {
    return new Promise((resolve, reject) => {
      try {
        if (
          typeof query.filter !== 'undefined' &&
          typeof query.fields !== 'undefined'
        ) {
          const data = {
            $or: []
          }
          const array = []
          // Takes fields param and builds an array by splitting with ','
          const arrayFields = query.fields.split(',')
          // Adds SQL Like %word% with regex
          arrayFields.map((item) => {
            array.push({
              [item]: {
                $regex: new RegExp(query.filter, 'i')
              }
            })
          })
          // Puts array result in data
          data.$or = array
          resolve(data)
        } else {
          resolve({})
        }
      } catch (err) {
        reject(buildErrObject(422, 'ERROR_WITH_FILTER'))
      }
    })
  },

  /**
   *
   */
  async findSettingsPlugin(idPlugin = null, model = null, tenant = null) {
    return new Promise(async (resolve, reject) => {
      try {
        const query = {
          'plugins._id': mongoose.Types.ObjectId(idPlugin)
        }
        model.byTenant(tenant).findOne(query, (err, item) => {
          resolve(item)
        })
      } catch (err) {
        reject(buildErrObject(422, 'ERROR_WITH_FILTER'))
      }
    })
  },
  /** */
  async findPluginsByName(names = [], model) {
    return new Promise((resolve, reject) => {
      model.find({
        path: {$in: [...names]}
      },(err , items) => {
        if(!err){
          resolve(items)
        }else{
          reject(buildErrObject(422, 'ERROR_WITH_FILTER'))
        }
      })
    })
  },
  /**
   *
   */
  async findSettingsAggregate(idPlugin = null, model = null, tenant = null) {
    return new Promise(async (resolve, reject) => {
      try {
        const query = {
          'plugins.path': idPlugin
        }
        model
          .byTenant(tenant)
          .findOne(
            query,
            {_id: 0, plugins: {$elemMatch: {path: idPlugin}}},
            (err, res) => {
              if (res && res.plugins) {
                resolve(res.plugins.find((a) => true))
              } else {
                resolve(null)
              }
            }
          )
      } catch (err) {
        reject(buildErrObject(422, 'ERROR_SETTING_AGGREGATE'))
      }
    })
  },
  /**
   * Get without Tenant
   */
  async getWithOutTenant(req, model, query) {
    const options = await listInitOptions(req)
    return new Promise((resolve, reject) => {
      model.paginate(query, options, (err, items) => {
        if (err) {
          reject(buildErrObject(422, err.message))
        }
        resolve(cleanPaginationID(items))
      })
    })
  },
  /**
   * Gets items from database
   * @param {Object} req - request object
   * @param {Object} query - query object
   */
  async getItems(req, model, query) {
    const options = await listInitOptions(req)
    const tenant = req.clientAccount
    return new Promise((resolve, reject) => {
      model.byTenant(tenant).paginate(query, options, (err, items) => {
        if (err) {
          reject(buildErrObject(422, err.message))
        }
        resolve(cleanPaginationID(items))
      })
    })
  },

  /**
   * Get with inventory
   */
  /**
   * Get with inventory
   */

  //TODO: Se debe chekcar si viene seriales y modificaar el query para que busque por elements

  getLookListPurchases(model, query = {}, tenant = null, author = null) {
    if (author && author.role.includes('seller')) {
      query = {
        ...query,
        ...{
          $and: [
            {author: mongoose.Types.ObjectId(author._id)}
          ]
        }
      }
    }
    return model.byTenant(tenant).aggregate([
      {
        $lookup: {
          from: 'users',
          let: {idUser: '$customer._id'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{$eq: ['$$idUser', '$_id']}]
                }
              }
            },
            {
              $project: {
                email: 1,
                lastName: 1,
                name: 1,
                phone: 1,
                _id: 1
              }
            }
          ],
          as: 'customer'
        }
      },
      {
        $lookup: {
          from: 'users',
          let: {idUser: '$author'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{$eq: ['$$idUser', '$_id']}]
                }
              }
            },
            {
              $project: {
                email: 1,
                lastName: 1,
                name: 1,
                _id: 1
              }
            }
          ],
          as: 'authorData'
        }
      },
      {
        $lookup: {
          from: 'orders',
          let: {idPurchase: '$_id'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{$eq: ['$$idPurchase', '$purchase._id']}]
                }
              }
            },
            {
              $project: {
                _id: 1,
                currency: 1,
                customData: 1,
                customer: 1,
                description: 1,
                platform: 1,
                tag: 1,
                status: 1,
                createdAt: 1,
                updatedAt: 1,
                amount: 1
              }
            }
          ],
          as: 'orders'
        }
      },
      {$unwind: '$customer'},
      {$unwind: '$authorData'},
      {
        $match: query
      },
      {
        $project: {
          _id: 1,
          orders: 1,
          customer: 1,
          items: 1,
          author: '$authorData',
          status: 1,
          invoiceAddress: 1,
          deliveryDate: 1,
          deliveryAddress: 1,
          download: 1,
          deliveryType: 1,
          total: 1,
          sumTaxesTotal: 1,
          taxes: 1,
          subTotal: 1,
          tag: 1,
          attachment: 1,
          convert: 1,
          controlNumber: 1,
          invoiceNumber: 1,
          description: 1,
          createdAt: 1,
          updatedAt: 1,
          totalPay: {"$sum": "$orders.amount"},
          totalSubtract: {"$subtract": ["$total", {"$sum": "$orders.amount"}]}
        }
      }
    ])
  },

  getLookInventories(model, query = {}, tenant = null) {
    return model.byTenant(tenant).aggregate([
      {
        $lookup: {
          from: 'products',
          let: {idProduct: '$product._id'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{$eq: ['$$idProduct', '$_id']}]
                }
              }
            }
          ],
          as: 'product'
        }
      },
      {$unwind: '$product'},
      {
        $match: query
      }
    ])
  },

  getLookNotification(model, query = {}, tenant = null, author = null) {
    return model.byTenant(tenant).aggregate([
      {
        $match: {
          $or: [{"receptor": null}, {"receptor": author._id}]
        }
      },
      {
        $lookup: {
          from: 'users',
          let: {idReceptor: '$receptor'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{$eq: ['$$idReceptor', '$_id']}]
                }
              }
            },
            {
              $project: {
                '_id': 1,
                'name': 1,
                'lastName': 1,
                'email': 1,
                'role': 1
              }
            }
          ],
          as: 'receptor'
        }
      },
      {
        $lookup: {
          from: 'users',
          let: {idSender: '$sender'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{$eq: ['$$idSender', '$_id']}]
                }
              }
            },
            {
              $project: {
                '_id': 1,
                'name': 1,
                'lastName': 1,
                'email': 1,
                'role': 1
              }
            }
          ],
          as: 'sender'
        }
      },
      {$unwind: '$receptor'},
      {$unwind: '$sender'},
      {
        $match: query
      }
    ])
  },

  getLookListProducts(model, query = {}, tenant = null) {
    return model.byTenant(tenant).aggregate([
      {
        $lookup: {
          from: 'inventories',
          let: {idProduct: '$_id'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    {
                      $eq: ['$$idProduct', '$product._id'],
                    }
                  ]
                }
              }
            },
            {
              $lookup: {
                from: 'deposits',
                let: {idDeposit: '$deposit._id'},
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [
                          {
                            $eq: ['$$idDeposit', '$_id'],
                          }
                        ]
                      }
                    }
                  }
                ],
                as: 'inDep'
              }
            },
            {$unwind: '$inDep'},
            {
              $group: {
                _id: '$deposit._id',
                qty: {"$sum": "$qty"},
                name: {
                  $first: "$inDep.name"
                },
                serial: {
                  $addToSet: '$serials'
                },
              }
            },
            {
              $project: {
                name: 1,
                qty: 1,
                serial: {
                  "$reduce": {
                    input: "$serial",
                    initialValue: [],
                    in: {$concatArrays: ["$$value", "$$this"]}
                  }
                }
              }
            }
          ],
          as: 'inventories'
        }
      },
      {
        $lookup: {
          from: 'inventories',
          let: {idProduct: '$_id'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{$eq: ['$$idProduct', '$product._id']}]
                }
              }
            },
            {
              $project: {
                'provider.name': 1,
                'provider._id': 1
              }
            }
          ],
          as: 'providers'
        }
      },
      {
        $lookup: {
          from: 'settings',
          let: {tenant: '$tenantId'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{$eq: ['$$tenant', '$tenantId']}]
                }
              }
            },
            {
              $project: {
                'tax': 1
              }
            }
          ],
          as: 'settings'
        }
      },
      {$unwind: '$settings'},
      {
        $project: {
          _id: 1,
          gallery: 1,
          name: 1,
          prices: 1,
          measures: 1,
          categories: 1,
          tag: 1,
          sku: 1,
          description: 1,
          author: 1,
          createdAt: 1,
          updatedAt: 1,
          qty: {$sum: '$inventories.qty'},
          tax: '$settings.tax',
          depositList: '$inventories',
          deposits: '$inventories.name',
          providers: '$providers.provider.name'
        }
      },
      {
        $match: query
      }
    ])
  },

  getLookListOrders(model, query = {}, tenant = null) {
    return model.byTenant(tenant).aggregate([
      {
        $lookup: {
          from: 'users',
          let: {idUser: '$customer'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{$eq: ['$$idUser', '$_id']}]
                }
              }
            },
            {
              $project: {
                _id: 1,
                name: 1,
                email: 1,
                role: 1,
                lastName: 1
              }
            }
          ],
          as: 'customer'
        }
      },
      {
        $lookup: {
          from: 'users',
          let: {idAuthor: '$author'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{$eq: ['$$idAuthor', '$_id']}]
                }
              }
            },
            {
              $project: {
                _id: 1,
                name: 1,
                email: 1,
                role: 1,
                lastName: 1
              }
            }
          ],
          as: 'author'
        }
      },
      {$unwind: '$customer'},
      {$unwind: '$author'},
      {
        $project: {
          _id: 1,
          status: 1,
          platform: 1,
          customer: 1,
          purchase: 1,
          purchase_data: 1,
          reference: 1,
          amount: 1,
          currency: 1,
          author: 1,
          description: 1,
          tag: 1,
          customData: 1,
          createdAt: 1,
          updatedAt: 1
        }
      },
      {
        $match: query
      }
    ])
  },

  getAnalyticStatus(model, query = {}, tenant = null) {
    return model.byTenant(tenant).aggregate([
      {
        $match: query
      },
      {
        $project: {
          tag: 0,
          currency: 0,
          customData: 0,
          "purchase.items": 0,
          "purchase.router": 0,
          "purchase.orders": 0,
        }
      },
      {
        $group:
          {_id: "", total: {$sum: {$toDecimal: "$amount"}}, general: {$push: "$$ROOT"}}
      },
      {
        $project: {_id: 0, total: 1, general: 1}
      }
    ])
  },

  getAnalyticAuthor(model, query = {}, tenant = null) {
    return model.byTenant(tenant).aggregate([
      {
        $match: query
      },
      {
        $group: {
          _id: {
            idAuthor: "$author",
            status: "$status",
          },
          purchases: {$addToSet: "$$ROOT"},
          total: {
            $sum: "$total"
          }
        }
      },
      {
        $group: {
          _id: "$_id.idAuthor",
          purchasesGroup: {
            $addToSet: {
              status: "$_id.status",
              total: "$total",
              purchases: "$purchases"
            }
          }
        }
      },
      {
        $lookup: {
          from: 'users',
          let: {idAuthor: '$_id'},
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{$eq: ['$$idAuthor', '$_id']}]
                }
              }
            },
            {
              $project: {
                _id: 1,
                name: 1,
                email: 1,
                role: 1,
                lastName: 1
              }
            }
          ],
          as: 'author'
        }
      },
      {$unwind: '$author'},
      {$unwind: '$purchasesGroup'},
      {
        $project: {
          "_id": 0,
          "purchasesGroup.purchases.items": 0,
          "purchasesGroup.purchases.attachment": 0,
        }
      },
      {$sort: {"purchasesGroup.total": -1}},
      {
        $project: {
          "items": 0,
          "router": 0,
          "orders": 0,
          "attachment": 0
        }
      }
    ])
  },

  getAnalyticItems(model, query = {}, tenant = null) {
    return model.byTenant(tenant).aggregate([
      {
        $match: query
      },
      {
        $project: {
          "router": 0,
          "orders": 0,
          "attachment": 0,
        }
      },
      {
        $unwind: "$items",
      },
      {
        $unwind: "$items.prices",
      },
      {
        $group: {
          _id: "$items._id",
          total: {$sum: {$multiply: [{$toDouble: "$items.qty"}, "$items.prices.amount"]}},
          item: {$addToSet: "$$ROOT.items"},
        }
      },
      {$unwind: "$item"},
      {
        $project: {
          "item.depositList": 0,
          "item.deposits": 0,
          "item.router": 0,
          "item.deposit": 0,
        }
      },
    ])
  },
   getAnalyticPurchases(model, query = {}, tenant = null) {
    return model.byTenant(tenant).aggregate([
      {
        $match: query
      },
      {
        $project: {
          items: 1
        }
      }
    ])
  },

  /**
   * GetItem aggregate
   */
  async getItemAggregate(aggregate = {}, model, tenant = null) {
    return new Promise((resolve, reject) => {
      model.byTenant(tenant).aggregate(aggregate, (err, item) => {
        itemNotFound(err, item, reject, 'NOT_FOUND')
        resolve(item.find((a) => true))
      })
    })
  },

  /**
   *
   */
  async getSettings(query, model, tenant = null) {
    return new Promise((resolve, reject) => {
      model.byTenant(tenant).findOne(query, (err, item) => {
        itemNotFound(err, item, reject, 'NOT_FOUND')
        resolve(item)
      })
    })
  },
  /**
   *
   */
  async getItemWithOut(id, model) {
    return new Promise((resolve, reject) => {
      model.findById(id, (err, item) => {
        itemNotFound(err, item, reject, 'NOT_FOUND')
        resolve(item)
      })
    })
  },
  /**
   * Gets item from database by id
   * @param {string} id - item id
   * @param model
   * @param tenant
   */
  async getItem(id, model, tenant = null) {
    return new Promise((resolve, reject) => {
      model.byTenant(tenant).findById(id, (err, item) => {
        itemNotFound(err, item, reject, 'NOT_FOUND')
        resolve(item)
      })
    })
  },

  /**
   * Creates a new item in database
   * @param {Object} req - request object
   * @param model
   * @param tenant
   */
  async createItem(req, model, tenant = null) {
    return new Promise((resolve, reject) => {
      model.byTenant(tenant).create(req, (err, item) => {
        if (err) {
          reject(buildErrObject(422, err.message))
        }
        resolve(item)
      })
    })
  },

  /**
   * Update plugin active
   */
  async updatePlugin(query = {}, model, req, tenant = null, push = true) {
    return new Promise((resolve, reject) => {
      const updateDataPush = {
        $push: {plugins: req}
      }
      const updateDataPull = {
        $pull: {plugins: {path: req.path}}
      }
      model.byTenant(tenant).findByIdAndUpdate(
        query,
        push ? updateDataPush : updateDataPull,
        {
          runValidators: true
        },
        (err, item) => {
          itemNotFound(err, item, reject, 'NOT_FOUND')
          resolve(item)
        }
      )
    })
  },
  /**
   * Updates an item in database by id
   * @param {string} id - item id
   * @param model
   * @param {Object} req - request object
   * @param tenant
   */
  async updateItem(id, model, req, tenant = null) {
    return new Promise((resolve, reject) => {
      model.byTenant(tenant).findByIdAndUpdate(
        id,
        req,
        {
          new: true,
          runValidators: true
        },
        (err, item) => {
          itemNotFound(err, item, reject, 'NOT_FOUND')
          resolve(item)
        }
      )
    })
  },
  /**
   * Updates an item in database by id
   * @param {string} id - item id
   * @param model
   * @param {Object} req - request object
   * @param tenant
   */
  async updateItemOrInsert(query, model, req, tenant = null) {
    return new Promise((resolve, reject) => {
      model.byTenant(tenant).update(
        query,
        req,
        {
          new: true,
          upsert: true,
          runValidators: true
        },
        (err, item) => {
          itemNotFound(err, item, reject, 'NOT_FOUND')
          resolve(item)
        }
      )
    })
  },

  /**
   * Deletes an item from database by id
   * @param {string} id - id of item
   * @param model
   * @param tenant
   */
  async deleteItem(id, model, tenant = null) {
    return new Promise((resolve, reject) => {
      model.byTenant(tenant).delete({_id: mongoose.Types.ObjectId(id)}, (err, item) => {
        itemNotFound(err, item, reject, 'NOT_FOUND')
        resolve(buildSuccObject('DELETED'))
      })
    })
  },

  /**
   *Force Delete NOT Soft Delete
   * @param {*} id
   * @param {*} model
   */
  async deleteItemForce(id, model) {
    return new Promise((resolve, reject) => {
      model.findByIdAndRemove(id, (err, item) => {
        itemNotFound(err, item, reject, 'NOT_FOUND')
        resolve(buildSuccObject('DELETED'))
      })
    })
  },


  /**
   * Gets items from database
   * @param {Object} req - request object
   * @param model
   * @param aggregate
   * @param tenant
   */
  async getItemsAggregate(req, model, aggregate, tenant = null) {
    const options = await listInitOptions(req)
    return new Promise((resolve, reject) => {
      model
        .byTenant(tenant)
        .aggregatePaginate(aggregate, options, (err, items) => {
          if (err) {
            reject(buildErrObject(422, err.message))
          }
          resolve(cleanPaginationID(items))
        })
    })
  },

  async getPreviousData(model = {}, filter = {}) {
    return new Promise((resolve, reject) => {
      model.findOne(filter, (err, item) => {
        if (!err) {
          resolve(item)
        } else {
          reject(err)
        }
      })
    })
  }
}
