const model = require('../models/calendar')
const db = require('../middleware/db')
const moment = require('moment')
const mongoose = require('mongoose')
exports.addDelivery = async (data, tenant) => {
  const dataParse = {
    start: moment(data.deliveryDate).startOf('day').toDate(),
    end: moment(data.deliveryDate).startOf('day').toDate(),
    title: `${data.customer.name} #${data.controlNumber}`,
    observation: data.description,
    customData: {
      purchase: mongoose.Types.ObjectId(data._id),
      route: `purchase/${data._id}`
    }
  }
  await db.updateItemOrInsert(dataParse, model, data, tenant)
}

