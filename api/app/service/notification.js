const settings = require('../models/settings');
const notifications = require('../models/notifications');
const emailer = require('../middleware/emailer');

const getSetting = (tenantId) => new Promise((resolve, reject) => {
  settings.byTenant(tenantId).aggregate(
    [
      {
        "$lookup": {
          "from": "users",
          "localField": "owner",
          "foreignField": "_id",
          "as": "user"
        }
      },
      {'$unwind': '$user'},
      {
        "$project": {
          "user": 1,
          "tenantId": 1,
          "name": 1,
          "_id": 1
        }
      }
    ], (err, item) => {
      if (!err) {
        resolve(item.find(() => true))
      } else {
        reject(err)
      }
    }
  )
})


exports.notiPurchase = async (data = {}, author = null, locale) => {
  const settingTenant = await getSetting(data.tenantId)
  const {user = {}, tenantId} = settingTenant;
  const body = {
    "receptor": user._id,
    "sender": author._id,
    "title": "NOTIFICATIONS.NEW_PURCHASE",
    "body": `${author.name}, realizó un nuevo pedido `,
    "behavior": "route",
    "url": `/purchase/${data._id}`,
    "event": null
  }
  notifications.byTenant(tenantId).create(body, (err, item) => {
    emailer.sendNotifications(locale, user, body.url, tenantId)
  })
}


exports.sendNotifications = async (data = {}, author = null, locale) => {

  data = JSON.parse(JSON.stringify(data))
  const settingTenant = await getSetting(data.tenantId)
  const {user = {}, tenantId} = settingTenant;
  const body = {
    "receptor": user._id,
    "sender": user._id,
    "title": `${data.title}`,
    "body": `${data.body}`,
    "behavior": "route",
    "url": null,
    "event": data.event ? data.event : null
  }


  notifications.byTenant(tenantId).create(body, (err, item) => {
    emailer.sendNotifications(locale, user, body.url, tenantId)
  })
}
