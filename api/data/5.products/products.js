const faker = require('faker')
const ObjectID = require('mongodb').ObjectID

module.exports = [
    {
        _id: new ObjectID('5f67b0ecf077774b10218641'),
        name: 'Producto Seed',
        prices: [
            { amount: 200 }
        ],
        tenantId: 'subdomain',
        author: "5aa1c2c35ef7a4e97b5e995a",
        measures: '',
        categories: [
            'cat_prueba'
        ],
        tag: [
            'prueba'
        ],
        gallery: [],
        sku: 'product_prueba',
        description: faker.commerce.productAdjective()
    }
]