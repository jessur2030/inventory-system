const faker = require('faker')
const ObjectID = require('mongodb').ObjectID
const user = require('../1.users/user')

module.exports = [
  {
    //   _id: new ObjectID('5aa1c2c35ef7a4e97b5e995h'),
    name: faker.name.firstName(),
    currencySymbol: '$',
    currency: 'USD',
    owner: user[0]._id,
    logo: faker.internet.avatar(),
    plugins: [],
    tenantId: 'subdomain'
  }
]