const faker = require('faker')
const ObjectID = require('mongodb').ObjectID
const manager = require('../1.users/user')

module.exports = [
    {
        _id: new ObjectID('5f681d5d35346711f885627e'),
        name: 'Proveedor Seed',
        address: faker.address.streetAddress(),
        manager:manager[0],
        trace:'',
        phone: faker.phone.phoneNumber(),
        email: faker.internet.email(),
        description: '',
      tenantId: 'subdomain'
    }
]
