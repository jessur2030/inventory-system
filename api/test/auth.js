/* eslint handle-callback-err: "off"*/

process.env.NODE_ENV = 'test'

const User = require('../app/models/user')
const Settings = require('../app/models/settings')
const faker = require('faker')
const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../server')
const {credentials} = require('./analytics')
// eslint-disable-next-line no-unused-vars

const should = chai.should()
const origin = "https://www.subdomain.kitagil.com"
let token = ''
const createdID = []
const settingsID = []
let verification = ''
let verificationForgot = ''
const email = faker.internet.email()
// const credentials =

chai.use(chaiHttp)

describe('*********** AUTH ***********', () => {
  describe('/GET /', () => {
    it('it should GET home API url', (done) => {
      chai
        .request(server)
        .get('/api/1.0/')
        .set('Origin', origin)
        .end((err, res) => {
          res.should.have.status(200)
          done()
        })
    })
  })

  describe('/GET /404url', () => {
    it('it should GET 404 url', (done) => {
      chai
        .request(server)
        .get('/api/1.0/404url')
        .end((err, res) => {
          res.should.have.status(404)
          res.body.should.be.an('object')
          done()
        })
    })
  })

  describe('/POST login', () => {
    it('it should GET token', (done) => {
      console.log(credentials.admin)
      chai
        .request(server)
        .post('/api/1.0/login')
        .set('Origin', origin)
        .send(credentials.admin)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.have.property('session')
          token = res.body.session
          done()
        })
    }),
    it('Se obtendra 404 si tenant no es detectado', (done) => {
      chai
      .request(server)
      .post('/api/1.0/login')
      .send(credentials.admin)
      .end((err, res) => {
        console.log(res)
        res.should.have.status(404)
          res.body.should.be.an('object')
        done()
      })
    })
  })

  describe('/POST register', () => {
    it('it should POST register whit new tenant', (done) => {
      const new_tenant = "newtenant"
      const user = {
        name: faker.random.words(),
        email: credentials.admin.email,
        password: credentials.admin.password,
      }
      chai
        .request(server)
        .post('/api/1.0/register')
        .set('Origin', `https://www.${new_tenant}.kitagil.com`)
        .send(user)
        .end((err, res) => {
          console.log(res.body.errors)
          res.should.have.status(201)
          res.body.should.be.an('object')
          res.body.should.include.keys('session', 'user')
          createdID.push(res.body.user._id)
          settingsID.push(new_tenant)
          verification = res.body.user.verification
          done()
        })
    })
    it('it should NOT POST a register if the tenant already exists', (done) => {
      const user = {
        name: 'New user',
        email : "admin@admin.com",
        password: "12345"
      }
      chai
        .request(server)
        .post('/api/1.0/register')
        .set('Origin', origin)
        .send(user)
        .end((err, res) => {
          res.should.have.status(422)
          res.body.should.be.a('object')
          res.body.should.have.property('errors')
          done()
        })
    })
  })

  // describe('/POST verify', () => {
  //   it('it should POST verify', (done) => {
  //     chai
  //       .request(server)
  //       .post('/api/1.0/verify')
  //       .set('Origin', origin)
  //       .send({
  //         id: verification
  //       })
  //       .end((err, res) => {
  //         res.should.have.status(200)
  //         res.body.should.be.an('object')
  //         res.body.should.include.keys('email', 'verified')
  //         res.body.verified.should.equal(true)
  //         done()
  //       })
  //   })
  // })

  describe('/POST forgot', () => {
    it('it should POST forgot', (done) => {
      chai
        .request(server)
        .post('/api/1.0/forgot')
        .set('Origin', origin)
        .send({
          email: 'admin@admin.com'
        })
        .end((err, res) => {
          console.log(res.body)
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.include.keys('msg', 'verification')
          verificationForgot = res.body.verification
          done()
        })
    })
  })

  describe('/POST reset', () => {
    it('it should POST reset', (done) => {
      chai
        .request(server)
        .post('/api/1.0/reset')
        .set('Origin', origin)
        .send({
          id: verificationForgot,
          password: '12345'
        })
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('msg').eql('PASSWORD_CHANGED')
          done()
        })
    })
  })

  describe('/GET token', () => {
    it('it should NOT be able to consume the route since no token was sent', (done) => {
      chai
        .request(server)
        .get('/api/1.0/token')
        .set('Origin', origin)
        .end((err, res) => {
          res.should.have.status(401)
          done()
        })
    })
    it('it should GET a fresh token', (done) => {
      chai
        .request(server)
        .get('/api/1.0/token')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.have.property('session')
          done()
        })
    })
  })

  after(() => {
    createdID.forEach((id) => {
      User.findByIdAndRemove(id, (err) => {
        if (err) {
          console.log(err)
        }
      })
    })
    settingsID.forEach((tenant) => {
      Settings.findOneAndDelete({tenantId:tenant}, (err) => {
        if (err) {
          console.log(err)
        }
      })
    })
  })
})
