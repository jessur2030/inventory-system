/* eslint handle-callback-err: "off"*/

process.env.NODE_ENV = 'test'

const chai = require('chai')
const chaiHttp = require('chai-http')
const Inventorie = require('../app/models/inventory')
const server = require('../server')
// eslint-disable-next-line no-unused-vars
const faker = require('faker')
const should = chai.should()
const createdID = []
const newName = faker.random.words()
const loginDetails = {
  admin: {
    id: '5aa1c2c35ef7a4e97b5e995a',
    email: 'admin@admin.com',
    password: '12345',
  },
  user: {
    id: '5aa1c2c35ef7a4e97b5e995b',
    email: 'user@user.com',
    password: '12345'
  },
  manager: {
    id: '5aa1c2c35ef7a4e97b5e995c',
    email: 'manager@manager.com',
    password: '12345'
  },
  seller: {
    id: '5aa1c2c35ef7a4e97b5e995d',
    email: 'seller@seller.com',
    password: '12345'
  },
}
const origin = "https://www.subdomain.kitagil.com"
const tokens = {
  admin: '',
  user: '',
  manager: '',
  seller: '',
}
const author = require('./../data/1.users/user')
const deposit = require('../data/4.deposits/deposits')
const { fake } = require('faker')
const products = require('../data/5.products/products')
const providers = require('../data/6.providers/providers')

chai.use(chaiHttp)

describe('*********** INVENTORIES ***********', () => {
  describe('/POST login', () => {
    it('it should GET token as admin', (done) => {
      chai
        .request(server)
        .post('/api/1.0/login')
        .set('Origin', origin)
        .send(loginDetails.admin)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.have.property('session')
          tokens.admin = res.body.session
          done()
        })
    })
    it('it should GET token as user', (done) => {
      chai
        .request(server)
        .post('/api/1.0/login')
        .set('Origin', origin)
        .send(loginDetails.user)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.have.property('session')
          tokens.user = res.body.session
          done()
        })
    })
    it('it should GET token as manager', (done) => {
      chai
        .request(server)
        .post('/api/1.0/login')
        .set('Origin', origin)
        .send(loginDetails.manager)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.have.property('session')
          tokens.manager = res.body.session
          done()
        })
    })
    it('it should GET token as seller', (done) => {
      chai
        .request(server)
        .post('/api/1.0/login')
        .set('Origin', origin)
        .send(loginDetails.seller)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.have.property('session')
          tokens.seller = res.body.session
          done()
        })
    })
  })
  // Get all inventories
  describe('/GET inventories', () => {
    it('it should NOT be able to consume the route since no token was sent', (done) => {
      chai
        .request(server)
        .get('/api/1.0/inventory')
        .set('Origin', origin)
        .end((err, res) => {
          res.should.have.status(401)
          done()
        })
    })
    it('it should GET all inventories sending a the token like admin and manager', (done) => {
      chai
        .request(server)
        .get('/api/1.0/inventory')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.include.keys('docs', 'hasNextPage','hasPrevPage','limit','nextPage','page',
          'pagingCounter','prevPage','totalDocs','totalPages')
          res.body.docs.should.be.a('array')
          res.body.hasNextPage.should.be.a('boolean')
          res.body.hasPrevPage.should.be.a('boolean')
          res.body.limit.should.be.a('number')
          res.body.page.should.be.a('number')
          res.body.pagingCounter.should.be.a('number')
          res.body.totalDocs.should.be.a('number')
          res.body.totalPages.should.be.a('number')
          res.body.docs[0].should.include.keys('address','author','deposit','description','priceBase','product','provider','qty','tag','tenantId',
          'trace','_id')
          res.body.docs[0].address.should.be.a('string')
          res.body.docs[0].author.should.be.a('object')
          res.body.docs[0].deposit.should.be.a('object')
          res.body.docs[0].description.should.be.a('string')
          res.body.docs[0].priceBase.should.be.a('number')
          res.body.docs[0].product.should.be.a('object')
          res.body.docs[0].provider.should.be.a('object')
          res.body.docs[0].qty.should.be.a('number')
          res.body.docs[0].tag.should.be.a('array')
          res.body.docs[0].tenantId.should.be.a('string')
          res.body.docs[0].trace.should.be.a('string')
          res.body.docs[0]._id.should.be.a('string')
          done()
        })
    })
    it('it should NOT be able to consume the route if the token is like user and seller', (done) => {
      chai
        .request(server)
        .get('/api/1.0/inventory')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.user}`)
        .end((err, res) => {
          res.should.have.status(403)
          res.body.should.be.an('object')
          // res.body.should.include.keys('name', 'email')
          done()
        })
    })
    it('it should GET the inventories with filters', (done) => {
      chai
        .request(server)
        .get('/api/1.0/inventory?filter=Producto Seed&fields=product.name')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.include.keys('docs', 'hasNextPage','hasPrevPage','limit','nextPage','page',
          'pagingCounter','prevPage','totalDocs','totalPages')
          res.body.docs.should.be.a('array')
          res.body.hasNextPage.should.be.a('boolean')
          res.body.hasPrevPage.should.be.a('boolean')
          res.body.limit.should.be.a('number')
          res.body.page.should.be.a('number')
          res.body.pagingCounter.should.be.a('number')
          res.body.totalDocs.should.be.a('number')
          res.body.totalPages.should.be.a('number')
          res.body.docs.should.be.a('array')
          res.body.docs.should.have.lengthOf(1)
          res.body.docs[0].should.include.keys('address','author','deposit','description','priceBase','product','provider','qty','tag','tenantId',
          'trace','_id')
          res.body.docs[0].address.should.be.a('string')
          res.body.docs[0].author.should.be.a('object')
          res.body.docs[0].deposit.should.be.a('object')
          res.body.docs[0].description.should.be.a('string')
          res.body.docs[0].priceBase.should.be.a('number')
          res.body.docs[0].product.should.be.a('object')
          res.body.docs[0].provider.should.be.a('object')
          res.body.docs[0].qty.should.be.a('number')
          res.body.docs[0].tag.should.be.a('array')
          res.body.docs[0].tenantId.should.be.a('string')
          res.body.docs[0].trace.should.be.a('string')
          res.body.docs[0]._id.should.be.a('string')
          res.body.docs[0].should.have.property("description").eql('Movimiento Seed')
          done()
        })
    })
  })
  describe('/POST inventories', () => {
    it('it should NOT be able to consume the route since no token was sent', (done) => {
      chai
        .request(server)
        .post('/api/1.0/inventory')
        .set('Origin', origin)
        .end((err, res) => {
          res.should.have.status(401)
          done()
        })
    })
    it('it should NOT POST a inventories without product, qty and author ', (done) => {
      const inventory = {}
      chai
        .request(server)
        .post('/api/1.0/inventory')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .send(inventory)
        .end((err, res) => {
          res.should.have.status(422)
          res.body.should.be.a('object')
          res.body.should.have.property('errors')
          done()
        })
    })
    it('it should POST a inventory only like admin', (done) => {
      const inventory = {
        product:products[0],
        deposit: deposit[0],
        address: faker.address.streetAddress(),
        author: author[0],
        provider: providers[0],
        serials: [],
        qty:100,
        priceBase: 20,
        tenantId: 'subdomain',
        tag: [],
        description: 'Movimiento test'
      }
      chai
        .request(server)
        .post('/api/1.0/inventory')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .send(inventory)
        .end((err, res) => {
          console.log(res.body.errors)
          res.should.have.status(201)
          res.body.should.be.a('object')
          res.body.should.include.keys('_id', 'description','author','deposit','priceBase','product','provider','purchase','qty'
          ,'serials','tag','tenantId','updatedAt','createdAt')
          // res.body.address.should.be.a('string')
          res.body.author.should.be.a('object')
          res.body.deposit.should.be.a('object')
          res.body.description.should.be.a('string')
          res.body.priceBase.should.be.a('number')
          res.body.product.should.be.a('object')
          res.body.provider.should.be.a('object')
          res.body.qty.should.be.a('number')
          res.body.tag.should.be.a('array')
          res.body.tenantId.should.be.a('string')
          // res.body.trace.should.be.a('string')
          res.body._id.should.be.a('string')
          createdID.push(res.body._id)
          done()
        })
    })
    it('it should NOT POST a inventory with token like seller, manager and user', (done) => {
      const inventory = {
        product:products[0],
        address: faker.address.streetAddress(),
        author: author[0],
        deposit: deposit[0],
        provider: providers[0],
        qty:100,
        priceBase: 20,
        tenantId: 'subdomain',
        tag: [],
        description: 'Movimiento test'
      }
      chai
        .request(server)
        .post('/api/1.0/inventory')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.seller}`)
        .send(inventory)
        .end((err, res) => {
          res.should.have.status(403)
          res.body.should.be.a('object')
          res.body.should.have.property('errors')
          done()
        })
    })
  })
  describe('/GET/:id inventory', () => {
    it('it should NOT be able to consume the route since no token was sent', (done) => {
      const id = createdID.slice(-1).pop()
      chai
        .request(server)
        .get(`/api/1.0/inventory/${id}`)
        .set('Origin', origin)
        .end((err, res) => {
          res.should.have.status(401)
          done()
        })
    })
    it('it should GET a inventory by the given id like admin, and manager', (done) => {
      const id = createdID.slice(-1).pop()
      chai
        .request(server)
        .get(`/api/1.0/inventory/${id}`)
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .end((error, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('description')
          res.body.should.have.property('_id').eql(id)
          done()
        })
    })
  })
  describe('/PATCH/:id inventory', () => {
    it('it should NOT be able to consume the route since no token was sent', (done) => {
      const inventory = {
        product:products[0],
        address: faker.address.streetAddress(),
        author: author[0],
        deposit: deposit[0],
        provider: providers[0],
        qty:100,
        serials: [],
        priceBase: 20,
        tenantId: 'subdomain',
        tag: [],
        description: 'Movimiento test'
      }
      const id = createdID.slice(-1).pop()
      chai
        .request(server)
        .patch(`/api/1.0/inventory/${id}`)
        .set('Origin', origin)
        .send(inventory)
        .end((err, res) => {
          res.should.have.status(401)
          done()
        })
    })
    it('it should UPDATE a inventory given the id with token only like admin', (done) => {
      const inventory = {
        product:products[0]._id,
        address: faker.address.streetAddress(),
        author: author[0],
        deposit: deposit[0]._id,
        provider: providers[0]._id,
        serials: [],
        qty:100,
        priceBase: 20,
        tenantId: 'subdomain',
        tag: [],
        description: 'Movimiento test update'
      }
      const id = createdID.slice(-1).pop()
      console.log(id)
      chai
        .request(server)
        .patch(`/api/1.0/inventory/${id}`)
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .send(inventory)
        .end((error, res) => {
          console.log(res.body.errors)
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.include.keys('product','provider','qty','priceBase','deposit','purchase','author','serials','tag',
          'description')
          res.body.should.have.property('_id').eql(id)
          res.body.should.have.property('description').eql('Movimiento test update')
          // res.body.address.should.be.a('string')
          // res.body.author.should.be.a('string')
          // res.body.deposit.should.be.a('object')
          // res.body.description.should.be.a('string')
          // res.body.priceBase.should.be.a('number')
          // res.body.product.should.be.a('object')
          // res.body.provider.should.be.a('object')
          // res.body.qty.should.be.a('number')
          // res.body.tag.should.be.a('array')
          // res.body.tenantId.should.be.a('string')
          // res.body.trace.should.be.a('string')
          res.body._id.should.be.a('string')
          done()
        })
    })
    it('it should NOT UPDATE a inventory given the id with token like manager, seller and user', (done) => {
      const inventory = {
        product:products[0],
        address: faker.address.streetAddress(),
        author: author[0],
        deposit: deposit[0],
        provider: providers[0],
        qty:100,
        priceBase: 20,
        tenantId: 'subdomain',
        tag: [],
        description: 'Movimiento test update'
      }
      const id = createdID.slice(-1).pop()
      chai
        .request(server)
        .patch(`/api/1.0/inventory/${id}`)
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.seller}`)
        .send(inventory)
        .end((err, res) => {
          res.should.have.status(403)
          res.body.should.be.a('object')
          res.body.should.have.property('errors')
          done()
        })
    })
  })
  describe('/DELETE/:id inventory', () => {
    it('it should DELETE a inventory given the id with token only like admin', (done) => {
      const inventory = {
        product:products[0],
        address: faker.address.streetAddress(),
        author: author[0],
        deposit: deposit[0],
        provider: providers[0],
        serials: [],
        qty:100,
        priceBase: 20,
        tenantId: 'subdomain',
        tag: [],
        description: 'Movimiento test update'
      }
      chai
        .request(server)
        .post('/api/1.0/inventory')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .send(inventory)
        .end((err, res) => {
          res.should.have.status(201)
          res.body.should.be.a('object')
          res.body.should.include.keys('_id', 'description')
          chai
            .request(server)
            .delete(`/api/1.0/inventory/${res.body._id}`)
            .set('Origin', origin)
            .set('Authorization', `Bearer ${tokens.admin}`)
            .end((error, result) => {
              result.should.have.status(200)
              result.body.should.be.a('object')
              result.body.should.have.property('msg').eql('DELETED')
              done()
            })
        })
    })
    it('it should NOT DELETE a inventory given the id with token like manager, seller and user', (done) => {
      const id = createdID.slice(-1).pop()
      chai
        .request(server)
        .delete(`/api/1.0/inventory/${id}`)
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.manager}`)
        .end((err, res) => {
          res.should.have.status(403)
          res.body.should.be.a('object')
          res.body.should.have.property('errors')
          done()
        })
    })
  })
  after(() => {
    createdID.forEach((id) => {
      Inventorie.findByIdAndRemove(id, (err) => {
        if (err) {
          console.log(err)
        }
      })
    })
  })

});