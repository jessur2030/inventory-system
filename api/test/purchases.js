/* eslint handle-callback-err: "off"*/

process.env.NODE_ENV = 'test'

const chai = require('chai')
const chaiHttp = require('chai-http')
const Purchase = require('../app/models/purchase')
const server = require('../server')
// eslint-disable-next-line no-unused-vars
const faker = require('faker')
const should = chai.should()
const createdID = []
const newName = faker.random.words()
const loginDetails = {
    admin: {
        id: '5aa1c2c35ef7a4e97b5e995a',
        email: 'admin@admin.com',
        password: '12345',
    },
    user: {
        id: '5aa1c2c35ef7a4e97b5e995b',
        email: 'user@user.com',
        password: '12345'
    },
    manager: {
        id: '5aa1c2c35ef7a4e97b5e995c',
        email: 'manager@manager.com',
        password: '12345'
    },
    seller: {
        id: '5aa1c2c35ef7a4e97b5e995d',
        email: 'seller@seller.com',
        password: '12345'
    },
}
const origin = "https://www.subdomain.kitagil.com"
const tokens = {
    admin: '',
    user: '',
    manager: '',
    seller: '',
}
const author = require('./../data/1.users/user')
const deposit = require('../data/4.deposits/deposits')
const { fake } = require('faker')
const products = require('../data/5.products/products')
const provider = require('../data/6.providers/providers')
const purchases = require('../data/8.purchases/purchases')

chai.use(chaiHttp)

describe('*********** PURCHASES ***********', () => {
    describe('/POST login', () => {
        it('it should GET token as admin', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.admin)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.admin = res.body.session
                    done()
                })
        })
        it('it should GET token as user', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.user)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.user = res.body.session
                    done()
                })
        })
        it('it should GET token as manager', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.manager)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.manager = res.body.session
                    done()
                })
        })
        it('it should GET token as seller', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.seller)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.seller = res.body.session
                    done()
                })
        })
    })
    // Get all purchases
    describe('/GET purchases', () => {
        it('it should NOT be able to consume the route since no token was sent', (done) => {
            chai
                .request(server)
                .get('/api/1.0/purchase')
                .set('Origin', origin)
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it('it should GET all purchases sending a the token like admin, manager and seller', (done) => {
            chai
                .request(server)
                .get('/api/1.0/purchase')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.include.keys('docs', 'hasNextPage', 'hasPrevPage', 'limit', 'nextPage', 'page',
                        'pagingCounter', 'prevPage', 'totalDocs', 'totalPages')
                    res.body.docs.should.be.a('array')
                    res.body.hasNextPage.should.be.a('boolean')
                    res.body.hasPrevPage.should.be.a('boolean')
                    res.body.limit.should.be.a('number')
                    res.body.page.should.be.a('number')
                    res.body.pagingCounter.should.be.a('number')
                    res.body.totalDocs.should.be.a('number')
                    res.body.totalPages.should.be.a('number')
                    res.body.docs[0].should.include.keys('author','customer','deliveryAddress','deliveryType','description','invoiceAddress','items','orders','status','total','_id')
                    res.body.docs[0].author.should.be.a('object')
                    res.body.docs[0].controlNumber.should.be.a('string')
                    res.body.docs[0].customer.should.be.a('object')
                    res.body.docs[0].deliveryAddress.should.be.a('string')
                    res.body.docs[0].deliveryType.should.be.a('string')
                    res.body.docs[0].description.should.be.a('string')
                    res.body.docs[0].invoiceAddress.should.be.a('string')
                    res.body.docs[0].items.should.be.a('array')
                    res.body.docs[0].orders.should.be.a('array')
                    res.body.docs[0].status.should.be.a('string')
                    res.body.docs[0].total.should.be.a('number')
                    done()
                })
        })
        it('it should NOT be able to consume the route if the token is like user', (done) => {
            chai
                .request(server)
                .get('/api/1.0/purchase')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.user}`)
                .end((err, res) => {
                    res.should.have.status(403)
                    res.body.should.be.an('object')
                    // res.body.should.include.keys('name', 'email')
                    done()
                })
        })
        it('it should GET the purchases with filters', (done) => {
            chai
                .request(server)
                .get('/api/1.0/purchase?filter=1601313425Mo&fields=controlNumber')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.docs.should.be.a('array')
                    res.body.docs.should.have.lengthOf(1)
                    res.body.should.include.keys('docs', 'hasNextPage', 'hasPrevPage', 'limit', 'nextPage', 'page',
                        'pagingCounter', 'prevPage', 'totalDocs', 'totalPages')
                    res.body.docs.should.be.a('array')
                    res.body.hasNextPage.should.be.a('boolean')
                    res.body.hasPrevPage.should.be.a('boolean')
                    res.body.limit.should.be.a('number')
                    res.body.page.should.be.a('number')
                    res.body.pagingCounter.should.be.a('number')
                    res.body.totalDocs.should.be.a('number')
                    res.body.totalPages.should.be.a('number')
                    res.body.docs[0].should.include.keys('author','customer','deliveryAddress','deliveryType','description','invoiceAddress','items','orders','status','total','_id')                    
                    res.body.docs[0].should.have.property("controlNumber").eql('SEED-1601313425Mo')
                    done()
                })
        })
    })
    describe('/POST purchases', () => {
        it('it should NOT be able to consume the route since no token was sent', (done) => {
            chai
                .request(server)
                .post('/api/1.0/purchase')
                .set('Origin', origin)
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it('it should NOT POST a purchases without customer, author, deliveryAddress and invoiceAddress', (done) => {
            const purchase = {}
            chai
                .request(server)
                .post('/api/1.0/purchase')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .send(purchase)
                .end((err, res) => {
                    res.should.have.status(422)
                    res.body.should.be.a('object')
                    res.body.should.have.property('errors')
                    done()
                })
        })
        it('it should POST a purchase only like admin, manager and seller', (done) => {
            var items = []
            items[0] = products[0]
            items[0].qty = 1
            items[0].depositList = [deposit[0]]
            items[0].deposits = [deposit[0].name]
            items[0].providers = [provider[0].name]
            items[0].deposit = deposit[0]
            const purchase = {
                controlNumber: 'TEST-1601233445FO',
                status: "hold",
                deliveryType: "to_send",
                total: 200,
                customer: author[2],
                deliveryDate: '2020-10-22T01:54:55.000Z',
                items: items,
                deliveryAddress: "Madrid",
                invoiceAddress: faker.address.streetAddress(),
                description: "Order Seed",
                author: author[0],
                tenantId: "subdomain",
            }
            chai
                .request(server)
                .post('/api/1.0/purchase')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .send(purchase)
                .end((err, res) => {
                    console.log(res.body.errors)
                    console.log(res.body)
                    res.should.have.status(201)
                    res.body.should.be.a('object')
                    res.body.should.include.keys('_id', 'description')
                    createdID.push(res.body._id)
                    done()
                })
        })
        it('it should NOT POST a purchase with token like customer and user', (done) => {
            var items = []
            items[0] = products[0]
            items[0].qty = 1
            items[0].depositList = [deposit[0]]
            items[0].deposits = [deposit[0].name]
            items[0].providers = [provider[0].name]
            items[0].deposit = deposit[0]
            const purchase = {
                controlNumber: 'TEST-1601233445Fo',
                status: "hold",
                deliveryType: "to_send",
                total: 200,
                customer: author[2],
                items: items,
                deliveryAddress: "Madrid",
                invoiceAddress: faker.address.streetAddress(),
                description: "Order Seed",
                author: author[0]._id,
                tenantId: "subdomain",
            }
            chai
                .request(server)
                .post('/api/1.0/purchase')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.user}`)
                .send(purchase)
                .end((err, res) => {
                    console.log(res.body)
                    console.log(res.body.errors)
                    res.should.have.status(403)
                    res.body.should.be.a('object')
                    res.body.should.have.property('errors')
                    done()
                })
        })
    })
    describe('/GET/:id purchase', () => {
        it('it should NOT be able to consume the route since no token was sent', (done) => {
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .get(`/api/1.0/purchase/${id}`)
                .set('Origin', origin)
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it('it should GET a purchase by the given id like admin, and manager', (done) => {
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .get(`/api/1.0/purchase/${id}`)
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((error, res) => {
                    res.should.have.status(200)
                    console.log(res.body)
                    res.body.should.be.a('object')
                    res.body.should.include.keys('_id','controlNumber','status','deliveryDate','deliveryType','convert','subTotal','sumTaxesTotal','taxes',
                    'total','attachment','customer','items','deliveryAddress','invoiceAddress','description','createdAt','updatedAt','orders','author')
                    res.body.author.should.be.a('object')
                    res.body.customer.should.be.a('object')
                    res.body.items.should.be.a('array')
                    res.body.orders.should.be.a('array')
                    res.body.controlNumber.should.be.a('string')
                    res.body.deliveryAddress.should.be.a('string')
                    res.body.deliveryType.should.be.a('string')
                    res.body.description.should.be.a('string')
                    res.body.invoiceAddress.should.be.a('string')
                    res.body.status.should.be.a('string')
                    res.body.total.should.be.a('number')
                    res.body._id.should.be.a('string')
                    res.body.should.have.property('_id').eql(id)
                    done()
                })
        })
    })
    describe('/PATCH/:id purchase', () => {
        it('it should NOT be able to consume the route since no token was sent', (done) => {
            var items = []
            items[0] = products[0]
            items[0].qty = 1
            items[0].depositList = [deposit[0]]
            items[0].deposits = [deposit[0].name]
            items[0].providers = [provider[0].name]
            items[0].deposit = deposit[0]
            const purchase = {
                controlNumber: 'TEST-1601233445Fo',
                status: "hold",
                deliveryType: "to_send",
                total: 200,
                customer: author[2]._id,
                items: items,
                deliveryAddress: "Madrid",
                invoiceAddress: faker.address.streetAddress(),
                description: "Order Seed",
                author: author[0]._id,
                tenantId: "subdomain",
            }
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .patch(`/api/1.0/purchase/${id}`)
                .set('Origin', origin)
                .send(purchase)
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it('it should UPDATE a purchase given the id with token like admin, manager and seller ', (done) => {
            var items = []
            items[0] = products[0]
            items[0].qty = 1
            items[0].depositList = [deposit[0]]
            items[0].deposits = [deposit[0].name]
            items[0].providers = [provider[0].name]
            items[0].deposit = deposit[0]
            const purchase = {
                controlNumber: 'TEST-1601233445Fo',
                status: "hold",
                deliveryType: "to_send",
                total: 200,
                customer: author[2],
                items: items,
                deliveryAddress: "Madrid",
                invoiceAddress: faker.address.streetAddress(),
                description: "Order Seed",
                author: author[0]._id,
                tenantId: "subdomain",
            }
            const id = createdID.slice(-1).pop()
            console.log(id)
            chai
                .request(server)
                .patch(`/api/1.0/purchase/${id}`)
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .send(purchase)
                .end((error, res) => {
                    console.log(res.body.errors)
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.have.property('_id').eql(id)
                    res.body.should.have.property('description').eql('Order Seed')
                    res.body.should.include.keys('controlNumber','status','deliveryDate','deliveryType','convert','subTotal','sumTaxesTotal','taxes','total','attachment','_id',
                    'deleted','customer','items','deliveryAddress','invoiceAddress','description','author','createdAt','updatedAt','tenantId')
                    done()
                })
        })
        it('it should NOT UPDATE a purchase given the id with token like customer and user', (done) => {
            var items = []
            items[0] = products[0]
            items[0].qty = 1
            items[0].depositList = [deposit[0]]
            items[0].deposits = [deposit[0].name]
            items[0].providers = [provider[0].name]
            items[0].deposit = deposit[0]
            const purchase = {
                controlNumber: 'TEST-1601233445Fo',
                status: "hold",
                deliveryType: "to_send",
                total: 200,
                customer: author[2],
                items: items,
                deliveryAddress: "Madrid",
                invoiceAddress: faker.address.streetAddress(),
                description: "Order Seed",
                author: author[0]._id,
                tenantId: "subdomain",
            }
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .patch(`/api/1.0/purchase/${id}`)
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.user}`)
                .send(purchase)
                .end((err, res) => {
                    res.should.have.status(403)
                    res.body.should.be.a('object')
                    res.body.should.have.property('errors')
                    done()
                })
        })
    })
    describe('/DELETE/:id purchase', () => {
        it('it should DELETE a purchase given the id with token like admin ', (done) => {
            var items = []
            items[0] = products[0]
            items[0].qty = 1
            items[0].depositList = [deposit[0]]
            items[0].deposits = [deposit[0].name]
            items[0].providers = [provider[0].name]
            items[0].deposit = deposit[0]
            const purchase = {
                controlNumber: 'TEST-1601233445FO',
                status: "hold",
                deliveryType: "to_send",
                total: 200,
                customer: author[2],
                deliveryDate: '2020-10-24',
                items: items,
                deliveryAddress: "Madrid",
                invoiceAddress: faker.address.streetAddress(),
                description: "Order Seed",
                author: author[0],
                tenantId: "subdomain",
            }
            chai
                .request(server)
                .post('/api/1.0/purchase')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .send(purchase)
                .end((err, res) => {
                    console.log(res.body.errors)
                    res.should.have.status(201)
                    res.body.should.be.a('object')
                    chai
                        .request(server)
                        .delete(`/api/1.0/purchase/${res.body._id}`)
                        .set('Origin', origin)
                        .set('Authorization', `Bearer ${tokens.admin}`)
                        .end((error, result) => {
                            result.should.have.status(200)
                            result.body.should.be.a('object')
                            result.body.should.have.property('msg').eql('DELETED')
                            done()
                        })
                })
        })
        it('it should NOT DELETE a purchase given the id with token like customer, and user', (done) => {
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .delete(`/api/1.0/purchase/${id}`)
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.user}`)
                .end((err, res) => {
                    res.should.have.status(403)
                    res.body.should.be.a('object')
                    res.body.should.have.property('errors')
                    done()
                })
        })
    })
    after(() => {
        createdID.forEach((id) => {
            Purchase.findByIdAndRemove(id, (err) => {
                if (err) {
                    console.log(err)
                }
            })
        })
    })

});