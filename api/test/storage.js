/* eslint handle-callback-err: "off"*/

process.env.NODE_ENV = 'test'

const chai = require('chai')
const chaiHttp = require('chai-http')
const Provider = require('../app/models/providers')
const server = require('../server')
// eslint-disable-next-line no-unused-vars
const faker = require('faker')
const should = chai.should()
const createdID = []
const newName = faker.random.words()
const fs = require('fs');
const loginDetails = {
    admin: {
        id: '5aa1c2c35ef7a4e97b5e995a',
        email: 'admin@admin.com',
        password: '12345',
    },
    user: {
        id: '5aa1c2c35ef7a4e97b5e995b',
        email: 'user@user.com',
        password: '12345'
    },
    manager: {
        id: '5aa1c2c35ef7a4e97b5e995c',
        email: 'manager@manager.com',
        password: '12345'
    },
    seller: {
        id: '5aa1c2c35ef7a4e97b5e995d',
        email: 'seller@seller.com',
        password: '12345'
    },
}
const origin = "https://www.subdomain.kitagil.com"
const tokens = {
    admin: '',
    user: '',
    manager: '',
    seller: '',
}
const file = `${__dirname}/../data/10.storages/test.png`

chai.use(chaiHttp)

describe('*********** STORAGE ***********', () => {
    describe('/POST login', () => {
        it('it should GET token as admin', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.admin)
                .end((err, res) => {                    
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.admin = res.body.session
                    done()
                })
        })
        it('it should GET token as user', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.user)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.user = res.body.session
                    done()
                })
        })
        it('it should GET token as manager', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.manager)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.manager = res.body.session
                    done()
                })
        })
        it('it should GET token as seller', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.seller)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.seller = res.body.session
                    done()
                })
        })
    })
    describe('/POST storage', () => {
        it('it should POST a storage ', (done) => {
            chai
                .request(server)
                .post('/api/1.0/storage')
                .set('Origin', origin)
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .set('Authorization', `Bearer ${tokens.admin}`)
                .attach('files[]', fs.readFileSync(file), 'test.jpg')
                .end((err, res) => {
                    res.should.have.status(201)
                    res.body.should.be.a('object')
                    res.body.should.include.keys('data')
                    res.body.data.should.be.a('array')
                    res.body.data[0].should.include.keys('author','createdAt','deleted','fileName','fileType','large','medium','original','small',
                    'tenantId','updatedAt','_id')
                    createdID.push(res.body.data[0]._id)
                    done()
                })
        })
    })
    // Get all storages
    describe('/GET storage', () => {
        it('it should GET storage sending a the token', (done) => {
            console.log(createdID)
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .get(`/api/1.0/storage/${id}`)
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    console.log(res.body)
                    res.body.should.be.an('object')
                    res.body.should.include.keys('deleted','_id','original','small','large','fileType', 'fileName', 'author', 'tenantId', 'createdAt', 'updatedAt')                    
                    done()
                })
        })        
    })
    describe('/DELETE storage', () => {
        it('it should DELETE storage sending a the token', (done) => {
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .delete(`/api/1.0/storage/${id}`)
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    console.log(res.body)
                    // res.body.should.be.an('object')
                    // res.body.should.include.keys('name', 'currencySymbol', 'currency', 'logo', 'language', 'initialInvoice', 'invoiceFormat', 'deleted',
                    //     '_id', 'plugins', 'tenantId', 'tax')
                    // res.body.currency.should.be.a('string')
                    // res.body.currencySymbol.should.be.a('string')
                    // res.body.language.should.be.a('string')
                    // res.body.logo.should.be.a('string')
                    // res.body.name.should.be.a('string')
                    // res.body.plugins.should.be.a('array')
                    // res.body.tenantId.should.be.a('string')
                    // res.body._id.should.be.a('string')
                    done()
                })
        })        
    })
    // after(() => {
    //     createdID.forEach((id) => {
    //         Provider.findByIdAndRemove(id, (err) => {
    //             if (err) {
    //                 console.log(err)
    //             }
    //         })
    //     })
    // })

});