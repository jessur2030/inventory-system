/* eslint handle-callback-err: "off"*/

process.env.NODE_ENV = 'test'

const chai = require('chai')
const chaiHttp = require('chai-http')
const Deposit = require('../app/models/deposits')
const server = require('../server')
// eslint-disable-next-line no-unused-vars
const faker = require('faker')
const should = chai.should()
const createdID = []
const newName = faker.random.words()
const {credentials} = require('./analytics')

// const loginDetails = {
//   admin: {
//     id: '5aa1c2c35ef7a4e97b5e995a',
//     email: 'admin@admin.com',
//     password: '12345',
//   },
//   user: {
//     id: '5aa1c2c35ef7a4e97b5e995b',
//     email: 'user@user.com',
//     password: '12345'
//   },
//   manager: {
//     id: '5aa1c2c35ef7a4e97b5e995c',
//     email: 'manager@manager.com',
//     password: '12345'
//   },
//   seller: {
//     id: '5aa1c2c35ef7a4e97b5e995d',
//     email: 'seller@seller.com',
//     password: '12345'
//   },
// }

const origin = "https://www.subdomain.kitagil.com"
const tokens = {
  admin: '',
  user: '',
  manager: '',
  seller: '',
}
const manager = require('./../data/1.users/user')
const { fake } = require('faker')

chai.use(chaiHttp)

describe('*********** DEPOSITS ***********', () => {
  describe('/POST login', () => {
    it('it should GET token as admin', (done) => {
      chai
        .request(server)
        .post('/api/1.0/login')
        .set('Origin', origin)
        .send(credentials.admin)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.have.property('session')
          tokens.admin = res.body.session
          done()
        })
    })
    it('it should GET token as customer', (done) => {
      chai
        .request(server)
        .post('/api/1.0/login')
        .set('Origin', origin)
        .send(credentials.customer)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.have.property('session')
          tokens.user = res.body.session
          done()
        })
    })
    it('it should GET token as manager', (done) => {
      chai
        .request(server)
        .post('/api/1.0/login')
        .set('Origin', origin)
        .send(credentials.manager)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.have.property('session')
          tokens.manager = res.body.session
          done()
        })
    })
    it('it should GET token as seller', (done) => {
      chai
        .request(server)
        .post('/api/1.0/login')
        .set('Origin', origin)
        .send(credentials.seller)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.have.property('session')
          tokens.seller = res.body.session
          done()
        })
    })
  })
  // Get all deposits
  describe('/GET deposits', () => {
    it('it should NOT be able to consume the route since no token was sent', (done) => {
      chai
        .request(server)
        .get('/api/1.0/deposits')
        .set('Origin', origin)
        .end((err, res) => {
          res.should.have.status(401)
          done()
        })
    })
    it('it should GET all deposits sending a the token like admin', (done) => {
      chai
        .request(server)
        .get('/api/1.0/deposits')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.include.keys('docs', 'hasNextPage','hasPrevPage','limit','nextPage','page',
          'pagingCounter','prevPage','totalDocs','totalPages')
          res.body.docs.should.be.a('array')
          res.body.hasNextPage.should.be.a('boolean')
          res.body.hasPrevPage.should.be.a('boolean')
          res.body.limit.should.be.a('number')
          res.body.page.should.be.a('number')
          res.body.pagingCounter.should.be.a('number')
          res.body.totalDocs.should.be.a('number')
          res.body.totalPages.should.be.a('number')
          res.body.docs[0].should.include.keys('address','manager','name','phone','tenantId','_id')
          res.body.docs[0].manager.should.be.a('object')
          res.body.docs[0].address.should.be.a('string')
          res.body.docs[0].name.should.be.a('string')
          res.body.docs[0].phone.should.be.a('string')
          res.body.docs[0].tenantId.should.be.a('string')
          res.body.docs[0]._id.should.be.a('string')
          done()
        })
    })
    it('it should NOT be able to consume the route if the token is like user', (done) => {
      chai
        .request(server)
        .get('/api/1.0/deposits')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.user}`)
        .end((err, res) => {
          res.should.have.status(403)
          res.body.should.be.an('object')
          done()
        })
    })
    it('it should GET all deposits sending a the token like manager', (done) => {
      chai
        .request(server)
        .get('/api/1.0/deposits')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.manager}`)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.include.keys('docs', 'hasNextPage','hasPrevPage','limit','nextPage','page',
          'pagingCounter','prevPage','totalDocs','totalPages')
          res.body.docs.should.be.a('array')
          res.body.hasNextPage.should.be.a('boolean')
          res.body.hasPrevPage.should.be.a('boolean')
          res.body.limit.should.be.a('number')
          res.body.page.should.be.a('number')
          res.body.pagingCounter.should.be.a('number')
          res.body.totalDocs.should.be.a('number')
          res.body.totalPages.should.be.a('number')
          res.body.docs.should.be.a('array')
          res.body.docs[0].should.include.keys('address','manager','name','phone','tenantId','_id')
          res.body.docs[0].manager.should.be.a('object')
          res.body.docs[0].address.should.be.a('string')
          res.body.docs[0].name.should.be.a('string')
          res.body.docs[0].phone.should.be.a('string')
          res.body.docs[0].tenantId.should.be.a('string')
          res.body.docs[0]._id.should.be.a('string')
          done()
        })
    })
    it('it should GET all deposits sending a the token like seller', (done) => {
      chai
        .request(server)
        .get('/api/1.0/deposits')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.seller}`)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.include.keys('docs', 'hasNextPage','hasPrevPage','limit','nextPage','page',
          'pagingCounter','prevPage','totalDocs','totalPages')
          res.body.docs.should.be.a('array')
          res.body.hasNextPage.should.be.a('boolean')
          res.body.hasPrevPage.should.be.a('boolean')
          res.body.limit.should.be.a('number')
          res.body.page.should.be.a('number')
          res.body.pagingCounter.should.be.a('number')
          res.body.totalDocs.should.be.a('number')
          res.body.totalPages.should.be.a('number')
          res.body.docs.should.be.a('array')
          res.body.docs[0].should.include.keys('address','manager','name','phone','tenantId','_id')
          res.body.docs[0].manager.should.be.a('object')
          res.body.docs[0].address.should.be.a('string')
          res.body.docs[0].name.should.be.a('string')
          res.body.docs[0].phone.should.be.a('string')
          res.body.docs[0].tenantId.should.be.a('string')
          res.body.docs[0]._id.should.be.a('string')
          done()
        })
    })
    it('it should GET the deposits with filters', (done) => {
      chai
        .request(server)
        .get('/api/1.0/deposits?filter=Dickinson - Upton&fields=name')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.include.keys('docs', 'hasNextPage','hasPrevPage','limit','nextPage','page',
          'pagingCounter','prevPage','totalDocs','totalPages')
          res.body.docs.should.be.a('array')
          res.body.hasNextPage.should.be.a('boolean')
          res.body.hasPrevPage.should.be.a('boolean')
          res.body.limit.should.be.a('number')
          res.body.page.should.be.a('number')
          res.body.pagingCounter.should.be.a('number')
          res.body.totalDocs.should.be.a('number')
          res.body.totalPages.should.be.a('number')
          res.body.docs.should.be.a('array')
          res.body.docs.should.have.lengthOf(1)
          res.body.docs[0].should.have.property('name').eql('Dickinson - Upton')
          res.body.docs[0].should.include.keys('address','manager','name','phone','tenantId','_id')
          res.body.docs[0].manager.should.be.a('object')
          res.body.docs[0].address.should.be.a('string')
          res.body.docs[0].name.should.be.a('string')
          res.body.docs[0].phone.should.be.a('string')
          res.body.docs[0].tenantId.should.be.a('string')
          res.body.docs[0]._id.should.be.a('string')
          done()
        })
    })
  })
  describe('/POST deposits', () => {
    it('it should NOT be able to consume the route since no token was sent', (done) => {
      chai
        .request(server)
        .post('/api/1.0/deposits')
        .set('Origin', origin)
        .end((err, res) => {
          res.should.have.status(401)
          done()
        })
    })
    it('it should NOT POST a deposits without name, address and manager', (done) => {
      const deposit = {}
      chai
        .request(server)
        .post('/api/1.0/deposits')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .send(deposit)
        .end((err, res) => {
          res.should.have.status(422)
          res.body.should.be.a('object')
          res.body.should.have.property('errors')
          done()
        })
    })
    it('it should POST a deposit like admin and manager', (done) => {
      const deposit = {
        name: faker.company.companyName(),
        address: faker.address.streetAddress(),
        tenantId: 'subdomain',
        phone: faker.phone.phoneNumber(),
        manager: manager[0],
        tag: null
      }
      chai
        .request(server)
        .post('/api/1.0/deposits')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .send(deposit)
        .end((err, res) => {
          // console.log(res.body.errors)
          res.should.have.status(201)
          res.body.should.be.a('object')
          res.body.should.include.keys('_id', 'name', 'address','manager','createdAt','phone','tenantId','updatedAt','tag','deleted')
          res.body.manager.should.be.a('object')
          createdID.push(res.body._id)
          done()
        })
    })
    it('it should NOT POST a deposit with token like seller and user', (done) => {
      const deposit = {
        name: faker.company.companyName(),
        address: faker.address.streetAddress(),
        tenantId: 'subdomain',
        phone: faker.phone.phoneNumber(),
        manager: manager[0],
        tag: ['Test']
      }
      chai
        .request(server)
        .post('/api/1.0/deposits')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.seller}`)
        .send(deposit)
        .end((err, res) => {
          res.should.have.status(403)
          res.body.should.be.a('object')
          res.body.should.have.property('errors')
          done()
        })
    })
  })
  describe('/GET/:id deposit', () => {
    it('it should NOT be able to consume the route since no token was sent', (done) => {
      const id = createdID.slice(-1).pop()
      chai
        .request(server)
        .get(`/api/1.0/deposits/${id}`)
        .set('Origin', origin)
        .end((err, res) => {
          res.should.have.status(401)
          done()
        })
    })
    it('it should GET a deposit by the given id like admin, manager and seller', (done) => {
      const id = createdID.slice(-1).pop()
      chai
        .request(server)
        .get(`/api/1.0/deposits/${id}`)
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .end((error, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.include.keys('address', 'deleted','manager','name','phone','tag','tenantId','_id')
          res.body.manager.should.be.a('object')
          res.body.address.should.be.a('string')
          res.body.name.should.be.a('string')
          res.body.phone.should.be.a('string')
          res.body.tenantId.should.be.a('string')
          res.body._id.should.be.a('string')
          res.body.should.have.property('_id').eql(id)
          done()
        })
    })
  })
  describe('/PATCH/:id deposit', () => {
    it('it should NOT be able to consume the route since no token was sent', (done) => {
      const deposit = {
        name: newName,
        address: faker.address.streetAddress(),
        tenantId: 'subdomain',
        phone: '',
        manager: manager[0],
        tag: ['Test']
      }
      const id = createdID.slice(-1).pop()
      chai
        .request(server)
        .patch(`/api/1.0/deposits/${id}`)
        .set('Origin', origin)
        .send(deposit)
        .end((err, res) => {
          res.should.have.status(401)
          done()
        })
    })
    it('it should UPDATE a deposit given the id with token like admin and manager', (done) => {
      const deposit = {
        name: newName,
        address: faker.address.streetAddress(),
        tenantId: 'subdomain',
        phone: faker.phone.phoneNumber(),
        manager: manager[0],
        tag: ['Prueba']
      }
      const id = createdID.slice(-1).pop()
      chai
        .request(server)
        .patch(`/api/1.0/deposits/${id}`)
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .send(deposit)
        .end((error, res) => {
          // console.log(res.body.errors)
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.include.keys('_id', 'name', 'address','manager','createdAt','phone','tenantId','updatedAt','tag','deleted')
          res.body.manager.should.be.a('object')
          res.body.should.have.property('_id').eql(id)
          res.body.should.have.property('name').eql(newName)
          done()
        })
    })
    it('it should NOT UPDATE a deposit given the id with token like seller', (done) => {
      const deposit = {
        name: newName,
        address: faker.address.streetAddress(),
        tenantId: 'subdomain',
        phone: faker.phone.phoneNumber(),
        manager: manager[0],
        tag: {}
      }
      const id = createdID.slice(-1).pop()
      chai
        .request(server)
        .patch(`/api/1.0/deposits/${id}`)
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.seller}`)
        .send(deposit)
        .end((err, res) => {
          res.should.have.status(403)
          res.body.should.be.a('object')
          res.body.should.have.property('errors')
          done()
        })
    })
  })
  describe('/DELETE/:id deposit', () => {
    it('it should DELETE a deposit given the id with token as admin and manager', (done) => {
      const deposit = {
        name: newName,
        address: faker.address.streetAddress(),
        tenantId: 'subdomain',
        phone: faker.phone.phoneNumber(),
        manager: manager[0],
        tag: ['Prueba']
      }
      chai
        .request(server)
        .post('/api/1.0/deposits')
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.admin}`)
        .send(deposit)
        .end((err, res) => {
          res.should.have.status(201)
          res.body.should.be.a('object')
          res.body.should.include.keys('_id', 'name')
          chai
            .request(server)
            .delete(`/api/1.0/deposits/${res.body._id}`)
            .set('Origin', origin)
            .set('Authorization', `Bearer ${tokens.admin}`)
            .end((error, result) => {
              result.should.have.status(200)
              result.body.should.be.a('object')
              result.body.should.have.property('msg').eql('DELETED')
              done()
            })
        })
    })
    it('it should NOT DELETE a deposit given the id with token as seller', (done) => {
      const id = createdID.slice(-1).pop()
      chai
        .request(server)
        .delete(`/api/1.0/deposits/${id}`)
        .set('Origin', origin)
        .set('Authorization', `Bearer ${tokens.seller}`)
        .end((err, res) => {
          res.should.have.status(403)
          res.body.should.be.a('object')
          res.body.should.have.property('errors')
          done()
        })
    })
  })
  after(() => {
    createdID.forEach((id) => {
      Deposit.findByIdAndRemove(id, (err) => {
        if (err) {
          console.log(err)
        }
      })
    })
  })

});