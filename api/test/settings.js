/* eslint handle-callback-err: "off"*/

process.env.NODE_ENV = 'test'

const chai = require('chai')
const chaiHttp = require('chai-http')
const Provider = require('../app/models/providers')
const server = require('../server')
// eslint-disable-next-line no-unused-vars
const faker = require('faker')
const should = chai.should()
const createdID = []
const newName = faker.random.words()
const loginDetails = {
    admin: {
        id: '5aa1c2c35ef7a4e97b5e995a',
        email: 'admin@admin.com',
        password: '12345',
    },
    user: {
        id: '5aa1c2c35ef7a4e97b5e995b',
        email: 'user@user.com',
        password: '12345'
    },
    manager: {
        id: '5aa1c2c35ef7a4e97b5e995c',
        email: 'manager@manager.com',
        password: '12345'
    },
    seller: {
        id: '5aa1c2c35ef7a4e97b5e995d',
        email: 'seller@seller.com',
        password: '12345'
    },
}
const origin = "https://www.subdomain.kitagil.com"
const tokens = {
    admin: '',
    user: '',
    manager: '',
    seller: '',
}
const manager = require('./../data/1.users/user')
const { fake } = require('faker')

chai.use(chaiHttp)

describe('*********** SETTINGS ***********', () => {
    describe('/POST login', () => {
        it('it should GET token as admin', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.admin)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.admin = res.body.session
                    done()
                })
        })
        it('it should GET token as user', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.user)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.user = res.body.session
                    done()
                })
        })
        it('it should GET token as manager', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.manager)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.manager = res.body.session
                    done()
                })
        })
        it('it should GET token as seller', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.seller)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.seller = res.body.session
                    done()
                })
        })
    })
    // Get all settings
    describe('/GET settings', () => {
        it('it should NOT be able to consume the route since no token was sent', (done) => {
            chai
                .request(server)
                .get('/api/1.0/settings')
                .set('Origin', origin)
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it('it should GET settings sending a the token and tenant like admin', (done) => {
            chai
                .request(server)
                .get('/api/1.0/settings')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    console.log(res.body)
                    res.body.should.be.an('object')
                    res.body.should.include.keys('name', 'currencySymbol', 'currency', 'logo', 'language', 'initialInvoice', 'invoiceFormat', 'deleted',
                        '_id', 'plugins', 'tenantId', 'tax')
                    res.body.currency.should.be.a('string')
                    res.body.currencySymbol.should.be.a('string')
                    res.body.language.should.be.a('string')
                    res.body.logo.should.be.a('string')
                    res.body.name.should.be.a('string')
                    res.body.plugins.should.be.a('array')
                    res.body.tenantId.should.be.a('string')
                    res.body._id.should.be.a('string')
                    done()
                })
        })
        it('it should GET all settings sending a the token like user', (done) => {
            chai
                .request(server)
                .get('/api/1.0/settings')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.user}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.include.keys('name', 'currencySymbol', 'currency', 'logo', 'language', 'initialInvoice', 'invoiceFormat', 'deleted',
                        '_id', 'plugins', 'tenantId', 'tax')
                    res.body.currency.should.be.a('string')
                    res.body.currencySymbol.should.be.a('string')
                    res.body.language.should.be.a('string')
                    res.body.logo.should.be.a('string')
                    res.body.name.should.be.a('string')
                    res.body.plugins.should.be.a('array')
                    res.body.tenantId.should.be.a('string')
                    res.body._id.should.be.a('string')
                    done()
                })
        })
        it('it should GET all settings sending a the token like manager', (done) => {
            chai
                .request(server)
                .get('/api/1.0/settings')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.manager}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    createdID.push(res.body)
                    res.body.should.include.keys('name', 'currencySymbol', 'currency', 'logo', 'language', 'initialInvoice', 'invoiceFormat', 'deleted',
                        '_id', 'plugins', 'tenantId', 'tax')
                    res.body.currency.should.be.a('string')
                    res.body.currencySymbol.should.be.a('string')
                    res.body.language.should.be.a('string')
                    res.body.logo.should.be.a('string')
                    res.body.name.should.be.a('string')
                    res.body.plugins.should.be.a('array')
                    res.body.tenantId.should.be.a('string')
                    res.body._id.should.be.a('string')
                    done()
                })
        })
    })
    describe('/PATCH/ provider', () => {
        it('it should UPDATE a setting given the id with token only like admin', (done) => {
            var newSetting = {
                name: faker.name.firstName(),
                currencySymbol: '$',
                currency: 'Dolar Estadounidense',
                language: 'es-ES',
                logo: faker.internet.avatar(),
                plugins: [],
                tenantId: 'subdomain',
            }
            chai
                .request(server)
                .get('/api/1.0/settings')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    createdID.push(res.body)
                    res.body.should.include.keys('name', 'currencySymbol', 'currency', 'logo', 'language', 'initialInvoice', 'invoiceFormat', 'deleted',
                        '_id', 'plugins', 'tenantId', 'tax')
                    chai
                        .request(server)
                        .patch(`/api/1.0/settings`)
                        .set('Origin', origin)
                        .set('Authorization', `Bearer ${tokens.admin}`)
                        .send(newSetting)
                        .end((error, result) => {
                            console.log(result.body)
                            result.should.have.status(200)
                            result.body.should.include.keys('name', 'currencySymbol', 'currency', 'logo', 'language', 'initialInvoice', 'invoiceFormat', 'deleted', '_id',
                                'plugins', 'tenantId', 'updatedAt', 'tax')
                            result.body.should.be.a('object')
                            done()
                        })
                })
        })
        it('it should GET the invoice desing sending the token only like admin', (done) => {
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .patch(`/api/1.0/settings/invoice`)
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.include.keys('invoiceDesign', 'logo', 'name', '_id')
                    done()
                })
        })
        it('it should UPDATE the invoice desing sending the token only like admin', (done) => {
            const desing = {
                css: '* { box-sizing: border-box; } body {margin: 0;}',
                html: ''
            }
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .patch(`/api/1.0/settings/invoice`)
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .send(desing)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.include.keys('currency', 'currencySymbol', 'deleted', 'initialInvoice', 'invoiceDesign', 'invoiceFormat', 'language', 'logo', 'name',
                        'plugins', 'tax', 'tenantId', 'updatedAt', '_id')
                    done()
                })
        })
    })
    after(() => {
        createdID.forEach((id) => {
            Provider.findByIdAndRemove(id, (err) => {
                if (err) {
                    console.log(err)
                }
            })
        })
    })

});