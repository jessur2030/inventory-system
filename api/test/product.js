process.env.NODE_ENV = 'test'

const chai = require('chai')
const chaiHttp = require('chai-http')
const Product = require('../app/models/products')
const server = require('../server')
// eslint-disable-next-line no-unused-vars
const faker = require('faker')
const should = chai.should()
const createdID = []
const newName = faker.random.words()
const loginDetails = {
    admin: {
        id: '5aa1c2c35ef7a4e97b5e995a',
        email: 'admin@admin.com',
        password: '12345',
    },
    user: {
        id: '5aa1c2c35ef7a4e97b5e995b',
        email: 'user@user.com',
        password: '12345'
    },
    manager: {
        id: '5aa1c2c35ef7a4e97b5e995c',
        email: 'manager@manager.com',
        password: '12345'
    },
    seller: {
        id: '5aa1c2c35ef7a4e97b5e995d',
        email: 'seller@seller.com',
        password: '12345'
    },
}
const origin = "https://www.subdomain.kitagil.com"
const tokens = {
    admin: '',
    user: '',
    manager: '',
    seller: '',
}

chai.use(chaiHttp)

describe('*********** PRODUCTS ***********', () => {
    describe('/POST login', () => {
        it('it should GET token as admin', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.admin)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.admin = res.body.session
                    done()
                })
        })
        it('it should GET token as user', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.user)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.user = res.body.session
                    done()
                })
        })
        it('it should GET token as manager', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.manager)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.manager = res.body.session
                    done()
                })
        })
        it('it should GET token as seller', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.seller)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.seller = res.body.session
                    done()
                })
        })
    })
    // Get all products
    describe('/GET products', () => {
        it('it should NOT be able to consume the route since no token was sent', (done) => {
            chai
                .request(server)
                .get('/api/1.0/products')
                .set('Origin', origin)
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it('it should GET all products sending a the token like admin', (done) => {
            chai
                .request(server)
                .get('/api/1.0/products')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.include.keys('docs', 'hasNextPage', 'hasPrevPage', 'limit', 'nextPage', 'page',
                        'pagingCounter', 'prevPage', 'totalDocs', 'totalPages')
                    res.body.docs.should.be.a('array')
                    res.body.hasNextPage.should.be.a('boolean')
                    res.body.hasPrevPage.should.be.a('boolean')
                    res.body.limit.should.be.a('number')
                    res.body.page.should.be.a('number')
                    res.body.pagingCounter.should.be.a('number')
                    res.body.totalDocs.should.be.a('number')
                    res.body.totalPages.should.be.a('number')
                    res.body.docs[0].should.include.keys('author','categories','depositList','deposits','description','gallery','measures','name',
                    'prices','providers','qty','sku','tag','_id')
                    res.body.docs[0].author.should.be.a('string')
                    res.body.docs[0].categories.should.be.a('array')
                    res.body.docs[0].depositList.should.be.a('array')
                    res.body.docs[0].deposits.should.be.a('array')
                    res.body.docs[0].description.should.be.a('string')
                    res.body.docs[0].gallery.should.be.a('array')
                    res.body.docs[0].measures.should.be.a('string')
                    res.body.docs[0].name.should.be.a('string')
                    res.body.docs[0].prices.should.be.a('array')
                    res.body.docs[0].providers.should.be.a('array')
                    res.body.docs[0].qty.should.be.a('number')
                    res.body.docs[0].sku.should.be.a('string')
                    res.body.docs[0].tag.should.be.a('array')
                    done()
                })
        })
        it('it should GET all products sending a the token like admin', (done) => {
            chai
                .request(server)
                .get('/api/1.0/products')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.user}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.include.keys('docs', 'hasNextPage', 'hasPrevPage', 'limit', 'nextPage', 'page',
                        'pagingCounter', 'prevPage', 'totalDocs', 'totalPages')
                    res.body.docs.should.be.a('array')
                    res.body.hasNextPage.should.be.a('boolean')
                    res.body.hasPrevPage.should.be.a('boolean')
                    res.body.limit.should.be.a('number')
                    res.body.page.should.be.a('number')
                    res.body.pagingCounter.should.be.a('number')
                    res.body.totalDocs.should.be.a('number')
                    res.body.totalPages.should.be.a('number')
                    res.body.docs[0].should.include.keys('author','categories','depositList','deposits','description','gallery','measures','name',
                    'prices','providers','qty','sku','tag','_id')
                    res.body.docs[0].author.should.be.a('string')
                    res.body.docs[0].categories.should.be.a('array')
                    res.body.docs[0].depositList.should.be.a('array')
                    res.body.docs[0].deposits.should.be.a('array')
                    res.body.docs[0].description.should.be.a('string')
                    res.body.docs[0].gallery.should.be.a('array')
                    res.body.docs[0].measures.should.be.a('string')
                    res.body.docs[0].name.should.be.a('string')
                    res.body.docs[0].prices.should.be.a('array')
                    res.body.docs[0].providers.should.be.a('array')
                    res.body.docs[0].qty.should.be.a('number')
                    res.body.docs[0].sku.should.be.a('string')
                    res.body.docs[0].tag.should.be.a('array')
                    done()
                })
        })
        it('it should GET all products sending a the token like manager', (done) => {
            chai
                .request(server)
                .get('/api/1.0/products')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.manager}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.include.keys('docs', 'hasNextPage', 'hasPrevPage', 'limit', 'nextPage', 'page',
                        'pagingCounter', 'prevPage', 'totalDocs', 'totalPages')
                    res.body.docs.should.be.a('array')
                    res.body.hasNextPage.should.be.a('boolean')
                    res.body.hasPrevPage.should.be.a('boolean')
                    res.body.limit.should.be.a('number')
                    res.body.page.should.be.a('number')
                    res.body.pagingCounter.should.be.a('number')
                    res.body.totalDocs.should.be.a('number')
                    res.body.totalPages.should.be.a('number')
                    res.body.docs[0].should.include.keys('author','categories','depositList','deposits','description','gallery','measures','name',
                    'prices','providers','qty','sku','tag','_id')
                    res.body.docs[0].author.should.be.a('string')
                    res.body.docs[0].categories.should.be.a('array')
                    res.body.docs[0].depositList.should.be.a('array')
                    res.body.docs[0].deposits.should.be.a('array')
                    res.body.docs[0].description.should.be.a('string')
                    res.body.docs[0].gallery.should.be.a('array')
                    res.body.docs[0].measures.should.be.a('string')
                    res.body.docs[0].name.should.be.a('string')
                    res.body.docs[0].prices.should.be.a('array')
                    res.body.docs[0].providers.should.be.a('array')
                    res.body.docs[0].qty.should.be.a('number')
                    res.body.docs[0].sku.should.be.a('string')
                    res.body.docs[0].tag.should.be.a('array')
                    done()
                })
        })
        it('it should GET all products sending a the token like seller', (done) => {
            chai
                .request(server)
                .get('/api/1.0/products')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.seller}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.include.keys('docs', 'hasNextPage', 'hasPrevPage', 'limit', 'nextPage', 'page',
                        'pagingCounter', 'prevPage', 'totalDocs', 'totalPages')
                    res.body.docs.should.be.a('array')
                    res.body.hasNextPage.should.be.a('boolean')
                    res.body.hasPrevPage.should.be.a('boolean')
                    res.body.limit.should.be.a('number')
                    res.body.page.should.be.a('number')
                    res.body.pagingCounter.should.be.a('number')
                    res.body.totalDocs.should.be.a('number')
                    res.body.totalPages.should.be.a('number')
                    res.body.docs[0].should.include.keys('author','categories','depositList','deposits','description','gallery','measures','name',
                    'prices','providers','qty','sku','tag','_id')
                    res.body.docs[0].author.should.be.a('string')
                    res.body.docs[0].categories.should.be.a('array')
                    res.body.docs[0].depositList.should.be.a('array')
                    res.body.docs[0].deposits.should.be.a('array')
                    res.body.docs[0].description.should.be.a('string')
                    res.body.docs[0].gallery.should.be.a('array')
                    res.body.docs[0].measures.should.be.a('string')
                    res.body.docs[0].name.should.be.a('string')
                    res.body.docs[0].prices.should.be.a('array')
                    res.body.docs[0].providers.should.be.a('array')
                    res.body.docs[0].qty.should.be.a('number')
                    res.body.docs[0].sku.should.be.a('string')
                    res.body.docs[0].tag.should.be.a('array')
                    done()
                })
        })
        it('it should GET the products with filters', (done) => {
            chai
                .request(server)
                .get('/api/1.0/products?filter=Producto Seed&fields=name')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.docs.should.be.a('array')
                    res.body.should.include.keys('docs', 'hasNextPage', 'hasPrevPage', 'limit', 'nextPage', 'page',
                        'pagingCounter', 'prevPage', 'totalDocs', 'totalPages')
                    res.body.docs.should.be.a('array')
                    res.body.hasNextPage.should.be.a('boolean')
                    res.body.hasPrevPage.should.be.a('boolean')
                    res.body.limit.should.be.a('number')
                    res.body.page.should.be.a('number')
                    res.body.pagingCounter.should.be.a('number')
                    res.body.totalDocs.should.be.a('number')
                    res.body.totalPages.should.be.a('number')
                    res.body.docs[0].should.include.keys('author','categories','depositList','deposits','description','gallery','measures','name',
                    'prices','providers','qty','sku','tag','_id')
                    res.body.docs[0].author.should.be.a('string')
                    res.body.docs[0].categories.should.be.a('array')
                    res.body.docs[0].depositList.should.be.a('array')
                    res.body.docs[0].deposits.should.be.a('array')
                    res.body.docs[0].description.should.be.a('string')
                    res.body.docs[0].gallery.should.be.a('array')
                    res.body.docs[0].measures.should.be.a('string')
                    res.body.docs[0].name.should.be.a('string')
                    res.body.docs[0].prices.should.be.a('array')
                    res.body.docs[0].providers.should.be.a('array')
                    res.body.docs[0].qty.should.be.a('number')
                    res.body.docs[0].sku.should.be.a('string')
                    res.body.docs[0].tag.should.be.a('array')
                    // res.body.docs.should.have.lengthOf(1)
                    res.body.docs[0].should.have.property('name').eql('Producto Seed')
                    done()
                })
        })
    })
    describe('/POST products', () => {
        it('it should NOT be able to consume the route since no token was sent', (done) => {
            chai
                .request(server)
                .post('/api/1.0/products')
                .set('Origin', origin)
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it('it should NOT POST a products without name, prices and author', (done) => {
            const product = {}
            chai
                .request(server)
                .post('/api/1.0/products')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .send(product)
                .end((err, res) => {
                    res.should.have.status(422)
                    res.body.should.be.a('object')
                    res.body.should.have.property('errors')
                    done()
                })
        })
        it('it should POST a product only token as admin', (done) => {
            const product = {
                name: 'Producto pruebas',
                prices: [
                    { amount: 500 }
                ],
                tenantId: 'subdomain',
                author: loginDetails.admin.id,
                measures: '',
                categories: [
                    'cat_prueba'
                ],
                tag: [
                    'prueba'
                ],
                gallery: [],
                sku: 'product_prueba',
                description: 'Este es un producto de prueba'
            }
            chai
                .request(server)
                .post('/api/1.0/products')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .send(product)
                .end((err, res) => {
                    // console.log(res.body.errors)
                    console.log(res.body)
                    res.should.have.status(201)
                    res.body.should.be.a('object')
                    res.body.should.include.keys('author','categories','deleted','description','gallery','measures','name','prices',
                    'sku','tag','tenantId','_id')
                    createdID.push(res.body._id)
                    done()
                })
        })
        it('it should NOT POST a product with token like seller, user and manager', (done) => {
            const product = {
                name: faker.company.companyName(),
                prices: [
                    { amount: 500 }
                ],
                tenantId: 'subdomain',
                author: loginDetails.admin.id,
                measures: '',
                categories: [
                    'cat_prueba'
                ],
                tag: [
                    'prueba'
                ],
                gallery: [],
                sku: 'product_prueba',
                description: 'Este es un producto de prueba'
            }
            chai
                .request(server)
                .post('/api/1.0/products')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.seller}`)
                .send(product)
                .end((err, res) => {
                    res.should.have.status(403)
                    res.body.should.be.a('object')
                    res.body.should.have.property('errors')
                    done()
                })
        })
    })
    describe('/GET/:id product', () => {
        it('it should NOT be able to consume the route since no token was sent', (done) => {
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .get(`/api/1.0/products/${id}`)
                .set('Origin', origin)
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it('it should GET a product by the given id like admin, manager, seller and user', (done) => {
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .get(`/api/1.0/products/${id}`)
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((error, res) => {
                    console.log(res.body)
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.include.keys('author','categories','deposits','description','gallery','measures','name','prices',
                    'qty','sku','tag','_id')
                    res.body.should.have.property('_id').eql(id)
                    done()
                })
        })
    })
    describe('/PATCH/:id product', () => {
        it('it should NOT be able to consume the route since no token was sent', (done) => {
            const product = {
                name: faker.company.companyName(),
                prices: [
                    { amount: 500 }
                ],
                tenantId: 'subdomain',
                author: loginDetails.admin.id,
                measures: '',
                categories: [
                    'cat_prueba'
                ],
                tag: [
                    'prueba'
                ],
                gallery: [],
                sku: 'product_prueba',
                description: 'Este es un producto de prueba'
            }
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .patch(`/api/1.0/products/${id}`)
                .set('Origin', origin)
                .send(product)
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it('it should UPDATE a product given the id with token only like admin', (done) => {
            const newName = faker.company.companyName()
            const product = {
                name: newName,
                prices: [
                    { amount: 500 }
                ],
                tenantId: 'subdomain',
                author: loginDetails.admin.id,
                measures: '',
                categories: [
                    'cat_prueba'
                ],
                tag: [
                    'prueba'
                ],
                gallery: [],
                sku: 'product_prueba',
                description: 'Este es un producto de prueba'
            }
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .patch(`/api/1.0/products/${id}`)
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .send(product)
                .end((error, res) => {
                    // console.log(res.body.errors)
                    console.log(res.body)
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.include.keys('tag','gallery','deleted','_id','name','prices','measures','categories','sku','description','author',
                    'createdAt','updatedAt','tenantId')
                    res.body.should.have.property('_id').eql(id)
                    res.body.should.have.property('name').eql(newName)
                    done()
                })
        })
        it('it should NOT UPDATE a product given the id with token like seller, manager and user', (done) => {
            const product = {
                name: faker.company.companyName(),
                prices: [
                    { amount: 500 }
                ],
                tenantId: 'subdomain',
                author: loginDetails.admin.id,
                measures: '',
                categories: [
                    'cat_prueba'
                ],
                tag: [
                    'prueba'
                ],
                gallery: [],
                sku: 'product_prueba',
                description: 'Este es un producto de prueba'
            }
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .patch(`/api/1.0/products/${id}`)
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.seller}`)
                .send(product)
                .end((err, res) => {
                    res.should.have.status(403)
                    res.body.should.be.a('object')
                    res.body.should.have.property('errors')
                    done()
                })
        })
    })
    describe('/DELETE/:id product', () => {
        it('it should DELETE a product given the id with token as admin', (done) => {
            const product = {
                name: faker.company.companyName(),
                prices: [
                    { amount: 500 }
                ],
                tenantId: 'subdomain',
                author: loginDetails.admin.id,
                measures: '',
                categories: [
                    'cat_prueba'
                ],
                tag: [
                    'prueba'
                ],
                gallery: [],
                sku: 'product_prueba',
                description: 'Este es un producto de prueba'
            }
            chai
                .request(server)
                .post('/api/1.0/products')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .send(product)
                .end((err, res) => {
                    res.should.have.status(201)
                    res.body.should.be.a('object')
                    res.body.should.include.keys('_id', 'name')
                    chai
                        .request(server)
                        .delete(`/api/1.0/products/${res.body._id}`)
                        .set('Origin', origin)
                        .set('Authorization', `Bearer ${tokens.admin}`)
                        .end((error, result) => {
                            result.should.have.status(200)
                            result.body.should.be.a('object')
                            result.body.should.have.property('msg').eql('DELETED')
                            done()
                        })
                })
        })
        it('it should NOT DELETE a product given the id with token as seller, manager and user', (done) => {
            const id = createdID.slice(-1).pop()
            chai
                .request(server)
                .delete(`/api/1.0/products/${id}`)
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.seller}`)
                .end((err, res) => {
                    res.should.have.status(403)
                    res.body.should.be.a('object')
                    res.body.should.have.property('errors')
                    done()
                })
        })
    })
    after(() => {
        createdID.forEach((id) => {
            Product.findByIdAndRemove(id, (err) => {
                if (err) {
                    console.log(err)
                }
            })
        })
    })

});