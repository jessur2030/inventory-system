/* eslint handle-callback-err: "off"*/

process.env.NODE_ENV = 'test'

const chai = require('chai')
const chaiHttp = require('chai-http')
const Provider = require('../app/models/providers')
const server = require('../server')
// eslint-disable-next-line no-unused-vars
const faker = require('faker')
const should = chai.should()
const createdID = []
const newName = faker.random.words()
const loginDetails = {
    admin: {
        id: '5aa1c2c35ef7a4e97b5e995a',
        email: 'admin@admin.com',
        password: '12345',
    },
    user: {
        id: '5aa1c2c35ef7a4e97b5e995b',
        email: 'user@user.com',
        password: '12345'
    },
    manager: {
        id: '5aa1c2c35ef7a4e97b5e995c',
        email: 'manager@manager.com',
        password: '12345'
    },
    seller: {
        id: '5aa1c2c35ef7a4e97b5e995d',
        email: 'seller@seller.com',
        password: '12345'
    },
}
const origin = "https://www.subdomain.kitagil.com"
const tokens = {
    admin: '',
    user: '',
    manager: '',
    seller: '',
}
const manager = require('./../data/1.users/user')
const { fake } = require('faker')

chai.use(chaiHttp)

describe('*********** NOTIFICATIONS ***********', () => {
    describe('/POST login', () => {
        it('it should GET token as admin', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.admin)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.admin = res.body.session
                    done()
                })
        })
        it('it should GET token as user', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.user)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.user = res.body.session
                    done()
                })
        })
        it('it should GET token as manager', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.manager)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.manager = res.body.session
                    done()
                })
        })
        it('it should GET token as seller', (done) => {
            chai
                .request(server)
                .post('/api/1.0/login')
                .set('Origin', origin)
                .send(loginDetails.seller)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('session')
                    tokens.seller = res.body.session
                    done()
                })
        })
    })
    // Get all notifications
    describe('/GET notifications', () => {
        it('it should be able to consume the route sending token like anyone', (done) => {
            chai
                .request(server)
                .get('/api/1.0/notifications')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.include.keys('docs', 'hasNextPage', 'hasPrevPage', 'limit', 'nextPage', 'page',
                        'pagingCounter', 'prevPage', 'totalDocs', 'totalPages')
                    res.body.docs.should.be.a('array')
                    res.body.hasNextPage.should.be.a('boolean')
                    res.body.hasPrevPage.should.be.a('boolean')
                    res.body.limit.should.be.a('number')
                    res.body.page.should.be.a('number')
                    res.body.pagingCounter.should.be.a('number')
                    res.body.totalDocs.should.be.a('number')
                    res.body.totalPages.should.be.a('number')
                    res.body.docs.should.be.a('array')
                    // res.body.docs[0].should.include.keys('behavior','body','createdAt','deleted','event','receptor','sender','title','updatedAt','url')
                    // res.body.docs[0].behavior.should.be.a('string')
                    // res.body.docs[0].createdAt.should.be.a('string')
                    // res.body.docs[0].deleted.should.be.a('boolean')
                    // res.body.docs[0].receptor.should.be.a('object')
                    // res.body.docs[0].sender.should.be.a('object')
                    // res.body.docs[0].tenantId.should.be.a('string')
                    // res.body.docs[0].title.should.be.a('string')
                    // res.body.docs[0].updatedAt.should.be.a('string')
                    // res.body.docs[0].url.should.be.a('string')
                    done()
                })
        })
    })
    // Post all notifications
    describe('/Post notifications', () => {
        it('it should be able to consume the route sending token like anyone', (done) => {
            const notification = {
                receptor : manager[0],
                sender : manager[0]._id,
                title : 'Notificacion test',
                url: '/notification/test',
                body: 'Esto es solo un test',
                behavior: 'route',
                event: {},
                customData : { raw: true }
            }
            chai
                .request(server)
                .post('/api/1.0/notifications')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .send(notification)
                .end((err, res) => {
                    console.log(res.body)
                    createdID.push(res.body._id)
                    res.should.have.status(201)
                    res.body.should.include.keys('receptor','sender','seen','behavior','_id','deleted','title','url','body','customData','updatedAt')
                    res.body.should.be.an('object')
                    done()
                })
        })
    })
    // Post all notifications
    describe('/Get notification/:id', () => {
        it('it should be able to consume the route sending token like anyone', (done) => {
            const notification = {
                receptor : manager[0],
                sender : manager[0]._id,
                title : 'Notificacion test',
                url: '/notification/test',
                body: 'Esto es solo un test',
                behavior: 'route',
                event: {},
                customData : { raw: true }
            }
            chai
                .request(server)
                .post('/api/1.0/notifications')
                .set('Origin', origin)
                .set('Authorization', `Bearer ${tokens.admin}`)
                .send(notification)
                .end((err, res) => {
                    console.log(res.body)
                    createdID.push(res.body._id)
                    res.should.have.status(201)
                    res.body.should.include.keys('receptor','sender','seen','behavior','_id','deleted','title','url','body','customData','updatedAt')
                    res.body.should.be.an('object')
                    chai
                        .request(server)
                        .get(`/api/1.0/notifications/${res.body._id}`)
                        .set('Origin', origin)
                        .set('Authorization', `Bearer ${tokens.admin}`)
                        .end((error, result) => {
                            console.log(result.body)
                            result.should.have.status(200)
                            result.body.should.include.keys('receptor','sender','seen','behavior','_id','deleted','title','url','body','customData','createdAt',
                            'updatedAt','tenantId')
                            result.body.should.be.a('object')
                            done()
                        })
                })
        })
    })
    // describe('/PATCH/ provider', () => {
    //     it('it should UPDATE a setting given the id with token only like admin', (done) => {
    //         var newSetting = {
    //             name: faker.name.firstName(),
    //             currencySymbol: '$',
    //             currency: 'MXN',
    //             language: 'es-ES',
    //             logo: faker.internet.avatar(),
    //             plugins: [],
    //             tenantId: 'subdomain',
    //         }
    //         chai
    //             .request(server)
    //             .get('/api/1.0/settings')
    //             .set('Origin', origin)
    //             .set('Authorization', `Bearer ${tokens.admin}`)
    //             .end((err, res) => {
    //                 res.should.have.status(200)
    //                 createdID.push(res.body)
    //                 res.body.should.include.keys('name', 'currencySymbol', 'currency', 'logo', 'language', 'initialInvoice', 'invoiceFormat', 'deleted',
    //                     '_id', 'plugins', 'tenantId', 'tax')
    //                 chai
    //                     .request(server)
    //                     .patch(`/api/1.0/settings`)
    //                     .set('Origin', origin)
    //                     .set('Authorization', `Bearer ${tokens.admin}`)
    //                     .send(newSetting)
    //                     .end((error, result) => {
    //                         console.log(result.body)
    //                         result.should.have.status(200)
    //                         result.body.should.include.keys('name','currencySymbol','currency','logo','language','initialInvoice','invoiceFormat','deleted','_id',
    //                         'plugins','tenantId','updatedAt','tax')
    //                         result.body.should.be.a('object')
    //                         done()
    //                     })
    //             })
    //     })
    //     it('it should GET the invoice desing sending the token only like admin', (done) => {
    //         const id = createdID.slice(-1).pop()
    //         chai
    //             .request(server)
    //             .patch(`/api/1.0/settings/invoice`)
    //             .set('Origin', origin)
    //             .set('Authorization', `Bearer ${tokens.admin}`)
    //             .end((err, res) => {
    //                 res.should.have.status(200)
    //                 res.body.should.be.a('object')
    //                 res.body.should.include.keys('invoiceDesign','logo','name','_id')
    //                 done()
    //             })
    //     })
    //     it('it should UPDATE the invoice desing sending the token only like admin', (done) => {
    //         const desing = {
    //             css: '* { box-sizing: border-box; } body {margin: 0;}',
    //             html: ''
    //         }
    //         const id = createdID.slice(-1).pop()
    //         chai
    //             .request(server)
    //             .patch(`/api/1.0/settings/invoice`)
    //             .set('Origin', origin)
    //             .set('Authorization', `Bearer ${tokens.admin}`)
    //             .send(desing)
    //             .end((err, res) => {
    //                 res.should.have.status(200)
    //                 res.body.should.be.a('object')
    //                 res.body.should.include.keys('currency','currencySymbol','deleted','initialInvoice','invoiceDesign','invoiceFormat','language','logo','name',
    //                 'plugins','tax','tenantId','updatedAt','_id')
    //                 done()
    //             })
    //     })
    // })
    after(() => {
        createdID.forEach((id) => {
          Provider.findByIdAndRemove(id, (err) => {
            if (err) {
              console.log(err)
            }
          })
        })
    })

});